let byIsDone = _.groupBy(window.myTests, test => {
  return test.is_done;
});
let dates, dateChanger, myCanvasDoughnut;
//console.log(byIsDone['0'].length);
$('.unfinished-procedures').text(byIsDone['0'].length);
let byIndicated = _.groupBy(window.isIndicated, a => a);
var { 0: not_indicated, 1: yes_indicated } = byIndicated;
let yes_indicated_length = yes_indicated ? yes_indicated.length : 0;
let indicatedPerecentage = Math.round(
  (yes_indicated_length / (not_indicated.length + yes_indicated_length)) * 100
);
//alert(indicatedPerecentage);
console.log(not_indicated);
$('#total-indications-header').text(
  not_indicated.length + yes_indicated_length
);
$('#total-indications').text(
  'Total : ' + (not_indicated.length + yes_indicated_length)
);
$('#indicated').text('Indicated : ' + yes_indicated_length);
$('#not-indicated').text('Not Indicated : ' + not_indicated.length);

function init_sidebar() {
  var a = function() {
    $RIGHT_COL.css('min-height', $(window).height());
    var a = $BODY.outerHeight(),
      b = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
      c = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
      d = a < c ? c : a;
    (d -= $NAV_MENU.height() + b), $RIGHT_COL.css('min-height', d);
  };
  $SIDEBAR_MENU.find('a').on('click', function(b) {
    var c = $(this).parent();
    c.is('.active')
      ? (c.removeClass('active active-sm'),
        $('ul:first', c).slideUp(function() {
          a();
        }))
      : (c.parent().is('.child_menu')
          ? $BODY.is('.nav-sm') &&
            ($SIDEBAR_MENU.find('li').removeClass('active active-sm'),
            $SIDEBAR_MENU.find('li ul').slideUp())
          : ($SIDEBAR_MENU.find('li').removeClass('active active-sm'),
            $SIDEBAR_MENU.find('li ul').slideUp()),
        c.addClass('active'),
        $('ul:first', c).slideDown(function() {
          a();
        }));
  }),
    $MENU_TOGGLE.on('click', function() {
      $BODY.hasClass('nav-md')
        ? ($SIDEBAR_MENU.find('li.active ul').hide(),
          $SIDEBAR_MENU
            .find('li.active')
            .addClass('active-sm')
            .removeClass('active'))
        : ($SIDEBAR_MENU.find('li.active-sm ul').show(),
          $SIDEBAR_MENU
            .find('li.active-sm')
            .addClass('active')
            .removeClass('active-sm')),
        $BODY.toggleClass('nav-md nav-sm'),
        a();
    }),
    $SIDEBAR_MENU
      .find('a[href="' + CURRENT_URL + '"]')
      .parent('li')
      .addClass('current-page'),
    $SIDEBAR_MENU
      .find('a')
      .filter(function() {
        return this.href == CURRENT_URL;
      })
      .parent('li')
      .addClass('current-page')
      .parents('ul')
      .slideDown(function() {
        a();
      })
      .parent()
      .addClass('active'),
    $(window).smartresize(function() {
      a();
    }),
    a(),
    $.fn.mCustomScrollbar &&
      $('.menu_fixed').mCustomScrollbar({
        autoHideScrollbar: !0,
        theme: 'minimal',
        mouseWheel: {
          preventDefault: !0
        }
      });
}
function countChecked() {
  'all' === checkState &&
    $(".bulk_action input[name='table_records']").iCheck('check'),
    'none' === checkState &&
      $(".bulk_action input[name='table_records']").iCheck('uncheck');
  var a = $(".bulk_action input[name='table_records']:checked").length;
  a
    ? ($('.column-title').hide(),
      $('.bulk-actions').show(),
      $('.action-cnt').html(a + ' Records Selected'))
    : ($('.column-title').show(), $('.bulk-actions').hide());
}
function gd(a, b, c) {
  return new Date(a, b - 1, c).getTime();
}
function init_last7Days() {
  let last7Days = window.myTests.map(a => moment(a.created_at).format('LL'));
  console.log({ last7Days });
  last7Days = last7Days.filter(a => {
    let today = new Date().getTime();
    // alert(new Date(a[0]).getTime() > (today- (7 * 24 * 60 * 60*1000)));
    return new Date(a).getTime() > today - 7 * 24 * 60 * 60 * 1000;
  }).length;
  $('#last-7-days').text(`+${last7Days}`);
}
function init_dates() {
  console.log(window.myTests);
  dates = window.myTests.map(a => moment(a.created_at).format('LL'));
  //dates = dates.slice(dates.length - 30, dates.length);
  dates = Object.values(
    _.groupBy(dates, test => {
      return test;
    })
  );
}
function init_flot_chart() {
  console.log({ dates });

  //console.log(moment(dates[0]).format('LL'));
  var dateArray = [];
  dates.forEach((e, i) => {
    let new_e = [];
    new_e[0] = new Date(e[0]).getTime();
    new_e[1] = e.length;
    dateArray.push(new_e);
  });
  //   console.log('%c D9', 'background: #222; color: #bada55');
  //   console.log({ d9 });
  //   console.log('%c D9', 'background: #222; color: #bada55');
  if ('undefined' != typeof $.plot) {
    console.log('init_flot_chart');
    for (
      var a = [
          //   [gd(2012, 1, 1), 17],
          //   [gd(2012, 1, 2), 74],
          //   [gd(2012, 1, 3), 6],
          //   [gd(2012, 1, 4), 39],
          //   [gd(2012, 1, 5), 20],
          //   [gd(2012, 1, 6), 85],
          //   [gd(2012, 1, 7), 7]
        ],
        b = [
          [gd(2012, 1, 1), 82],
          [gd(2012, 1, 2), 23],
          [gd(2012, 1, 3), 66],
          [gd(2012, 1, 4), 9],
          [gd(2012, 1, 5), 119],
          [gd(2012, 1, 6), 6],
          [gd(2012, 1, 7), 9]
        ],
        d = [],
        e = [
          //   [0, 1], //   [1, 9],  //   [2, 6],//   [3, 10],//   [4, 5],//   [5, 17],//   [6, 6],//   [7, 10],//   [8, 7],//   [9, 11],//   [10, 35],//   [11, 9],//   [12, 12],//   [13, 5],//   [14, 3],//   [15, 4],//   [16, 9]
        ],
        f = 0;
      f < 30;
      f++
    )
      d.push([
        new Date(
          Date.today()
            .add(f)
            .days()
        ).getTime(),
        randNum() + f + f + 10
      ]);
    var g = {
        series: {
          lines: {
            show: !1,
            fill: !0
          },
          splines: {
            show: !0,
            tension: 0.4,
            lineWidth: 1,
            fill: 0.4
          },
          points: {
            radius: 0,
            show: !0
          },
          shadowSize: 2
        },
        grid: {
          verticalLines: !0,
          hoverable: !0,
          clickable: !0,
          tickColor: '#d5d5d5',
          borderWidth: 1,
          color: '#fff'
        },
        colors: ['rgba(38, 185, 154, 0.38)', 'rgba(3, 88, 106, 0.38)'],
        xaxis: {
          tickColor: 'rgba(51, 51, 51, 0.06)',
          mode: 'time',
          tickSize: [tickSize(dateArray), 'day'],
          axisLabel: 'Date',
          axisLabelUseCanvas: !0,
          axisLabelFontSizePixels: 12,
          axisLabelFontFamily: 'Verdana, Arial',
          axisLabelPadding: 10
        },
        yaxis: {
          ticks: 8,
          tickColor: 'rgba(51, 51, 51, 0.06)'
        },
        tooltip: !1
      },
      h = {
        grid: {
          show: !0,
          aboveData: !0,
          color: '#3f3f3f',
          labelMargin: 10,
          axisMargin: 0,
          borderWidth: 0,
          borderColor: null,
          minBorderMargin: 5,
          clickable: !0,
          hoverable: !0,
          autoHighlight: !0,
          mouseActiveRadius: 100
        },
        series: {
          lines: {
            show: !0,
            fill: !0,
            lineWidth: 2,
            steps: !1
          },
          points: {
            show: !0,
            radius: 4.5,
            symbol: 'circle',
            lineWidth: 3
          }
        },
        legend: {
          position: 'ne',
          margin: [0, -25],
          noColumns: 0,
          labelBoxBorderColor: null,
          labelFormatter: function(a, b) {
            return b + '&nbsp;&nbsp;';
          },
          width: 40,
          height: 1
        },
        colors: [
          '#96CA59',
          '#3F97EB',
          '#72c380',
          '#6f7a8a',
          '#f7cb38',
          '#5a8022',
          '#2c7282'
        ],
        shadowSize: 0,
        tooltip: !0,
        tooltipOpts: {
          content: '%s: %y.0',
          xDateFormat: '%d/%m',
          shifts: {
            x: -30,
            y: -50
          },
          defaultTheme: !1
        },
        yaxis: {
          min: 0
        },
        xaxis: {
          mode: 'time',
          minTickSize: [1, 'day'],
          timeformat: '%d/%m/%y',
          min: d[0][0],
          max: d[20][0]
        }
      },
      i = {
        series: {
          curvedLines: {
            apply: !0,
            active: !0,
            monotonicFit: !0
          }
        },
        colors: ['#26B99A'],
        grid: {
          borderWidth: {
            top: 0,
            right: 0,
            bottom: 1,
            left: 1
          },
          borderColor: {
            bottom: '#7F8790',
            left: '#7F8790'
          }
        }
      };
    console.log({ a });
    console.log({ dateArray });
    if (dateArray.length == 1)
      dateArray.push([new Date(moment().subtract(1, 'days')).getTime(), 0]);
    let amir = [[1549836000000, 2], [1549231200000, 2]];
    $('#chart_plot_01').length &&
      (console.log('Plot1'), $.plot($('#chart_plot_01'), [dateArray], g)), //313  [a, b], g)),
      $('#chart_plot_02').length &&
        (console.log('Plot2'),
        $.plot(
          $('#chart_plot_02'),
          [
            {
              label: 'Email Sent',
              data: d,
              lines: {
                fillColor: 'rgba(150, 202, 89, 0.12)'
              },
              points: {
                fillColor: '#fff'
              }
            }
          ],
          h
        )),
      $('#chart_plot_03').length &&
        (console.log('Plot3'),
        $.plot(
          $('#chart_plot_03'),
          [
            {
              label: 'Registrations',
              data: e,
              lines: {
                fillColor: 'rgba(150, 202, 89, 0.12)'
              },
              points: {
                fillColor: '#fff'
              }
            }
          ],
          i
        ));
  }
}
function init_starrr() {
  'undefined' != typeof starrr &&
    (console.log('init_starrr'),
    $('.stars').starrr(),
    $('.stars-existing').starrr({
      rating: 4
    }),
    $('.stars').on('starrr:change', function(a, b) {
      $('.stars-count').html(b);
    }),
    $('.stars-existing').on('starrr:change', function(a, b) {
      $('.stars-count-existing').html(b);
    }));
}

function init_chart_doughnut() {
  let byUser = Object.values(window.byUser);
  console.log({ byUser });
  let labels = byUser.map(
      a => a[0].users_title + ' ' + a[0].first_name + ' ' + a[0].last_name
    ),
    backgroundColorFa = [
      '#3498DB',
      '#BDC3C7',
      '#9B59B6',
      '#E74C3C',
      '#26B99A',
      '#A52A2A'
    ]; // ['brown', 'aero', 'purple', 'red', 'green', 'blue'];
  console.log(labels);
  let data = byUser.map(a => a.length);
  $.each(byUser, function(key, value) {
    let percentage = Math.round((data[key] * 100) / window.myTests.length);
    $('#byUserTable').append(
      $('<tr  class=" "></tr>')
        .attr('value', key)
        .html(
          '<td style=""><p  style="cursor:pointer;margin-left:10px"  data-placement="right" title="Usage : ' +
            percentage +
            '%" ><i  class="fa fa-square hover-square" style="color: ' +
            backgroundColorFa[key] +
            '" ></i > ' +
            '<span class="users-title-usage">' +
            '<span class="users-title-usage-td"> ' +
            value[0].users_title +
            '</span> ' +
            value[0].first_name +
            ' ' +
            value[0].last_name +
            '</span></p></td><td class="users-title-usage-td" style="">' +
            percentage +
            '%</td>'
        )
    );
  });

  if (
    'undefined' != typeof Chart &&
    (console.log('init_chart_doughnut'), $('.canvasDoughnut').length)
  ) {
    var a = {
      type: 'doughnut',
      tooltipFillColor: 'rgba(51, 51, 51, 0.55)',
      data: {
        labels: labels, // ['Symbian', 'Blackberry', 'Other', 'Android', 'IOS'],
        datasets: [
          {
            data: data, //[15, 20, 30, 10, 30], //!backGroundFA!!
            backgroundColor: backgroundColorFa,
            hoverBackgroundColor: [
              '#49A9EA',
              '#CFD4D8',
              '#B370CF',
              '#E95E4F',
              '#36CAAB',
              '#651a1a'
            ]
          }
        ]
      },
      options: {
        legend: !1,
        responsive: !1
      }
    };
    $('.canvasDoughnut').each(function() {
      var b = $(this);
      new Chart(b, a);
    });
  }
}
function init_gauge() {
  if ('undefined' != typeof Gauge) {
    console.log('init_gauge [' + $('.gauge-chart').length + ']'),
      console.log('init_gauge');
    var a = {
      lines: 12,
      angle: 0,
      lineWidth: 0.4,
      pointer: {
        length: 0.75,
        strokeWidth: 0.042,
        color: '#1D212A'
      },
      limitMax: 'false',
      colorStart: '#1ABC9C',
      colorStop: '#1ABC9C',
      strokeColor: '#F0F3F3',
      generateGradient: !0
    };
    if ($('#chart_gauge_01').length)
      var b = document.getElementById('chart_gauge_01'),
        c = new Gauge(b).setOptions(a);
    if (
      ($('#gauge-text').length &&
        ((c.maxValue = 100),
        (c.animationSpeed = 32),
        c.set(indicatedPerecentage), //c.set(indicatedPerecentage),
        c.setTextField(document.getElementById('gauge-text'))),
      $('#chart_gauge_02').length)
    )
      var d = document.getElementById('chart_gauge_02'),
        e = new Gauge(d).setOptions(a);
    $('#gauge-text2').length &&
      ((e.maxValue = 9e3),
      (e.animationSpeed = 32),
      e.set(2400),
      e.setTextField(document.getElementById('gauge-text2')));
  }
}

function init_autosize() {
  'undefined' != typeof $.fn.autosize && autosize($('.resizable_textarea'));
}
function init_parsley() {
  if ('undefined' != typeof parsley) {
    console.log('init_parsley'),
      $('parsley:field:validate', function() {
        a();
      }),
      $('#demo-form .btn').on('click', function() {
        $('#demo-form')
          .parsley()
          .validate(),
          a();
      });
    var a = function() {
      !0 ===
      $('#demo-form')
        .parsley()
        .isValid()
        ? ($('.bs-callout-info').removeClass('hidden'),
          $('.bs-callout-warning').addClass('hidden'))
        : ($('.bs-callout-info').addClass('hidden'),
          $('.bs-callout-warning').removeClass('hidden'));
    };
    $('parsley:field:validate', function() {
      a();
    }),
      $('#demo-form2 .btn').on('click', function() {
        $('#demo-form2')
          .parsley()
          .validate(),
          a();
      });
    var a = function() {
      !0 ===
      $('#demo-form2')
        .parsley()
        .isValid()
        ? ($('.bs-callout-info').removeClass('hidden'),
          $('.bs-callout-warning').addClass('hidden'))
        : ($('.bs-callout-info').addClass('hidden'),
          $('.bs-callout-warning').removeClass('hidden'));
    };
    try {
      hljs.initHighlightingOnLoad();
    } catch (a) {}
  }
}

function init_TagsInput() {
  'undefined' != typeof $.fn.tagsInput &&
    $('#tags_1').tagsInput({
      width: 'auto'
    });
}
function init_select2() {
  'undefined' != typeof select2 &&
    (console.log('init_toolbox'),
    $('.select2_single').select2({
      placeholder: 'Select a state',
      allowClear: !0
    }),
    $('.select2_group').select2({}),
    $('.select2_multiple').select2({
      maximumSelectionLength: 4,
      placeholder: 'With Max Selection limit 4',
      allowClear: !0
    }));
}
function init_knob() {
  if ('undefined' != typeof $.fn.knob) {
    console.log('init_knob'),
      $('.knob').knob({
        change: function(a) {},
        release: function(a) {
          console.log('release : ' + a);
        },
        cancel: function() {
          console.log('cancel : ', this);
        },
        draw: function() {
          if ('tron' == this.$.data('skin')) {
            this.cursorExt = 0.3;
            var b,
              a = this.arc(this.cv),
              c = 1;
            return (
              (this.g.lineWidth = this.lineWidth),
              this.o.displayPrevious &&
                ((b = this.arc(this.v)),
                this.g.beginPath(),
                (this.g.strokeStyle = this.pColor),
                this.g.arc(
                  this.xy,
                  this.xy,
                  this.radius - this.lineWidth,
                  b.s,
                  b.e,
                  b.d
                ),
                this.g.stroke()),
              this.g.beginPath(),
              (this.g.strokeStyle = c ? this.o.fgColor : this.fgColor),
              this.g.arc(
                this.xy,
                this.xy,
                this.radius - this.lineWidth,
                a.s,
                a.e,
                a.d
              ),
              this.g.stroke(),
              (this.g.lineWidth = 2),
              this.g.beginPath(),
              (this.g.strokeStyle = this.o.fgColor),
              this.g.arc(
                this.xy,
                this.xy,
                this.radius - this.lineWidth + 1 + (2 * this.lineWidth) / 3,
                0,
                2 * Math.PI,
                !1
              ),
              this.g.stroke(),
              !1
            );
          }
        }
      });
    var a,
      b = 0,
      c = 0,
      d = 0,
      e = $('div.idir'),
      f = $('div.ival'),
      g = function() {
        d++,
          e
            .show()
            .html('+')
            .fadeOut(),
          f.html(d);
      },
      h = function() {
        d--,
          e
            .show()
            .html('-')
            .fadeOut(),
          f.html(d);
      };
    $('input.infinite').knob({
      min: 0,
      max: 20,
      stopper: !1,
      change: function() {
        a > this.cv
          ? b
            ? (h(), (b = 0))
            : ((b = 1), (c = 0))
          : a < this.cv && (c ? (g(), (c = 0)) : ((c = 1), (b = 0))),
          (a = this.cv);
      }
    });
  }
}
function init_InputMask() {
  'undefined' != typeof $.fn.inputmask &&
    (console.log('init_InputMask'), $(':input').inputmask());
}

function init_IonRangeSlider() {
  'undefined' != typeof $.fn.ionRangeSlider &&
    (console.log('init_IonRangeSlider'),
    $('#range_27').ionRangeSlider({
      type: 'double',
      min: 1e6,
      max: 2e6,
      grid: !0,
      force_edges: !0
    }),
    $('#range').ionRangeSlider({
      hide_min_max: !0,
      keyboard: !0,
      min: 0,
      max: 5e3,
      from: 1e3,
      to: 4e3,
      type: 'double',
      step: 1,
      prefix: '$',
      grid: !0
    }),
    $('#range_25').ionRangeSlider({
      type: 'double',
      min: 1e6,
      max: 2e6,
      grid: !0
    }),
    $('#range_26').ionRangeSlider({
      type: 'double',
      min: 0,
      max: 1e4,
      step: 500,
      grid: !0,
      grid_snap: !0
    }),
    $('#range_31').ionRangeSlider({
      type: 'double',
      min: 0,
      max: 100,
      from: 30,
      to: 70,
      from_fixed: !0
    }),
    $('.range_min_max').ionRangeSlider({
      type: 'double',
      min: 0,
      max: 100,
      from: 30,
      to: 70,
      max_interval: 50
    }),
    $('.range_time24').ionRangeSlider({
      min: +moment()
        .subtract(12, 'hours')
        .format('X'),
      max: +moment().format('X'),
      from: +moment()
        .subtract(6, 'hours')
        .format('X'),
      grid: !0,
      force_edges: !0,
      prettify: function(a) {
        var b = moment(a, 'X');
        return b.format('Do MMMM, HH:mm');
      }
    }));
}
function init_daterangepicker() {
  if ('undefined' != typeof $.fn.daterangepicker) {
    console.log('init_daterangepicker');
    var a = function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c),
          $('#reportrange span').html(
            a.format('MMMM D, YYYY') + ' - ' + b.format('MMMM D, YYYY')
          );
      },
      b = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2020',
        dateLimit: {
          days: 60
        },
        showDropdowns: !0,
        showWeekNumbers: !0,
        timePicker: !1,
        timePickerIncrement: 1,
        timePicker12Hour: !0,
        ranges: {
          Today: [moment(), moment()],
          Yesterday: [
            moment().subtract(1, 'days'),
            moment().subtract(1, 'days')
          ],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [
            moment()
              .subtract(1, 'month')
              .startOf('month'),
            moment()
              .subtract(1, 'month')
              .endOf('month')
          ]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          monthNames: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
          ],
          firstDay: 1
        }
      };
    $('#reportrange span').html(
      moment()
        .subtract(29, 'days')
        .format('MMMM D, YYYY') +
        ' - ' +
        moment().format('MMMM D, YYYY')
    ),
      $('#reportrange').daterangepicker(b, a),
      $('#reportrange').on('show.daterangepicker', function() {
        console.log('show event fired');
      }),
      $('#reportrange').on('hide.daterangepicker', function() {
        console.log('hide event fired');
      }),
      $('#reportrange').on('apply.daterangepicker', function(a, b) {
        console.log(
          'apply event fired, start/end dates are ' +
            b.startDate.format('MMMM D, YYYY') +
            ' to ' +
            b.endDate.format('MMMM D, YYYY')
        );
        init_dates();
        dates = dates.filter(a => {
          let time = new Date(a[0]).getTime();

          let s_time = new Date(b.startDate).getTime();
          let e_time = new Date(b.endDate).getTime();
          console.log({ date: a[0], time, s_time, e_time });
          return time >= s_time && time <= e_time;
        });

        console.log({ dates });
        init_flot_chart();
      }),
      $('#reportrange').on('cancel.daterangepicker', function(a, b) {
        console.log('cancel event fired');
      }),
      $('#options1').click(function() {
        $('#reportrange')
          .data('daterangepicker')
          .setOptions(b, a);
      }),
      $('#options2').click(function() {
        $('#reportrange')
          .data('daterangepicker')
          .setOptions(optionSet2, a);
      }),
      $('#destroy').click(function() {
        $('#reportrange')
          .data('daterangepicker')
          .remove();
      });
  }
}
function init_daterangepicker_right() {
  if ('undefined' != typeof $.fn.daterangepicker) {
    console.log('init_daterangepicker_right');
    var a = function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c),
          $('#reportrange_right span').html(
            a.format('MMMM D, YYYY') + ' - ' + b.format('MMMM D, YYYY')
          );
      },
      b = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2020',
        dateLimit: {
          days: 60
        },
        showDropdowns: !0,
        showWeekNumbers: !0,
        timePicker: !1,
        timePickerIncrement: 1,
        timePicker12Hour: !0,
        ranges: {
          Today: [moment(), moment()],
          Yesterday: [
            moment().subtract(1, 'days'),
            moment().subtract(1, 'days')
          ],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [
            moment()
              .subtract(1, 'month')
              .startOf('month'),
            moment()
              .subtract(1, 'month')
              .endOf('month')
          ]
        },
        opens: 'right',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          monthNames: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
          ],
          firstDay: 1
        }
      };
    $('#reportrange_right span').html(
      moment()
        .subtract(29, 'days')
        .format('MMMM D, YYYY') +
        ' - ' +
        moment().format('MMMM D, YYYY')
    ),
      $('#reportrange_right').daterangepicker(b, a),
      $('#reportrange_right').on('show.daterangepicker', function() {
        console.log('show event fired');
      }),
      $('#reportrange_right').on('hide.daterangepicker', function() {
        console.log('hide event fired');
      }),
      $('#reportrange_right').on('apply.daterangepicker', function(a, b) {
        console.log(
          'apply event fired, start/end dates are ' +
            b.startDate.format('MMMM D, YYYY') +
            ' to ' +
            b.endDate.format('MMMM D, YYYY')
        );
      }),
      $('#reportrange_right').on('cancel.daterangepicker', function(a, b) {
        console.log('cancel event fired');
      }),
      $('#options1').click(function() {
        $('#reportrange_right')
          .data('daterangepicker')
          .setOptions(b, a);
      }),
      $('#options2').click(function() {
        $('#reportrange_right')
          .data('daterangepicker')
          .setOptions(optionSet2, a);
      }),
      $('#destroy').click(function() {
        $('#reportrange_right')
          .data('daterangepicker')
          .remove();
      });
  }
}
function init_daterangepicker_single_call() {
  'undefined' != typeof $.fn.daterangepicker &&
    (console.log('init_daterangepicker_single_call'),
    $('#single_cal1').daterangepicker(
      {
        singleDatePicker: !0,
        singleClasses: 'picker_1'
      },
      function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c);
      }
    ),
    $('#single_cal2').daterangepicker(
      {
        singleDatePicker: !0,
        singleClasses: 'picker_2'
      },
      function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c);
      }
    ),
    $('#single_cal3').daterangepicker(
      {
        singleDatePicker: !0,
        singleClasses: 'picker_3'
      },
      function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c);
      }
    ),
    $('#single_cal4').daterangepicker(
      {
        singleDatePicker: !0,
        singleClasses: 'picker_4'
      },
      function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c);
      }
    ));
}
function init_daterangepicker_reservation() {
  'undefined' != typeof $.fn.daterangepicker &&
    (console.log('init_daterangepicker_reservation'),
    $('#reservation').daterangepicker(null, function(a, b, c) {
      console.log(a.toISOString(), b.toISOString(), c);
    }),
    $('#reservation-time').daterangepicker({
      timePicker: !0,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY h:mm A'
      }
    }));
}
function init_SmartWizard() {
  'undefined' != typeof $.fn.smartWizard &&
    (console.log('init_SmartWizard'),
    $('#wizard').smartWizard(),
    $('#wizard_verticle').smartWizard({
      transitionEffect: 'slide'
    }),
    $('.buttonNext').addClass('btn btn-success'),
    $('.buttonPrevious').addClass('btn btn-primary'),
    $('.buttonFinish').addClass('btn btn-default'));
}
function init_validator() {
  'undefined' != typeof validator &&
    (console.log('init_validator'),
    (validator.message.date = 'not a real date'),
    $('form')
      .on(
        'blur',
        'input[required], input.optional, select.required',
        validator.checkField
      )
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress),
    $('.multi.required').on('keyup blur', 'input', function() {
      validator.checkField.apply(
        $(this)
          .siblings()
          .last()[0]
      );
    }),
    $('form').submit(function(a) {
      a.preventDefault();
      var b = !0;
      return validator.checkAll($(this)) || (b = !1), b && this.submit(), !1;
    }));
}
function init_PNotify() {
  'undefined' != typeof PNotify &&
    (console.log('init_PNotify'),
    new PNotify({
      title: 'PNotify',
      type: 'info',
      text:
        "Welcome. Try hovering over me. You can click things behind me, because I'm non-blocking.",
      nonblock: {
        nonblock: !0
      },
      addclass: 'dark',
      styling: 'bootstrap3',
      hide: !1,
      before_close: function(a) {
        return (
          a.update({
            title: a.options.title + ' - Enjoy your Stay',
            before_close: null
          }),
          a.queueRemove(),
          !1
        );
      }
    }));
}
function init_CustomNotification() {
  if ((console.log('run_customtabs'), 'undefined' != typeof CustomTabs)) {
    console.log('init_CustomTabs');
    var a = 10;
    (TabbedNotification = function(b) {
      var c =
        "<div id='ntf" +
        a +
        "' class='text alert-" +
        b.type +
        "' style='display:none'><h2><i class='fa fa-bell'></i> " +
        b.title +
        "</h2><div class='close'><a href='javascript:;' class='notification_close'><i class='fa fa-close'></i></a></div><p>" +
        b.text +
        '</p></div>';
      document.getElementById('custom_notifications')
        ? ($('#custom_notifications ul.notifications').append(
            "<li><a id='ntlink" +
              a +
              "' class='alert-" +
              b.type +
              "' href='#ntf" +
              a +
              "'><i class='fa fa-bell animated shake'></i></a></li>"
          ),
          $('#custom_notifications #notif-group').append(c),
          a++,
          CustomTabs(b))
        : alert('doesnt exists');
    }),
      (CustomTabs = function(a) {
        $('.tabbed_notifications > div').hide(),
          $('.tabbed_notifications > div:first-of-type').show(),
          $('#custom_notifications').removeClass('dsp_none'),
          $('.notifications a').click(function(a) {
            a.preventDefault();
            var b = $(this),
              c =
                '#' + b.parents('.notifications').data('tabbed_notifications'),
              d = b
                .closest('li')
                .siblings()
                .children('a'),
              e = b.attr('href');
            d.removeClass('active'),
              b.addClass('active'),
              $(c)
                .children('div')
                .hide(),
              $(e).show();
          });
      }),
      CustomTabs();
    var b = (idname = '');
    $(document).on('click', '.notification_close', function(a) {
      (idname = $(this)
        .parent()
        .parent()
        .attr('id')),
        (b = idname.substr(-2)),
        $('#ntf' + b).remove(),
        $('#ntlink' + b)
          .parent()
          .remove(),
        $('.notifications a')
          .first()
          .addClass('active'),
        $('#notif-group div')
          .first()
          .css('display', 'block');
    });
  }
}
function init_EasyPieChart() {
  if ('undefined' != typeof $.fn.easyPieChart) {
    console.log('init_EasyPieChart'),
      $('.chart').easyPieChart({
        easing: 'easeOutElastic',
        delay: 3e3,
        barColor: '#26B99A',
        trackColor: '#fff',
        scaleColor: !1,
        lineWidth: 20,
        trackWidth: 16,
        lineCap: 'butt',
        onStep: function(a, b, c) {
          $(this.el)
            .find('.percent')
            .text(Math.round(c));
        }
      });
    var a = (window.chart = $('.chart').data('easyPieChart'));
    $('.js_update').on('click', function() {
      a.update(200 * Math.random() - 100);
    });
    var b = $.fn.popover.Constructor.prototype.leave;
    ($.fn.popover.Constructor.prototype.leave = function(a) {
      var d,
        e,
        c =
          a instanceof this.constructor
            ? a
            : $(a.currentTarget)
                [this.type](this.getDelegateOptions())
                .data('bs.' + this.type);
      b.call(this, a),
        a.currentTarget &&
          ((d = $(a.currentTarget).siblings('.popover')),
          (e = c.timeout),
          d.one('mouseenter', function() {
            clearTimeout(e),
              d.one('mouseleave', function() {
                $.fn.popover.Constructor.prototype.leave.call(c, c);
              });
          }));
    }),
      $('body').popover({
        selector: '[data-popover]',
        trigger: 'click hover',
        delay: {
          show: 50,
          hide: 400
        }
      });
  }
}
function init_charts() {
  if (
    (console.log('run_charts  typeof [' + typeof Chart + ']'),
    'undefined' != typeof Chart)
  ) {
    if (
      (console.log('init_charts'),
      (Chart.defaults.global.legend = {
        enabled: !1
      }),
      $('#canvas_line').length)
    ) {
      new Chart(document.getElementById('canvas_line'), {
        type: 'line',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July'
          ],
          datasets: [
            {
              label: 'My First dataset',
              backgroundColor: 'rgba(38, 185, 154, 0.31)',
              borderColor: 'rgba(38, 185, 154, 0.7)',
              pointBorderColor: 'rgba(38, 185, 154, 0.7)',
              pointBackgroundColor: 'rgba(38, 185, 154, 0.7)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointBorderWidth: 1,
              data: [31, 74, 6, 39, 20, 85, 7]
            },
            {
              label: 'My Second dataset',
              backgroundColor: 'rgba(3, 88, 106, 0.3)',
              borderColor: 'rgba(3, 88, 106, 0.70)',
              pointBorderColor: 'rgba(3, 88, 106, 0.70)',
              pointBackgroundColor: 'rgba(3, 88, 106, 0.70)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(151,187,205,1)',
              pointBorderWidth: 1,
              data: [82, 23, 66, 9, 99, 4, 2]
            }
          ]
        }
      });
    }
    if ($('#canvas_line1').length) {
      new Chart(document.getElementById('canvas_line1'), {
        type: 'line',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July'
          ],
          datasets: [
            {
              label: 'My First dataset',
              backgroundColor: 'rgba(38, 185, 154, 0.31)',
              borderColor: 'rgba(38, 185, 154, 0.7)',
              pointBorderColor: 'rgba(38, 185, 154, 0.7)',
              pointBackgroundColor: 'rgba(38, 185, 154, 0.7)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointBorderWidth: 1,
              data: [31, 74, 6, 39, 20, 85, 7]
            },
            {
              label: 'My Second dataset',
              backgroundColor: 'rgba(3, 88, 106, 0.3)',
              borderColor: 'rgba(3, 88, 106, 0.70)',
              pointBorderColor: 'rgba(3, 88, 106, 0.70)',
              pointBackgroundColor: 'rgba(3, 88, 106, 0.70)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(151,187,205,1)',
              pointBorderWidth: 1,
              data: [82, 23, 66, 9, 99, 4, 2]
            }
          ]
        }
      });
    }
    if ($('#canvas_line2').length) {
      new Chart(document.getElementById('canvas_line2'), {
        type: 'line',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July'
          ],
          datasets: [
            {
              label: 'My First dataset',
              backgroundColor: 'rgba(38, 185, 154, 0.31)',
              borderColor: 'rgba(38, 185, 154, 0.7)',
              pointBorderColor: 'rgba(38, 185, 154, 0.7)',
              pointBackgroundColor: 'rgba(38, 185, 154, 0.7)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointBorderWidth: 1,
              data: [31, 74, 6, 39, 20, 85, 7]
            },
            {
              label: 'My Second dataset',
              backgroundColor: 'rgba(3, 88, 106, 0.3)',
              borderColor: 'rgba(3, 88, 106, 0.70)',
              pointBorderColor: 'rgba(3, 88, 106, 0.70)',
              pointBackgroundColor: 'rgba(3, 88, 106, 0.70)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(151,187,205,1)',
              pointBorderWidth: 1,
              data: [82, 23, 66, 9, 99, 4, 2]
            }
          ]
        }
      });
    }
    if ($('#canvas_line3').length) {
      new Chart(document.getElementById('canvas_line3'), {
        type: 'line',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July'
          ],
          datasets: [
            {
              label: 'My First dataset',
              backgroundColor: 'rgba(38, 185, 154, 0.31)',
              borderColor: 'rgba(38, 185, 154, 0.7)',
              pointBorderColor: 'rgba(38, 185, 154, 0.7)',
              pointBackgroundColor: 'rgba(38, 185, 154, 0.7)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointBorderWidth: 1,
              data: [31, 74, 6, 39, 20, 85, 7]
            },
            {
              label: 'My Second dataset',
              backgroundColor: 'rgba(3, 88, 106, 0.3)',
              borderColor: 'rgba(3, 88, 106, 0.70)',
              pointBorderColor: 'rgba(3, 88, 106, 0.70)',
              pointBackgroundColor: 'rgba(3, 88, 106, 0.70)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(151,187,205,1)',
              pointBorderWidth: 1,
              data: [82, 23, 66, 9, 99, 4, 2]
            }
          ]
        }
      });
    }
    if ($('#canvas_line4').length) {
      new Chart(document.getElementById('canvas_line4'), {
        type: 'line',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July'
          ],
          datasets: [
            {
              label: 'My First dataset',
              backgroundColor: 'rgba(38, 185, 154, 0.31)',
              borderColor: 'rgba(38, 185, 154, 0.7)',
              pointBorderColor: 'rgba(38, 185, 154, 0.7)',
              pointBackgroundColor: 'rgba(38, 185, 154, 0.7)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointBorderWidth: 1,
              data: [31, 74, 6, 39, 20, 85, 7]
            },
            {
              label: 'My Second dataset',
              backgroundColor: 'rgba(3, 88, 106, 0.3)',
              borderColor: 'rgba(3, 88, 106, 0.70)',
              pointBorderColor: 'rgba(3, 88, 106, 0.70)',
              pointBackgroundColor: 'rgba(3, 88, 106, 0.70)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(151,187,205,1)',
              pointBorderWidth: 1,
              data: [82, 23, 66, 9, 99, 4, 2]
            }
          ]
        }
      });
    }
    if ($('#lineChart').length) {
      var f = document.getElementById('lineChart');
      new Chart(f, {
        type: 'line',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July'
          ],
          datasets: [
            {
              label: 'My First dataset',
              backgroundColor: 'rgba(38, 185, 154, 0.31)',
              borderColor: 'rgba(38, 185, 154, 0.7)',
              pointBorderColor: 'rgba(38, 185, 154, 0.7)',
              pointBackgroundColor: 'rgba(38, 185, 154, 0.7)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointBorderWidth: 1,
              data: [31, 74, 6, 39, 20, 85, 7]
            },
            {
              label: 'My Second dataset',
              backgroundColor: 'rgba(3, 88, 106, 0.3)',
              borderColor: 'rgba(3, 88, 106, 0.70)',
              pointBorderColor: 'rgba(3, 88, 106, 0.70)',
              pointBackgroundColor: 'rgba(3, 88, 106, 0.70)',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(151,187,205,1)',
              pointBorderWidth: 1,
              data: [82, 23, 66, 9, 99, 4, 2]
            }
          ]
        }
      });
    }
    if ($('#mybarChart').length) {
      var f = document.getElementById('mybarChart');
      new Chart(f, {
        type: 'bar',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July'
          ],
          datasets: [
            {
              label: '# of Votes',
              backgroundColor: '#26B99A',
              data: [51, 30, 40, 28, 92, 50, 45]
            },
            {
              label: '# of Votes',
              backgroundColor: '#03586A',
              data: [41, 56, 25, 48, 72, 34, 12]
            }
          ]
        },
        options: {
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: !0
                }
              }
            ]
          }
        }
      });
    }
    if ($('#canvasDoughnut').length) {
      var f = document.getElementById('canvasDoughnut'),
        i = {
          labels: [
            'Dark Grey',
            'Purple Color',
            'Gray Color',
            'Green Color',
            'Blue Color'
          ],
          datasets: [
            {
              data: [120, 50, 140, 180, 100],
              backgroundColor: [
                '#455C73',
                '#9B59B6',
                '#BDC3C7',
                '#26B99A',
                '#3498DB'
              ],
              hoverBackgroundColor: [
                '#34495E',
                '#B370CF',
                '#CFD4D8',
                '#36CAAB',
                '#49A9EA'
              ]
            }
          ]
        };
      new Chart(f, {
        type: 'doughnut',
        tooltipFillColor: 'rgba(51, 51, 51, 0.55)',
        data: i
      });
    }
    if ($('#canvasRadar').length) {
      var f = document.getElementById('canvasRadar'),
        i = {
          labels: [
            'Eating',
            'Drinking',
            'Sleeping',
            'Designing',
            'Coding',
            'Cycling',
            'Running'
          ],
          datasets: [{}]
        };
      new Chart(f, {
        type: 'radar',
        data: i
      });
    }
    if ($('#pieChart').length) {
      var f = document.getElementById('pieChart'),
        i = {
          datasets: [
            {
              data: [120, 50, 140, 180, 100],
              backgroundColor: [
                '#455C73',
                '#9B59B6',
                '#BDC3C7',
                '#26B99A',
                '#3498DB'
              ],
              label: 'My dataset'
            }
          ],
          labels: ['Dark Gray', 'Purple', 'Gray', 'Green', 'Blue']
        };
      new Chart(f, {
        data: i,
        type: 'pie',
        otpions: {
          legend: !1
        }
      });
    }
    if ($('#polarArea').length) {
      var f = document.getElementById('polarArea'),
        i = {
          datasets: [
            {
              data: [120, 50, 140, 180, 100],
              backgroundColor: [
                '#455C73',
                '#9B59B6',
                '#BDC3C7',
                '#26B99A',
                '#3498DB'
              ],
              label: 'My dataset'
            }
          ],
          labels: ['Dark Gray', 'Purple', 'Gray', 'Green', 'Blue']
        };
      new Chart(f, {
        data: i,
        type: 'polarArea',
        options: {
          scale: {
            ticks: {
              beginAtZero: !0
            }
          }
        }
      });
    }
  }
}
function init_compose() {
  'undefined' != typeof $.fn.slideToggle &&
    (console.log('init_compose'),
    $('#compose, .compose-close').click(function() {
      $('.compose').slideToggle();
    }));
}
function init_calendar() {
  if ('undefined' != typeof $.fn.fullCalendar) {
    console.log('init_calendar');
    var e,
      f,
      a = new Date(),
      b = a.getDate(),
      c = a.getMonth(),
      d = a.getFullYear(),
      g = $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listMonth'
        },
        selectable: !0,
        selectHelper: !0,
        select: function(a, b, c) {
          $('#fc_create').click(),
            (e = a),
            (ended = b),
            $('.antosubmit').on('click', function() {
              var a = $('#title').val();
              return (
                b && (ended = b),
                (f = $('#event_type').val()),
                a &&
                  g.fullCalendar(
                    'renderEvent',
                    {
                      title: a,
                      start: e,
                      end: b,
                      allDay: c
                    },
                    !0
                  ),
                $('#title').val(''),
                g.fullCalendar('unselect'),
                $('.antoclose').click(),
                !1
              );
            });
        },
        eventClick: function(a, b, c) {
          $('#fc_edit').click(),
            $('#title2').val(a.title),
            (f = $('#event_type').val()),
            $('.antosubmit2').on('click', function() {
              (a.title = $('#title2').val()),
                g.fullCalendar('updateEvent', a),
                $('.antoclose2').click();
            }),
            g.fullCalendar('unselect');
        },
        editable: !0,
        events: [
          {
            title: 'All Day Event',
            start: new Date(d, c, 1)
          },
          {
            title: 'Long Event',
            start: new Date(d, c, b - 5),
            end: new Date(d, c, b - 2)
          },
          {
            title: 'Meeting',
            start: new Date(d, c, b, 10, 30),
            allDay: !1
          },
          {
            title: 'Lunch',
            start: new Date(d, c, b + 14, 12, 0),
            end: new Date(d, c, b, 14, 0),
            allDay: !1
          },
          {
            title: 'Birthday Party',
            start: new Date(d, c, b + 1, 19, 0),
            end: new Date(d, c, b + 1, 22, 30),
            allDay: !1
          },
          {
            title: 'Click for Google',
            start: new Date(d, c, 28),
            end: new Date(d, c, 29),
            url: 'http://google.com/'
          }
        ]
      });
  }
}
function init_DataTables() {
  if ((console.log('run_datatables'), 'undefined' != typeof $.fn.DataTable)) {
    console.log('init_DataTables');
    var a = function() {
      $('#datatable-buttons').length &&
        $('#datatable-buttons').DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'copy',
              className: 'btn-sm'
            },
            {
              extend: 'csv',
              className: 'btn-sm'
            },
            {
              extend: 'excel',
              className: 'btn-sm'
            },
            {
              extend: 'pdfHtml5',
              className: 'btn-sm'
            },
            {
              extend: 'print',
              className: 'btn-sm'
            }
          ],
          responsive: !0
        });
    };
    (TableManageButtons = (function() {
      'use strict';
      return {
        init: function() {
          a();
        }
      };
    })()),
      $('#datatable').dataTable(),
      $('#datatable-keytable').DataTable({
        keys: !0
      }),
      $('#datatable-responsive').DataTable(),
      $('#datatable-scroller').DataTable({
        ajax: 'js/datatables/json/scroller-demo.json',
        deferRender: !0,
        scrollY: 380,
        scrollCollapse: !0,
        scroller: !0
      }),
      $('#datatable-fixed-header').DataTable({
        fixedHeader: !0
      });
    var b = $('#datatable-checkbox');
    b.dataTable({
      order: [[1, 'asc']],
      columnDefs: [
        {
          orderable: !1,
          targets: [0]
        }
      ]
    }),
      b.on('draw.dt', function() {
        $('checkbox input').iCheck({
          checkboxClass: 'icheckbox_flat-green'
        });
      }),
      TableManageButtons.init();
  }
}
function init_morris_charts() {
  'undefined' != typeof Morris &&
    (console.log('init_morris_charts'),
    $('#graph_bar').length &&
      Morris.Bar({
        element: 'graph_bar',
        data: [
          {
            device: 'iPhone 4',
            geekbench: 380
          },
          {
            device: 'iPhone 4S',
            geekbench: 655
          },
          {
            device: 'iPhone 3GS',
            geekbench: 275
          },
          {
            device: 'iPhone 5',
            geekbench: 1571
          },
          {
            device: 'iPhone 5S',
            geekbench: 655
          },
          {
            device: 'iPhone 6',
            geekbench: 2154
          },
          {
            device: 'iPhone 6 Plus',
            geekbench: 1144
          },
          {
            device: 'iPhone 6S',
            geekbench: 2371
          },
          {
            device: 'iPhone 6S Plus',
            geekbench: 1471
          },
          {
            device: 'Other',
            geekbench: 1371
          }
        ],
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['Geekbench'],
        barRatio: 0.4,
        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: !0
      }),
    $('#graph_bar_group').length &&
      Morris.Bar({
        element: 'graph_bar_group',
        data: [
          {
            period: '2016-10-01',
            licensed: 807,
            sorned: 660
          },
          {
            period: '2016-09-30',
            licensed: 1251,
            sorned: 729
          },
          {
            period: '2016-09-29',
            licensed: 1769,
            sorned: 1018
          },
          {
            period: '2016-09-20',
            licensed: 2246,
            sorned: 1461
          },
          {
            period: '2016-09-19',
            licensed: 2657,
            sorned: 1967
          },
          {
            period: '2016-09-18',
            licensed: 3148,
            sorned: 2627
          },
          {
            period: '2016-09-17',
            licensed: 3471,
            sorned: 3740
          },
          {
            period: '2016-09-16',
            licensed: 2871,
            sorned: 2216
          },
          {
            period: '2016-09-15',
            licensed: 2401,
            sorned: 1656
          },
          {
            period: '2016-09-10',
            licensed: 2115,
            sorned: 1022
          }
        ],
        xkey: 'period',
        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
        ykeys: ['licensed', 'sorned'],
        labels: ['Licensed', 'SORN'],
        hideHover: 'auto',
        xLabelAngle: 60,
        resize: !0
      }),
    $('#graphx').length &&
      Morris.Bar({
        element: 'graphx',
        data: [
          {
            x: '2015 Q1',
            y: 2,
            z: 3,
            a: 4
          },
          {
            x: '2015 Q2',
            y: 3,
            z: 5,
            a: 6
          },
          {
            x: '2015 Q3',
            y: 4,
            z: 3,
            a: 2
          },
          {
            x: '2015 Q4',
            y: 2,
            z: 4,
            a: 5
          }
        ],
        xkey: 'x',
        ykeys: ['y', 'z', 'a'],
        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
        hideHover: 'auto',
        labels: ['Y', 'Z', 'A'],
        resize: !0
      }).on('click', function(a, b) {
        console.log(a, b);
      }),
    $('#graph_area').length &&
      Morris.Area({
        element: 'graph_area',
        data: [
          {
            period: '2014 Q1',
            iphone: 2666,
            ipad: null,
            itouch: 2647
          },
          {
            period: '2014 Q2',
            iphone: 2778,
            ipad: 2294,
            itouch: 2441
          },
          {
            period: '2014 Q3',
            iphone: 4912,
            ipad: 1969,
            itouch: 2501
          },
          {
            period: '2014 Q4',
            iphone: 3767,
            ipad: 3597,
            itouch: 5689
          },
          {
            period: '2015 Q1',
            iphone: 6810,
            ipad: 1914,
            itouch: 2293
          },
          {
            period: '2015 Q2',
            iphone: 5670,
            ipad: 4293,
            itouch: 1881
          },
          {
            period: '2015 Q3',
            iphone: 4820,
            ipad: 3795,
            itouch: 1588
          },
          {
            period: '2015 Q4',
            iphone: 15073,
            ipad: 5967,
            itouch: 5175
          },
          {
            period: '2016 Q1',
            iphone: 10687,
            ipad: 4460,
            itouch: 2028
          },
          {
            period: '2016 Q2',
            iphone: 8432,
            ipad: 5713,
            itouch: 1791
          }
        ],
        xkey: 'period',
        ykeys: ['iphone', 'ipad', 'itouch'],
        lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
        labels: ['iPhone', 'iPad', 'iPod Touch'],
        pointSize: 2,
        hideHover: 'auto',
        resize: !0
      }),
    $('#graph_donut').length &&
      Morris.Donut({
        element: 'graph_donut',
        data: [
          {
            label: 'Jam',
            value: 25
          },
          {
            label: 'Frosted',
            value: 40
          },
          {
            label: 'Custard',
            value: 25
          },
          {
            label: 'Sugar',
            value: 10
          }
        ],
        colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
        formatter: function(a) {
          return a + '%';
        },
        resize: !0
      }),
    $('#graph_line').length &&
      (Morris.Line({
        element: 'graph_line',
        xkey: 'year',
        ykeys: ['value'],
        labels: ['Value'],
        hideHover: 'auto',
        lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
        data: [
          {
            year: '2012',
            value: 20
          },
          {
            year: '2013',
            value: 10
          },
          {
            year: '2014',
            value: 5
          },
          {
            year: '2015',
            value: 5
          },
          {
            year: '2016',
            value: 20
          }
        ],
        resize: !0
      }),
      $MENU_TOGGLE.on('click', function() {
        $(window).resize();
      })));
}

!(function(a, b) {
  var c = function(a, b, c) {
    var d;
    return function() {
      function h() {
        c || a.apply(f, g), (d = null);
      }
      var f = this,
        g = arguments;
      d ? clearTimeout(d) : c && a.apply(f, g), (d = setTimeout(h, b || 100));
    };
  };
  jQuery.fn[b] = function(a) {
    return a ? this.bind('resize', c(a)) : this.trigger(b);
  };
})(jQuery, 'smartresize');
var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
  $BODY = $('body'),
  $MENU_TOGGLE = $('#menu_toggle'),
  $SIDEBAR_MENU = $('#sidebar-menu'),
  $SIDEBAR_FOOTER = $('.sidebar-footer'),
  $LEFT_COL = $('.left_col'),
  $RIGHT_COL = $('.right_col'),
  $NAV_MENU = $('.nav_menu'),
  $FOOTER = $('footer'),
  randNum = function() {
    return Math.floor(21 * Math.random()) + 20;
  };
$(document).ready(function() {
  $('.collapse-link').on('click', function() {
    var a = $(this).closest('.x_panel'),
      b = $(this).find('i'),
      c = a.find('.x_content');
    a.attr('style')
      ? c.slideToggle(200, function() {
          a.removeAttr('style');
        })
      : (c.slideToggle(200), a.css('height', 'auto')),
      b.toggleClass('fa-chevron-up fa-chevron-down');
  }),
    $('.close-link').click(function() {
      var a = $(this).closest('.x_panel');
      a.remove();
    });
}),
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
      //   container: 'body'
    });
  }),
  $('.progress .progress-bar')[0] && $('.progress .progress-bar').progressbar(),
  $(document).ready(function() {
    if ($('.js-switch')[0]) {
      var a = Array.prototype.slice.call(
        document.querySelectorAll('.js-switch')
      );
      a.forEach(function(a) {
        new Switchery(a, {
          color: '#26B99A'
        });
      });
    }
  }),
  $(document).ready(function() {
    $('input.flat')[0] &&
      $(document).ready(function() {
        $('input.flat').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
      });
  }),
  $('table input').on('ifChecked', function() {
    (checkState = ''),
      $(this)
        .parent()
        .parent()
        .parent()
        .addClass('selected'),
      countChecked();
  }),
  $('table input').on('ifUnchecked', function() {
    (checkState = ''),
      $(this)
        .parent()
        .parent()
        .parent()
        .removeClass('selected'),
      countChecked();
  });
var checkState = '';
$('.bulk_action input').on('ifChecked', function() {
  (checkState = ''),
    $(this)
      .parent()
      .parent()
      .parent()
      .addClass('selected'),
    countChecked();
}),
  $('.bulk_action input').on('ifUnchecked', function() {
    (checkState = ''),
      $(this)
        .parent()
        .parent()
        .parent()
        .removeClass('selected'),
      countChecked();
  }),
  $('.bulk_action input#check-all').on('ifChecked', function() {
    (checkState = 'all'), countChecked();
  }),
  $('.bulk_action input#check-all').on('ifUnchecked', function() {
    (checkState = 'none'), countChecked();
  }),
  $(document).ready(function() {
    $('.expand').on('click', function() {
      $(this)
        .next()
        .slideToggle(200),
        ($expand = $(this).find('>:first-child')),
        '+' == $expand.text() ? $expand.text('-') : $expand.text('+');
    });
  }),
  'undefined' != typeof NProgress &&
    ($(document).ready(function() {
      NProgress.start();
    }),
    $(window).load(function() {
      NProgress.done();
    }));
var originalLeave = $.fn.popover.Constructor.prototype.leave;
($.fn.popover.Constructor.prototype.leave = function(a) {
  var c,
    d,
    b =
      a instanceof this.constructor
        ? a
        : $(a.currentTarget)
            [this.type](this.getDelegateOptions())
            .data('bs.' + this.type);
  originalLeave.call(this, a),
    a.currentTarget &&
      ((c = $(a.currentTarget).siblings('.popover')),
      (d = b.timeout),
      c.one('mouseenter', function() {
        clearTimeout(d),
          c.one('mouseleave', function() {
            $.fn.popover.Constructor.prototype.leave.call(b, b);
          });
      }));
}),
  $('body').popover({
    selector: '[data-popover]',
    trigger: 'click hover',
    delay: {
      show: 50,
      hide: 400
    }
  }),
  $(document).ready(function() {
    //init_sparklines(),
    init_last7Days(),
      init_dates(),
      init_flot_chart(), //!flot_chart
      init_sidebar(),
      //init_wysiwyg(),
      init_InputMask(),
      //init_cropper(),
      init_knob(),
      init_IonRangeSlider(),
      //init_TagsInput(),
      init_parsley(),
      init_daterangepicker(),
      init_daterangepicker_right(),
      init_daterangepicker_single_call(),
      init_daterangepicker_reservation(),
      init_SmartWizard(),
      init_EasyPieChart(),
      init_charts(),
      //init_echarts(),
      init_morris_charts(),
      //init_skycons(),
      init_select2(),
      init_validator(),
      init_DataTables(),
      init_chart_doughnut(),
      init_gauge(),
      init_PNotify(),
      init_starrr(),
      init_calendar(),
      //init_compose(),
      init_CustomNotification(),
      init_autosize(),
      //  init_autocomplete();
      init_byUserSelect();
  });

function init_byUserSelect() {
  console.log($('select#by-user').val());
  var arr = [
    { val: 1, text: 'One' },
    { val: 2, text: 'Two' },
    { val: 3, text: 'Three' }
  ];
  let byUser = Object.values(window.byUser);
  arr = byUser;
  //var sel = $('<select>').appendTo('#by-user-select');
  $(arr).each(function() {
    $('select#by-user').append(
      $('<option selected>')
        .attr('value', this[0].last_name)
        .text(
          this[0].users_title +
            ' ' +
            this[0].first_name +
            ' ' +
            this[0].last_name
        )
    );
  });
}

function tickSize(arr) {
  if (arr.length >= 15) return 3;
  if (arr.length == 1) return 1;
  return 2;
}

$("<div id='tooltip'></div>")
  .css({
    position: 'absolute',
    display: 'none'
  })
  .appendTo('body');

$('td').hover(function() {
  //$("[data-toggle='tooltip']").tooltip('destroy');
  $('.hover-square').hover(
    function() {
      console.log($('.canvasDoughnut'));
      //   var activeSegment =
      //       $('.canvasDoughnut').data.datasets[0].metaData[index];
      //   $('.canvasDoughnut').tooltip.initialize();
      //   $('.canvasDoughnut').tooltip._active = [activeSegment];
      //   $('.canvasDoughnut').tooltip.update(true);
      //   $('.canvasDoughnut').data.datasets[0].controller.setHoverStyle(
      //     activeSegment
      //   );
      //   $('.canvasDoughnut').render(
      //     $('.canvasDoughnut').options.hover.animationDuration,
      //     true
      //   );
      //   $('.canvasDoughnut').data.datasets[0].controller.removeHoverStyle(
      //     activeSegment
      //   );
      // $(this)
      //   .find('.edit-approval-button')
      //   .slideDown();
      // $(this).css({ color: 'blue', cursor: 'move' });
    },
    function() {
      // let button = $(this).find('.edit-approval-button'); //,
      // //  onSubmit = $(this).find('.on-submit');
      // button.slideUp();
      // //  onSubmit.slideUp();
      // let id = $(button).attr('id');
      // // if (button.data().showSubmit) toggleForm(id);
      // //  button.data().showSubmit = false;
    }
  );
});
if (window.matchMedia('(min-width: 1767px)').matches) {
  //   alert('okat');
} else {
  //alert('not okat');
}
