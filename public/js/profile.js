$(() => {
  var profileTable = $('.profiles-table').DataTable({
    //   dom: 'Bfrtip',
    //   autoWidth: false,
    //   order: [[0, 'asc']],
    responsive: true
  });
  $(() => {
    $('.profiles-table_filter').addClass('searchBarClass');
  });
  $('.delete-test-confirm').on('click', function() {
    var token = $(this).attr('token');
    var url = $(this).attr('url');
    var testId = $(this).attr('testId');
    swal(
      {
        title: `Are you sure you want to delete test ${testId} ?`,
        text:
          'This action will permanently delete this record from your system...',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: url,
            type: 'POST',
            data: {
              _method: 'DELETE',
              _token: token
            },
            dataType: 'html',
            success: function success() {
              swal('Done!', 'It was succesfully deleted!', 'success');
              setTimeout(function() {
                location.reload();
              }, 1500);
            },
            error: function error(xhr, ajaxOptions, thrownError) {
              swal('Error deleting!', 'Please try again', 'error');
            }
          });
        }
      }
    );
  });
  $('#toggleHorizon').change(function() {
    //alert('hhff');
    $('.profiles-table')
      .parents('div.dataTables_wrapper')
      .first()
      .slideToggle();
  });

  // //!CHART JS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // //*__________________________________________________________________________________________________________

  // var canvas = document.getElementById('myChart');
  // var ctx = canvas.getContext('2d');

  // Chart.defaults.global.defaultFontColor = 'black';
  // Chart.defaults.global.defaultFontSize = 12;
  // var theHelp = Chart.helpers;

  // var data = {
  //   labels: ['Indicated', 'Not Indicated'],
  //   datasets: [
  //     {
  //       fill: true,
  //       backgroundColor: ['rgba(30,144,255,1)', 'rgba(119,119,119,0.7)'], //30,144,255,1
  //       data: [
  //         indicated, //   Math.round(indicated / (indicated + notIndicated) * 100),
  //         notIndicated //Math.round(notIndicated / (indicated + notIndicated) * 100)
  //       ],
  //       borderColor: ['black', 'black'],
  //       borderWidth: [0.2, 0.2]
  //     }
  //   ]
  // };

  // var options = {
  //   title: {
  //     display: true,
  //     text: '',
  //     position: 'top'
  //   },
  //   rotation: -0.7 * Math.PI,
  //   legend: {
  //     position: 'top',
  //     display: true,

  //     // generateLabels changes from chart to chart,  check the source,
  //     // this one is from the doughut :
  //     // https://github.com/chartjs/Chart.js/blob/master/src/controllers/controller.doughnut.js#L42
  //     labels: {
  //       generateLabels: function(chart) {
  //         var data = chart.data;
  //         if (data.labels.length && data.datasets.length) {
  //           return data.labels.map(function(label, i) {
  //             var meta = chart.getDatasetMeta(0);
  //             var ds = data.datasets[0];
  //             var arc = meta.data[i];
  //             var custom = (arc && arc.custom) || {};
  //             var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
  //             var arcOpts = chart.options.elements.arc;
  //             var fill = custom.backgroundColor
  //               ? custom.backgroundColor
  //               : getValueAtIndexOrDefault(
  //                   ds.backgroundColor,
  //                   i,
  //                   arcOpts.backgroundColor
  //                 );
  //             var stroke = custom.borderColor
  //               ? custom.borderColor
  //               : getValueAtIndexOrDefault(
  //                   ds.borderColor,
  //                   i,
  //                   arcOpts.borderColor
  //                 );
  //             var bw = custom.borderWidth
  //               ? custom.borderWidth
  //               : getValueAtIndexOrDefault(
  //                   ds.borderWidth,
  //                   i,
  //                   arcOpts.borderWidth
  //                 );
  //             return {
  //               // And finally :
  //               text: label, // ds.data[i] + "% of the time " + label,
  //               fillStyle: fill,
  //               strokeStyle: stroke,
  //               lineWidth: bw,
  //               hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
  //               index: i
  //             };
  //           });
  //         }
  //         return [];
  //       }
  //     }
  //   }
  // };

  // // Chart declaration:
  // var myPieChart = new Chart(ctx, {
  //   type: 'doughnut',
  //   data: data,
  //   options: options
  // });

  // console.log(myPieChart.generateLegend());

  // //Plugin from githubExample:
  // //https://github.com/chartjs/Chart.js/blob/master/samples/data_labelling.html

  // Chart.plugins.register({
  //   afterDatasetsDraw: function(chartInstance, easing) {
  //     // To only draw at the end of animation, check for easing === 1
  //     var ctx = chartInstance.chart.ctx;
  //     chartInstance.data.datasets.forEach(function(dataset, i) {
  //       var meta = chartInstance.getDatasetMeta(i);
  //       if (!meta.hidden) {
  //         meta.data.forEach(function(element, index) {
  //           // Draw the text in black, with the specified font
  //           ctx.fillStyle = 'black';
  //           var fontSize = 24;
  //           var fontStyle = 'bold';
  //           var fontFamily = 'Helvetica Neue';
  //           ctx.font = Chart.helpers.fontString(
  //             fontSize, //+ 4,
  //             fontStyle,
  //             fontFamily
  //           );
  //           // Just naively convert to string for now

  //           var dataString = dataset.data[index].toString();
  //           // Make sure alignment settings are correct
  //           ctx.textAlign = 'center';
  //           ctx.textBaseline = 'bottom';
  //           var padding = 5;
  //           var position = element.tooltipPosition();
  //           console.log({ position });
  //           ctx.fillText(
  //             dataString, // + '%',
  //             position.x,
  //             position.y +
  //               36 -
  //               fontSize / 2 -
  //               padding +
  //               (index == 1 ? position.y / 6 : -(position.y / 2.0))
  //           );
  //         });
  //       }
  //     });
  //   }
  // });
});
