/******/ (function(modules) {
  // webpackBootstrap
  /******/ // The module cache
  /******/ var installedModules = {}; // The require function
  /******/
  /******/ /******/ function __webpack_require__(moduleId) {
    /******/
    /******/ // Check if module is in cache
    /******/ if (installedModules[moduleId]) {
      /******/ return installedModules[moduleId].exports;
      /******/
    } // Create a new module (and put it into the cache)
    /******/ /******/ var module = (installedModules[moduleId] = {
      /******/ i: moduleId,
      /******/ l: false,
      /******/ exports: {}
      /******/
    }); // Execute the module function
    /******/
    /******/ /******/ modules[moduleId].call(
      module.exports,
      module,
      module.exports,
      __webpack_require__
    ); // Flag the module as loaded
    /******/
    /******/ /******/ module.l = true; // Return the exports of the module
    /******/
    /******/ /******/ return module.exports;
    /******/
  } // expose the modules object (__webpack_modules__)
  /******/
  /******/
  /******/ /******/ __webpack_require__.m = modules; // expose the module cache
  /******/
  /******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
  /******/
  /******/ /******/ __webpack_require__.d = function(exports, name, getter) {
    /******/ if (!__webpack_require__.o(exports, name)) {
      /******/ Object.defineProperty(exports, name, {
        /******/ configurable: false,
        /******/ enumerable: true,
        /******/ get: getter
        /******/
      });
      /******/
    }
    /******/
  }; // getDefaultExport function for compatibility with non-harmony modules
  /******/
  /******/ /******/ __webpack_require__.n = function(module) {
    /******/ var getter =
      module && module.__esModule
        ? /******/ function getDefault() {
            return module['default'];
          }
        : /******/ function getModuleExports() {
            return module;
          };
    /******/ __webpack_require__.d(getter, 'a', getter);
    /******/ return getter;
    /******/
  }; // Object.prototype.hasOwnProperty.call
  /******/
  /******/ /******/ __webpack_require__.o = function(object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  }; // __webpack_public_path__
  /******/
  /******/ /******/ __webpack_require__.p = '/'; // Load entry module and return exports
  /******/
  /******/ /******/ return __webpack_require__((__webpack_require__.s = 774));
  /******/
})(
  /************************************************************************/
  /******/ {
    /***/ 774: /***/ function(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(775);

      /***/
    },

    /***/ 775: /***/ function(module, exports) {
      $('.combinations-table').DataTable({
        dom: 'Bfrtip',
        autoWidth: true,
        order: [[0, 'desc']],
        responsive: true,
        buttons: [
          {
            text: '+ NEW COMBINATION',
            action: function action() {
              $.ajax({
                url: 'combinations/create',
                cache: false
              }).done(function(html) {
                $('.modal-content').html(html);
                $('#formModal').modal('show');
              });
            }
          },
          {
            extend: 'collection',
            text: 'CONNECTION: ' + connection,
            buttons: [
              {
                text: 'beta',
                action: function action(e, dt, node, config) {
                  window.location.replace(
                    BASE_URL + '/combinations?connection=beta'
                  );
                }
              },
              {
                text: 'production',
                action: function action(e, dt, node, config) {
                  window.location.replace(
                    BASE_URL + '/combinations?connection=production'
                  );
                }
              }
            ]
          }
        ],
        initComplete: function initComplete() {
          this.api()
            .columns([0, 1, 2, 3, 4])
            .every(function() {
              var column = this;
              var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function() {
                  var val = $.fn.dataTable.util.escapeRegex($(this).val());

                  column.search(val ? '^' + val + '$' : '', true, false).draw();
                });

              column
                .data()
                .unique()
                .sort()
                .each(function(d, j) {
                  select.append('<option value="' + d + '">' + d + '</option>');
                });
            });
          $(this).show();
        }
      });
      $('.tryTest').on('click', function() {
        alert('in');
      });
      /***/
    }

    /******/
  }
);
