$('.users-table').DataTable({
    dom: "Bfrtip",
    autoWidth: false,
    order: [[0, "asc"]],
    responsive: true,
    buttons: [
        {
            text: '+ NEW USER',
            className: 'bootstrap-modal-form-open',
            action: function () {
                $.ajax({
                    url: 'users/create',
                    cache: false
                })
                    .done(function (html) {
                        $(".modal-body").html(html);
                        $('#user-modal').modal();
                    });
            }
        }
    ],
    initComplete: function () {
        this.api().columns([1, 2, 3, 4, 5, 6]).every(function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });

            column.data().unique().sort().each(function (d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
        $(this).show();
    },
})

$("body").on('click', '.edit-user', function () {
    $('h4.modal-title').text('Edit User')
    var url = $(this).data('url');
    $.ajax({
        url: url,
        cache: false
    })
        .done(function (html) {
            $(".modal-body").html(html);
            $('#user-modal').modal('show');
        });
});

$("body").on('click', '.edit-roles', function () {
    $('h4.modal-title').text('Edit User Roles')
    var url = $(this).data('url');
    $.ajax({
        url: url,
        cache: false
    })
        .done(function (html) {
            $(".modal-body").html(html);
            $('#user-modal').modal('show');
        });
});

$("body").on('click', '.edit-procedure', function () {
  $('h4.modal-title').text('Assign Procedures')
  var url = $(this).data('url');
  $.ajax({
    url: url,
    cache: false
  })
   .done(function (html) {
     $(".modal-body").html(html);
     $('#user-modal').modal('show');
   });
});