let apiResponse = '',
  undoProcId;

function drag(ev) {
  console.log('in function drag');
  console.log(ev.target.id);

  //!if is combination Page
  if (window.location.pathname == '/combinations') {
    console.log('indrag');

    ev.dataTransfer.setData('Text', ev.target.id);
  } else ev.dataTransfer.setData('Text', ev.target.id);
}
function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData('text');
  console.log(data);

  //alert('ProcID: ' + data);
  changeCatToTempApi(data);
  $('.droptarget').css({
    color: '#73879c'
    // cursor: 'move'
  });
}
function dragleave(event) {
  console.log('leave');
  $('.droptarget').removeClass('hoverOnDrag');

  $('.droptarget').css({
    color: '#73879c'
    // cursor: 'move'
  });
}
function allowDrop(event) {
  event.target.style.border = '4px dotted green !important';
  event.preventDefault();
  $('.droptarget').addClass('hoverOnDrag');

  // cursor: 'move'

  $('.droptarget').css({
    color: 'blue'

    // cursor: 'move'
  });
}
function changeCatToTemp(id, isCombination = null) {
  let modal = $('#myModal');
  modal.modal('show');
  $('#archiveMessage').text(
    (isCombination ? 'Combination' : 'Procedure') + id + ' moved to temp'
  );
  setTimeout(() => {
    modal.modal('hide');
  }, 4000);
}
function setTempCombination(combinationID) {
  try {
    let response = axios
      .post(`/combinations/set-temp-category/${combinationID}`, {})
      .then(async response => {
        console.log(response);
        window.removeRow(combinationID);
        //apiResponse = response.data;
        //  window.removeRow(procId);
        changeCatToTemp(combinationID, 'combination');
        //  undoProcId = procId;
      });
  } catch (e) {
    alert('api failed');
  }
}
function changeCatToTempApi(id) {
  if (window.location.pathname == '/combinations') {
    setTempCombination(id);
    return changeCatToTemp(id);
  } else {
    let procId = id;
  }
  try {
    let response = axios
      .post(`/procedures/set-temp-category/${procId}`, {})
      .then(async response => {
        apiResponse = response.data;
        window.removeRow(procId);
        changeCatToTemp(procId);
        undoProcId = procId;
      });
  } catch (e) {
    alert('api failed');
  }
}
$('#undoChanges').on('click', function() {
  //window.console.log(apiResponse);
  let previousCatId = apiResponse.previousCatId;
  //  let undoProcId = $(this).data().procId;
  // window.console.log(apiResponse.previousCatId);
  let response = axios
    .post(`/procedures/set-temp-category/${undoProcId}/undo`, {
      previousCatId
    })
    .then(response => {
      location.reload();
    });

  //  window.console.log($(this).data().procId);
});
