var passwordInput = $('#password-input');
$(document).ready(function() {
  passwordInput
    .parent()
    .append(
      '<div style="display:inline-block;position:absolute;right:5px;top:112px"><i class="fa fa-eye hovereye" ></i></div>'
    );
  $('.hovereye').on('click', () => {
    if (passwordInput.attr('type') == 'password')
      passwordInput.attr('type', 'text');
    else passwordInput.attr('type', 'password');
  });
});
