//let indicated = (notIndicated = 0);

// isIndicated = Object.values(isIndicated);
// isIndicated.forEach(e => {
//   if (e) indicated++;
//   else notIndicated++;
// });
// console.log({ indicated }, { notIndicated });
// console.log(JSON.parse(isIndicated));

// $('select').on('change', function() {
//   alert( this.value );
// });

function selectClick(_this) {
  //return console.log(_this);
  let id = _this.attr('id');
  let submit = _this.data('showSubmit') || 0;
  _this
    .attr(
      'title',
      _this.context.dataset.originalTitle == 'Cancel' ? 'Edit' : 'Cancel'
    )
    .tooltip('fixTitle')
    .tooltip('show');

  _this.data('showSubmit', !submit);

  if (submit && _this.hasClass('toggleEdit'))
    _this.html('<i class="fa fa-edit"></i>');
  else if (submit) _this.html('<i class="fa fa-arrow-circle-down"></i>');
  else _this.html('  <span class="glyphicon glyphicon-remove"></span> ');

  // $(`label.${id}`).slideToggle();
  toggleForm(id);
}

function toggleForm(id) {
  if ($(`.${id}`).hasClass('edit-decision')) {
    $(`.${id}`).animate({
      height: 'toggle',
      opacity: 'toggle'
    });
  }

  $(`#${id}.on-submit`).toggle();
  $(`#${id}.toggle-no-decision-yet`).toggle();

  $(`.decision-form .${id}`).animate({
    height: 'toggle',
    opacity: 'toggle'
  });
}
function onSubmit(_this) {
  let id = _this.attr('id');
  //console.log(id)
  //console.log($(`select.${id}`).val());
  //console.log($(this));
  let supervisor_decision_explanation = _this
    .parent()
    .parent()
    .find('input')
    .val();

  let supervisor_decision = _this
    .parent()
    .parent()
    .find('select')
    .val();
  //let supervisor_decision = $(`select.${id}`).val();
  //let supervisor_decision_explanation = $(`input#${id}`).val();
  // console.log($(`input #${id}`));

  store(id, supervisor_decision, supervisor_decision_explanation);
}

function store(test_id, supervisor_decision, supervisor_decision_explanation) {
  axios
    .post('/client-decisions', {
      test_id: test_id,
      supervisor_decision,
      supervisor_decision_explanation
    })
    .then(response => {
      $(`button#${test_id}.toggleSelect`).html(
        '<i class="fa fa-arrow-circle-down"></i>'
      );
      toggleForm(test_id);
      console.log(response.data);
      swal('Done!', 'Action was performed successfully!', 'success');
      setTimeout(function() {
        location.reload();
      }, 1000);
      //   this.$events.fire('decision-updated', response.data);
      // this.show = false;
    })
    .catch(error => {
      console.log(error);
      //  this.errors = error.response.data;
    });
}
$('.amir').on('click', function() {
  console.log('amire clicked');
});
// $('td ').hover(
//   function() {
//     console.log($(this));

//     // $(this)
//     //   .find('.edit-approval-button')
//     //   .slideDown();
//     // $(this).css({ color: 'blue', cursor: 'move' });
//   },
//   function() {
//     // let button = $(this).find('.edit-approval-button'); //,
//     // //  onSubmit = $(this).find('.on-submit');
//     // button.slideUp();
//     // //  onSubmit.slideUp();
//     // let id = $(button).attr('id');
//     // // if (button.data().showSubmit) toggleForm(id);
//     // //  button.data().showSubmit = false;
//   }
// );
//$('.edit-approval-button').on('click', selectClick);
$('.profiles-table ').on('click', '.edit-approval-button', function() {
  selectClick($(this));
});
$('.profiles-table ').on('click', '.on-submit', function() {
  onSubmit($(this));
});
