@extends('master')

@section('title', 'Clients')

@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table text-center clients-table table-striped table-bordered nowrap" style = "display: none"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Code</th>
                    <th class = "text-center">Reports e-mail list</th>
                    <th class = "text-center noExport">Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($clients))
                    @foreach($clients as $client)
                        <tr>
                            <td>{{$client->id}}</td>
                            <td>{{$client->client_name}}</td>
                            <td>{{$client->client_code}}</td>
                            <td>{{$client->send_report_emails}}</td>
                            <td>
                                <div class = "btn-group btn-group-sm">
                                    <button style = "color: #73879C"
                                            class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{route('clients.edit', $client->id)}}"><i
                                                class = "far fa-edit" aria-hidden = "true"></i> Edit
                                    </button>
                                    <button style = "color: #73879C" token = "{{csrf_token()}}"
                                            url = "{{route('clients.destroy', $client->id)}}" href = "#"
                                            class = "btn btn-default delete-confirm">
                                        <i class = "far fa-trash-alt"
                                           aria-hidden = "true"></i> Delete
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- /page content -->
    @include('components.modal')
@endsection

@section('script')
    <script src = "{{asset('js/clients.js')}}"></script>
@endsection