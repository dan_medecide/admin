@extends('master')

@section('title', 'Client Stats')

@section('content')
    <style>
        [v-cloak] {
            display: none
        }
    </style>
    <div class = "right_col" role = "main" id = "app" v-cloak>

    </div>
@endsection

@section('script')
    <script src = "{{asset('js/client-statistics.js')}}"></script>
@stop