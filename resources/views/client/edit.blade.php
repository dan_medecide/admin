<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Update Client</h4>
</div>
<div class = "modal-body">
    {!! Form::model($client, ['route' => ['clients.update', $client->id],'class' => 'bootstrap-modal-form','method' => 'put']) !!}
    <div class = "form-group">
        {!! Form::label('client_name','Client Name') !!}
        {!! Form::text('client_name',$client->client_name,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('client_code','Client Code') !!}
        {!! Form::text('client_code',$client->client_code,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('send_report_emails','Reports E-mail List') !!}
        {!! Form::text('send_report_emails',$client->send_report_emails,['class' => 'form-control']) !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "Client has been updated successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Update
    </button>
    {!! Form::close() !!}
</div>