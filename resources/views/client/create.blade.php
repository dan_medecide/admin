<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Add Client</h4>
</div>
<div class = "modal-body">
    {!! Form::open(['route' => 'clients.store','class' => 'bootstrap-modal-form']) !!}

    <div class = "form-group">
        {!! Form::label('client_name','Client Name') !!}
        {!! Form::text('client_name',null,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('client_code','Client Code') !!}
        {!! Form::text('client_code',null,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('send_report_emails','Reports E-mail List') !!}
        {!! Form::text('send_report_emails',null,['class' => 'form-control']) !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "New Client has been created successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Create
    </button>
    {!! Form::close() !!}
</div>