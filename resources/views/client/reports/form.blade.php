{!! Form::open(['class' => 'decision-form']) !!}
<div class = "form-group">
    {{ Form::label('supervisor_decision', 'Decision')}}
    <select class = "form-control" name = "supervisor_decision" id="supervisor_decision">
        <option value = "" disabled selected>Choose an option...</option>
        @foreach($decisions as $decision)
            <option value = "{{$decision->code}}">{{$decision->title}}</option>
        @endforeach
    </select>
    @if ($errors->has('supervisor_decision'))
        <p class = "help-block">{{$errors->first('supervisor_decision')}}</p>
    @endif
</div>

<div class = "form-group">
    {{ Form::label('supervisor_decision_explanation', 'Decision Explanation')}}
    {{ Form::textarea('supervisor_decision_explanation', NULL, ['class' => 'form-control']) }}
    @if ($errors->has('supervisor_decision'))
        <p class = "help-block">{{$errors->first('supervisor_decision')}}</p>
    @endif
</div>