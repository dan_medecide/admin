<div class = "btn-group">
    <button data-toggle = "modal"
            data-target = "#reports-modal"
            class = " btn btn-sm btn-default"
            datasrc = "{{$_ENV['MEDECIDE_APP_URL'] . '/report/' . $report->test_id . '/' . \App\Home::auth($report->test_id)}}"
            onclick = "$('.report-frame').attr('src', $(this).attr('datasrc'))">Report
    </button>
    <button type = "button"
            class = "btn btn-default btn-sm decision-modal-form-open"
            data-toggle = "modal"
            data-target = "#decision-modal"
            data-content="{{$report->test_id}}"
            datasrc = "{{url('client-decisions/' . $report->test_id)}}"
    >
        Decision
    </button>
    <a target = "_blank"
       href = "{{\App\Home::getCoreAppLink('/teach/' . $report->proc_id . '/' . $report->test_id)}}"
       class = "btn btn-sm btn-default">Teach</a>
</div>