<table class = "table client-reports-table table-condensed table-bordered table-striped text-center"
       cellspacing = "0" width = "100%">
    <thead>
    <tr>
        <th class = "text-center">Date</th>
        <th class = "text-center">Procedure</th>
        <th class = "text-center">Test id</th>
        <th class = "text-center">Physician</th>
        <th class = "text-center">Patient ID</th>
        <th class = "text-center">MEDecide&trade; Decision</th>
        <th class = "text-center">Indication</th>
        <th class = "text-center">Decision</th>
        <th data-priority = "1" class = "text-center noExport">Action</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th class = "text-center">Date</th>
        <th class = "text-center">Procedure</th>
        <th class = "text-center">Test id</th>
        <th class = "text-center">Physician</th>
        <th class = "text-center">Patient ID</th>
        <th class = "text-center">MEDecide&trade; Decision</th>
        <th class = "text-center">Indication</th>
        <th class = "text-center"></th>
        <th data-priority = "1" class = "text-center noExport"></th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($reports as $report)
        <?php $supervisor_decision = DB::table( 'decision_codes' )
                                       ->where( 'code', $report->supervisor_decision )
                                       ->value( 'title' ); ?>
        <tr>
            <td>{{$report->created_at}}</td>
            <td data-toggle = "popover"
                title = "Procedure"
                data-container = "body"
                data-placement = "top"
                data-trigger = "hover"
                data-content = "{{$report->procedure}}">{{str_limit($report->procedure, $limit = 20, $end = '...')}}</td>
            <td>{{$report->test_id}}</td>
            <td>{{$report->physician_name}}</td>
            <td>{{$report->patient_id}}</td>
            <td data-toggle = "popover"
                title = "MEDecide&trade; Decision"
                data-container = "body"
                data-placement = "top"
                data-trigger = "hover"
                data-content = "{{$report->decision}}">{{str_limit($report->decision, $limit = 20, $end = '...')}}</td>
            <td>
                @if($report->indication == 1)
                    <i class = "fa fa-check" aria-hidden = "true"></i>
                @else
                    <i class = "fa fa-times" aria-hidden = "true"></i>
                @endif
            </td>
            @if(!empty($report->supervisor_decision))
                <td id="{{$report->test_id}}" data-toggle = "popover"
                    title = "Decision"
                    data-container = "body"
                    data-placement = "top"
                    data-trigger = "hover"
                    data-content = "{{$supervisor_decision}}">
                    {{str_limit($supervisor_decision, $limit = 20, $end = '...')}}
                </td>
            @else
                <td id="{{$report->test_id}}"></td>
            @endif
            <td>
                @include('client.partials.actions')
            </td>
        </tr>
    @endforeach
    </tbody>
</table>