<div class = "modal fade"
     id = "reports-modal"
     tabindex = "-1"
     role = "dialog"
     aria-labelledby = "myLargeModalLabel">
    <div class = "modal-dialog modal-lg" role = "document">
        <div class = "modal-content" id = "reports-modal-content">
            <iframe class = "report-frame" src = ""></iframe>
        </div>
    </div>
</div>