<div class = "modal fade" id = "decision-modal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel">
    <div class = "modal-dialog" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                            aria-hidden = "true">&times;</span></button>
                <h4 class = "modal-title" id = "myModalLabel">Add/Update Decision</h4>
            </div>
            <div class = "modal-body">
                @include('client.reports.form')
            </div>
            <div class = "modal-footer">
                <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
                <button type = "submit" class = "btn btn-primary decision-submit">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>