@extends('master')

@section('title', 'Client Reports')

@section('css')
    <link rel = "stylesheet" href = "{{asset('css/client-reports.css')}}">
@stop

@section('content')
    <div class = "right_col" role = "main" id = "app">
        <client-reports-table></client-reports-table>
    </div>
    <script>const clientId = "{{Auth::user()->client()->value('id')}}"</script>
    <script>const appUrl = "https://medecide.net"</script>
@endsection

@section('script')
    <script src = "https://unpkg.com/vuetable-2@1.6.0"></script>
    <script src = "{{asset('js/client-reports.js')}}"></script>
@stop