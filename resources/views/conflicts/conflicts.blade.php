@extends('master')

@section('title', 'Conflicts')

@section('css')
    <link rel = "stylesheet" href = "https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
@endsection

@section('content')
    <div class = "right_col" role = "main">
        <div class = "row">
            <table class = "table text-center conflicts-table table-striped table-bordered nowrap"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Date</th>
                    <th class = "text-center">Test ID</th>
                    <th class = "text-center">User ID</th>
                    <th class = "text-center">User Name</th>
                    <th class = "text-center">Question ID</th>
                    <th class = "text-center">Question title</th>
                    <th class = "text-center">Answer ID</th>
                    <th class = "text-center">Answer Title</th>
                    <th class = "text-center">Test Answer ID</th>
                    <th class = "text-center">Probability</th>
                    <th class = "text-center">Conflict set</th>
                    <th class = "text-center">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">Date</th>
                    <th class = "text-center">Test ID</th>
                    <th class = "text-center">User ID</th>
                    <th class = "text-center">User Name</th>
                    <th class = "text-center">Question ID</th>
                    <th class = "text-center">Question title</th>
                    <th class = "text-center">Answer ID</th>
                    <th class = "text-center">Answer Title</th>
                    <th class = "text-center">Test Answer ID</th>
                    <th class = "text-center">Probability</th>
                    <th class = "text-center">Conflict set</th>
                    <th class = "text-center">Action</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($conflicts as $conflict)
                    <tr>
                        <td>{{$conflict->date->toFormattedDateString()}}</td>
                        <td>{{$conflict->test_id}}</td>
                        <td>{{$conflict->user_id}}</td>
                        <td>{{$conflict->user}}</td>
                        <td>{{$conflict->question_id}}</td>
                        <td>{{$conflict->question}}</td>
                        <td>{{$conflict->answer_id}}</td>
                        <td data-toggle = "popover" title = "Answer Title"
                            data-content = "{{$conflict->answer}}"
                            data-container = "body" data-placement = "top" data-trigger = "hover">
                            {{str_limit($conflict->answer,25,'...')}}
                        </td>
                        <td>{{$conflict->test_answer_id}}</td>
                        <td>{{$conflict->probability}}</td>
                        <td>{{$conflict->conflict_set}}</td>
                        <td>
                            <button class = "btn btn-sm btn-default delete-confirm" id = "{{$conflict->id}}">delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script src = "https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>
    <script>const token = '{{csrf_token()}}';</script>
    <script src = "{{asset('js/conflicts.js')}}"></script>
@stop
