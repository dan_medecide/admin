<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Edit Category</h4>
</div>
<div class = "modal-body">
    {!! Form::model($category, ['route' => ['categories.update', $category->id],'class' => 'bootstrap-modal-form','method' => 'put']) !!}
    <div class = "form-group">
        {!! Form::label('title', 'Title'); !!}
        {!! Form::text('title',null,['required','class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('description', 'Description'); !!}
        {!! Form::text('description',null,['required','class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        <input type="hidden" name="production" value="0">
        {!! Form::checkbox('production'); !!}
        {!! Form::label('production', 'Add to production'); !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "This category has been updated successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Update Category
    </button>
    {!! Form::close() !!}
</div>