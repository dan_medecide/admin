@extends('master')

@section('title', 'Categories')

@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table text-center categories-table table-striped table-bordered nowrap" style="display: none"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">description</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">description</th>
                    <th class = "text-center noExport"></th>
                </tr>
                </tfoot>
                <tbody>
                @if(isset($categories))
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category['id']}}</td>
                            <td>{{$category['title']}}</td>
                            <td>{{$category['description']}}</td>
                            <td>
                                <div class="btn-group btn-group-sm">
                                    <button style = "color: #73879C"
                                            class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{ 'categories/' . $category['id'] . '/edit'  }}"><i
                                                class = "far fa-edit" aria-hidden = "true"></i> Edit
                                    </button>
                                    <button style = "color: #73879C" token = "{{csrf_token()}}"
                                            url = "{{'/categories/' . $category['id']}}" href = "#"
                                            class = "btn btn-default delete-confirm">
                                        <i class = "far fa-trash-alt"
                                           aria-hidden = "true"></i> Delete</button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- /page content -->
    @include('components.modal')
@endsection

@section('script')
    <script src = "{{asset('js/categories.js')}}"></script>
@endsection