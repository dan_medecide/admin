<!DOCTYPE html>
<html lang = "en">
<head>
    <meta http-equiv = "Content-Type" content = "text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE=edge">
    <meta name = "viewport" content = "width=device-width, initial-scale=1">

    <title>SerenusAI™</title>

    <!-- Bootstrap -->
    <link href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
    <!-- Font Awesome -->
    <link href = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel = "stylesheet">
    <!-- NProgress -->
    <link href = "https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.css" rel = "stylesheet">
    <!-- Animate.css -->
    <link href = "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" rel = "stylesheet">
    <!-- Custom Theme Style -->
    <link href = "{{asset('css/custom.min.css')}}" rel = "stylesheet">
</head>

<body class = "nav-md">
<div class = "container body">
    <div class = "main_container">
        <!-- page content -->
        <div class = "col-md-12">
            <div class = "col-middle">
                <div class = "text-center">
                    <h1 class = "error-number">500</h1>
                    <h2>Internal Server Error</h2>
                    <p>We track these errors automatically, but if the problem persists feel free to contact us. In the
                        meantime, try refreshing.
                    </p>
                    {{--<div class = "mid_center">--}}
                        {{--<h3>Search</h3>--}}
                        {{--<form>--}}
                            {{--<div class = "col-xs-12 form-group pull-right top_search">--}}
                                {{--<div class = "input-group">--}}
                                    {{--<input type = "text" class = "form-control" placeholder = "Search for...">--}}
                                    {{--<span class = "input-group-btn">--}}
                              {{--<button class = "btn btn-default" type = "button">Go!</button>--}}
                          {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<!-- jQuery -->
<script crossorigin = "anonymous" integrity = "sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        src = "https://code.jquery.com/jquery-3.2.1.min.js">
</script>
<!-- Bootstrap -->
<script src = "../vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Custom Theme Scripts -->
<script src = "{{asset('js/custom.js')}}">
</script>
</body>
</html>

{{--<div class="content">--}}
{{--<div class="title">Something went wrong.</div>--}}

{{--@if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))--}}
{{--<p>foo</p>--}}
{{--<div class="subtitle">Error ID: {{ Sentry::getLastEventID() }}</div>--}}

{{--<!-- Sentry JS SDK 2.1.+ required -->--}}
{{--<script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>--}}

{{--<script>--}}
{{--Raven.showReportDialog({--}}
{{--eventId: '{{ Sentry::getLastEventID() }}',--}}
{{--// use the public DSN (dont include your secret!)--}}
{{--dsn: 'https://e9ebbd88548a441288393c457ec90441@sentry.io/3235',--}}
{{--user: {--}}
{{--'name': 'Roman Gutkin',--}}
{{--'email': 'roman@medecide.net',--}}
{{--}--}}
{{--});--}}
{{--</script>--}}
{{--@endif--}}
{{--</div>--}}