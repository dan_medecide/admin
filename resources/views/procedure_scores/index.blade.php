@extends('master')

@section('title', 'Procedure Scores')

@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table scores-table table-condensed table-striped table-bordered text-center"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">From</th>
                    <th class = "text-center">To</th>
                    <th class = "text-center">Indication</th>
                    <th class = "text-center">Decision Code</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($scores as $score)
                    <tr>
                        <td>{{$score->id}}</td>
                        <td>{{$score->title}}</td>
                        <td>{{$score->score_from}}</td>
                        <td>{{$score->score_to}}</td>
                        <td>{{$score->decision['is_indication']}}</td>
                        <td>{{$score->decision['title']}}</td>
                        <td>
                            <div class = "btn-group" role = "group">
                                <button
                                        class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                        data-url = "{{route('procedure-scores.edit',$score->id)}}"><i
                                            class = "fa fa-pencil fa-fw" aria-hidden = "true"></i> Edit
                                </button>
                                <button token = "{{csrf_token()}}"
                                            url = "{{'/procedure-scores/' . $score->id}}" href = "#"
                                            class = "btn btn-default delete-confirm">
                                        <i class = "fa fa-trash-o"
                                           aria-hidden = "true"></i> Delete</button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /page content -->
    @include('components.modal')
@endsection
