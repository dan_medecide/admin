<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Update Scores</h4>
</div>
<div class = "modal-body">
    {!! Form::model($score, ['route' => ['procedure-scores.update', $score->id],'class' => 'bootstrap-modal-form','method' => 'put']) !!}
    <div class = "form-group">
        {!! Form::label('title','Title') !!}
        {!! Form::text('title',$score->title,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('score_from','From') !!}
        {!! Form::number('score_from',$score->score_from,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('score_to','To') !!}
        {!! Form::number('score_to',$score->score_to,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('decision_code','Decision') !!}
        {!! Form::select('decision_code',$decisions,null,['class' => 'form-control']) !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "Combination has been updated successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Update
    </button>
    {!! Form::close() !!}
</div>