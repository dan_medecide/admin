@extends('master')

@section('title', 'Notes')

@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">

        <div class = "container">
            <table class = "table text-center notes-table table-bordered table-striped nowrap" style="display: none"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>title</th>
                    <th class = "noExport">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($notes))
                    @foreach($notes as $note)
                        <tr>
                            <td>{{$note->id}}</td>
                            <td>{{$note->title}}</td>
                            <td>
                                <div class = "btn-group btn-group-sm">
                                    <button style = "color: #73879C"
                                            class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{'notes/' . $note->id . '/edit'}}"><i
                                                class = "far fa-edit" aria-hidden = "true"></i> Edit
                                    </button>
                                    <button style = "color: #73879C"
                                            url = "{{'notes/' . $note->id}}"
                                            token = "{{csrf_token()}}"
                                            class = "btn btn-default delete-confirm">
                                        <i class = "far fa-trash-alt" aria-hidden = "true"></i> Delete
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- /page content -->
    @include('components.modal')
@endsection

@section('script')
    <script src = "{{asset('js/notes.js')}}"></script>
@stop