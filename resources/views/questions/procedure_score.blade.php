<table class = "table table-bordered text-center table-responsive table-striped">
    <thead>
    <tr>
        <th class="text-center">From</th>
        <th class="text-center">To</th>
        <th class="text-center">Title</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for = "procedure_score in procedureScores">
        <td>@{{procedure_score.score_from}}</td>
        <td>@{{procedure_score.score_to}}</td>
        <td>@{{procedure_score.title}}</td>
    </tr>
    </tbody>
</table>