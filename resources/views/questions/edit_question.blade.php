<div class = "panel panel-default" v-if = "editQuestionForm">
    <div class = "panel-heading clearfix">
        <h2 class = "panel-title pull-left" style = "padding-top: 3px;">@{{ question.title_doctor }}</h2>
        <button type = "button" class = "close panel-title pull-right" @click = "editSection = false"
                aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
    </div>
    <div class = "panel-body">
        <form method = "POST" action = "/questions" @submit.prevent = "updateQuestion()">
            <div class = "form-group">
                <label for = "automatic" class = "checkbox-inline"><input type = "checkbox"
                                                                          name = "automatic"
                                                                          id = "automatic"
                                                                          v-model = "question.is_auto">AUTOMATIC
                                                                                                       QUESTION</label>
                <label for = "general" class = "checkbox-inline"><input type = "checkbox"
                                                                        name = "general"
                                                                        id = "general"
                                                                        v-model = "question.is_general">GENERAL
                                                                                                        QUESTION</label>
            </div>
            <div :class = "{'form-group': true, 'has-error': question.errors.has('title_doctor')}">
                <label for = "title_doctor">TITLE FOR DOCTOR</label>
                <input type = "text" placeholder = "Question title for doctor"
                       @keydown = "question.errors.clear('title_doctor')"
                       class = "form-control"
                       name = "title_doctor"
                       id = "title_doctor" v-model = "question.title_doctor">
                <span class = "help-block" v-if = "question.errors.has('title_doctor')"
                      v-text = "question.errors.get('title_doctor')"></span>
            </div>
            <div :class = "{'form-group': true, 'has-error': question.errors.has('title')}">
                <label for = "title">TITLE FOR PATIENT</label>
                <input type = "text" placeholder = "Question title" class = "form-control"
                       @keydown = "question.errors.clear('title')" name = "title"
                       id = "title"
                       v-model = "question.title">
                <span class = "help-block" v-if = "question.errors.has('title')"
                      v-text = "question.errors.get('title')"></span>
            </div>
            <question-tag></question-tag>
            <question-indication></question-indication>
            <div class = "col-md-12" style = "padding: 0;">
                <div style = "padding-left: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('question_type')}">
                    <label for = "question_type">QUESTION TYPE</label>
                    <select class = "form-control" v-model = "question.question_type"
                            name = "question_type" id = "question_type">
                        <option value = "Main Surgery Factor">Main Surgery Factor</option>
                        <option value = "Critical Exam">Critical Exam</option>
                        <option value = "General Health">General Health</option>
                    </select>
                    <span class = "help-block" v-if = "question.errors.has('question_type')"
                          v-text = "question.errors.get('question_type')"></span>
                </div>
                <div style = "padding-right: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('answers_type')}">
                    <label for = "answers_type">ANSWERS TYPE</label>
                    <select class = "form-control" v-model = "question.answers_type"
                            name = "answers_type" id = "answers_type">
                        <option value = "Single Answer">Single Answer</option>
                        <option value = "Multiple Answers">Multiple Answers</option>
                        <option value = "Slider">Slider</option>
                        <option value = "VAS">VAS</option>
                        <option value = "Automatic Question">Automatic Question</option>
                        <option value = "Numeric Answer">Numeric Answer</option>
                    </select>
                    <span class = "help-block" v-if = "question.errors.has('answers_type')"
                          v-text = "question.errors.get('answers_type')"></span>
                </div>
            </div>
            <div :class = "{'form-group': true, 'has-error': question.errors.has('explanation')}">
                <label for = "explanation">EXPLANATION</label>
                <input type = "text" placeholder = "Explanation" class = "form-control"
                       @keydown = "question.errors.clear('explanation')"
                       name = "explanation" id = "explanation"
                       v-model = "question.explanation">
                <span class = "help-block" v-if = "question.errors.has('explanation')"
                      v-text = "question.errors.get('explanation')"></span>
            </div>
            <div :class = "{'form-group': true, 'has-error': question.errors.has('priority')}">
                <label for = "priority">PRIORITY</label>
                <input type = "text"
                       placeholder = "Questions are sorted by priority or entrance order"
                       class = "form-control"
                       @keydown = "question.errors.clear('priority')"
                       name = "priority"
                       id = "priority"
                       v-model = "question.priority">
                <span class = "help-block" v-if = "question.errors.has('priority')"
                      v-text = "question.errors.get('priority')"></span>
            </div>
            <editable label = "REQUIRED KEY" :content = "question.tag"
                      @update = "question.tag = $event"
                      help_text = "Required key for showing this question. Use @ for selecting keys"></editable>
            <div class = "col-md-12" style = "padding: 0;">
                <div style = "padding-left: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('min_score')}">
                    <label for = "min_score">MIN SCORE</label>
                    <input type = "text" placeholder = "Min score to show this question" class = "form-control"
                           @keydown = "question.errors.clear('min_score')"
                           name = "min_score"
                           id = "min_score"
                           v-model = "question.min_score">
                    <span class = "help-block" v-if = "question.errors.has('min_score')"
                          v-text = "question.errors.get('min_score')"></span>
                </div>
                <div style = "padding-right: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('max_score')}">
                    <label for = "max_score">MAX SCORE</label>
                    <input type = "text" placeholder = "Max score to show this question" class = "form-control"
                           @keydown = "question.errors.clear('max_score')"
                           name = "max_score"
                           id = "max_score"
                           v-model = "question.max_score">
                    <span class = "help-block" v-if = "question.errors.has('max_score')"
                          v-text = "question.errors.get('max_score')"></span>
                </div>
            </div>
            <div class = "col-md-12" style = "padding: 0;">
                <div style = "padding-left: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('vas_min')}">
                    <label for = "vas_min">VAS MIN SCORE</label>
                    <input type = "text" placeholder = "VAS min score" class = "form-control"
                           @keydown = "question.errors.clear('vas_min')"
                           name = "vas_min"
                           id = "vas_min"
                           v-model = "question.vas_min">
                    <span class = "help-block" v-if = "question.errors.has('vas_min')"
                          v-text = "question.errors.get('vas_min')"></span>
                </div>
                <div style = "padding-right: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('vas_max')}">
                    <label for = "vas_max">VAS MAX SCORE</label>
                    <input type = "text" placeholder = "VAS max score" class = "form-control"
                           @keydown = "question.errors.clear('vas_max')"
                           name = "vas_max"
                           id = "vas_max"
                           v-model = "question.vas_max">
                    <span class = "help-block" v-if = "question.errors.has('vas_max')"
                          v-text = "question.errors.get('vas_max')"></span>
                </div>
            </div>
            <div class = "col-md-12" style = "padding: 0;">
                <div style = "padding-left: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('vas_min_display')}">
                    <label for = "vas_min_display">VAS MIN DISPLAY VALUE</label>
                    <input type = "text" placeholder = "VAS min display score" class = "form-control"
                           @keydown = "question.errors.clear('vas_min_display')"
                           name = "vas_min_display"
                           id = "vas_min_display"
                           v-model = "question.vas_min_display">
                    <span class = "help-block" v-if = "question.errors.has('vas_min_display')"
                          v-text = "question.errors.get('vas_min_display')"></span>
                </div>
                <div style = "padding-right: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('vas_max_display')}">
                    <label for = "vas_max_display">VAS MAX DISPLAY VALUE</label>
                    <input type = "text" placeholder = "VAS max display score" class = "form-control"
                           @keydown = "question.errors.clear('vas_max_display')"
                           name = "vas_max_display"
                           id = "vas_max_display"
                           v-model = "question.vas_max_display">
                    <span class = "help-block" v-if = "question.errors.has('vas_max_display')"
                          v-text = "question.errors.get('vas_max_display')"></span>
                </div>
            </div>
            <div :class = "{'form-group': true, 'has-error': question.errors.has('vas_step')}">
                <label for = "vas_step">VAS STEP</label>
                <input type = "text" placeholder = "VAS step" class = "form-control"
                       @keydown = "question.errors.clear('vas_step')" name = "vas_step"
                       id = "vas_step"
                       v-model = "question.vas_step">
                <span class = "help-block" v-if = "question.errors.has('vas_step')"
                      v-text = "question.errors.get('vas_step')"></span>
            </div>
            <div class = "col-md-12" style = "padding: 0;">
                <div style = "padding-left: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('numric_min_range')}">
                    <label for = "numric_min_range">NUMERIC MIN RANGE</label>
                    <input type = "text" placeholder = "Numeric min range" class = "form-control"
                           @keydown = "question.errors.clear('numric_min_range')"
                           name = "numric_min_range"
                           id = "numric_min_range"
                           v-model = "question.numric_min_range">
                    <span class = "help-block" v-if = "question.errors.has('numric_min_range')"
                          v-text = "question.errors.get('numric_min_range')"></span>
                </div>
                <div style = "padding-right: 0;"
                     :class = "{'form-group': true, 'col-md-6': true, 'has-error': question.errors.has('numric_max_range')}">
                    <label for = "numric_max_range">NUMERIC MAX RANGE</label>
                    <input type = "text" placeholder = "Numeric max range" class = "form-control"
                           @keydown = "question.errors.clear('numric_max_range')"
                           name = "numric_max_range"
                           id = "numric_max_range"
                           v-model = "question.numric_max_range">
                    <span class = "help-block" v-if = "question.errors.has('numric_max_range')"
                          v-text = "question.errors.get('numric_max_range')"></span>
                </div>
            </div>
            <input type = "submit" class = "btn btn-sm btn-success" :disabled = "question.errors.any()" value = "SAVE">
        </form>
    </div>
</div>