<div v-for="question in filteredQuestions"
     :class="{ 'panel':true, 'panel-default':!question.color, 'panel-primary':question.color }">
    @include('questions.question_heading')
    <collapse class="panel-body" v-model="question.display">
        @include('questions.question_parameters')
        @include('questions.answers')
    </collapse>
</div>