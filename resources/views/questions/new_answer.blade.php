<div class = "panel panel-default" v-if = "newAnswerForm">
    <div class = "panel-heading clearfix">
        <h2 class = "panel-title pull-left" style = "padding-top: 3px;">
            @{{ newAnswer.title }}
        </h2>
        <button @click = "editSection = false"
                aria-label = "Close"
                class = "close panel-title pull-right"
                type = "button">
            <span aria-hidden = "true">
                ×
            </span>
        </button>
    </div>
    <div class = "panel-body">
        <form @keydown = "newAnswer.errors.clear($event.target.name)"
              @submit.prevent = "storeAnswer()"
              action = "/answers"
              method = "POST">
            <div class = "form-group">
                <label class = "checkbox-inline" for = "end_test">
                    <input id = "end_test" name = "end_test" type = "checkbox" v-model = "newAnswer.end_test">
                    END TEST ON THIS ANSWER
                    </input>
                </label>
            </div>

            <div :class = "{'form-group':true, 'has-error':newAnswer.errors.has('title')}">
                <label for = "title">
                    TITLE
                </label>
                <input class = "form-control" id = "title" name = "title" placeholder = "Title" type = "text"
                       v-model = "newAnswer.title">
                <span class = "help-block"
                      v-if = "newAnswer.errors.has('title')"
                      v-text = "newAnswer.errors.get('title')">
                    </span>
                </input>
            </div>

            <new-answer-indication></new-answer-indication>
            <new-answer-notes></new-answer-notes>
            <new-answer-tag></new-answer-tag>

            <div :class = "{'form-group':true, 'has-error':newAnswer.errors.has('justification')}">
                <label for = "justification">
                    JUSTIFICATION
                </label>
                <input class = "form-control" id = "justification" name = "justification" placeholder = "Justification"
                       type = "text" v-model = "newAnswer.justification">
                <span class = "help-block" v-if = "newAnswer.errors.has('justification')"
                      v-text = "newAnswer.errors.get('justification')">
                    </span>
                </input>
            </div>
            <div :class = "{'form-group':true, 'has-error':newAnswer.errors.has('weight')}">
                <label for = "weight">
                    WEIGHT
                </label>
                <input class = "form-control"
                       id = "weight"
                       name = "weight"
                       placeholder = "Weight - positive or negative number"
                       type = "number"
                       v-model = "newAnswer.weight">
                <span class = "help-block"
                      v-if = "newAnswer.errors.has('weight')"
                      v-text = "newAnswer.errors.get('weight')">
                    </span>
                </input>
            </div>
            <editable label = "WEIGHT FORMULA" :content = "newAnswer.weight_formula"
                      @update = "newAnswer.weight_formula = $event"
                      help_text = "Weight formula. use @ to select keys, or leave empty."></editable>

            <editable label = "KEYS FOR SHOWING ANSWER" :content = "newAnswer.keys_for_showing_answer"
                      @update = "newAnswer.keys_for_showing_answer = $event"
                      help_text = "Condition for showing this newAnswer. use @ to select keys, or leave empty"></editable>

            <editable label = "KEYS FOR SHOWING ANSWER TEACH" :content = "newAnswer.keys_for_showing_answer_teach"
                      @update = "newAnswer.keys_for_showing_answer_teach = $event"
                      help_text = "Condition for showing this answer in teach. use @ to select keys, or leave empty"></editable>

            <div class = "col-md-12" style = "padding: 0;">
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':newAnswer.errors.has('force_score')}"
                     style = "padding-left: 0;">
                    <label for = "force_score">
                        FORCE SCORE
                    </label>
                    <input class = "form-control" id = "force_score" name = "force_score"
                           placeholder = "Force score - positive or negative number" type = "number"
                           v-model = "newAnswer.force_score">
                    <span class = "help-block" v-if = "newAnswer.errors.has('force_score')"
                          v-text = "newAnswer.errors.get('force_score')">
                        </span>
                    </input>
                </div>
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':newAnswer.errors.has('vas_step')}"
                     style = "padding-right: 0;">
                    <label for = "vas_step">
                        VAS STEP
                    </label>
                    <input class = "form-control"
                           id = "vas_step"
                           name = "vas_step"
                           placeholder = "VAS step"
                           type = "text"
                           v-model = "newAnswer.vas_step">
                    <span class = "help-block" v-if = "newAnswer.errors.has('vas_step')"
                          v-text = "newAnswer.errors.get('vas_step')">
                        </span>
                    </input>
                </div>
            </div>
            <div class = "col-md-12" style = "padding: 0;">
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':newAnswer.errors.has('vas_min')}"
                     style = "padding-left: 0;">
                    <label for = "vas_min">
                        VAS MIN SCORE
                    </label>
                    <input class = "form-control"
                           id = "vas_min"
                           name = "vas_min"
                           placeholder = "VAS min score"
                           type = "text"
                           v-model = "newAnswer.vas_min">
                    <span class = "help-block" v-if = "newAnswer.errors.has('vas_min')"
                          v-text = "newAnswer.errors.get('vas_min')">
                        </span>
                    </input>
                </div>
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':newAnswer.errors.has('vas_max')}"
                     style = "padding-right: 0;">
                    <label for = "vas_max">
                        VAS MAX SCORE
                    </label>
                    <input class = "form-control"
                           id = "vas_max"
                           name = "vas_max"
                           placeholder = "VAS max score"
                           type = "text"
                           v-model = "newAnswer.vas_max">
                    <span class = "help-block" v-if = "newAnswer.errors.has('vas_max')"
                          v-text = "newAnswer.errors.get('vas_max')">
                        </span>
                    </input>
                </div>
            </div>
            <div class = "col-md-12" style = "padding: 0;">
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':newAnswer.errors.has('vas_min_display')}"
                     style = "padding-left: 0;">
                    <label for = "vas_min_display">
                        VAS MIN DISPLAY VALUE
                    </label>
                    <input class = "form-control" id = "vas_min_display" name = "vas_min_display"
                           placeholder = "VAS min display score" type = "text" v-model = "newAnswer.vas_min_display">
                    <span class = "help-block" v-if = "newAnswer.errors.has('vas_min_display')"
                          v-text = "newAnswer.errors.get('vas_min_display')">
                        </span>
                    </input>
                </div>
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':newAnswer.errors.has('vas_max_display')}"
                     style = "padding-right: 0;">
                    <label for = "vas_max_display">
                        VAS MAX DISPLAY VALUE
                    </label>
                    <input class = "form-control" id = "vas_max_display" name = "vas_max_display"
                           placeholder = "VAS max display score" type = "text" v-model = "newAnswer.vas_max_display">
                    <span class = "help-block" v-if = "newAnswer.errors.has('vas_max_display')"
                          v-text = "newAnswer.errors.get('vas_max_display')">
                        </span>
                    </input>
                </div>
            </div>
            <div :class = "{'form-group':true, 'has-error':newAnswer.errors.has('vas_explanation')}">
                <label for = "vas_explanation">
                    VAS EXPLANATION
                </label>
                <input class = "form-control" id = "vas_explanation" name = "vas_explanation"
                       placeholder = "VAS explanation text" type = "text" v-model = "newAnswer.vas_explanation">
                <span class = "help-block" v-if = "newAnswer.errors.has('vas_explanation')"
                      v-text = "newAnswer.errors.get('vas_explanation')">
                    </span>
                </input>
            </div>

            <editable label = "VAS AUTO ANSWER FORMULA" :content = "newAnswer.vas_auto_answer_formula"
                      @update = "newAnswer.vas_auto_answer_formula = $event"
                      help_text = "VAS auto answer formula. use @ to select keys, or leave empty"></editable>


            <editable label = "AUTO KEYS" :content = "newAnswer.auto_keys"
                      @update = "newAnswer.auto_keys = $event"
                      help_text = "Auto keys condition for triggering this answer in automatic question. Use @ to select keys."></editable>

            <div :class = "{'form-group':true, 'has-error':newAnswer.errors.has('priority')}">
                <label for = "priority">
                    PRIORITY
                </label>
                <input class = "form-control"
                       id = "priority"
                       name = "priority"
                       placeholder = "Priority - number. higher shows first"
                       type = "number"
                       v-model = "newAnswer.priority">
                <span class = "help-block" v-if = "newAnswer.errors.has('priority')"
                      v-text = "newAnswer.errors.get('priority')">
                    </span>
                </input>
            </div>

            <editable label = "KEYS FOR END TEST" :content = "newAnswer.keys_for_end_test"
                      @update = "newAnswer.keys_for_end_test = $event"
                      help_text = "Required key condition for ENDING the test. Use @ to select keys."></editable>

            <div :class = "{'form-group':true, 'has-error':newAnswer.errors.has('end_test_text')}">
                <label for = "end_test_text">
                    END TEST TEXT
                </label>
                <input class = "form-control" id = "end_test_text" name = "end_test_text" placeholder = "End test text"
                       type = "text" v-model = "newAnswer.end_test_text">
                <span class = "help-block" v-if = "newAnswer.errors.has('end_test_text')"
                      v-text = "newAnswer.errors.get('end_test_text')">
                    </span>
                </input>
            </div>
            <input :disabled = "newAnswer.errors.any()" class = "btn btn-sm btn-success" type = "submit" value = "SAVE">
            </input>
        </form>
    </div>
</div>
