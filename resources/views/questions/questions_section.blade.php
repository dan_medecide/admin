<div :class = "{ 'col-md-8': editSection}">
    <div class = "container questions-container">
        <div class = "row" v-if = "!loading">
            <button class = "btn btn-sm btn-default" @click = "createQuestion()">+ New Question
            </button>
            <button class = "btn btn-sm btn-default" @click = "$refs.questions.$refs.validate.validateQuestions()">
                Validate Questions
            </button>
            <button @click="$refs.combineQuestionsModal.open = true" class = "btn btn-sm btn-default">
                Combine Questions
            </button>
            <span v-show="!editSection"><b>Procedure: </b>@{{procedureTitle}} ({{$proc_id}})</span>
            <filter-questions :questions="questions" @questions-filtered = updateQuestions></filter-questions>
        </div>
        <div class = "row">
            <questions ref = "questions" :questions = "questions"></questions>
        </div>
    </div>
</div>