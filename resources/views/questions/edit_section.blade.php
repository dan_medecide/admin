<div v-if = "editSection" class = "col-md-4">
    <div class = "container form-container">
        <div class = "row">
            <span style="margin-bottom: 15px;padding-top: 10px;" class = "pull-right" v-show = "editSection"><b>Procedure: </b>@{{procedureTitle}} ({{$proc_id}})</span>
        </div>
        <div class = "row">
            <component :is = "currentForm"
                       :indications = "indications"
                       :tags = "tags"
                       :object = "object"
                       :create-new = "createNew"
                       v-if = "formDisplay">
                <button slot = "dismissBtn"
                        type = "button"
                        class = "close panel-title pull-right"
                        @click = "editSection = false"
                        aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
            </component>
            <spinner class = "loading" v-else size = "massive">
            </spinner>
        </div>
    </div>
</div>