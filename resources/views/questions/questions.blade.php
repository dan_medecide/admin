@extends('master')

 
 
 
@section('title', 'Questions')

 
 
@section('meta')
    <meta content = "{{ csrf_token() }}" name = "csrf-token">
@endsection

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/vue2-animate/dist/vue2-animate.min.css"/>
    <link href = "{{asset('css/vue-multiselect.min.css')}}" rel = "stylesheet">
    <link rel = "stylesheet" href = "{{asset('css/questions.css')}}">
@stop
@section('procedureActions')
 <table>
         <tr><td>
          @component('layouts.proceduresActions', ['procedure' => $procedure]) @endcomponent
          </td></tr>
     </table>
@stop
@section('content')
<div class = "right_col" role = "main">

        <div class = "container" id = "app" v-cloak></div>
    </div>
    <script>
      const proc_id = '{{$proc_id}}';
      const proc_title = '{{$proc_title}}';
      const user_token = '{{$user_token}}';
      const cat_id = '{{$cat_id}}';
      const api_token = '{{session('api-token')}}'
    </script>
@endsection

@section('script')
    <?php $x=rand(1,1000)?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js"></script>
    <script src = "{{asset('js/questions.js?'.$x)}}"></script>
 
@stop