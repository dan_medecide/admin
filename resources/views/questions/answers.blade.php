<h4 v-if = "question.answers.length != 0" class = "pull-left">Possible Answers</h4>
<button class = "btn btn-sm btn-default pull-right" @click = "createAnswer(question.id)" style = "margin-right: 0;">+
                                                                                                                    Add
                                                                                                                    Answer
</button>
{{--<button class = "btn btn-sm btn-default pull-right" @click = "validateAnswers(question.answers)" style = "margin-right: 0;">Validate Answers--}}
{{--</button>--}}
<table v-if = "question.answers.length != 0"
       class = "table table-condensed table-responsive table-bordered table-striped text-center">
    <thead>
    <tr>
        <th class = "text-center">Title</th>
        <th class = "text-center">Weight</th>
        <th class = "text-center">End Test</th>
        <th class = "text-center">Force Score</th>
        <th class = "text-center">Notes</th>
        <th class = "text-center">Created Keys</th>
        <th class = "text-center">Auto Answer Keys</th>
        <th class = "text-center">Show Answer Keys</th>
        {{--<th class = "text-center">Show Answer Keys (teach)</th>--}}
        <th class = "text-center">Indications</th>
        <th class = "text-center">VAS</th>
        {{--<th class = "text-center">VAS Auto Answer Formula</th>--}}
        <th class = "text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for = "answer in question.answers">

        <td class = "handHover">
            <tooltip :text = "String('ID ' + answer.id)">
                <span>@{{answer.title}}</span>
            </tooltip>
        </td>
        <td>@{{answer.weight}}</td>
        <td v-if = "answer.keys_for_end_test">@{{ answer.keys_for_end_test }}</td>
        <td v-else><i v-if = "answer.end_test" class = "fa fa-check fa-lg" aria-hidden = "true"></i></td>
        <td>@{{answer.force_score}}</td>
        <td>@{{ answer.notes }}</td>
        <td>@{{ answer.tag }}</td>
        <td>@{{ answer.auto_keys }}</td>
        <td>@{{ answer.keys_for_showing_answer }}</td>
        {{--<td>@{{ answer.keys_for_showing_answer_teach }}</td>--}}
        <td>@{{ answer.indication }}</td>
        <td style = "min-width: 120px;">
            <p>
                @{{answer.vas_min}} - @{{answer.vas_max}} <br> ( @{{answer.vas_min_display}} -
                @{{answer.vas_max_display}} | Step: @{{answer.vas_step}} ) <br>
                @{{answer.vas_auto_answer_formula}}</p>
        </td>
        {{--<td>@{{answer.vas_auto_answer_formula}}</td>--}}
        <td style = "min-width: 120px;">
            <div class = "btn-group" role = "group" aria-label = "...">
                <button class = "btn btn-sm btn-default" @click = "editAnswer(answer.id)">Edit</button>
                <button class = "btn btn-sm btn-default" @click = "confirmDelete('answer', answer.id)">Delete</button>
            </div>
        </td>
    </tr>
    </tbody>
</table>