<h4>Question Parameters</h4>
<table class = "text-center table table-striped table-bordered">
    <thead>
    <tr>
        <th class = "text-center">Title for patient</th>
        <th class = "text-center">Required key</th>
        <th class = "text-center">Score</th>
        <th class = "text-center">Priority</th>
        <th class = "text-center">Indication</th>
        <th class = "text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>@{{question.title}}</td>
        <td>@{{question.tag}}</td>
        <td>@{{question.min_score}} - @{{question.max_score}}</td>
        <td>@{{question.priority}}</td>
        <td>@{{question.indication}}</td>
        <td style = "min-width: 200px;">
            <div class = "btn-group" role = "group" aria-label = "...">
                <button type = "button" class = "btn btn-sm btn-default" @click = "editQuestion(question.id)">Edit
                </button>
                <button type = "button" class = "btn btn-sm btn-default" @click = "duplicateQuestion(question.id)">
                    Duplicate
                </button>
                <button type = "button"
                        class = "btn btn-sm btn-default"
                        @click = "confirmDelete('question',question.id)">Delete
                </button>
            </div>
        </td>
    </tr>
    </tbody>
</table>