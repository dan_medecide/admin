<div class = "panel panel-default" v-if = "editAnswerForm">
    <div class = "panel-heading clearfix">
        <h2 class = "panel-title pull-left" style = "padding-top: 3px;">@{{ answer.title }}</h2>
        <button type = "button" class = "close panel-title pull-right" @click = "editSection = false"
                aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
    </div>
    <div class = "panel-body">
        <form method = "POST" action = "/answers" @submit.prevent = "updateAnswer()"
              @keydown = "answer.errors.clear($event.target.name)">
            <div class = "form-group">
                <label class = "checkbox-inline" for = "end_test">
                    <input id = "end_test" name = "end_test" type = "checkbox" v-model = "answer.end_test">
                    END TEST ON THIS ANSWER
                    </input>
                </label>
            </div>

            <answer-indication></answer-indication>
            <answer-notes></answer-notes>
            <answer-tag></answer-tag>

            <div :class = "{'form-group':true, 'has-error':answer.errors.has('title')}">
                <label for = "title">
                    TITLE
                </label>
                <input class = "form-control" id = "title" name = "title" placeholder = "Title" type = "text"
                       v-model = "answer.title">
                <span class = "help-block"
                      v-if = "answer.errors.has('title')"
                      v-text = "answer.errors.get('title')">
                    </span>
                </input>
            </div>

            <div :class = "{'form-group':true, 'has-error':answer.errors.has('justification')}">
                <label for = "justification">
                    JUSTIFICATION
                </label>
                <input class = "form-control" id = "justification" name = "justification" placeholder = "Justification"
                       type = "text" v-model = "answer.justification">
                <span class = "help-block" v-if = "answer.errors.has('justification')"
                      v-text = "answer.errors.get('justification')">
                    </span>
                </input>
            </div>
            <div :class = "{'form-group':true, 'has-error':answer.errors.has('weight')}">
                <label for = "weight">
                    WEIGHT
                </label>
                <input class = "form-control"
                       id = "weight"
                       name = "weight"
                       placeholder = "Weight - positive or negative number"
                       type = "number"
                       v-model = "answer.weight">
                <span class = "help-block"
                      v-if = "answer.errors.has('weight')"
                      v-text = "answer.errors.get('weight')">
                    </span>
                </input>
            </div>
            <editable label = "WEIGHT FORMULA" :content = "answer.weight_formula"
                      @update = "answer.weight_formula = $event"
                      help_text = "Weight formula. use @ to select keys, or leave empty."></editable>

            <editable label = "KEYS FOR SHOWING ANSWER" :content = "answer.keys_for_showing_answer"
                      @update = "answer.keys_for_showing_answer = $event"
                      help_text = "Condition for showing this answer. use @ to select keys, or leave empty"></editable>

            <editable label = "KEYS FOR SHOWING ANSWER TEACH" :content = "answer.keys_for_showing_answer_teach"
                      @update = "answer.keys_for_showing_answer_teach = $event"
                      help_text = "Condition for showing this answer in teach. use @ to select keys, or leave empty"></editable>

            <div class = "col-md-12" style = "padding: 0;">
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':answer.errors.has('force_score')}"
                     style = "padding-left: 0;">
                    <label for = "force_score">
                        FORCE SCORE
                    </label>
                    <input class = "form-control" id = "force_score" name = "force_score"
                           placeholder = "Force score - positive or negative number" type = "number"
                           v-model = "answer.force_score">
                    <span class = "help-block" v-if = "answer.errors.has('force_score')"
                          v-text = "answer.errors.get('force_score')">
                        </span>
                    </input>
                </div>
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':answer.errors.has('vas_step')}"
                     style = "padding-right: 0;">
                    <label for = "vas_step">
                        VAS STEP
                    </label>
                    <input class = "form-control"
                           id = "vas_step"
                           name = "vas_step"
                           placeholder = "VAS step"
                           type = "text"
                           v-model = "answer.vas_step">
                    <span class = "help-block" v-if = "answer.errors.has('vas_step')"
                          v-text = "answer.errors.get('vas_step')">
                        </span>
                    </input>
                </div>
            </div>
            <div class = "col-md-12" style = "padding: 0;">
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':answer.errors.has('vas_min')}"
                     style = "padding-left: 0;">
                    <label for = "vas_min">
                        VAS MIN SCORE
                    </label>
                    <input class = "form-control"
                           id = "vas_min"
                           name = "vas_min"
                           placeholder = "VAS min score"
                           type = "text"
                           v-model = "answer.vas_min">
                    <span class = "help-block" v-if = "answer.errors.has('vas_min')"
                          v-text = "answer.errors.get('vas_min')">
                        </span>
                    </input>
                </div>
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':answer.errors.has('vas_max')}"
                     style = "padding-right: 0;">
                    <label for = "vas_max">
                        VAS MAX SCORE
                    </label>
                    <input class = "form-control"
                           id = "vas_max"
                           name = "vas_max"
                           placeholder = "VAS max score"
                           type = "text"
                           v-model = "answer.vas_max">
                    <span class = "help-block" v-if = "answer.errors.has('vas_max')"
                          v-text = "answer.errors.get('vas_max')">
                        </span>
                    </input>
                </div>
            </div>
            <div class = "col-md-12" style = "padding: 0;">
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':answer.errors.has('vas_min_display')}"
                     style = "padding-left: 0;">
                    <label for = "vas_min_display">
                        VAS MIN DISPLAY VALUE
                    </label>
                    <input class = "form-control" id = "vas_min_display" name = "vas_min_display"
                           placeholder = "VAS min display score" type = "text" v-model = "answer.vas_min_display">
                    <span class = "help-block" v-if = "answer.errors.has('vas_min_display')"
                          v-text = "answer.errors.get('vas_min_display')">
                        </span>
                    </input>
                </div>
                <div :class = "{'form-group':true, 'col-md-6':true, 'has-error':answer.errors.has('vas_max_display')}"
                     style = "padding-right: 0;">
                    <label for = "vas_max_display">
                        VAS MAX DISPLAY VALUE
                    </label>
                    <input class = "form-control" id = "vas_max_display" name = "vas_max_display"
                           placeholder = "VAS max display score" type = "text" v-model = "answer.vas_max_display">
                    <span class = "help-block" v-if = "answer.errors.has('vas_max_display')"
                          v-text = "answer.errors.get('vas_max_display')">
                        </span>
                    </input>
                </div>
            </div>
            <div :class = "{'form-group':true, 'has-error':answer.errors.has('vas_explanation')}">
                <label for = "vas_explanation">
                    VAS EXPLANATION
                </label>
                <input class = "form-control" id = "vas_explanation" name = "vas_explanation"
                       placeholder = "VAS explanation text" type = "text" v-model = "answer.vas_explanation">
                <span class = "help-block" v-if = "answer.errors.has('vas_explanation')"
                      v-text = "answer.errors.get('vas_explanation')">
                    </span>
                </input>
            </div>

            <editable label = "VAS AUTO ANSWER FORMULA" :content = "answer.vas_auto_answer_formula"
                      @update = "answer.vas_auto_answer_formula = $event"
                      help_text = "VAS auto answer formula. use @ to select keys, or leave empty"></editable>


            <editable label = "AUTO KEYS" :content = "answer.auto_keys"
                      @update = "answer.auto_keys = $event"
                      help_text = "Auto keys condition for triggering this answer in automatic question. Use @ to select keys."></editable>

            <div :class = "{'form-group':true, 'has-error':answer.errors.has('priority')}">
                <label for = "priority">
                    PRIORITY
                </label>
                <input class = "form-control"
                       id = "priority"
                       name = "priority"
                       placeholder = "Priority - number. higher shows first"
                       type = "number"
                       v-model = "answer.priority">
                <span class = "help-block" v-if = "answer.errors.has('priority')"
                      v-text = "answer.errors.get('priority')">
                    </span>
                </input>
            </div>

            <editable label = "KEYS FOR END TEST" :content = "answer.keys_for_end_test"
                      @update = "answer.keys_for_end_test = $event"
                      help_text = "Required key condition for ENDING the test. Use @ to select keys."></editable>

            <div :class = "{'form-group':true, 'has-error':answer.errors.has('end_test_text')}">
                <label for = "end_test_text">
                    END TEST TEXT
                </label>
                <input class = "form-control" id = "end_test_text" name = "end_test_text" placeholder = "End test text"
                       type = "text" v-model = "answer.end_test_text">
                <span class = "help-block" v-if = "answer.errors.has('end_test_text')"
                      v-text = "answer.errors.get('end_test_text')">
                    </span>
                </input>
            </div>
            <input type = "submit" class = "btn btn-sm btn-success" :disabled = "answer.errors.any()" value = "SAVE">
        </form>
    </div>
</div>