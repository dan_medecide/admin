<div class = "panel-heading clearfix handHover"
     @click = "question.display = !question.display, setDisplay(question.id)">
    <h2 class = "panel-title pull-left question-title" style = "padding-top: 7.5px;">
        <span class = "badge pull-left">#@{{question.id}}</span>
        &nbsp;&nbsp;@{{question.title_doctor}}</h2>
    <div class = "pull-right">
        <span v-if = "question.is_auto" class = "label label-primary">AUTO</span>
        <Popover v-if = "question.vas_min != 0 || question.vas_min_display != 0 || question.vas_step != 0" title = "VAS"
                 :trigger = "triggerPopover">
            <span data-role = "trigger" class = "label label-primary">VAS</span>
            <div slot = "popover">
                <p>@{{ question.vas_min }} - @{{ question.vas_max }} <br> (Display: @{{question.vas_min_display}} -
                   @{{question.vas_max_display}} | Step: @{{question.vas_step}})</p>
            </div>
        </Popover>
        <Popover v-if = "question.tag" title = "Required Key"
                 :trigger = "triggerPopover">
            <span data-role = "trigger" class = "label label-primary">KEY</span>
            <div slot = "popover">
                <p>@{{ question.tag }}</p>
            </div>
        </Popover>
        <Popover v-if = "question.question_tag" :trigger = "triggerPopover"
                 title = "Question Key">
            <span v-if = "question.question_tag" data-role = "trigger" class = "label label-primary">Q.KEY | @{{ question.question_tag }}</span>
            <div slot = "popover">
                <p>@{{ question.question_tag }}</p>
            </div>
        </Popover>
        <Popover v-if = "question.question_type" :trigger = "triggerPopover"
                 title = "Question Type">
            <span data-role = "trigger" class = "label label-primary">Q.TYPE | @{{ question.question_type }}</span>
            <div slot = "popover">
                <p>@{{ question.question_type }}</p>
            </div>
        </Popover>
        <Popover v-if = "question.answers_type" :trigger = "triggerPopover"
                 title = "Answers Type">
            <span data-role = "trigger" class = "label label-primary">A.TYPE | @{{question.answers_type }}</span>
            <div slot = "popover">
                <p>@{{question.answers_type }}</p>
            </div>
        </Popover>
    </div>
</div>