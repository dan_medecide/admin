<transition name = "fade">
    {{--    @include('questions.edit_question')--}}
    <edit-question ref = "editQuestion" v-show = "editQuestionForm"></edit-question>
</transition>
<transition name = "fade">
    {{--    @include('questions.new_question')--}}
    <new-question ref = "newQuestion" v-show = "newQuestionForm"></new-question>
</transition>
<transition name = "fade">
    {{--@include('questions.new_answer')--}}
    <new-answer ref = "newAnswer" v-show = "newAnswerForm"></new-answer>
</transition>
<transition name = "fade">
    {{--@include('questions.edit_answer')--}}
    <edit-answer ref = "editAnswer" v-show = "editAnswerForm"></edit-answer>
</transition>
