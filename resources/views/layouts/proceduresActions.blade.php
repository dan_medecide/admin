 
                          
                                <div class="btn-group" role="group">
                                    <a class="btn btn-default"
                                      href="{{route('procedure-scores.index', $procedure->id)}}"
                                       data-toggle="tooltip" title="Scores"><i class="fas fa-signal"></i>
                                    </a>
                                    <a class="btn btn-default"
                                       href="{{url('questions/' . $procedure->id)}}"
                                       data-toggle="tooltip" ti tle="@lang('Questions Dev')"><i class="fas fa-code"></i>
                                    </a>
                                    @if($procedure->is_ready == 0)
                                        <button class="btn btn-default"
                                          onclick="$('form#mark-ready-form').attr('action','../ready-procedures/{{$procedure->id}}').submit();"
                                                data-toggle="tooltip" title="@lang('Mark as ready')">
                                            <i class="far fa-thumbs-up"></i>
                                        </button>
                                    @else
                                        <button class="btn btn-default"
                                                onclick="$('form#mark-ready-form').attr('action','../ready-procedures/{{$procedure->id}}').submit();"
                                                data-toggle="tooltip" title="@lang('Mark as ready')">
                                            <i class="far fa-thumbs-down"></i>
                                        </button>
                                    @endif
                                    {{--<a class = "btn btn-default"--}}
                                    {{--href = "{{getCoreAppLink('/admin_question/'.$procedure->id)}}"--}}
                                    {{--data-toggle = "tooltip" title = "@lang('Questions')"><i class = "fa fa-question"--}}
                                    {{--aria-hidden = "true"></i></a>--}}
                                    <a class="btn btn-default"
                                        href="{{route('procedures.edit',$procedure->id)}}"
                                        data-toggle="tooltip" title="@lang('Edit')"><i class="far fa-edit"></i></a>
                                    <a class="btn btn-default  "
                                        href="{{route('procedures.teach', $procedure->id)}}"
                                        target="_blank" 
                                        data-toggle="tooltip" title="@lang('Teach')"><i
                                        class="fas fa-graduation-cap"></i></a>
                                    <a class="btn btn-default teach-{{$procedure->title}}"  
                                        href="{{route('procedures.beta-teach', $procedure->id)}}"
                                        target="_blank"  
                                        data-toggle="tooltip" title="@lang('Beta-Teach')"><i
                                        class="fas fa-chess-rook"></i></a>
                                    <a class="btn btn-default"
                                        href="{{getCoreAppLink('/feedback/' . $procedure->id)}}"
                                        data-toggle="tooltip" title="@lang('Feedback')"><i
                                        class="far fa-comments"></i></a>
                                    {{--<a class = "btn btn-default"--}}
                                    {{--href = "{{getCoreAppLink('/proc_test/' . $procedure->id)}}"--}}
                                    {{--data-toggle = "tooltip" title = "@lang('Run')"><i class="far fa-play-circle"></i></a>--}}
                                    <button token="{{csrf_token()}}"
                                            url="{{route('procedures.duplicate',$procedure->id)}}"
                                            class="btn btn-default duplicate-confirm"
                                            data-toggle="tooltip"
                                            title="@lang('Duplicate')">
                                            <i class="far fa-clone"></i></button>
                                    <button token="{{auth()->user()->token}}"
                                            procedure="{{$procedure->id}}"
                                            auth="{{salt($procedure->id)}}"
                                            class="btn btn-default publish-confirm"
                                            data-toggle="tooltip"
                                            title="@lang('Publish')">
                                            <i class="far fa-paper-plane"></i></button>
                                    <a class="btn btn-default"
                                       href="{{url('conflicts/' . $procedure->id)}}"
                                       data-toggle="tooltip"
                                       title="@lang('Conflicts')"><i class="fas fa-exclamation-triangle"></i>
                                    </a>
                                    <a class="btn btn-default"
                                       href="{{route('procedures.run', $procedure->id)}}"
                                       target="_blank"
                                       data-toggle="tooltip"
                                       title="Run procedure">
                                        <i class="far fa-share-square"></i>
                                    </a>
                                </div>
                          
              
            <div>
                {!! Form::open(['url' => '','id' => 'mark-ready-form','method' => 'put']) !!}
                {!! Form::close() !!}
            </div>
        
    <!-- /page content -->
 
 
 
     
 
