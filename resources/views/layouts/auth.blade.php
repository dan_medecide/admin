<!DOCTYPE html>
<html lang = "en">
<head>
    <meta http-equiv = "Content-Type" content = "text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE=edge">
    <meta name = "viewport" content = "width=device-width, initial-scale=1">

    <title>Log In</title>

    <!-- Bootstrap -->
    <link href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
    <!-- Font Awesome -->
    <link href = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel = "stylesheet">
    <!-- NProgress -->
    <link href = "https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.css" rel = "stylesheet">
    <!-- Animate.css -->
    <link href = "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" rel = "stylesheet">
    <!-- Custom Theme Style -->
    <link href = "{{asset('css/custom.min.css')}}" rel = "stylesheet">
    <style>
        .login_content form div a {
            font-size: 1.1em;
            margin: 0;
       
        }
        .login_content form input[type=password] {
    display:inline:block;
    
        }
        .hovereye {
}
.hovereye:hover {
    cursor:pointer;
    color: rgb(60, 66, 63);
     margin-right: -1px;
     margin-top: 1px;
    font-size: 16px !important;
}
.hovereye:active {
    color: black;
   
}
    </style>
</head>

<body class = "login">
<div>
    <a class = "hiddenanchor" id = "signup"></a>
    <a class = "hiddenanchor" id = "signin"></a>

    <div class = "login_wrapper">
        <div class = "animate form login_form">
            <section class = "login_content">
                @yield('content')
                <div class = "clearfix"></div>

                <div class = "separator">
                    <div class = "clearfix"></div>
                    <br />

                    <div>
                        <img class = "side-menu-logo"
                             style = "height: 76px;margin-top:0;margin-left:0"
                             src = "{{asset('images/serenus-logo.png')}}"><br><br>
                             <!-- logo-black.png
                        <h5>Medical Decisions. Reinvented™.</h5> -->
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
  @yield('login')
</body>
</html>

