@if(Session::has('message'))
<div id="myAlert" class="alert alert-success alert-dismissible fade in out" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Well done!</strong> {{ Session::get('message') }}
</div>
@endif
