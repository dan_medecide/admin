 
 
    <!-- page content -->
     
        <div class="container float-right">
            <table> 
                <thead>
                <tr>
            
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center noExport"></th>
                </tr>
                </tfoot>
                <tbody>
               
                        <tr>
                     
                            <td>
                                <div class="btn-group" role="group">
                                    <a class="btn btn-default"
                                      href="{{route('procedure-scores.index', $proc_id)}}"
                                       data-toggle="tooltip" title="Scores"><i class="fas fa-signal"></i>
                                    </a>
                                    <a class="btn btn-default"
                                       href="{{url('questions/' . $proc_id)}}"
                                       data-toggle="tooltip" title="@lang('Questions Dev')"><i class="fas fa-code"></i>
                                    </a>
                                    @if($proc_is_ready == 0)
                                        <button class="btn btn-default"
                                          onclick="$('form#mark-ready-form').attr('action','../ready-procedures/{{$proc_id}}').submit();"
                                                data-toggle="tooltip" title="@lang('Mark as ready')">
                                            <i class="far fa-thumbs-up"></i>
                                        </button>
                                    @else
                                        <button class="btn btn-default"
                                                onclick="$('form#mark-ready-form').attr('action','../ready-procedures/{{$proc_id}}').submit();"
                                                data-toggle="tooltip" title="@lang('Mark as ready')">
                                            <i class="far fa-thumbs-down"></i>
                                        </button>
                                    @endif
                                    {{--<a class = "btn btn-default"--}}
                                    {{--href = "{{getCoreAppLink('/admin_question/'.$proc_id)}}"--}}
                                    {{--data-toggle = "tooltip" title = "@lang('Questions')"><i class = "fa fa-question"--}}
                                    {{--aria-hidden = "true"></i></a>--}}
                                    <a class="btn btn-default"
                                        href="{{route('procedures.edit',$proc_id)}}"
                                        data-toggle="tooltip" title="@lang('Edit')"><i class="far fa-edit"></i></a>
                                    <a class="btn btn-default  "
                                        href="{{route('procedures.teach', $proc_id)}}"
                                        target="_blank" 
                                        data-toggle="tooltip" title="@lang('Teach')"><i
                                        class="fas fa-graduation-cap"></i></a>
                                    <a class="btn btn-default teach-{{$proc_title}}"  
                                        href="{{route('procedures.beta-teach', $proc_id)}}"
                                        target="_blank"  
                                        data-toggle="tooltip" title="@lang('Beta-Teach')"><i
                                        class="fas fa-chess-rook"></i></a>
                                    <a class="btn btn-default"
                                        href="{{getCoreAppLink('/feedback/' . $proc_id)}}"
                                        data-toggle="tooltip" title="@lang('Feedback')"><i
                                        class="far fa-comments"></i></a>
                                    {{--<a class = "btn btn-default"--}}
                                    {{--href = "{{getCoreAppLink('/proc_test/' . $proc_id)}}"--}}
                                    {{--data-toggle = "tooltip" title = "@lang('Run')"><i class="far fa-play-circle"></i></a>--}}
                                    <button token="{{csrf_token()}}"
                                            url="{{route('procedures.duplicate',$proc_id)}}"
                                            class="btn btn-default duplicate-confirm"
                                            data-toggle="tooltip"
                                            title="@lang('Duplicate')">
                                            <i class="far fa-clone"></i></button>
                                    <button token="{{auth()->user()->token}}"
                                            procedure="{{$proc_id}}"
                                            auth="{{salt($proc_id)}}"
                                            class="btn btn-default publish-confirm"
                                            data-toggle="tooltip"
                                            title="@lang('Publish')">
                                            <i class="far fa-paper-plane"></i></button>
                                    <a class="btn btn-default"
                                       href="{{url('conflicts/' . $proc_id)}}"
                                       data-toggle="tooltip"
                                       title="@lang('Conflicts')"><i class="fas fa-exclamation-triangle"></i>
                                    </a>
                                    <a class="btn btn-default"
                                       href="{{route('procedures.run', $proc_id)}}"
                                       target="_blank"
                                       data-toggle="tooltip"
                                       title="Run procedure">
                                        <i class="far fa-share-square"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                </tbody>
            </table>
            <div>
                {!! Form::open(['url' => '','id' => 'mark-ready-form','method' => 'put']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    <!-- /page content -->
 
 
 
     
 
