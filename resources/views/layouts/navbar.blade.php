<!-- top navigation -->
<div class = "top_nav">
    <div class = "nav_menu">
        <nav>
            <div class = "nav toggle">
                <a id = "menu_toggle"><i class = "fa fa-bars"></i></a>
            </div>
            
            <ul class = "nav navbar-nav navbar-right">
   
                @if($user = Auth::user())
                  
                <li>
                        <a href = "{{ route('logout') }}"
                           onclick = "event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id = "logout-form"
                              action = "{{ route('logout') }}"
                              method = "POST"
                              style = "display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    <li class = "nav-item">
                        <a href = "{{url('users') . '/' . Auth::user()->id}}"
                           class = "nav-item nav-link"><span><i class = "fa fa-user-md fa-2x" aria-hidden = "true"></i>&nbsp;{{$user['title']}} {{$user['first_name']}} {{$user['last_name']}}</span></a>
                    </li>

                @else
                    No User...
                @endif
               
@if(Request::path() == "combinations" )
                            <li style="margin-top: 3px;"> 
        <a  ondragover="allowDrop(event)" ondrop="drop(event)" ondragleave="dragleave(event)"   class="btn btn-default droptarget"
        style="padding:6px 16px 6px 16px" data-toggle="tooltip" title="@lang('View Archive')"
        href="{{route('procedures.index', 'temp=true')}}">
    <i class="fa fa-archive aria-hidden=true" style=" margin-bottom:10px"></i>
</a>
            </li>
@endif

            </ul>
        </nav>
    </div>
    
</div>
<!-- /top navigation -->