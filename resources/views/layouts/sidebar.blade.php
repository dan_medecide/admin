<div class = "col-md-3 left_col menu_fixed">
    <div class = "left_col scroll-view">
        <div class = "navbar nav_title" style = "border: 0;">
        <div class="thumbnail"  id = "seranus-logo">
            <img class = "side-menu-logo img-responsive" src = "{{asset('images/serenus-logo.png')}}">
        </div>
        </div>
        <div class = "clearfix"></div>
        <div id = "sidebar-menu" class = "main_menu_side hidden-print main_menu" >
            <div class = "menu_section">
                <ul class = "nav side-menu">
                    @if($user->hasRole('admin'))
                        <li>
                            <a href = "{{url('')}}"><i class = "fas fa-home"></i> Home</a>
                        </li>
                        <li><a><i class="fas fa-cog"></i> admin</a>
                            <ul class = "nav child_menu">
                                <li><a href = "{{url('procedures')}}">Procedures</a></li>
                                <li><a href = "{{url('users')}}">Users</a></li>
                                <li><a href = "{{url('roles')}}">Roles</a></li>
                                <li><a href = "{{url('categories')}}">Categories</a></li>
                                <li><a href = "{{url('decisions')}}">Decisions</a></li>
                                <li><a href = "{{url('statistics')}}">Statistics</a></li>
                                <li><a href = "{{url('client-params')}}">Clients Parameters</a></li>
                                <li><a href = "{{url('notes')}}">Notes</a></li>
                                <li><a href = "{{url('reports')}}">Reports</a></li>
                                <li><a href = "{{route('clients.index')}}">Clients</a></li>
                                <li><a href = "{{route('combinations.index')}}">Combinations</a></li>
                                <li><a href = "{{route('params.index')}}">Answer Parameters</a></li>
                                <li><a href = "#">v3619A</a></li>
                            </ul>
                        </li>
                    @endif
                    @if($user->hasRole('supervisor') and !empty($user->client->id))
                        <li>
                            <a href = "{{route('home')}}"><i class="fas fa-tachometer-alt"> </i> Dashboard</a>
                        </li>
                         <li style="margin-left:6px">
                            <a type="button" 
                                href="{{route('procedures_page.run')}}"
                                target="_blank"  
                                data-toggle="tooltip" title="@lang('SerenusAI`s Procedures Page')">
                                <i class="fas fa-info" aria-hidden="true"></i>
                                <span style="margin-left:3px"> Run SerenusAI</span>
                            </a>
                        </li>
                          @if(1==0)
                        <li>
                            <a href = "{{route('client-reports', $user->client->id)}}">
                                <i class = "fa fa-print" aria-hidden = "true"></i>Reports</a>
                            @endif             
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
