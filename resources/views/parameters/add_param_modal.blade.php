<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Add Parameter</h4>
</div>
<div class = "modal-body">
    {!! Form::open(['route' => 'client-params.store','class' => 'bootstrap-modal-form']) !!}
    <div class = "form-group">
        {!! Form::label('param_name','Name') !!}
        {!! Form::text('param_name',null,['class' => 'form-control']) !!}
    </div>

    <div class = "form-group">
        {!! Form::label('param_value','Value') !!}
        {!! Form::text('param_value',null,['class' => 'form-control']) !!}
    </div>

    <div class = "form-group">
        {!! Form::label('client_id','Client') !!}
        {!!  Form::select('client_id', $clients_id, null, ['placeholder' => 'choose client\'s id...','class' => 'form-control']) !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "New parameter has been created successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Create Parameter
    </button>
    {{--{!! BootForm::submit('Submit')->addClass('btn-primary') !!}--}}
    {!! Form::close() !!}
</div>