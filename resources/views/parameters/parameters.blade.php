@extends('master')

@section('title', 'Decisions')

@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table text-center parameters-table table-striped table-bordered nowrap"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">ID</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Value</th>
                    <th class = "text-center">Client ID</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">ID</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Value</th>
                    <th class = "text-center">Client ID</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </tfoot>
                <tbody>
                @if(isset($beta_params))
                    @foreach($beta_params as $param)
                        <tr>
                            <td>{{$param->id}}</td>
                            <td>{{$param->param_name}}</td>
                            <td>{{$param->param_value}}</td>
                            <td>{{$param->client_id}}</td>
                            <td>
                                <div class = "btn-group btn-group-sm">
                                    <button style = "color: #73879C"
                                            class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{ '/client-params/' . $param->id . '/edit'  }}"><i
                                                class = "fa fa-pencil fa-fw" aria-hidden = "true"></i> Edit
                                    </button>
                                    <button style = "color: #73879C" token = "{{csrf_token()}}"
                                            url = "{{'/client-params/' . $param->id}}" href = "#"
                                            class = "btn btn-default delete-confirm">
                                        <i class = "fa fa-trash-o"
                                           aria-hidden = "true"></i> Delete
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <br>
        <div class = "container">
            <table class = "table text-center parameters-table table-striped table-bordered nowrap"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">ID</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Value</th>
                    <th class = "text-center">Client ID</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">ID</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Value</th>
                    <th class = "text-center">Client ID</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </tfoot>
                <tbody>
                @if(isset($production_params))
                    @foreach($production_params as $param)
                        <tr>
                            <td>{{$param->id}}</td>
                            <td>{{$param->param_name}}</td>
                            <td>{{$param->param_value}}</td>
                            <td>{{$param->client_id}}</td>
                            <td>
                                <button style = "color: #73879C"
                                        class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                        data-url = "{{ '/client-params/' . $param->id . '/edit'  }}"><i
                                            class = "fa fa-pencil fa-fw" aria-hidden = "true"></i> Edit
                                </button>
                                <a style = "color: #73879C" token = "{{csrf_token()}}"
                                   url = "{{'/client-params/' . $param->id}}" href = "#"
                                   class = "btn btn-default delete-confirm">
                                    <i class = "fa fa-trash-o"
                                       aria-hidden = "true"></i> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- /page content -->
    @include('components.modal')
@endsection

@section('script')
    <script src = "{{asset('js/parameters.js')}}"></script>
@stop