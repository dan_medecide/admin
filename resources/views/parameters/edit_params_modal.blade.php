<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Parameter</h4>
</div>
<div class="modal-body">
    {!! Form::model($param,['route' => ['client-params.update',12],'class' => 'bootstrap-modal-form','method' => 'put']) !!}
    <div class = "form-group">
        {!! Form::label('param_name','Name') !!}
        {!! Form::text('param_name',null,['class' => 'form-control']) !!}
    </div>

    <div class = "form-group">
        {!! Form::label('param_value','Value') !!}
        {!! Form::text('param_value',null,['class' => 'form-control']) !!}
    </div>

    <div class = "form-group">
        {!! Form::label('client_id','Client') !!}
        {!!  Form::select('client_id', $clients_id, $param->client_id, ['placeholder' => 'choose client\'s id...','class' => 'form-control']) !!}
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" title="Congratulations!" data-content="This parameter has been updated successfully."
            data-animation="true"
            data-placement="left" class="btn btn-primary bootstrap-modal-submit">Update Parameter
    </button>
    {!! Form::close() !!}
</div>