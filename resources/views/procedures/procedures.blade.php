@extends('master')

@section('title', 'Procedures')

@section('content')
<!-- page content -->

<div class="right_col" role="main">
<div class="container">

@component('components.procedure_archive_confirm', [ 'procedures' => $procedures])
@endcomponent
<input type="checkbox" id="toggleHorizon" data-on="Horizon On" data-off="Horrizon Off" data-toggle="toggle" data-onstyle="primary" data-width="130" data-height="46.8">
<input type="checkbox" id="toggleHartford" data-on="Hartford On" data-off="Hartford Off" data-toggle="toggle" data-onstyle="warning" data-width="130" data-height="46.8">
@if (request()->has('temp'))
    <a class="btn btn-default to-procedures" data-toggle="tooltip" title="@lang('Active Procedures')"
        href="{{route('procedures.index')}}">
        <i class="fab fa-pinterest-p"></i>
    </a>
@else
   <a  ondragover="allowDrop(event)" ondrop="drop(event)" ondragleave="dragleave(event)"   class="btn btn-default droptarget"
        data-toggle="tooltip" title="@lang('View Archive')"
        href="{{route('procedures.index', 'temp=true')}}">
    <i class="fa fa-archive aria-hidden="true" style=" margin-bottom:10px"></i>
</a>
@endif
<div style="cursor:pointer">
Toggle column: <a class="toggle-vis" data-column="0">ID</a>
 - <a class="toggle-vis" data-column="1">Priority</a> 
 - <a class="toggle-vis" data-column="2">Title</a> 
 - <a class="toggle-vis" data-column="3">Abbreviation</a>
  - <a class="toggle-vis" data-column="4">Category </a>
   - <a class="toggle-vis" data-column="5">Version</a>
   - <a class="toggle-vis" data-column="6">Ready</a>
   - <a class="toggle-vis" data-column="7">Action</a>
				</div>
<table class="table procedures-table table-condensed table-striped table-bordered text-center"
cellspacing="0" width="100%" style="display: none">
<thead>
<tr >
<th class="text-center idTd">@lang('ID')</th>
<th class="text-center">@lang('Priority')</th>
<th class="text-center">@lang('Title')</th>
<th class="text-center">@lang('Abbreviation')</th>
<th class="text-center">@lang('Category')</th>
<th class="text-center">@lang('Version')</th>
<th class="text-center">@lang('Ready')</th>
<th class="text-center noExport">@lang('Action')</th>
</tr>
</thead>
 <tfoot>
<tr>
<th class="text-center"></th>
<th class="text-center"></th>
<th class="text-center"></th>
<th class="text-center"></th>
<th class="text-center"></th>
    <th class="text-center"></th>
<th class="text-center"></th>
<th class="text-center noExport"></th>
</tr>
</tfoot>
<tbody>
   
@if(isset($procedures))
@foreach($procedures as $key=>$procedure)

<tr id="{{$procedure->id}}" draggable="true" ondragstart="drag(event)">
        <td class="archive idTd" id="id{{$procedure->id}}">{{$procedure->id}}</td>
<td>{{$procedure->priority}}</td>     
<td> {{str_limit($procedure->title,40,'...')}}</td>
<td>{{$procedure->abbreviation}}</td>
<td>{{$procedure->category->title}}</td>
<td>{{$procedure->version}}</td>
<td>{{$procedure->is_ready}}</td>
<td>
       @component('layouts.proceduresActions', compact('procedure') ) @endcomponent
 
</td>
</tr>
@endforeach
@endif
</tbody>
</table>
</div>
</div>
<div>
{!! Form::open(['url' => '','id' => 'mark-ready-form','method' => 'put']) !!}
{!! Form::close() !!}

</div>

<!-- /page content -->
<?php $x=rand(1,10000)?>
<script>
const addProcedureBtnText = '@lang('NEW PROCEDURE')';
const searchFieldText = '@lang('Search')';
const infoText = '@lang('Showing _START_ to _END_ of _TOTAL_ entries')';
const nextText = '@lang('Next')';
const PreviousText = '@lang('Previous')';
</script>
@endsection
@section('script')
<script src="{{asset('/js/procedures.js?'.$x)}}"></script>
@stop

