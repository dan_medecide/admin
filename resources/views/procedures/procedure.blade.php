@extends('master')

@section('title', 'Categories')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Procedures</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <h1>{{$procedure->title}}</h1>
                        <hr>
                        <h4><b>Description:</b></h4>
                        <p>{{$procedure->description}}</p><br>
                        <h4><b>Category ID: </b>{{$procedure->cat_id}}</h4><br>
                        <h4><b>Priority: </b>{{$procedure->priority}}</h4><br>
                        <h4><b>Short Description:</b></h4>
                        <p>{{$procedure->short_desc}}</p><br>
                        <h4><b>General Text:</b></h4>
                        <p>{{$procedure->general_text}}</p><br>
                        <h4><b>Indications Text:</b></h4>
                        <p>{{$procedure->indications_text}}</p><br>
                        <h4><b>Recovery Text:</b></h4>
                        <p>{{$procedure->recovery_text}}</p><br>
                        <h4><b>Risks Text:</b></h4>
                        <p>{{$procedure->risks_text}}</p><br>
                        <h4><b>Alternative Text:</b></h4>
                        <p>{{$procedure->alternative_text}}</p><br>
                        <h4><b>Preliminary: </b>{{$procedure->is_preliminary}}</h4><br>
                        <h4><b>Version: </b>{{$procedure->version}}</h4><br>
                        <h4><b>ICD Code: </b>{{$procedure->icd_code}}</h4><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


@endsection

@section('script')
    <!-- FastClick -->
    <script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
    <!-- Datatables -->
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
@endsection