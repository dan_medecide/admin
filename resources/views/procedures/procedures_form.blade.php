@extends('master')

@section('meta')
    <!-- CSRF Token -->
    <meta name = "csrf-token" content = "{{ csrf_token() }}">
@endsection

@section('title', 'Procedures')

@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "">
            <div class = "clearfix"></div>

            <div class = "row">
                <div class = "col-md-12 col-sm-12 col-xs-12">
                    <div class = "x_panel">
                        <div class = "x_title">
                            @if(isset($procedure))
                                <h2>Edit Procedure</h2>
                            @else
                                <h2>Add Procedure</h2>
                            @endif
                            <div class = "clearfix"></div>
                        </div>
                        <div class = "x_content">

                            @if(isset($procedure))
                                {!! Form::model($procedure, ['route' => ['procedures.update', $procedure->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
                            @else
                                {!! Form::open(['route' => 'procedures.store']) !!}
                            @endif
                            {!!Form::token()!!}

                            <div class = "col-md-6">
                                <div class = "form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    {!! Form::label('title', 'Procedure title*') !!}
                                    {!! Form::text('title', old('title'), $attributes = ['required autofocus', 'class' => 'form-control']) !!}

                                    @if ($errors->has('title'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('abbreviation') ? ' has-error' : '' }}">
                                    {!! Form::label('abbreviation', 'Procedure Abbreviation') !!}
                                    {!! Form::text('abbreviation', old('abbreviation'), $attributes = ['autofocus', 'class' => 'form-control']) !!}

                                    @if ($errors->has('abbreviation'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('abbreviation') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('cat_id') ? ' has-error' : '' }}">
                                    {!! Form::label('cat_id', 'Category ID*') !!}
                                    @if(isset($categories))
                                        <select name = "cat_id" class = "form-control">
                                            @foreach($categories as $category)
                                                <option @if(isset($procedure) && $category->id == $procedure->category->id) selected
                                                        @endif value = "{{$category->id}}">{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                    @if ($errors->has('cat_id'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('cat_id') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
                                    {!! Form::label('priority', 'Procedure Priority') !!}
                                    {!! Form::number('priority', old('priority'), $attributes = ['autofocus', 'class' => 'form-control']) !!}

                                    @if ($errors->has('priority'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('is_preliminary') ? ' has-error' : '' }}">
                                    {!! Form::label('is_preliminary', 'preliminary') !!}
                                    {!! Form::select('is_preliminary', ['1' => 'Yes', '0' => 'No'], old('is_preliminary'), $attributes = ['autofocus', 'class' => 'form-control']) !!}
                                    @if ($errors->has('is_preliminary'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('is_preliminary') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('version') ? ' has-error' : '' }}">
                                    {!! Form::label('version', 'Version*') !!}
                                    {!! Form::number('version', old('version'), $attributes = ['autofocus required', 'class' => 'form-control', 'step'=>'any']) !!}
                                    @if ($errors->has('version'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('version') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('icd_code') ? ' has-error' : '' }}">
                                    {!! Form::label('icd_code', 'ICD Code') !!}
                                    {!! Form::text('icd_code', old('icd_code'), $attributes = ['autofocus', 'class' => 'form-control']) !!}
                                    @if ($errors->has('icd_code'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('icd_code') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    {!! Form::label('description', 'Procedure description') !!}
                                    {!! Form::textarea('description', old('description'), $attributes = ['autofocus', 'class' => 'form-control']) !!}

                                    @if ($errors->has('description'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('short_desc') ? ' has-error' : '' }}">
                                    {!! Form::label('short_desc', 'Short Description') !!}
                                    {!! Form::textarea('short_desc', old('short_desc'), $attributes = ['autofocus', 'class' => 'form-control']) !!}

                                    @if ($errors->has('short_desc'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('short_desc') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <br>
                                <div class = "form-group">
                                    @if(isset($procedure))
                                        {!! Form::submit('Update Procedure', ['class' => 'btn btn-primary']) !!}
                                    @else
                                        {!! Form::submit('Create Procedure', ['class' => 'btn btn-primary']) !!}
                                    @endif
                                    <a href = "{{url('procedures')}}" class = "btn btn-default" type = "button">Back</a>
                                </div>
                            </div>

                            <div class = "col-md-6">
                                <div class = "form-group{{ $errors->has('general_text') ? ' has-error' : '' }}">
                                    {!! Form::label('general_text', 'General Text') !!}
                                    {!! Form::textarea('general_text', old('general_text'), $attributes = ['autofocus', 'class' => 'form-control']) !!}
                                    @if ($errors->has('general_text'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('general_text') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('indications_text') ? ' has-error' : '' }}">
                                    {!! Form::label('indications_text', 'Indications Text') !!}
                                    {!! Form::textarea('indications_text', old('indications_text'), $attributes = ['autofocus', 'class' => 'form-control']) !!}
                                    @if ($errors->has('indications_text'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('indications_text') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('recovery_text') ? ' has-error' : '' }}">
                                    {!! Form::label('recovery_text', 'Recovery Text') !!}
                                    {!! Form::textarea('recovery_text', old('recovery_text'), $attributes = ['autofocus', 'class' => 'form-control']) !!}
                                    @if ($errors->has('recovery_text'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('recovery_text') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('risks_text') ? ' has-error' : '' }}">
                                    {!! Form::label('risks_text', 'Risks Text') !!}
                                    {!! Form::textarea('risks_text', old('risks_text'), $attributes = ['autofocus', 'class' => 'form-control']) !!}
                                    @if ($errors->has('risks_text'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('risks_text') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class = "form-group{{ $errors->has('alternative_text') ? ' has-error' : '' }}">
                                    {!! Form::label('alternative_text', 'Alternative Text') !!}
                                    {!! Form::textarea('alternative_text', old('alternative_text'), $attributes = ['autofocus', 'class' => 'form-control']) !!}
                                    @if ($errors->has('alternative_text'))
                                        <span class = "help-block">
                                        <strong>{{ $errors->first('alternative_text') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('script')
    <!-- Datatables -->
    <script src = "{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src = "{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src = "{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src = "{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>
    <script src = "{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src = "{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src = "{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src = "{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src = "{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
@endsection

