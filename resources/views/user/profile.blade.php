@extends('master')
@section('meta')
{!! Charts::assets() !!}
@endsection
@section('title', 'User Profile')
@section('content')
<div class="right_col" role="main">
   @include('layouts.message')
   <div class="page-header" >
      <h2>{{$user->title}} {{$user->first_name}} {{$user->last_name}}
         <br>
         <small>
         <i class="fa fa-external-link user-profile-icon"></i> {{$user->email}}
         </small>
         <br>
         <small>
         <i class="fa fa-tags"
            aria-hidden="true"></i> {{$user->category()->value('title')}}
         </small>
      </h2>
   </div>
   @if($labels_chart and ($role!="supervisor" || auth()->user()->role=="admin"))  
   <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title">User Activity Report</h3>
            </div>
            <div class="panel-body">
               {!! $labels_chart->render() !!}
            </div>
         </div>
      </div>
   </div>
   @endif
   <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title">Reports:</h3>
            </div>
            <!-- <input type="checkbox" id="toggleHorizon" data-on="Approval" data-off="Approval Off" data-toggle="toggle" data-onstyle="primary" data-width="130" data-height="36.8" > -->
         </div>
          
         <div class = "page-header" style="margin-left:10px">
            <h1>
               SerenusAI™ Analytics Dashboard
            </h1>
         </div>
         <a target="_blank" style="color: #73879C;margin-left:15px" type="button"
            class="btn btn-default"
            href="{{route('procedures_page.run')}}"
            target="_blank"  
            data-toggle="tooltip" title="@lang('SerenusAI`s Procedures Page')">
         <i class="fas fa-info" aria-hidden="true"></i>
         Run SerenusAI 
         </a>
         @if(auth()->user()->roles[0]->name=="supervisor")
         <div class="row">
            <div class="col-sm-4 col-sm-offset-4 pull-right" style="margin:20px">
               <canvas id="myChart"  width="50" height="70"></canvas>
            </div>
         </div>
         @endif
         <div class="table-responsive" style="margin:10px">
            <table class="table profiles-table  table-condensed  table-bordered text-center " cellspacing="0" width="100%">
               <thead>
                  <tr class="padd-left" >
                     @if( auth()->user()->roles[0]->name !="supervisor")
                     <th class="text-center" >Teach:  </th>
                     @else   
                     <th class=" text-center"  > Case#:</th>
                     @endif
                     <th class=" text-center"  > User Name:</th>
                     <th class=" text-center"  > Date/Time: </th>
                     @if( auth()->user()->roles[0]->name !="supervisor")  
                     <th class=" text-center"  > Completed:</th>
                     @endif
                     <th class=" text-center"  > Procedure:</th>
                     <th class=" text-center"  > Supervisor's Decision :</th>
                     <th class="text-center noExport" >SerenusAI's Decision:</th>
                     <th class=" text-center"  > Actions:</th>
                     @if(auth()->user()->role=="admin")
                     <th class="text-center noExport"> Manual Decision:</th>
                     @endif
                     <th class="text-center" > Notes/Comments:</th>
                  </tr>
               </thead>
               <tbody>
                  @if(isset($teach))  
                  <tr>
                     @foreach($teach as $key => $test)
                     @if($role!="supervisor") 
                     <td style="  vertical-align: middle;" >
                        @if (!empty($test['manualDecision'])) Teach  @else Test @endif
                     </td>
                     @else
                     <td style=" vertical-align: middle;" >
                        {{$test['patient_id']}}
                        </td style=" vertical-align: middle;" >
                        @endif
                     <td style=" vertical-align: middle;" > {{ $test['users_title'] }} {{ $test['first_name'] }}  {{ $test['last_name'] }}</td>
                     @if( auth()->user()->roles[0]->name !="supervisor")                  
                     </td>
                     @endif
                     <td style="vertical-align: middle;" > {{   $test['created_at'] }} </td>
                     @if(auth()->user()->roles[0]->name!="supervisor")
                     <td style=" vertical-align: middle;" >{{!$test['is_done'] ? "No" : "Yes"}}</td>
                     @endif
                     <td style=" vertical-align: middle;"> 
                        @if(isset($test['title'] ))
                        {{$test['proc_id'] }}. {{$test['title'] }} 
                        @endif
                     </td>
                     @component('components.decisions', [ 
                     'supervisor_decision_explanation' => $test['supervisor_decision_explanation'] ,
                     'supervisor_decision' => $test['supervisor_decision'] ,
                     'tests_json' => $test['tests_json'] ,
                     'mappedDecisions' => $mappedDecisions,
                     'is_done'=> $test['is_done'],
                     'decisions' => $decisions,  'test_id'=>$test['test_id']
                     ])
                     @endcomponent
                     @if(auth()->user()->role=="admin") 
                     <td style=" vertical-align: middle;" >
                        @component('components.manual_decisions', ['manual_decisions' => $test['tests_manual_decision']])
                        @endcomponent
                     </td>
                     @endif
                     <td  style="  vertical-align: middle;" >
                        {{$test['notes']}}
                     </td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
   let indicated=notIndicated=0;    
   let isIndicated = 
   	<?php echo json_encode($isIndicated); ?>;
   
       
</script>
<?php $x=rand(1,100)?>
<script src="{{asset('js/approval.js?'.$x)}}"></script>
<script src="{{asset('js/profile.js?x='.$x)}}"></script>
@stop