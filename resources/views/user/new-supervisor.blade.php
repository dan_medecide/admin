@extends('master')

@section('meta')
    {!! Charts::assets() !!}
@endsection

@section('title', 'User Profile')
@section('css')
    

<link rel = "stylesheet" href = "https://colorlib.com/polygon/vendors/nprogress/nprogress.css">
	<link rel = "stylesheet" href = "https://colorlib.com/polygon/vendors/iCheck/skins/flat/green.css">
		<link rel = "stylesheet" href = "https://colorlib.com/polygon/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
			<link rel = "stylesheet" href = "https://colorlib.com/polygon/vendors/jqvmap/dist/jqvmap.min.css">
				<link rel = "stylesheet" href = "https://colorlib.com/polygon/vendors/bootstrap-daterangepicker/daterangepicker.css">
					<link href="https://colorlib.com/polygon/build/css/custom.min.css" rel="stylesheet">

@stop
@section('content')
 

						
						<div class="right_col" role="main">
							<div class="row tile_count">
								<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
									<span class="count_top">
										<i class="fa fa-user"></i> Total Users
									
									</span>
									<div class="count"> {{count($clientUsers)}} </div>
									<!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> -->
								</div>
								<div class="col-md-3  col-sm-4 col-xs-6 tile_stats_count">
									<span class="count_top">
										<i class="fa fa-list-alt"></i> Total Reports
									
									</span>
									<div class="count"> {{count($teach)}}</div>
									<span class="count_bottom">
										<i class="green" style="margin-top:-18px;margin-bottom:-18px;">
											<i class="fa fa-sort-asc"  style="margin-top:-18px;margin-bottom:-18px;"></i>
											<span id="last-7-days"></span>
										</i> From last Week
									
									</span>
								</div>
								<div class="col-md-3  col-sm-4 col-xs-6 tile_stats_count">
									<span class="count_top">
										<span class="glyphicon glyphicon-adjust"></span>
									</i> Total Unfinished Reports
								
								</span>
								<div class="count green unfinished-procedures"></div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
								<span class="count_top">
									<i class="fa fa-bullseye"></i> Total Finished Reports
								
								</span>
								<div id="total-indications-header" class="count"></div>
							</div>
						</div>
						<div class="row x_title">
							<div class="col-md-6">
								<h3>{{  auth()->user()->client->client_name }}  
											
									<small>/ Platform Activity</small>
								</h3>
							</div>
						</div>
						<div class="row"   >
							<br>
								<div class="row"  >
									<div class="col-md-9 col-sm-9 col-xs-12">
										<div class="x_panel tile fixed_height_425"  >
											<!-- <div class="x_title"><h2 style="width:50%">Reports/Per Day</h2><div class="pull-right"></div> -->
											<div class="x_title">
												<h2>Reports/Per Day</h2>
		@component('components.supervisor.navbar_tools') @endcomponent
 		
												<!-- <div class="clearfix"></div></div> -->
												<div class="clearfix"></div>
											</div>
											<div class="x_content" style="margin-bottom:5px;">
												<div id="reportrange" class="pull-right" style=" background: #fff; cursor: pointer; padding:5px  5px 6px 10px; border: 1px solid #ccc">
													<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
													<span>January 5, 2019 - February 3, 2019</span>
													<b class="caret"></b>
												</div>
												<div>
													<!-- <h4>App Usage across versions</h4> -->
													<br>
													</div>
													<div class="dashboard_graph">
														<div id="chart_plot_01" class="demo-placeholder_330" style="padding: 0px; position: relative;" height="400">
															<canvas class="flot-base" width="1161" height="400" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1161px; height: 400px;"></canvas>
											    @foreach ( $teach as $key => $test)
											
															<div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
																<div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
																	<div class="flot-tick-label tickLabel tickLabel-div"  >{{$test['created_at']}}</div>
																	<div class="flot-tick-label tickLabel tickLabel-div" >Jan 01</div>
																</div>
																<div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
																	<div class="flot-tick-label tickLabel" style="position: absolute; top: 252px; left: 13px; text-align: right;">0</div>
																	<div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 1px; text-align: right;">130</div>
																</div>
															</div>
    											@endforeach
    
											
															<canvas class="flot-overlay" width="1161" height="400" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1161px; height: 400px;"></canvas>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-12">
			
			 
								@component('components.supervisor.toggle_user_procedures_indications', [ 'decisions' => $decisions
								])@endcomponent
								</div>
		 
	 




											  @php //!end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! @endphp


										<div class="row"  >
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-4 col-sm-6 col-xs-12">
													<div class="x_panel tile fixed_height_320">
														<div class="x_title">
															<h2>Procedures Usage</h2>
				@component('components.supervisor.navbar_tools') @endcomponent
				
															<div class="clearfix"></div>
														</div>
														<div class="x_content" style="margin-bottom:5px">
															<div>
																<span class="pull-right">Reports</span>
																<!-- <h4>App Usage across versions</h4> -->
																<br>
																</div>
    @foreach ($byProcedures as $procTitle    => $byProcedure)
        
					
																<div class="widget_summary">
																	<div class="w_left w_35">
																		<span>{{str_replace('(H)','',$procTitle)}} </span>
																	</div>
																	<div class="w_center w_55">
																		<div class="progress">
																			<div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{round(count($byProcedure)*100/count($teach))}}%;">
																				<span class="sr-only">60% Complete</span>
																			</div>
																		</div>
																	</div>
																	<div class="w_right w_10">
																		<span>{{ count($byProcedure)}}</span>
																	</div>
																	<div class="clearfix"></div>
																</div>
@endForeach
 

				
															</div>
														</div>
													</div>
													<div class="col-md-5 col-sm-6 col-xs-12">
														<div class="x_panel tile fixed_height_320 overflow_hidden">
															<div class="x_title">
																<h2>Platform Usage</h2>
				@component('components.supervisor.navbar_tools') @endcomponent
					
																<div class="clearfix"></div>
															</div>
															<div class="x_content">
                                                                
                                                               <div class="play"> 
                                                                        <table class="platform-usage   ">
                                                                            <tbody>
                                                                                <tr>
																			<td width="30%">
																		 
																			 
                                                                                    <canvas class="canvasDoughnut"  height= "230px" width="230px" ></canvas>
                                                                                
																			</td>
																			<Td>
																				<table class="tile_info text-center by-User-Table">
																					<tbody id="byUserTable" >
																						<tr>
																							<th class="users-title-usage">
																								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
																									<p class="">Supervisor</p>
																								</div>
																							</th>
																							<th  class="users-title-usage-td">
																								<div   style=" ">
																									<p style="text-align:left">Usage</p>
																								</div>
																							</th>
																						</tr>
																					</tbody>
																				</table>
																			</Td>
																		</tr>
																	</tbody>
                                                                </table>
                                                              
															</div>
															</div>
														</div>
                                                    </div>
                                                    
													<div class="col-md-3 col-sm-6 col-xs-12">
                                                
														<div class="x_panel tile fixed_height_320">

															<div class="x_title" style="max-height:40px !important">
                                                                        
															<div class="recommendations">
                                                                	<h2  class="serenus-ai-recommendations" style=" overflow:hidden">SerenusAI Recommendations</h2>
                                                            </div>
                                              
                                                                 	@component('components.supervisor.navbar_tools') @endcomponent
                                                            
				
					 
																<div class="clearfix"></div>
															</div>
															<div class="x_content">
																<div class="dashboard-widget-content">
																	<ul class="quick-list"  style="margin-left:9px">
																		<!-- <li><i class="fa fa-calendar-o"></i><a href="#">Map</a></li> -->
																		<!-- <li><i class="fa fa-bars"></i><a href="#">Subscription</a></li> -->
																		<li id="total-indications">
																			<i class="fa fa-bar-chart" ></i>
																		</li>
																		<li id="indicated" >
																			<i class="fa fa-line-chart"></i>
																			<a href="#"></a>
																		</li>
																		<li  id="not-indicated" >
																			<i class="fa fa-bar-chart"></i>
																			<a href="#"></a>
																		</li>
																		<!-- <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a></li><li><i class="fa fa-area-chart"></i><a href="#">Logout</a></li> -->
																	</ul>
																	<div class="sidebar-widget">
																		<span  >Recommendations</h4>
																		<canvas width="150" height="80" id="chart_gauge_01" class="" style="width: 160px; height: 100px;"></canvas>
																		<div class="goal-wrapper">
																			<span id="gauge-text" class="gauge-value pull-left"></span>
																			<span class="gauge-value pull-left">%</span>
																			<span id="goal-text" class="goal-value pull-right">100%</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row"></div>
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="col-md-12 col-sm-12 col-xs-12">
														<div class="x_panel tile">
															<div class="x_title">
																<h2>Reports </h2>
		@component('components.supervisor.navbar_tools') @endcomponent
 		
																<!-- <div class="clearfix"></div></div> -->
																<div class="clearfix"></div>
															</div>
@component('components.supervisor_table', [ 
                                    'isIndicated' => $isIndicated,
                                        'mappedDecisions' => $mappedDecisions,
                                        'role' => $role,
                                       'teach' => $teach,
                                        'decisions' => $decisions,  'test_id'=>$test['test_id']
                                            ])@endcomponent

														</div>
													</div>
												</div>
											</div>
										</div>

@section('datatable')
 
										<script>
 var profileTable = $('.profiles-table').DataTable({
    //   dom: 'Bfrtip',
    //   autoWidth: false,
    //   order: [[0, 'asc']],
    responsive: true
  });
  //byUserTable

  $(() => {
setTimeout(() => {
	   console.log('%c D9', 'background: #222; color: #bada55');
 let gaugeVal=$("#gauge-text");
 if (!gaugeVal.text()  ) gaugeVal.text('0')
   console.log(gaugeVal.text()) //
     console.log('%c D9', 'background: #222; color: #bada55');
}, 3000);
    $('.profiles-table_filter').addClass('searchBarClass');
  });
  
 
 </script>
@stop
@endsection
@section('script')


										<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/jquery/dist/jquery.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/fastclick/lib/fastclick.js"></script>
										<script src="https://colorlib.com/polygon/vendors/nprogress/nprogress.js"></script>
										<script src="https://colorlib.com/polygon/vendors/Chart.js/dist/Chart.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/gauge.js/dist/gauge.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/iCheck/icheck.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/skycons/skycons.js"></script>
										<script src="https://colorlib.com/polygon/vendors/Flot/jquery.flot.js"></script>
										<script src="https://colorlib.com/polygon/vendors/Flot/jquery.flot.pie.js"></script>
										<script src="https://colorlib.com/polygon/vendors/Flot/jquery.flot.time.js"></script>
										<script src="https://colorlib.com/polygon/vendors/Flot/jquery.flot.stack.js"></script>
										<script src="https://colorlib.com/polygon/vendors/Flot/jquery.flot.resize.js"></script>
										<!-- <script src="https://colorlib.com/polygon/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script> -->
										<script src="https://colorlib.com/polygon/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/flot.curvedlines/curvedLines.js"></script>
										<script src="https://colorlib.com/polygon/vendors/DateJS/build/date.js"></script>
 
										<script src="https://colorlib.com/polygon/vendors/moment/min/moment.min.js"></script>
										<script src="https://colorlib.com/polygon/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
										<?php $x=rand(1,100)?>
										<script src="{{asset('js/approval.js?'.$x)}}"></script>
										<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-23581568-13', 'auto');
    ga('send', 'pageview');
    let indicated=notIndicated=0;    
    let myTest='';
   
    window.isIndicated = 
											<?=json_encode($isIndicated); ?>;
    window.myTests= 
											<?= json_encode($teach); ?>;
   
    bySupervisorDecision=_.groupBy(window.myTests, test => {
            return  test.supervisor_decision 
        });
    byIsDoneReason=_.groupBy(window.myTests, test => {
            return  test.is_done_reason 
        });     
  
        byUser=_.groupBy(window.myTests, test => {
            return  test.last_name 
        });    
                byTestsJson=_.groupBy(window.myTests, test => {
            return  test.testsJson
        });    
    console.log({bySupervisorDecision,byIsDoneReason,byUser,byTestsJson})

										</script>
										<script src="{{asset('js/custom-new-supervisor.js')}}"></script>

@yield('toggle-user-procedure-indications')
@stop



