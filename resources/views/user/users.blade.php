
 
@extends('master')

@section('title', 'Users')

@section('content')

    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
         <table  class="table users-table table-condensed table-striped table-bordered text-center"
                   cellspacing="0" width="100%" >
  <thead>
    <tr>
        <th class = "text-center" >@lang('Id')</th>
        <th class = "text-center">@lang('Name')</th>
        <th class = "text-center">@lang('Title')</th>
        <th class = "text-center">@lang('Category')</th>
        <th class = "text-center">@lang('Email')</th>
        <th class = "text-center">@lang('Roles')</th>
        <th class = "text-center">@lang('Client')</th>
        <th class = "text-center noExport">@lang('Action')</th>
    </tr>
  </thead>
    <tfoot>
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                    <th class="text-center noExport"></th>
                </tr>
                </tfoot>
  <tbody>
     @foreach($users as $user)
                    
        <tr>
        <th class = "text-center" scope="row">{{$user['id']}}</th>
        <td class = "text-center"  scope="row">{{$user['first_name']}} {{$user['last_name']}} </td>
        <td class = "text-center"  scope="row">{{$user['title']}}</td>
        <td class = "text-center"  scope="row"> {{$user['category_id']}}</td>
        <td class = "text-center"  scope="row"> {{$user['email']}}</td>
        <td class = "text-center" 
   
          scope="row"> {{$user['role']}}  </td> 
        <td class = "text-center"  scope="row"> {{$user['client_id']}}</td>
            <td>
                                <div class = "btn-group btn-group-sm">
                                    <button class = "edit-procedure btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{ 'users-procedures/' . $user['id'] . '/edit'}}">
                                        <i class = "fas fa-paperclip" aria-hidden = "true"></i> Procedures
                                    </button>
                                    <button style = "color: #73879C"
                                            class = "edit-user btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{ 'users/' . $user['id'] . '/edit'  }}"><i
                                                class = "far fa-edit" aria-hidden = "true"></i> Edit
                                    </button>

                                    <button style = "color: #73879C;
                                      @if (!$user['role_exists'])  border: 1px solid red   @endif "
                                            class = "btn btn-default edit-roles bootstrap-modal-form-open"
                                            data-url = "{{'user-roles/' . $user['id'] . '/edit'}}"><i
                                                class = "fas fa-link" aria-hidden = "true"></i> Role
                                    </button>

                                    <a style = "color: #73879C" class = "btn btn-default"
                                       href = "{{url('users/' . $user['id'])}}"><i
                                                class = "fas fa-eye"
                                                aria-hidden = "true"></i> Display</a>

                                    <a style = "color: #73879C" token = "{{csrf_token()}}"
                                       url = "{{'/users/' . $user['id']}}" href = "#"
                                       class = "btn btn-default delete-confirm">
                                        <i class = "far fa-trash-alt"
                                        aria-hidden = "true"></i> Delete</a>
                                </div>
    @endforeach
  </tbody>
</table>
        </div>
    </div>
    
    @include('user.partials.modal_form')
@endsection

@section('script')
    <script src = "js/users.js"></script>
@stop
