@extends('master')

@section('title', 'Users')

@section('content')

    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <h1>Users</h1>
            <h2>update user:</h2>
            <form class="form-inline">
  <div class="form-group mb-2">
       
    <label for="client_id" >Client id :</label>
     <select>
              @foreach($users as $user)
              @if ($user['client_id'] and $user['client_id'] !=1 )
               <option value="{{ $user['client_id'] }}">{{ $user['client_id'] }}</option>
               @endif
               @endforeach  
               <option value="1">1</option>
               <option value="NULL">No client id</option>
           </select>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <label for="category" >Category:</label>
    <select>
        <option value="1">1</option>
        <option value="0">0</option>
        @foreach($users as $user)
            @if ( $user['category_id']  and   $user['category_id']  !=1 )
                <option value="{{ $user['category_id']  }}">{{ $user['category_id'] }}</option>
            @endif
        @endforeach  
    </select>
  </div>
  <button type="submit" class="btn btn-primary mb-2">Submit</button>
</form>
         
            <table class = "table text-center table-bordered users-table table-striped nowrap" style = "display: none"
           
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">Category</th>
                    <th class = "text-center">Email</th>
                    <th class = "text-center">Roles</th>
                    <th class = "text-center">Client</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">Category</th>
                    <th class = "text-center">Email</th>
                    <th class = "text-center">Roles</th>
                    <th class = "text-center">Client</th>
                    <th class = "text-center noExport"></th>
                </tr>
                </tfoot>
                <tbody>
           
                    
                        <tr>
                
                            </td>
                            <td>
                               
                                <div class = "btn-group btn-group-sm">
                                  
                                        <i class = "fas fa-paperclip" aria-hidden = "true"></i> Procedures
                                    </button>
                                  

                       
                                    </button>

    
                                </div>
                            </td>
                        </tr>
                   
                
                </tbody>
            </table>
        </div>
    </div>
    
    @include('user.partials.modal_form')
@endsection

@section('script')
    <script src = "js/users.js"></script>
@stop
