@extends('master')

@section('title', 'Users')

@section('content')

    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table text-center table-bordered users-table table-striped nowrap" style = "display: none"
           
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">Category</th>
                    <th class = "text-center">Email</th>
                    <th class = "text-center">Roles</th>
                    <th class = "text-center">Client</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Name</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">Category</th>
                    <th class = "text-center">Email</th>
                    <th class = "text-center">Roles</th>
                    <th class = "text-center">Client</th>
                    <th class = "text-center noExport"></th>
                </tr>
                </tfoot>
                <tbody>
                @if(isset($users))
                    @foreach($users as $user)
                    
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->first_name}} {{$user->last_name}}</td>
                            <td>{{$user->title}}</td>
                            <td>
                                       
                                @if(count($user->category))
                                    {{$user->category->title}}
                                @endif
                                 
                            </td>
                            <td>{{$user->email}}</td>
                            <td>{{implode(' | ',$user->roles()->pluck('name')->toArray())}}</td>
                            <td>
                   
                                @if(count($user->client))
                                    {{$user->client->client_name}}
                                @endif
                            </td>
                            <td>
                                <div class = "btn-group btn-group-sm">
                                    <button class = "edit-procedure btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{ 'users-procedures/' . $user->id . '/edit'}}">
                                        <i class = "fas fa-paperclip" aria-hidden = "true"></i> Procedures
                                    </button>
                                    <button style = "color: #73879C"
                                            class = "edit-user btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{ 'users/' . $user->id . '/edit'  }}"><i
                                                class = "far fa-edit" aria-hidden = "true"></i> Edit
                                    </button>

                                    <button style = "color: #73879C"
                                            class = "btn btn-default edit-roles bootstrap-modal-form-open"
                                            data-url = "{{'user-roles/' . $user->id . '/edit'}}"><i
                                                class = "fas fa-link" aria-hidden = "true"></i> Role
                                    </button>

                                    <a style = "color: #73879C" class = "btn btn-default"
                                       href = "{{url('users/' . $user->id)}}"><i
                                                class = "fas fa-eye"
                                                aria-hidden = "true"></i> Display</a>

                                    <a style = "color: #73879C" token = "{{csrf_token()}}"
                                       url = "{{'/users/' . $user->id}}" href = "#"
                                       class = "btn btn-default delete-confirm">
                                        <i class = "far fa-trash-alt"
                                           aria-hidden = "true"></i> Delete</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    
    @include('user.partials.modal_form')
@endsection

@section('script')
    <script src = "js/users.js"></script>
@stop
