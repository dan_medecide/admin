@extends('master')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('title', 'Add Roles')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Choose User Roles</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @if(isset($user))
                                {!!Form::model($user, ['route' => ['update_user_roles', $user->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
                                {!!Form::token()!!}

                                @if(isset($roles))
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Assign Roles</div>
                                            <ul class="list-group">
                                                @foreach($roles as $role)
                                                    <li class="list-group-item">
                                                        {{$role['name']}}
                                                        <div class="material-switch pull-right">
                                                            <input value="{{$role['id']}}" id="{{$role['name']}}"
                                                                   name="{{$role['name']}}"
                                                                   type="checkbox"
                                                                   @if($user->hasRole($role['name'])) checked="checked" @endif/>
                                                            <label for="{{$role['name']}}"
                                                                   class="label-default"></label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        {!! Form::submit('Attach user roles', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                @endif
                                {!!Form::close()!!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('script')

    <!-- Datatables -->
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>

@endsection

