<div class = "modal fade" id = "user-modal" tabindex = "-1" role = "dialog" aria-labelledby = "user-modal">
    <div class = "modal-dialog modal-lg" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                            aria-hidden = "true">&times;</span></button>
                <h4 class = "modal-title">Add New User</h4>
            </div>
            <div class = "modal-body">

            </div>
            <div class = "modal-footer">
                <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
                <button type = "submit" class = "btn btn-primary bootstrap-modal-submit">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>