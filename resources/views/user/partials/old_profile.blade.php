@extends('master')

@section('meta')
    {!! Charts::assets() !!}
@endsection

@section('title', 'User Profile')

@section('content')
    <div class="right_col" role="main">

        @include('layouts.message')
        <div class="page-header">
            <h2>{{$user->title}} {{$user->first_name}} {{$user->last_name}}
                <br>
                <small><i class="fa fa-external-link user-profile-icon"></i> {{$user->email}}</small>
                <br>
                <small><i class="fa fa-tags"
                          aria-hidden="true"></i> {{$user->category()->value('title')}}
                </small>
            </h2>
        </div>

        <div class="row">
            @if($labels_chart)
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">User Activity Report</h3>
                        </div>
                        <div class="panel-body">
                            {!! $labels_chart->render() !!}
                        </div>
                    </div>
                    
               
                
            @endif
              
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Procedures</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table text-center table-bordered datatable-buttons table-striped nowrap"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Procedure Name</th>
                                <th class="text-center">Version</th>
                                <th class="all text-center noExport">Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Procedure Name</th>
                                <th class="text-center">Version</th>
                                <th class="all text-center noExport">Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @if(isset($procedures))
                                @foreach($procedures as $procedure)
                                    <tr>
                                        <td>{{$procedure->id}}</td>
                                        <td class="procedure-title">{{$procedure->title}}</td>
                                        <td>{{$procedure->version}}</td>
                                        <td class="all">
                                            <a target="_blank" style="color: #73879C" type="button"
                                               class="btn btn-default"
                                               href="{{\App\Home::getCoreAppLink('/teach/' . $procedure->id)}}"><i
                                                        class="fa fa-cogs" aria-hidden="true"></i>
                                                Teach</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
   <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Procedures</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table text-center table-bordered datatable-buttons table-striped nowrap"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Procedure Name</th>
                                <th class="text-center">Version</th>
                                <th class="all text-center noExport">Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Procedure Name</th>
                                <th class="text-center">Version</th>
                                <th class="all text-center noExport">Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @if(isset($procedures))
                                @foreach($procedures as $procedure)
                                    <tr>
                                        <td>{{$procedure->id}}</td>
                                        <td class="procedure-title">{{$procedure->title}}</td>
                                        <td>{{$procedure->version}}</td>
                                        <td class="all">
                                            <a target="_blank" style="color: #73879C" type="button"
                                               class="btn btn-default"
                                               href="{{\App\Home::getCoreAppLink('/teach/' . $procedure->id)}}"><i
                                                        class="fa fa-cogs" aria-hidden="true"></i>
                                                Teach</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
       
       
        </div>
        
    </div>
    <!-- /page content -->
    <script type="text/javascript">
        $(function () {
            $(".procedure-title").each(function () {
                len = $(this).text().length;
                if (len > 40) {
                    $(this).text($(this).text().substr(0, 40) + '...');
                }
            });
        });
    </script>
@endsection














