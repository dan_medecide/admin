{!! Form::model($user, ['route' => ['user-roles.update', $user->id], 'class' => 'bootstrap-modal-form', 'method' => 'patch']) !!}

<ul class = "list-group">
    @foreach($roles as $role)
        <li class = "list-group-item">
            {{$role['name']}}
            <div class = "material-switch pull-right">
                <input value = "{{$role['id']}}" id = "{{$role['name']}}"
                       name = "{{$role['name']}}"
                       type = "checkbox"
                       @if($user->hasRole($role['name'])) checked = "checked" @endif/>
                <label for = "{{$role['name']}}"
                       class = "label-default"></label>
            </div>
        </li>
    @endforeach
</ul>