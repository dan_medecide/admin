{!! Form::model($user, ['route' => ['users-procedures.update', $user->id], 'class' => 'bootstrap-modal-form', 'method' => 'patch']) !!}

<ul class = "list-group">
    @foreach($procedures as $procedure)
        <li class = "list-group-item">
            {!!trim($procedure->title) . '  <span class="label label-success">Version: ' . $procedure->version . '</span>  <span class="label label-primary">Category: ' . $procedure->category['title'] . '</span>'!!}
            <div class = "material-switch pull-right">
                <input value = "{{$procedure->id}}" id = "{{trim($procedure->title)}}"
                       name = "{{trim($procedure->title)}}"
                       type = "checkbox" @if($user->hasProcedure($procedure->id)) checked @endif/>
                <label for = "{{trim($procedure->title)}}"
                       class = "label-default"></label>
            </div>
        </li>
    @endforeach
</ul>