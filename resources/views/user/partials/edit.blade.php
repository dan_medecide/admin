{!! Form::model($user, ['route' => ['users.update', $user->id], 'class' => 'bootstrap-modal-form', 'method' => 'put']) !!}
 
@component('components.text_field')
    @slot('name', 'first_name')
    @slot('title', 'First Name')
@endcomponent

@component('components.text_field')
    @slot('name', 'last_name')
    @slot('title', 'Last Name')
@endcomponent

@component('components.text_field')
    @slot('name', 'title')
    @slot('title', 'Title')
@endcomponent

@component('components.select_field')
    @slot('name', 'category_id')
    @slot('title', 'Category')
    @slot('list', $categories)
@endcomponent

@component('components.select_field')
    @slot('name', 'client_id')
    @slot('title', 'Client')
    @slot('list', $clients)
@endcomponent

@component('components.email_field')
    @slot('name', 'email')
    @slot('title', 'Email')
@endcomponent

@component('components.role_field')
    @slot('name', 'roles')
    @slot('title', 'Role (for display only!)')
    @slot('value', $user->role)
@endcomponent

@component('components.password_field')
    @slot('name', 'password')
    @slot('title', 'Password')
    @slot('value', 'oldPassword')
@endcomponent