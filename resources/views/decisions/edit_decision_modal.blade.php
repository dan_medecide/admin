<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Edit Decision</h4>
</div>
<div class = "modal-body">
    {!! Form::model($decision,['route' => ['decisions.update',$decision->id],'class' => 'bootstrap-modal-form','method' => 'put']) !!}

    <div class = "form-group">
        {!! Form::label('title','Title') !!}
        {!! Form::text('title',null,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('description','Description') !!}
        {!! Form::text('description',null,['class' => 'form-control']) !!}
    </div>

    <div class = "form-group">
        {!! Form::label('code','Code') !!}
        {!! Form::text('code',null,['class' => 'form-control']) !!}
    </div>

    <div class = "form-group">
        <input type="hidden" name="is_indication" value="0">
        {!! Form::checkbox('is_indication') !!}
        {!! Form::label('is_indication','Indication') !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "This decision has been updated successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Update Decision
    </button>
    {{--{!! BootForm::submit('Submit')->addClass('btn-primary') !!}--}}
    {!! Form::close() !!}
</div>