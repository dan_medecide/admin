@extends('master')

@section('title', 'Decisions')

@section('content')
    <td style=" vertical-align: middle;" > 
    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table text-center decisions-table table-striped table-bordered nowrap"
                   style = "display:none;"
                   cellspacing = "0"
                   width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">ID</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">description</th>
                    <th class = "text-center">Indication</th>
                    <th class = "text-center">Code</th>
                    <th class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">ID</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">description</th>
                    <th class = "text-center">Indication</th>
                    <th class = "text-center">Code</th>
                    <th class = "text-center noExport"></th>
                </tr>
                </tfoot>
                <tbody>
                @if(isset($decisions))
                    @foreach($decisions as $decision)
                        <tr>
                            <td>{{$decision['id']}}</td>
                            <td>{{$decision['title']}}</td>
                            <td>{{$decision['description']}}</td>
                            <td>
                                @if($decision['is_indication'] == 1)
                                    YES
                                @else
                                    NO
                                @endif
                            </td>
                            <td>{{$decision['code']}}</td>
                            <td>
                                <div class = "btn-group btn-group-sm">
                                    <button style = "color: #73879C"
                                            class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                            data-url = "{{ 'decisions/' . $decision['id'] . '/edit'  }}"><i
                                                class = "far fa-edit" aria-hidden = "true"></i> Edit
                                    </button>
                                    <button style = "color: #73879C" token = "{{csrf_token()}}"
                                            url = "{{'/decisions/' . $decision['id']}}" href = "#"
                                            class = "btn btn-default delete-confirm">
                                        <i class = "far fa-trash-alt"
                                           aria-hidden = "true"></i> Delete
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</td>
    <!-- /page content -->
    @include('components.modal')
@endsection

@section('script')
    <script src = "{{asset('js/decisions.js')}}"></script>
@stop