@extends('master')

@section('title', 'Roles')

@section('content')
    <style>
        [v-cloak] {
            display: none
        }
    </style>
    <div class = "right_col" role = "main" id = "app">
        <div class = "row" v-cloak>
            <div class = "col-md-7 col-sm-12 col-xs-12">
                @component('components.panel')
                    @slot('title')
                        Roles
                    @endslot
                    <table class = "table text-center table-bordered table-striped nowrap"
                           cellspacing = "0" width = "100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>description</th>
                            <th class = "noExport">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for = "role in roles">
                            <td>@{{role.id}}</td>
                            <td>@{{role.name}}</td>
                            <td>@{{role.description}}</td>
                            <td>
                                <button class = "btn btn-default" @click = "edit(role.id)">
                                    <i class = "far fa-edit" aria-hidden = "true"></i> Edit
                                </button>
                                <a class = "btn btn-default" @click = "confirmDelete(role.id)">
                                    <i class="far fa-trash-alt"></i> Delete</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                @endcomponent
            </div>
            <div class = "col-md-5">
                @component('components.panel')
                    @slot('title')
                        @{{ formTitle }}
                    @endslot
                    <form @keydown = "role.errors.clear($event.target.name)">
                        <div :class = "{'form-group':true, 'has-error':role.errors.has('name')}">
                            <label for = "name">NAME</label>
                            <input type = "text"
                                   class = "form-control"
                                   v-model = "role.name"
                                   id = "name"
                                   name = "name"
                                   placeholder = "name the role.">
                            <span class = "help-block"
                                  v-if = "role.errors.has('name')"
                                  v-text = "role.errors.get('name')">
                            </span>
                        </div>
                        <div :class = "{'form-group':true, 'has-error':role.errors.has('description')}">
                            <label for = "description">DESCRIPTION</label>
                            <textarea class = "form-control"
                                      v-model = "role.description"
                                      id = "description"
                                      name = "description"
                                      placeholder = "describe waht the role contains."></textarea>
                            <span class = "help-block"
                                  v-if = "role.errors.has('description')"
                                  v-text = "role.errors.get('description')">
                            </span>
                        </div>
                        <button class = "btn btn-success"
                                :disabled = "role.errors.any()"
                                v-if = "role.id"
                                @click.prevent = "submit(`/roles/${role.id}/update`)">UPDATE
                        </button>
                        <button class = "btn btn-success"
                                :disabled = "role.errors.any()"
                                v-else
                                @click.prevent = "submit()">SAVE
                        </button>
                        <button class = "btn btn-default" @click.prevent = "clear">CLEAR</button>
                    </form>
                @endcomponent
            </div>
        </div>
        {{--<div class = "row" v-else>--}}
            {{--<spinner style = "margin-top: 400px;" message = "Loading..." size = "massive">--}}
            {{--</spinner>--}}
        {{--</div>--}}
    </div>
@endsection

@section('script')
    <script src = "{{asset('js/roles.js')}}"></script>
@stop