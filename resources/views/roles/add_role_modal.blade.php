<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Add Role</h4>
</div>
<div class="modal-body">
    {!! BootForm::open()->action('/roles')->addClass('bootstrap-modal-form') !!}
    {!!Form::token()!!}
    {!! BootForm::text('Name', 'name') !!}
    {!! BootForm::text('Description', 'description') !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" title="Congratulations!" data-content="New role has been created successfully."
            data-animation="true"
            data-placement="left" class="btn btn-primary bootstrap-modal-submit">Create Role
    </button>
    {{--{!! BootForm::submit('Submit')->addClass('btn-primary') !!}--}}
    {!! BootForm::close() !!}
</div>