<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Role</h4>
</div>
<div class="modal-body">
    {!! BootForm::open()->action('/roles/' . $role->id)->addClass('bootstrap-modal-form')->put() !!}
    {!! BootForm::bind($role) !!}
    {!!Form::token()!!}
    {!! BootForm::text('Name', 'name') !!}
    {!! BootForm::text('Description', 'description') !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" title="Congratulations!" data-content="This role has been updated successfully."
            data-animation="true"
            data-placement="left" class="btn btn-primary bootstrap-modal-submit">Update Role
    </button>
    {{--{!! BootForm::submit('Submit')->addClass('btn-primary') !!}--}}
    {!! BootForm::close() !!}
</div>