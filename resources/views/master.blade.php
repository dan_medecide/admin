<!DOCTYPE html>
<html lang = "he">
<head>
    
    @include('layouts.meta')
    @yield('meta')
    <title>SerenusAI™ | @yield('title')</title>
     <!-- <div style="position:absolute;color:white;z-index:1000000">
      {{getenv('DB_DATABASE') }}
     </div> -->
    @include('layouts.css')
    @yield('css')
</head>

<body class = "nav-md">

   
<div class = "container body">
    <div class = "main_container">
     
        @include('layouts.sidebar')
       
        @include('layouts.navbar')
        
        @isset($proc_id)
     <div class="pull-right"; style=" margin-right:17px; margin-bottom:10px;">
     
      @yield('procedureActions')
       
     </div>
    
        @endisset
        @yield('content')
        @include('layouts.footer')
    </div>
</div>
<script>BASE_URL = "{{url('')}}"
 
</script>
@include('layouts.script')
@yield('script')
@yield('login_js')
</body>
</html>

