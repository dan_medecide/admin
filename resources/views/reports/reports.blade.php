@extends('master')

@section('title', 'Reports')

@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table reports-table table-condensed table-bordered table-striped text-center"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Date</th>
                    <th class = "text-center">Client</th>
                    <th class = "text-center">Procedure</th>
                    <th class = "text-center">Code</th>
                    <th class = "text-center">Test id</th>
                    <th class = "text-center">Physician</th>
                    <th class = "text-center">Patient ID</th>
                    <th class = "text-center">Labeled</th>
                    <th data-priority = "1" class = "text-center noExport">Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class = "text-center">Date</th>
                    <th class = "text-center">Client</th>
                    <th class = "text-center">Procedure</th>
                    <th class = "text-center">Code</th>
                    <th class = "text-center">Test id</th>
                    <th class = "text-center">Physician</th>
                    <th class = "text-center">Patient ID</th>
                    <th class = "text-center">Labeled</th>
                    <th data-priority = "1" class = "text-center noExport"></th>
                </tr>
                </tfoot>
                <tbody>
                @if(isset($reports))
                    @foreach($reports as $report)
                        <tr>
                            <td>{{$report->created_at}}</td>
                            <td>{{$report->client_name}}</td>
                            <td>{{$report->procedures}}</td>
                            <td>{{$report->code}}</td>
                            <td>{{$report->test_id}}</td>
                            <td>{{$report->physician_name}}</td>
                            <td>{{$report->patient_id}}</td>
                            @if(in_array($report->test_id,$tests))
                                <td>YES</td>
                            @else
                                <td>NO</td>
                            @endif
                            <td>
                                <div class = "btn-group btn-group-sm">
                                    <a target = "_blank" style = "color: #73879C"
                                       href = "{{'http://medecide.net/report/' . $report->test_id . '/' . \App\Home::auth($report->test_id)}}"
                                       class = "btn btn-sm btn-default"><i class="fas fa-print"></i> Report</a>
                                    <a target = "_blank" style = "color: #73879C"
                                       href = "{{\App\Home::getProductionLink('/teach/' . $report->proc_id . '/' . $report->test_id)}}"
                                       class = "btn btn-sm btn-default"><i class="fas fa-graduation-cap"></i> Teach</a>
                                       
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- /page content -->
    @include('components.modal')
@endsection

@section('script')
    <script src = "{{asset('js/reports.js')}}"></script>
@stop