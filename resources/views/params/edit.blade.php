<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Edit Param</h4>
</div>
<div class = "modal-body">
    {!! Form::model($param, ['route' => ['params.update', $param->id],'class' => 'bootstrap-modal-form','method' => 'patch']) !!}
    <div class = "form-group">
        {!! Form::label('title', 'Title'); !!}
        {!! Form::text('title',null,['required','class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('type', 'Type'); !!}
        {!! Form::text('type',null,['required','class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('target', 'target'); !!}
        {!! Form::text('target',null,['required','class' => 'form-control']) !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "This param has been updated successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Update Param
    </button>
    {!! Form::close() !!}
</div>