<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Add Param</h4>
</div>
<div class = "modal-body">
    {!! Form::open(['route' => 'params.store','class' => 'bootstrap-modal-form']) !!}
    <div class = "form-group">
        {!! Form::label('title', 'Title'); !!}
        {!! Form::text('title',null,['required','class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('type', 'Type'); !!}
        {!! Form::text('type',null,['required','class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('target', 'target'); !!}
        {!! Form::text('target',null,['required','class' => 'form-control']) !!}
    </div>
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "New Param has been created successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Create Param
    </button>
    {!! Form::close() !!}
</div>