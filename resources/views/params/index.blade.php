@extends('master')

@section('title', 'Params')

@section('content')

    <div class = "right_col" role = "main">
        <div class = "container">
            <table class = "table params-table table-bordered table-striped text-center">
                <thead>
                <tr>
                    <th class = "text-center">ID</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">Type</th>
                    <th class = "text-center">Target</th>
                    <th class = "text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($params as $param)
                    <tr>
                        <td>{{$param->id}}</td>
                        <td>{{$param->title}}</td>
                        <td>{{$param->type}}</td>
                        <td>{{$param->target}}</td>
                        <td>
                            <div class = "btn-group btn-group-sm">
                                <button class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                        data-url = "{{ 'params/' . $param->id . '/edit'  }}"><i
                                            class = "far fa-edit" aria-hidden = "true"></i> Edit
                                </button>
                                <button token = "{{csrf_token()}}"
                                        url = "{{'/params/' . $param->id}}" href = "#"
                                        class = "btn btn-default delete-confirm">
                                    <i class = "far fa-trash-alt"
                                       aria-hidden = "true"></i> Delete
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('components.modal')
@endsection

@section('script')
    <script src = "{{asset('js/params.js')}}"></script>
@stop