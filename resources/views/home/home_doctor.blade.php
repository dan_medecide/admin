@extends('master')

@section('meta')
    {!! Charts::assets() !!}
@endsection

@section('title', 'User Profile')

@section('content')
    <!-- page content -->
    
    <div class="right_col" role="main">
        <div class="page-header">
            <h2>{{$user->title}} {{$user->first_name}} {{$user->last_name}}
                <br>
                <small><i class="fa fa-external-link user-profile-icon"></i> {{$user->email}}</small>
                <br>
                <small><i class="fa fa-tags" aria-hidden="true"></i> {{$user->category->title}}</small>
            </h2>
        </div>
        <div class="row">
            @if($labels_chart->labels->count() > 0)
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">User Activity Report</h3>
                        </div>
                        <div class="panel-body">
                            {!! $labels_chart->render() !!}
                        </div>
                    </div>
                </div>
            @endif
            <div class="@if($labels_chart->labels->count() > 0) col-md-6 @else col-md-12 @endif">
                <table class="table text-center table-bordered datatable-buttons table-striped nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Procedure Name</th>
                        <th class="text-center">Version</th>
                        <th class="text-center noExport">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($procedures))
                        @foreach($procedures as $procedure)
                            <tr>
                                <td>{{$procedure->id}}</td>
                                <td>{{$procedure->title}}</td>
                                <td>{{$procedure->version}}</td>
                                <td>
                                    <a target="_blank" type="button"
                                       class="btn btn-default"
                                       href="{{route('procedures.beta-teach', $procedure->id)}}"> 
                                       <i
                                                class="fa fa-graduation-cap"
                                                aria-hidden="true"></i>
                                        Teach</a>
                                    {{--<a class="btn btn-default"--}}
                                    {{--data-toggle="tooltip" title="Run"><i class="fa fa-cogs"--}}
                                    {{--aria-hidden="true"></i> Run</a>--}}
                                    <a class="btn btn-default"
                                       href="{{route('procedures.run', $procedure->id)}}"
                                       target="_blank">
                                        <i class="far fa-share-square"></i>
                                        Run
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection














