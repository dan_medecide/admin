@extends('master')

@section('title', 'Admin System')

@section('content')
    <div class = "right_col" role = "main" id = "app">
        <div class = "page-header">
            <h1>
                SerenusAI™ Analytics Dashboard
            </h1>
        </div>
        <div class = "row">
            <tests-count-by-physician></tests-count-by-physician>
            <tests-total-count></tests-total-count>
            <physicians-tests-count-by-procedure></physicians-tests-count-by-procedure>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Referrals waiting for approval</h3>
                    </div>
                    <div class="panel-body" style="padding: 20px">
                            <client-reports-table :unapproved="1"></client-reports-table>
                        </div>
                    </div>
                </div>
            </div>
            <approved-referrals></approved-referrals>
            <div class="row">
                <div class="col-md-12">
                   <key-map></key-map>  
                </div>
            </div>
        </div>
        
        <script>
            const clientId = "{{$client_id}}"
            const appUrl = "http://medecide.net";
            const userCategory = "{{$category_id}}"
    </script>
@stop

@section('script')
<script src = "{{asset('js/client-dashboard.js')}}"></script>
@stop