@extends('master')

@section('title', 'Statistics')

@section('meta')
    {!! Charts::assets() !!}
@endsection

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Tests</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Quantity</th>
                                    <th>Percentage</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Indicated</td>
                                    <td>{{$statistics['indicated']}}</td>
                                    <td>{{round((float)$statistics['indicated'] / $statistics['total_tests']*100 ) . '%'}}</td>
                                </tr>
                                <tr>
                                    <td>Not Indicated</td>
                                    <td>{{$statistics['not_indicated']}}</td>
                                    <td>{{round((float)$statistics['not_indicated'] / $statistics['total_tests']*100). '%'}}</td>
                                </tr>
                                <tr>
                                    <td>Not Done</td>
                                    <td>{{$statistics['not_done']}}</td>
                                    <td>{{round((float)$statistics['not_done'] / $statistics['total_tests']*100) . '%'}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Tests</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {!! $tests_chart->render() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection
