<td class="approval-cell" style="max-height:200px;max-width:200px; ">
   <?php //?toggle-display; ?>
   @if ($is_done) 
   <div class="row">
      <button 
         data-submit="false"
         id="{{$test_id}}"
         class="btn btn-default
         edit-approval-button
         toggleSelect
         toggleEdit
         pull-right"
         style="color: #73879C;margin-right:10px">
      <i class="fa fa-edit"></i>
      </button>
      <button 
         id="{{$test_id}}"
         style="color: #73879C;  display:none; margin-right:10px "
         class="btn btn-default on-submit pull-right" >
      <span class="glyphicon glyphicon-send"></span>
      </button>
   </div>
   @endif
   <div class="panel panel-default" style="margin-bottom: 0px;">
      <div class="panel-body">
         @if (empty ($supervisor_decision) ) 
         <span id="{{$test_id}}" class="toggle-no-decision-yet">No decision yet.</span>
         <div class="decision-form {{$test_id}}">
            <div >
               <label class="{{$test_id}}" style="display:none;" > Select your decision : </label>
               <select  class="form-control {{$test_id}} decision-form" id="sel1" style="display:none;max-width: 50% !important; ">
                  <option value="" selected disabled>...</option>
                  @foreach ($mappedDecisions as $decisionVal => $decisionOption)
                  <option value="{{$decisionVal}}">  {{$decisionOption}}</option>
                  @endforeach
               </select>
            </div>
            <br>
            <br>
            <div class="{{$test_id}}" style="display:none;">
               <label  >Enter explanation : &nbsp;&nbsp;&nbsp; </label>
               <input placeholder="&nbsp;..."  type="text" class="form-control decision-form" id="{{$test_id}}" style="max-width: 50% !important">
            </div>
         </div>
         @else 
         <div class="form-group">
            <span>Final Decision:</span>
            <br>
            <span class="edit-decision {{$test_id}}" >
            <b>{{$decisions[$supervisor_decision]}}</b>
            </span>
            <select  class="form-control {{$test_id}} decision-form" id="sel1" style="display:none;width:50%">
               <option value=""  disabled>...</option>
               @foreach ($mappedDecisions as $decisionVal => $decisionOption)
               <option value="{{$decisionVal}}"  @if ($decisionVal==$supervisor_decision) selected @endif> {{$decisionOption}}</option>
               @endforeach
            </select >
         </div>
      </div>
   </div>
   <div class="panel panel-default" style="margin-bottom: 0px;">
      <div class="panel-body">
         <span>Explanation:</span>
         <br>
         <span class="edit-decision {{$test_id}}">
         <b>{{$supervisor_decision_explanation}}</b>
         </span>
         <div>
            <input type="text" 
               class="form-control decision-form edit-decision {{$test_id}}"
               style="display:none;width:50%" id="{{$test_id}}" value="{{$supervisor_decision_explanation}}">
         </div>
      </div>
      @endif 
   </div>
   </div>
</td>
<?php //!                                        !   S  E   R   E   N   U   S     A   I           D  E   C   I   S   I   O   N                               ?>
<?php //!                                        !   S  E   R   E   N   U   S     A   I           D  E   C   I   S   I   O   N                               ?>
<?php //!                                        !   S  E   R   E   N   U   S     A   I           D  E   C   I   S   I   O   N                               ?>
<td>
   <div>
      @if ( !empty ($tests_json) )
      @isset ($tests_json)
      @foreach ($tests_json as $decisionTitle => $decisionValues)
      <div class="panel panel-default" style="margin-bottom: 0px;">
         <div class="panel-body">
            <b>  {{ title_case($decisionTitle) }}: </b> 
            {{$decisionValues['score']['title']  ??  null }}
         </div>
      </div>
      @endforeach
      @endisset
      @endif 
   </div>
   <div>
      @if (!empty ($manual_decision) )
      @isset ($manual_decision)
      @foreach ($manual_decision as $decisionTitle => $decisionValues)
      <div class="panel panel-default" style="margin-bottom: 0px;">
         <div class="panel-body">
            <b>  {{ $decisionTitle }}: </b>
            @if( $decisionValues>=1)
            @foreach ($decisionValues as $decisionValue)
            @if (!is_int($decisionValue))
            {{ $decisionValue }}  
            @endif
            @endforeach
            @endif
         </div>
      </div>
      @endforeach
      @endisset
      @else
      @if (!empty($manual_decision) )
      @foreach ($manual_decision as $title => $code)
      <div class="panel panel-default" style="margin-bottom: 0px;">
         <div class="panel-body" >
            <b>  {{str_replace("_" , " " , $title) }}: </b>
            {{ $decisions[$code] }}   {{$code}}
         </div>
      </div>
      @endforeach
      @endif
      @endif
   </div>
</td>
<td>
   <div style="margin:0 auto">
      @if(auth()->user()->role=="admin") 
      <button style="position:absolute;right:0"    url = "{{'/test/' . $test_id}}"  
         class = "btn btn-default delete-test-confirm"
         testId = "{{$test_id}}"
         token = "{{csrf_token()}}"
         data-toggle="tooltip" title="@lang('Delete test')">
      <i class = "far fa-trash-alt"
         aria-hidden = "true"></i>
      </button>   @endif
      @if ($is_done) 
      <a target="_blank"  style="color:green;" type="button"
         class="btn btn-default action-button action-button-play"
         href="{{route('user.test-report', $test_id )}}"
         target="_blank"  
         data-toggle="tooltip" title="@lang('Report '. $test_id )">
      <span class="glyphicon glyphicon-play-circle"></span>
      </a>
      @if(auth()->user()->role=="admin") 
      <a target="_blank" type="button" 
         class="btn btn-default action-button action-button-play"
         href="{{route('user.pdf-report', $test_id )}}"
         target="_blank"  
         data-toggle="tooltip" title="@lang('Print  '. $test_id )">
      <i style="font-size:20px; color:red"    class="far fa-file-pdf"></i>
      </a>
      @endif
      @elseif (auth()->user()->roles[0]->name=="supervisor")
      <a
         target="_blank" style="color: #73879C" type="button"
         class="btn btn-default action-button action-button-redo"
         href="{{route('combination.unfinished.run', $test_id )}}"
         target="_blank"  
         data-toggle="tooltip" title="@lang('Continue Report '.$test_id.' from last factor')">
      <i class="fas fa-redo action-button-rotate"></i>
      </a>
      @endif  
   </div>
</td>