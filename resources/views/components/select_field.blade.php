<div class = "form-group">
    {{Form::label($name, $title)}}
    {{Form::select($name, $list, $selected = NULL, ['class' => 'form-control', 'placeholder' => 'Choose an option...'])}}
    @if ($errors->has($name))
        <p class = "help-block">{{$errors->first($name)}}</p>
    @endif
</div>