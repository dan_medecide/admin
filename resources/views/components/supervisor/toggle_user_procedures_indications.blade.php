 
 
<div class="x_panel tile fixed_height_425 overflow_hidden">
	<div class="x_title">
		<h2>By User</h2>
		@component('components.supervisor.navbar_tools') @endcomponent
		<div class="clearfix"></div>
	</div>
	<div class="x_content">
		<div class="row"    style="margin: 0 auto">
            <div style="padding:  0px;width:50%;float;float:left;height:31px">
			<select name="" id="by-user"  class="form-control  toggle-select" ></select>
		</div>
		<div  style="padding: 0px;width:50%;float;float:left;height:31px">
			<select name="" id="toggle-indications-procedures" class="form-control  toggle-select">
				<option value="title">Procedures</option>
				<option value="supervisor_decision">Decisions</option>
			</select>
        </div>
		</div>
        

        	<div id="chartjs-legend" class="noselect" ></div>
     
    <div style="max-width:300px;padding:0; margin: 0 auto ">
	 <table   style="margin: 0 auto"><tr><td style="max-width:200px;">
           <canvas  height= "250" width="250"     id="myPieChart"  ></canvas> 
   </td></tr></table>

    </div>
       
	</div>
</div>
  
  @section('toggle-user-procedure-indications')
    <script type="text/javascript">
      var byUserVal;
      let updatedData= [
            {
              label: 'My First dataset',
            //   pointBorderWidth: 1,
             backgroundColor: ['rgba(52,152,219,0.65)','rgba(3, 88, 106, 0.3)','rgba(115,135,156,0.7)','rgba(3, 2, 106, 0.3)'],
              data: [21,54,33,11,7]
            },
            {
              label: 'My Second dataset',
              backgroundColor: 'rgba(26,187,156,0.13)',
             // pointBorderWidth: 1,
              data: [21,54,33,11,7]
            }
          ];
        
      let decisions = <?=json_encode($decisions); ?>;
      var labels = ['dummy1','dummy2'];
     // var ctx = document.getElementById('myPieChart').getContext('2d');
      
      var currentChart;
        ctx=   new Chart(document.getElementById('myPieChart'), {
        type: 'doughnut',
        responsive:true,
        data: {
          labels: ['dummy1','dummy2'],
          datasets:updatedData,
         

        },
        options: {
            legend:false,
               legendCallback: function(chart) {
    var text = [];
    text.push('<ul class="' + chart.id + '-legend">');
    for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
        //background-color:' + chart.data.datasets[0].backgroundColor[i] + '
      text.push('<li><i class="fas fa-square" style="color:' + chart.data.datasets[0].backgroundColor[i] + '"></i> <span style="">'); //onClick="amir(event,'+chart.data.datasets[0].data[i]+')" 
      if (chart.data.labels[i]) {
        text.push(chart.data.labels[i]);
      }
      text.push('</span></li>');
    }
    text.push('</ul>');
    return text.join("");
  },
        // legend: {
        //     display: true,
        //     labels: {
        //         // fontColor: 'rgb(255, 99, 132)'
        //         // position: 'left'
        //     }
        } 
      });
    
      function updateChart() {
       console.log(labels)   ;
       // var determineChart = $('#chartType').val();
    ctx.update();
    }

    $('#chartType').on('change', updateChart);
       updateChart();

      $('#dataType').on('change', updateChart);

      

$(()=>{
    onChangebyUserVal();
    
    // init_fitToContainer(document.getElementById('myPieChart'));
})
function init_fitToContainer(canvas) {
  
    canvas.style.width = '70%';
   canvas.style.height = '70%';
      canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
}

    function onChangebyUserVal(){
           
        byUserVal = $('select#by-user').val();
      ;
        let userData = window.byUser[byUserVal];
        let toggleName = $('#toggle-indications-procedures').val();
        let toggleData = Object.values(_.groupBy(userData, a => a[toggleName]));
        let toggleKeysData = Object.keys(_.groupBy(userData, a => a[toggleName]));
            console.log({ toggleData,toggleKeysData ,decisions})
            labels=[];
            console.log({toggleKeysData})
            if(toggleName=="supervisor_decision")
         {   decisions={...decisions, 'null':'Not approved yet.', 2:'Low level indication.',3:'Medium level indication',4: 'High level indication',8:'Equivocal',1:'No decision'}
        toggleKeysData.forEach(el => {
               
        labels.push(decisions[el]) ;    
        });}
        else {
            labels=toggleKeysData;
        }
        let     countProcedures=toggleData.map(el =>el.length)   
              ctx.data.datasets.map(dataSet => {
            dataSet.data= countProcedures    
              dataSet.label= 'My First dataset';
            return dataSet
        })
        ctx.data.labels=labels
        ctx.update();
        $("#chartjs-legend").html(ctx.generateLegend());
  console.log({toggleKeysData});
    }
    $('select#by-user').change(onChangebyUserVal);
    $('#toggle-indications-procedures').change(onChangebyUserVal);
       function amir(e, datasetIndex) {
        var index = datasetIndex;
        var ci = e.view.myPieChart;
          var meta =ctx.getDatasetMeta(1);
        console.log(meta.data[0].hidden=true)

        // See controller.isDatasetVisible comment
        //meta.hidden = meta.hidden === null? !ctx.data.datasets[1].hidden : null;

        // We hid a dataset ... rerender the chart
        ctx.update();
    };
</script>
    @endsection
 