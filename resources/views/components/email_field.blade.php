<div class = "form-group">
    {{Form::label($name, $title)}}
    {{Form::email($name, $value = NULL, ['class' => 'form-control'])}}
    @if ($errors->has($name))
        <p class = "help-block">{{$errors->first($name)}}</p>
    @endif
</div>