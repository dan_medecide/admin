
<div>
  
 
@if ( !empty ($tests_json) )
@isset ($tests_json)
@foreach ($tests_json as $decisionTitle => $decisionValues)
    <div class="panel panel-default" style="margin-bottom: 0px;">
        <div class="panel-body">
            <b>  {{ ucfirst($decisionTitle) }}: </b>
     
            {{$decisionValues['score']['title']  ??  null }}
        </div>
    </div>
@endforeach
@endif 
@endisset
</div>
<div>   
  
@if (!empty ($manual_decisions) )
@isset ($manual_decisions)
@foreach ($manual_decisions as $decisionTitle => $decisionValues)
<div class="panel panel-default" style="margin-bottom: 0px;">
        <div class="panel-body">
            <b>  {{ str_replace("_" , " " ,ucfirst($decisionTitle)) }}: </b>
               
            @if( is_array($decisionValues))
          
           
            
        
            {{$decisionValues['title']  ?? null}}  
                
                    @endif
               
                
        </div>
    </div>
@endforeach
@endisset
@else
    @if (!empty($manual_decisions) )
    @foreach ($manual_decisions as $title => $code)
    <div class="panel panel-default" style="margin-bottom: 0px;">
        <div class="panel-body" >
          
            <b>  {{str_replace("_" , " " , $title) }}: </b>
            {{ $decisions[$code] }}   {{$code}}
        </div>
    </div>        
    @endforeach
    @endif
    @endif
</div>