       <div id="myModal" data-backdrop="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="text-center" id="gridSystemModalLabel"> <span id="archiveMessage"></span></h4>
              <button id="undoChanges" style="float:right" type="button" class="btn btn-default" data-dismiss="modal">Undo</button>
      </div>
      <!-- <div class="modal-body">
      
      </div> -->
      <!-- <div class="modal-footer"> -->
   
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      <!-- </div> -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
 
