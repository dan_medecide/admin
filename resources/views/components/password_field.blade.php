<div class = "form-group">
    {{Form::label($name, $title)}}
    <input type = "password" name = "{{$name}}" value = "{{$value}}" class = "form-control">
    @if ($errors->has($name))
        <p class = "help-block">{{$errors->first($name)}}</p>
    @endif
</div>