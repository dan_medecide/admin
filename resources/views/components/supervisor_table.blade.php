<div class="x_content">
	<div class="table-responsive" style="margin:10px">
		<table   class="table profiles-table  table-condensed  table-bordered text-center " cellspacing="0" width="100%">
			<thead>
				<tr class="padd-left" >
@if( auth()->user()->roles[0]->name !="supervisor")
                    
					<th class="text-center" >Teach:  </th>  
            @else   
					<th class=" text-center"  > Case#:</th> @endif
                    
					<th class=" text-center"  > User Name:</th>
					<th class=" text-center"  > Date/Time: </th>
@if( auth()->user()->roles[0]->name !="supervisor")  

                    
					<th class=" text-center"  > Completed:</th>@endif
                    
					<th class=" text-center"  > Procedure:</th>
					<th class=" text-center" style="min-width:25%"  > Supervisor's Decision :</th>
					<th class="text-center noExport" >SerenusAI's Decision:</th>
					<th class=" text-center"  > Reports:</th>
@if(auth()->user()->role=="admin")
                    
					<th class="text-center noExport"> Manual Decision:</th>   @endif
                    
					<th class="text-center" style="max-width:30%" > Notes/Comments:</th>
				</tr>
			</thead>
			<tbody>
                
                @if(isset($teach))  
                    
				<tr>
                        @foreach($teach as $key => $test)
                            
                            @if($role!="supervisor") 
                            
					<td style="  vertical-align: middle;" >
                                @if (!empty($test['manualDecision'])) Teach  @else Test @endif
                            </td> 
                            @else
                            
					<td style=" vertical-align: middle;" >
                            {{$test['patient_id']}}
                            </td style=" vertical-align: middle;" >
                            @endif
                            
					<td style=" vertical-align: middle;" > {{ $test['users_title'] }} {{ $test['first_name'] }}  {{ $test['last_name'] }}</td>
                        @if( auth()->user()->roles[0]->name !="supervisor")                  
                            
				</td>
                                @endif
                                
				<td style="vertical-align: middle;" > {{   $test['created_at'] }} </td>
                                @if(auth()->user()->roles[0]->name!="supervisor")
                                
				<td style=" vertical-align: middle;" >{{!$test['is_done'] ? "No" : "Yes"}}</td>
                                @endif
                                
				<td style=" vertical-align: middle;"> 
                                    @if(isset($test['title'] ))
                                        {{$test['proc_id'] }}. {{$test['title'] }} 
                                    @endif
                                </td>

                                
                                    @component('components.decisions', [ 
                                        'supervisor_decision_explanation' => $test['supervisor_decision_explanation'] ,
                                        'supervisor_decision' => $test['supervisor_decision'] ,
                                        'tests_json' => $test['tests_json'] ,
                                        'mappedDecisions' => $mappedDecisions,
                                        'is_done'=> $test['is_done'],
                                        'decisions' => $decisions,  'test_id'=>$test['test_id']
                                            ])
                                    @endcomponent
                                
                                
                                    @if(auth()->user()->role=="admin") 
				<td style=" vertical-align: middle;" >
                                    @component('components.manual_decisions', ['manual_decisions' => $test['tests_manual_decision']])
                                    @endcomponent
                                </td> @endif
                                
				<td  style="  vertical-align: middle;" >
                                    {{$test['notes']}}
                                </td>
			</tr>

                    @endforeach
                    

                @endif

                
		</tbody>
	</table>
</div>
 
 
 
 

 
   
   
     
 