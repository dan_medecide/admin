@extends('master')

@section('title', 'Admin System')

@section('meta')
    {!! Charts::assets() !!}
@endsection

@section('content')
    <!-- page content -->
    <div class="right_col" role="main" id="app">
        <!-- top tiles -->
        <div class="row">
            <div class="col-md-2 col-sm-4 col-xs-6">
                <div class="text-center well well-sm">
                    <h2><i class="fa fa-user"></i> Total Users <span
                                class="badge">{{$data['users']->total_count}}</span></h2>
                    <span><i class="green">{{round((float)$data['users']->since_last_week / $data['users']->total_count*100 ) . '%'}} </i> From last Week</span>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <div class="well well-sm text-center">
                    <h2><i class="fa fa-tags" aria-hidden="true"></i> Total Categories <span
                                class="badge">{{$data['categories']->total_count}}</span></h2>
                    <span><i class="green">{{round((float)$data['categories']->since_last_week / $data['categories']->total_count*100 ) . '%'}}</i> From last Week</span>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <div class="well well-sm text-center">
                    <h2><i class="fa fa-stethoscope" aria-hidden="true"></i> Total Procedures <span
                                class="badge">{{$data['procedures']->total_count}}</span></h2>
                    <span><i class="green">{{round((float)$data['procedures']->since_last_week / $data['procedures']->total_count*100 ) . '%'}} </i> From last Week</span>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <div class="well well-sm text-center">
                    <h2><i class="fa fa-check" aria-hidden="true"></i> Total Tests <span
                                class="badge">{{$data['tests']->total_count}}</span></h2>
                    <span><i class="green">{{round((float)$data['tests']->since_last_week / $data['tests']->total_count*100 ) . '%'}} </i> From last Week</span>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <div class="well well-sm text-center">
                    <h2><i class="fa fa-graduation-cap" aria-hidden="true"></i> Total Labels <span
                                class="badge">{{$data['labels']->total_count}}</span></h2>
                    <span><i class="green">{{round((float)$data['labels']->since_last_week / $data['labels']->total_count*100 ) . '%'}} </i> From last Week</span>
                </div>
            </div>
        </div>

        {{--<div class="row">--}}
        {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
        {{--<div class="panel panel-default">--}}
        {{--<div class="panel-heading">--}}
        {{--<h2 class="panel-title">Tests</h2>--}}
        {{--</div>--}}
        {{--<br>--}}
        {{--<div class="panel-body" id="tests_pie_chart" style="display: none">--}}
        {{--{!! $tests_charts['pie_chart']->render() !!}--}}
        {{--</div>--}}
        {{--<div class="panel-body" id="tests_bar_chart">--}}
        {{--{!! $tests_charts['bar_chart']->render() !!}--}}
        {{--</div>--}}
        {{--<div class="panel-footer">--}}
        {{--<button href="#" class="btn btn-default"--}}
        {{--onclick="showPieView('tests_pie_chart','tests_bar_chart')"><i--}}
        {{--class="fa fa-pie-chart"--}}
        {{--aria-hidden="true"></i> Pie Chart--}}
        {{--</button>--}}
        {{--<button class="btn btn-default"--}}
        {{--onclick="showBarView('tests_pie_chart','tests_bar_chart')"><i--}}
        {{--class="fa fa-bar-chart"--}}
        {{--aria-hidden="true"></i> Bar Chart--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
        {{--<div class="panel panel-default">--}}
        {{--<div class="panel-heading">--}}
        {{--<h2 class="panel-title">Total labels</h2>--}}
        {{--</div>--}}
        {{--<br>--}}
        {{--<div class="panel-body" id="labels_bar_chart">--}}
        {{--{!! $labels_date_chart->render() !!}--}}
        {{--</div>--}}
        {{--<div class="panel-body" id="labels_pie_chart" style="display: none">--}}
        {{--{!! $labels_procedures_chart->render() !!}--}}
        {{--</div>--}}
        {{--<div class="panel-footer">--}}
        {{--<button href="#" class="btn btn-default"--}}
        {{--onclick="showPieView('labels_pie_chart','labels_bar_chart')"><i--}}
        {{--class="fa fa-pie-chart"--}}
        {{--aria-hidden="true"></i> Pie Chart--}}
        {{--</button>--}}
        {{--<button class="btn btn-default"--}}
        {{--onclick="showBarView('labels_pie_chart','labels_bar_chart')"><i--}}
        {{--class="fa fa-bar-chart"--}}
        {{--aria-hidden="true"></i> Bar Chart--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Physicians</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table class="table text-center table-bordered table-hover table-striped nowrap"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                @if($user->hasRole('doctor'))
                                    <tr>
                                        <td><i class="fa fa-user-md fa-lg"
                                               aria-hidden="true"></i>&nbsp;{{$user->title}} {{$user->first_name}} {{$user->last_name}}
                                        </td>
                                        <td><i class="fa fa-tags"
                                               aria-hidden="true"></i>&nbsp;{{$user->category()->value('title')}}
                                        </td>
                                        <td><a href="{{url('users/' . $user->id)}}" class="btn btn-sm btn-default"><i
                                                        class="fa fa-eye" aria-hidden="true"></i> Display</a></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection