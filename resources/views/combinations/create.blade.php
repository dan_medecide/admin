<div class = "modal-header">
    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Add Combination</h4>
</div>
<div class = "modal-body">

    {!! Form::open(['route' => 'combinations.store','class' => 'bootstrap-modal-form']) !!}
    <div class = "form-group">
        {!! Form::label('title','Title*') !!}
        {!! Form::text('title',null,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('description','Description') !!}
        {!! Form::textarea('description',null,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('code','Code*') !!}
        {!! Form::text('code',null,['class' => 'form-control']) !!}
    </div>

    <div class = "form-group">
        {!! Form::label('cat_id','Category') !!}
        {!! Form::select('cat_id',$categories,null,['class' => 'form-control', 'placeholder' => 'Pick a category from the list...']) !!}
    </div>

     <!-- <div class = "form-group">
        
        {!! Form::label('procedures','Procedures (select multiple using Ctrl)') !!}
        {!! Form::select('procedures[]',$procedures,null,['class' => 'form-control','placeholder' => 'Pick a procedure from the list...','multiple' => 'multiple']) !!}
    </div>   -->
  <div class = "form-group">
      {!! Form::label('procedures','Procedures (type add from the list):') !!}
       {!! Form::label('procedures','Procedures (select multiple using Ctrl)') !!} 
      {!! Form::macro('myField', function()
       {
            return ' <input type="text"  name ="procedures[]" class="form-control procedure-input" id="procedure" data-role="tagsinput"   /> ';
        }); !!}
        {!! Form::myField();  !!}
    </div>

    <div class = "form-group">
        {!! Form::label('client_id','Clients') !!}
        {!! Form::select('client_id',$clients,null,['class' => 'form-control', 'placeholder' => 'Pick a client from the list...']) !!}
        
    </div>
   
</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "New combination has been created successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Create
    </button>
    {!! Form::close() !!}
</div>
<script >
    // let newtry =  "{{ $procedures }}" ;
    //    newtry =  JSON.stringify(newtry);
    //    newtry =  JSON.parse(newtry);
    //    console.log(newtry);
    $(function(){
      
 var procedures = "{{ $procedures->implode(",") }}";
 procedures=procedures.split(",");
       // alert("in");
   $('.procedure-input').tagsinput({
  typeahead: {
    source: procedures// ['Amsterdam', 'Washington', 'Sydney', 'Beijing', 'Cairo']
  },
  freeInput: false  
});
$('.procedure-input').on('itemAdded', function(event) {
    setTimeout(function(){
        //console.log($(">input[type=text]",".bootstrap-tagsinput"))
         console.log($('.procedure-input').val());
        $(">input[type=text]",".bootstrap-tagsinput").val("");
    }, 1);
});
})
//  $(function(){// var procedures = "{{ $procedures->implode(",") }}";// procedures=procedures.split(",");// $(document).ready(function() {//   // Call the data population//   procedureList();// });// procedureList = function() {//    // $("#procedureSearch").on('change', function(){alert(this.val()) })//   // Call TagsInput on the input, and set the typeahead source to our data//   $('#procedureSearch').focus(function(){//   alert("hii")// }).tagsinput({//     typeahead: {//       source: function() {//          return procedures ;                //       }//     }//   }); //   $('#procedureSearch').on('itemAdded', function(event) {//     // Hide the suggestions menu/     $('.typeahead.dropdown-menu').css('display', 'none');//     // Clear the typed text after a tag is added//     $('.bootstrap-tagsinput > input').val('');     
//      // $("#procedureSearch").on('change', function(){alert(_this.val()) })
</script>
