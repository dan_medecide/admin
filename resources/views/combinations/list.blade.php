 
  <div class = "container">
            <table class = "table text-center table-striped combinations-table table-bordered nowrap"
                   style = "display: none"
                   cellspacing = "0" width = "100%">
                <thead>
                <tr>
                    <th class = "text-center">Id</th>
                    <th class = "text-center">Title</th>
                    <th class = "text-center">Description</th>
                    <th class = "text-center">Procedures</th>
                    <th class = "text-center">Category</th>
                    <th class = "text-center">Client</th>
                    <th class = "text-center">Code</th>
                    <th class = "text-center noExport">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($combinations as $combination)
                    <tr draggable="true" ondragstart="drag(event)" id="{{$combination->id}}">
                        <td>{{$combination->id}}</td>
                        <td>{{$combination->title}}</td>
                        <td>{{$combination->description}}</td>
                        <td>{{str_limit($combination->procedures,40,'...')}}</td>
                        <td>
                            @if($combination->category)
                                {{$combination->category->title}}
                            @endif
                        </td>
                        <td>
                            @if($combination->client)
                                {{$combination->client->client_name}}
                            @endif
                        </td>
                        <td>{{$combination->code}}</td>
                        <td>
                        
                            <div class = "btn-group btn-group-sm">
                                <button style = "color: #73879C"
                                        class = "edit-registry btn btn-default bootstrap-modal-form-open"
                                        data-url = "{{route('combinations.edit', $combination->id)}}"><i
                                            class = "far fa-edit" aria-hidden = "true"></i> Edit
                                </button>
                                <button style = "color: #73879C" token = "{{csrf_token()}}"
                                        url = "{{route('combinations.destroy', $combination->id)}}" href = "#"
                                        class = "btn btn-default delete-confirm">
                                    <i class = "far fa-trash-alt"
                                       aria-hidden = "true"></i> Delete
                                </button>
                                  
                                   @if  ($combination->client AND head($combination->client->users)) 
                                  <a href = "{{route('combination.run' , ['combination'=> $combination->id, 'user_id' => head($combination->client->users)[0]['id'] ])}}"
                                       target="_blank" 
                                       class = "btn btn-default ">
                                     <i style=' font-weight:1000; padding:0'>{{ clientSymbol($combination->client->id) }}</i> 
                                     {{ $combination->client->client_name }}  
                                   </a>
                                      @endif 
                                  
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
 
