@extends('master')

@section('title', 'Combinations')
 
@section('content')
    <!-- page content -->
    <div class = "right_col" role = "main">
             @component('components.temp_archive_confirm', [])
            @endcomponent
    <!-- /page content -->
    @include('components.modal')
    <script type="text/javascript">const connection = "{{$connection}}"</script>

    <h2>Combinations Page</h2>

<button class="collapsible">Medical Combinations </button>

<div class="content" style="overflow:'visible'; max-height:2000px">
    @component('combinations.list', ['combinations' => $medicalCombinations ])  @endcomponent
</div>
 
<button class="collapsible">Client Combinations </button>

<div class="content" style="overflow:'visible'; max-height:2000px">
    @component('combinations.list', ['combinations' => $combinations ])  @endcomponent
</div>
<button class="collapsible">Temp Combinations </button>
<div class="content">
@component('combinations.list', ['combinations' => $combinationsTemp ])  @endcomponent
</div>

<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", toggleActiveCol);
}

function toggleActiveCol() {
    this.classList.toggle("active-col");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  };
</script>
@endsection

@section('script')
    <script src = "{{asset('js/combinations.js')}}"></script>
    

    
@endsection
