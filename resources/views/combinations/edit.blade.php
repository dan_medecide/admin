

<div class = "modal-header">
    


    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                aria-hidden = "true">&times;</span>
    </button>
    <h4 class = "modal-title">Update Combination</h4>
</div>
<div class = "modal-body">


    {!! Form::model($combination, ['route' => ['combinations.update', $combination->id],'class' => 'bootstrap-modal-form','method' => 'put']) !!}
    
    <div class = "form-group">
       {!! Form::label('procedures','Procedures') !!} 
      {!! Form::macro('myField', function()
       {
            return ' <input type="text"  name ="procedures" class="form-control procedure-input" id="procedure" data-role="tagsinput" /> ';
        }); !!}
        {!! Form::myField();  !!}
    </div>
    
 <?php  //   <div class = "form-group">
      //  {!! Form::label('title','Title') !!}
      //   {!! Form::macro('myField', function()
      // {
       //     return ' <input type="text"  name ="title" class="form-control procedure-input" id="procedure" data-role="tagsinput" /> ';
       // }); !!}
       // {!! Form::myField();  !!}
   // </div> ?>
    <div class = "form-group">
        {!! Form::label('description','Description') !!}
        {!! Form::textarea('description',null,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        {!! Form::label('code','Code') !!}
        {!! Form::text('code',null,['class' => 'form-control']) !!}
    </div>
    <div class = "form-group">
        
        {!! Form::label('category','Category') !!}
        {!! Form::select('category',$categories,$current_categories,['class' => 'form-control', 'placeholder' => 'Pick a category from the list...']) !!}
    </div>

    {{--<div class = "form-group">--}}
        {{--{!! Form::label('procedures','Procedures  !!}--}}
        {{--{!! Form::select('procedures[]',$procedures,$current_procedures,['class' => 'form-control','placeholder' => 'Pick a procedure from the list...','multiple' => 'multiple']) !!}--}}
    {{--</div>--}}
 
    <div class = "form-group">
        {!! Form::label('clients','Clients') !!}
        {!! Form::select('clients',$clients, $current_client_id,['class' => 'form-control', 'placeholder' => 'Pick a client from the list...']) !!}
    </div>


</div>
<div class = "modal-footer">
    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
    <button type = "submit" title = "Congratulations!" data-content = "Combination has been updated successfully."
            data-animation = "true"
            data-placement = "left" class = "btn btn-primary bootstrap-modal-submit">Update
    </button>
    {!! Form::close() !!}
</div>

 
<script >
   
       
        
     
     // newtry =  JSON.stringify(newtry);
      // newtry =  JSON.parse(newtry);
    //   console.log(newtry);
    $(()=>{
      var current_procedures= "{{ implode(',',$current_procedures)}}";
      console.log(current_procedures);
      //
        //  var proceduresIDS = "{{ $procedures_ids->implode('|-|') }}";
        // proceduresIDS = proceduresIDS.split("|-|");
        // proceduresIDS= proceduresIDS.map(a => a.split("@split@"));
        // console.log({proceduresIDS})

        var procedures = "{{ $procedures->implode(",") }}";
        procedures = procedures.split(",");
       // alert("in");
       // $('.procedure-input')
       console.log(current_procedures)
   $('.procedure-input').val(current_procedures).tagsinput({
  typeahead: {
    source: procedures 
  },
  freeInput: false  
});
$('.procedure-input').on('itemAdded', function(event) {
    setTimeout(function(){
        //console.log($(">input[type=text]",".bootstrap-tagsinput"))
      //  let foundVal = $('.procedure-input').val('');
      //   console.log(foundVal);
      //  let newVal =  $(">input[type=text]",".bootstrap-tagsinput")[1].textContent ;
        // console.log(proceduresIDS.find(a => a[0] == newVal )[1])
       
        //   console.log({eventtargetvalue :event.target.value.split(',')})
        //     let myArray = event.target.value.split(',');
        //     $(".procedure-ids").html('');
        //      myArray.forEach(function(name, j) {
        //          name = (proceduresIDS.find(a => a[0] == name )[0]+'('+proceduresIDS.find(a => a[0] == name )[1]+')')
        //    $(".procedure-ids").
        //   append('<button disabled>' + name + '</button>');
         // });
          
          //append(proceduresIDS.find(a => a[0] == newVal )[0]+'('+proceduresIDS.find(a => a[0] == newVal )[1]+')');
          //text(proceduresIDS.find(a => a[0] == newVal )[0]+'('+proceduresIDS.find(a => a[0] == newVal )[1]+')');
        $(">input[type=text]",".bootstrap-tagsinput").val("");
    }, 1);
});
})
//  $(function(){// var procedures = "{{ $procedures->implode(",") }}";// procedures=procedures.split(",");// $(document).ready(function() {//   // Call the data population//   procedureList();// });// procedureList = function() {//    // $("#procedureSearch").on('change', function(){alert(this.val()) })//   // Call TagsInput on the input, and set the typeahead source to our data//   $('#procedureSearch').focus(function(){//   alert("hii")// }).tagsinput({//     typeahead: {//       source: function() {//          return procedures ;                //       }//     }//   }); //   $('#procedureSearch').on('itemAdded', function(event) {//     // Hide the suggestions menu/     $('.typeahead.dropdown-menu').css('display', 'none');//     // Clear the typed text after a tag is added//     $('.bootstrap-tagsinput > input').val('');     
//      // $("#procedureSearch").on('change', function(){alert(_this.val()) })
</script>
