@extends('layouts.auth')

@section('content')
    <form class = "form-horizontal" method = "POST" action = "{{ route('password.request') }}">
        <h1>Reset Password</h1>
        {{ csrf_field() }}

        <input type = "hidden" name = "token" value = "{{ $token }}">

        <div class = "form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id = "email"
                   type = "email"
                   class = "form-control"
                   name = "email"
                   placeholder = "E-mail Address"
                   value = "{{ $email or old('email') }}"
                   required
                   autofocus>

            @if ($errors->has('email'))
                <span class = "help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>

        <div class = "form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id = "password"
                   type = "password"
                   class = "form-control"
                   placeholder = "Password"
                   name = "password"
                   required>
            @if ($errors->has('password'))
                <span class = "help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>

        <div class = "form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id = "password-confirm"
                   type = "password"
                   class = "form-control"
                   placeholder = "Confirm Password"
                   name = "password_confirmation"
                   required>

            @if ($errors->has('password_confirmation'))
                <span class = "help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
            @endif
        </div>

        <button type = "submit" class = "btn btn-default">
            Reset Password
        </button>
    </form>
@endsection
