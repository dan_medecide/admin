<?php if(1==0)  dd(session()->get('redirect_url')) ?>

@extends('layouts.auth')

@section('content')
    {!! Form::open(['route' => 'login']) !!}
    <h1>Login Please</h1>
    {{ csrf_field() }}
    @if (session('status'))
        <div class="alert alert-info">
            {{ session('status') }}
        </div>
    @endif

    <div class = "form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {{Form::email('email', old('email'), ['required autofocus','class' => 'form-control', 'placeholder' => 'Email address'])}}
        @if ($errors->has('email'))
            <span class = "help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
        @endif
    </div>

    <div class = "form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::password('password', ['required autofocus','id'=>'password-input', 'class' => 'form-control','placeholder' => 'Password']) !!}
        
        @if ($errors->has('password'))
            <span class = "help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                            </span>
        @endif
    </div>

    {{--<div class = "form-group">--}}
    {{--<div class = "col-md-6 col-md-offset-4">--}}
    {{--<div class = "checkbox">--}}
    {{--<label>--}}
    {{--<input type = "checkbox" name = "remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {!! Form::submit('Login', ['class' => 'btn btn-default']) !!}
    <a class = "btn btn-default" href = "{{ route('password.request') }}">Lost your password?</a>
    {!!Form::close()!!}

@endsection
 @section('login')
    <script crossorigin = "anonymous" integrity = "sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    src = "https://code.jquery.com/jquery-3.2.1.min.js">
</script>
 <script src="{{asset('js/login_password_eye.js')}}"></script>
 @stop
