import Vue from 'vue';
import lodash from 'lodash';
import VueLodash from 'vue-lodash/dist/vue-lodash.min';
import TestsTotalCount from './components/client_dashboard/TestsTotalCount.vue';
import TestsCountByPhysician from './components/client_dashboard/TestsTotalCountByPhysician.vue';
import ClientReportsTable from './components/client_reports/ClientReportsTable.vue';
import PhysiciansTestsCountByProcedure from './components/client_dashboard/PhysiciansTestsCountByProcedure.vue';
import ApprovedReferrals from './components/client_dashboard/ApprovedReferrals';
import KeyMap from './components/client_dashboard/KeyMap.vue';
import VueEvents from 'vue-events';
import * as uiv from 'uiv';

Vue.use(VueEvents);
Vue.use(uiv);
Vue.use(VueLodash, lodash);

new Vue({
  el: '#app',
  components: {
    ClientReportsTable,
    ApprovedReferrals,
    TestsTotalCount,
    TestsCountByPhysician,
    PhysiciansTestsCountByProcedure,
    KeyMap,
  },
});
