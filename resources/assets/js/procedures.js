let modal = $('#myModal');
var table,
  //   apiResponse,
  flag,
  selectedRow = '';
var procIdsArray = [];
let dropTarget = $('.droptarget');

$(document).ready(function() {
  window.tableToArchive = table;

  table = $('.procedures-table').DataTable({
    // rowReorder: true,
    // columnDefs: [
    //   {
    //     targets: [0],
    //     visible: false,
    //     searchable: false
    //   }
    // ],
    dom: 'Bfrtip',
    autoWidth: false,
    // order: [[3, 'desc'], [5, 'desc']],
    responsive: true,
    oLanguage: {
      //sSearch: searchFieldText,
      sInfo: infoText,
      oPaginate: {
        // sNext: nextText,
        // sPrevious: PreviousText
      }
    },
    buttons: [
      {
        text: '+ ' + addProcedureBtnText,
        action: function() {
          window.location.href = BASE_URL + '/api/procedures/api/create';
        }
      }
    ],
    initComplete: function() {
      this.api()
        .columns([0, 1, 2, 3, 4, 5])
        .every(function() {
          var column = this;
          var select = $('<select><option value=""></option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function() {
              var val = $.fn.dataTable.util.escapeRegex($(this).val());

              column.search(val ? '^' + val + '$' : '', true, false).draw();
            });

          column
            .data()
            .unique()
            .sort()
            .each(function(d, j) {
              select.append('<option value="' + d + '">' + d + '</option>');
            });
        });
      $(this).show();
    }
  });
  $('a.toggle-vis').on('click', function(e) {
    e.preventDefault();
    console.log(this.style.color);

    // Get the column API object
    var column = table.column($(this).attr('data-column'));
    // Toggle the visibility
    this.style.color = !column.visible() ? '#5a738e' : 'darkGray';
    column.visible(!column.visible());
    // this.style.color = 'darkGray' ? '#5a738e' : 'darkGray';
  });
});
$('#toggleHorizon').change(function() {
  if ($(this).prop('checked')) {
    $('#toggleHartford').bootstrapToggle('off');
    table
      .column(1)
      .search('horizon')
      .draw();
  } else
    table
      .column(1)
      .search('')
      .draw();
});
$('#toggleHartford').change(function() {
  if ($(this).prop('checked')) {
    $('#toggleHorizon').bootstrapToggle('off');
    table
      .column(1)
      .search('hartford')
      .draw();
  } else
    table
      .column(1)
      .search('')
      .draw();
});
$(document).on('click', function() {
  $('#myModal').modal('hide');
});
window.removeRow = function(procId) {
  console.log('inside remove Row');
  table
    .row($('#' + procId))
    .remove()
    .draw();
};
