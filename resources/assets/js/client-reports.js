import Vue from 'vue';
import ClientReportsTable from './components/client_reports/ClientReportsTable.vue';
new Vue({
    el: '#app',
    components: {
        ClientReportsTable
    },
})