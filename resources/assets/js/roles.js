import Vue from 'vue';
import Form from './components/Form';
import Spinner from 'vue-simple-spinner';
import Toasted from 'vue-toasted';
import lodash from 'lodash';
import VueLodash from 'vue-lodash/dist/vue-lodash.min';

Vue.use(Toasted);
Vue.use(VueLodash, lodash)

let vm = new Vue({
        el: '#app',
        components: {
            Spinner,
        },

        created() {
            this.loading = true;
        },

        mounted() {
            this.index();
        },

        data: {
            roles: [],
            role: new Form({
                name: '',
                description: '',
            }),
            loading: false,
            formTitle: 'Add Roles',
            formSubmit: 'SAVE',
        },

        methods: {
            edit(id) {
                this.role = new Form(this._.find(this.roles, (role) => {
                    return role.id === id
                }));
                this.formTitle = this.role.name;
                this.formSubmit = 'UPDATE';
            },

            index() {
                axios.get('/roles/index').then(response => {
                    this.roles = response.data;
                    setTimeout(() => {
                        this.loading = false;
                    }, '500');
                })
            },

            submit(url = '/roles') {
                this.role.post(url).then(() => {
                    this.$toasted.success("Success!", {
                        theme: "outline",
                        position: "top-right",
                        duration: 2000
                    });
                    this.clear();
                    this.index();
                }).catch(error => {
                    console.log(error);
                    this.$toasted.error("Something went wrong...", {
                        theme: "outline",
                        position: "top-right",
                        duration: 2000
                    });
                });
            },

            clear() {
                this.role = new Form({
                    name: '',
                    description: '',
                })
                this.formTitle = 'Add Roles';
                this.formSubmit = 'SAVE';
            },

            confirmDelete(id) {
                swal({
                        title: "Are you sure?",
                        text: "This action will delete this record from your database",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    () => {
                        this.delete(id);
                    });
            },

            delete(id) {
                this.role.post(`/roles/${id}/delete`).then(response => {
                    this.$toasted.success("Success!", {
                        theme: "outline",
                        position: "top-right",
                        duration: 2000
                    });
                    this.index();
                }).catch(error => {
                    console.log(error);
                    this.$toasted.error("Something went wrong...", {
                        theme: "outline",
                        position: "top-right",
                        duration: 2000
                    });
                });
            }
        }
    })
;
