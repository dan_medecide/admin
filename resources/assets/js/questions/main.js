import Vue from 'vue';
import App from './App.vue';
import Toasted from 'vue-toasted';
import Vuebar from 'vuebar';
import 'vue-instant/dist/vue-instant.css';
import VueInstant from 'vue-instant/dist/vue-instant.common';
import { store } from './store/store';
import VeeValidate from 'vee-validate';
import VueFuse from 'vue-fuse';
import VTooltip from 'v-tooltip';
import lodash from 'lodash';
import VueLodash from 'vue-lodash';
import InputText from './components/inputs/InputText';

import PortalVue from 'portal-vue';

import InputCheckbox from './components/inputs/InputCheckbox';
import Velocity from 'velocity-animate';
import InputFormula from './components/inputs/InputFormula';
import InputMulti from './components/inputs/InputMulti';
import InputNumber from './components/inputs/InputNumber';
import InputTag from './components/inputs/InputTag';
import InputSelect from './components/inputs/InputSelect';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
//import InputMultiNew from './components/inputs/InputMultiNew';
// Vue.use(Velocity);

Vue.use(PortalVue);
Vue.use(VTooltip);
Vue.use(VueFuse);
Vue.use(VeeValidate);
Vue.use(VueInstant);
Vue.use(Vuebar);
Vue.use(Toasted);
Vue.use(VueLodash, lodash);
Vue.use(ElementUI);

Vue.component('input-text', InputText);
Vue.component('input-checkbox', InputCheckbox);
Vue.component('input-formula', InputFormula);
Vue.component('input-multi', InputMulti);
//Vue.component('input-multi-new', InputMultiNew);
Vue.component('input-number', InputNumber);
Vue.component('input-tag', InputTag);
Vue.component('input-select', InputSelect);
Vue.filter('noUnderScore', function(value) {
  return value.split('_').join(' ');
});

Vue.filter('capitalize', function(value) {
  if (!value) return '';
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
});

Vue.filter('limitString', function(str) {
  if (str && str.length > 50) {
    return str.substr(0, 50) + '...';
  }
  return str;
});

export const INTERCEPTORS = axios.interceptors.response.use(
  response => {
    return response;
  },

  async error => {
    if (error.response.status == 401) {
      location.reload();
    }
    return Promise.reject(error);
  }
);

let vm = new Vue({
  el: '#app',
  store,
  template: `<App proc-id="${proc_id}" proc-title="${proc_title}" api-token="${api_token}"/>`,
  components: { App },
  ready() {
    this.grabData();
  },
  ready() {
    this.grabData();
  },
  methods: {
    //debug show json when pressed back
    grabData: function() {
      this.$http.get('/grabData').then(function(data) {
        this.logged_user = data.logged_user;
      });
    }
  }
});
window.vm = vm;
window.Velocity = Velocity;
