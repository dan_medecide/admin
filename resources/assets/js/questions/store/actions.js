// import axios from 'axios';
import _ from 'lodash';

export default {
  async setEditGlobalScoreAction(
    { state, commit, getters, dispatch },
    { comp_indication, characteristics, answer_tag, question_id, createdKeys }
  ) {
    let dummys = [];
    let globalScoreTypes = state.globalScoreTypes;
    globalScoreTypes.forEach(type => {
      let dummy = '';
      createdKeys.forEach(createdKey => {
        dummy += ' ' + createdKey + ' = ' + type.name + ' and';
      });

      dummy = dummy.substring(0, dummy.length - 3);

      dummys.push(dummy);
    });

    return console.log(dummys);

    let keyForShowingAnswers = [];
    let keyForShowingAnswer = '';

    globalScoreTypes.forEach(type => {
      keyForShowingAnswer = '';
      if (type.value)
        characteristics.forEach((characteristic, i) => {
          if (!createdKeys) {
            keyForShowingAnswer +=
              ' @' +
              //   type.value +
              answer_tag +
              ' ' +
              getters.unCapitalize(characteristic) +
              ' = ' +
              type.name +
              ' and';
          } else {
            keyForShowingAnswer +=
              ' ' + createdKeys[i] + ' = ' + type.name + ' and';
          }
        });
      keyForShowingAnswer = keyForShowingAnswer.substring(
        0,
        keyForShowingAnswer.length - 3
      );
      //console.log(KeyForShowingAnswer);
      //console.log("HI VALUE" + type.name + " " + type.value);
      if (keyForShowingAnswer)
        keyForShowingAnswers.push({
          keyForShowingAnswer,
          sub_answer: type.name,
          weight: type.value,
          created_keys: `${answer_tag} ${type.name}`
        });
    });
    return console.log({ keyForShowingAnswers });
    let payload = {
      comp_indication,
      keyForShowingAnswers,
      question_id
    };
    if (characteristics.length) {
      dispatch('globalScoreApi', payload);
      //   await this.globalScoreApi(payload);
    } else console.log('no characteristics.length ');
  },

  async setGlobalScoreAction(
    { state, commit, getters, dispatch },
    { comp_indication, characteristics, answer_tag, question_id, createdKeys }
  ) {
    let keyForShowingAnswers = [];
    let keyForShowingAnswer = '';
    let globalScoreTypes = state.globalScoreTypes;
    globalScoreTypes.forEach(type => {
      keyForShowingAnswer = '';
      if (type.value)
        characteristics.forEach((characteristic, i) => {
          if (!createdKeys) {
            keyForShowingAnswer +=
              ' @' +
              //   type.value +
              answer_tag +
              ' ' +
              getters.unCapitalize(characteristic) +
              ' = ' +
              type.name +
              ' and';
          } else {
            //EDIT MODE@!!!
            keyForShowingAnswer +=
              ' ' + '@' + createdKeys[i] + ' = ' + type.name + ' and';
          }
        });
      keyForShowingAnswer = keyForShowingAnswer.substring(
        0,
        keyForShowingAnswer.length - 3
      );
      //console.log(KeyForShowingAnswer);
      //console.log("HI VALUE" + type.name + " " + type.value);
      if (keyForShowingAnswer)
        keyForShowingAnswers.push({
          keyForShowingAnswer,
          sub_answer: type.name,
          weight: type.value,
          created_keys: `${answer_tag} ${type.name}`
        });
    });
    let payload = {
      comp_indication,
      keyForShowingAnswers,
      question_id
    };
    if (characteristics.length) {
      dispatch('globalScoreApi', payload);
      //   await this.globalScoreApi(payload);
    } else console.log('no characteristics.length ');
  },
  async displayGroups({ state, commit }, payload) {
    // try {
    let { data: displayGroups } = await axios.get(`/display-groups`);
    commit('setDisplayGroups', displayGroups);

    //   if (answers.error) {
    //     console.log(answers);
    //     throw answers.error;
    //   } else return true;
    // } catch (error) {
    //   if (error) {
    //     //   commit('addAnswersValidationErrors', error);
    //   }
    //   return false;
    // }
  },

  async updateOrCreateGroup({ state, commit, dispatch }, payload) {
    try {
      //? modalType = display-groups || answer-groups
      let { data: answers } = await axios.post(`/${state.modalType}`, payload);
      if (answers.error) {
        console.log(answers);
        throw answers.error;
      } else {
        await dispatch(_.camelCase(state.modalType));
        return true;
      }
    } catch (error) {
      if (error) {
        commit('addAnswersValidationErrors', error);
      }
      return false;
    }
  },
  async globalScoreApi({ state, commit }, payload) {
    let {
      keyForShowingAnswers,
      question_id,
      comp_indication: indication
    } = payload;
    //alert(question_id);
    //console.log(KeyForShowingAnswers);
    alert(indication);
    let { data: answers } = await axios.post(
      `/api/questions/global_score/${question_id}/create`,
      { keyForShowingAnswers, indication }
    );
  },
  async updateAnswersDisplayGroupsMultiLabels(
    { state, commit },
    display_group
  ) {
    let { data: answers } = await axios.post(
      `/display-group/multi-labels/update/${display_group.replace('/', '')}`,
      { answer: state.answer }
    );
    // commit('setCompareLabelForDisplayGroup', { answers, param_id });
    // state.answersDisplayGroup = answersDisplayGroup;
    // console.log(state.answersDisplayGroup);
  },
  async compareLabelforDisplayGroup({ state, commit }, param_id) {
    let { data: answers } = await axios.post(
      `/display-group/${state.answer.question_id}`,
      {
        //  display_groups: state.answer.display_groups[0]
      }
    );
    commit('setCompareLabelForDisplayGroup', { answers, param_id });
    // state.answersDisplayGroup = answersDisplayGroup;
    // console.log(state.answersDisplayGroup);
  },
  getAnswerParams({ commit }) {
    return axios.get(`/params`);
  },
  async updateAnswerParams({ state, commit }, id) {
    // //? Modifiy AnswerGroupProbability!
    // if (state.answer.answer_groups.join().trim())
    //   await commit('modifyAnswerGroupProbability', id);
    // else alert('no an group');
    //return false;
    return axios.post(`/answer-params/${id}`, { params: state.params });
  },
  async editAnswerParams({ commit, dispatch }, id) {
    let response = await dispatch('getAnswerParams');
    commit('setEditing', {
      parent: id,
      type: 'answerParams'
    });
    commit('setAnswerParams', response.data);
  },

  async editAnswerParamsNew({ commit, dispatch }, id) {
    let response = await dispatch('getAnswerParams');
    commit('setEditingParams', {
      parent: id,
      type: 'answerParams'
    });
    commit('setAnswerParams', response.data);
  },
  getQuestions({ state, commit, dispatch }) {
    commit('setLoading', true);
    axios
      .get(`/api/questions/${proc_id}`)
      .then(response => {
        dispatch('setQuestionsDisplay', response.data)
          .then(questions => {
            commit('setQuestions', questions);
            commit('setAllQuestions', questions);
            commit('setLoading', false);
          })
          .catch(error => console.error(error));
      })
      .catch(error => {
        console.log(error);
      });
  },

  setQuestionsDisplay({ state }, questions) {
    questions.forEach((question, index) => {
      if (state.visible.includes(question.id)) {
        questions[index].display = true;
      }
    });

    return questions;
  },

  duplicateQuestion({ state, dispatch }, { id, imported }) {
    let proc_id = '';
    if (imported) proc_id = 'proc_id=' + state.questions[0].proc_id;
    axios
      .post(`/api/questions/${id}/duplicate?${proc_id}`)
      .then(() => {
        vm.$toasted.success('Question Duplicated!', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
        dispatch('getQuestions');
      })
      .catch(error => {
        console.log(error);
        vm.$toasted.error('Something went wrong...', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
      });
  },

  editQuestion({ commit }, id) {
    axios.get(`/api/questions/${id}/edit`).then(({ data }) => {
      commit('setQuestion', { ...data, errors: {} });
      commit('setEditing', false);
      vm.$nextTick(() => {
        commit('setEditing', { type: 'question', parent: null });
      });
    });
  },

  async deleteQuestion({ dispatch, state }, id) {
    try {
      let response = await axios.post(`/api/questions/${id}/delete`);
      dispatch('getQuestions');
      return response;
    } catch (e) {
      throw new Error(e);
    }
  },

  createQuestion({ commit }) {
    commit('resetQuestion');
    commit('setEditing', false);
    vm.$nextTick(() => {
      commit('setEditing', { type: 'question', parent: null });
    });
  },

  async getKeys({ commit }, query) {
    try {
      let { data } = await axios.get(`/api/tags/${proc_id}`);
      commit('setKeys', Object.values(data));
      return data;
    } catch (error) {
      console.error(error);
    }
  },

  async getIndications({ commit }) {
    try {
      let { data } = await axios.get(`/api/indications/${proc_id}`);
      commit('setIndications', Object.values(data));
      return data;
    } catch (error) {
      console.error(error);
    }
  },

  async getAnswerGroups({ commit }) {
    try {
      let { data } = await axios.get(`/api/answers-groups/${cat_id}`);
      commit('setAnswerGroups', Object.values(data));
      return data;
    } catch (error) {
      console.error(error);
    }
  },

  async getDisplayGroups({ commit }, questionId) {
    try {
      let { data } = await axios.get(`/api/display-groups/${questionId}`);
      commit('setDisplayGroups', Object.values(data));
      return data;
    } catch (error) {
      console.error(error);
    }
  },

  storeQuestion({ dispatch, state, commit }) {
    axios
      .post(`/api/questions/${proc_id}`, state.question)
      .then(() => {
        commit('resetQuestion');
        commit('setEditing', false);
        vm.$nextTick(() => {
          commit('setEditing', { type: 'question', parent: null });
        });
        dispatch('getQuestions');
        vm.$toasted.success('Question Created!', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
      })
      .catch(({ response }) => {
        commit('setValidationErrors', {
          target: 'question',
          errors: response.data
        });
        vm.$toasted.error('Something went wrong...', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
      });
  },

  updateQuestion({ dispatch, state, commit }) {
    axios
      .post(`/api/questions/${state.question.id}/update`, state.question)
      .then(() => {
        dispatch('getQuestions');
        vm.$toasted.success('Question Updated!', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
      })
      .catch(({ response }) => {
        commit('setValidationErrors', {
          target: 'question',
          errors: response.data
        });
        vm.$toasted.error('Something went wrong...', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
      });
  },

  createAnswer({ commit }, parent) {
    commit('resetAnswer');
    commit('setEditing', false);
    vm.$nextTick(() => {
      commit('setEditing', { type: 'answer', parent });
    });
  },

  editAnswer({ state, commit, dispatch }, id) {
    axios.get(`/api/answers/${id}/edit`).then(({ data }) => {
      commit('setAnswer', { ...data, errors: {} });
      commit('setEditing', false);
      vm.$nextTick(() => {
        //   commit('setAnswerParams', response.data);
        commit('setEditing', {
          type: 'answer',
          parent: data.question_id
        });
      });
    });
  },

  async duplicateAnswer({ dispatch, state }, id) {
    try {
      let response = await axios.post(`/api/answers/${id}/duplicate`);
      dispatch('getQuestions');
      return response;
    } catch (e) {
      throw new Error(e);
    }
  },

  async deleteAnswer({ dispatch, state }, id) {
    try {
      let response = await axios.post(`/api/answers/${id}/delete`);
      dispatch('getQuestions');
      return response;
    } catch (e) {
      throw new Error(e);
    }
  },

  storeAnswer({ dispatch, state, commit }) {
    axios
      .post(`/api/answers/${state.editing.parent}`, state.answer)
      .then(() => {
        vm.$toasted.success('Answer Created!', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
        const parent = state.editing.parent;
        commit('resetAnswer');
        commit('setEditing', false);
        vm.$nextTick(() => {
          commit('setEditing', { type: 'answer', parent });
        });
        dispatch('getQuestions');
      })
      .catch(({ response }) => {
        commit('setValidationErrors', {
          target: 'answer',
          errors: response.data
        });
        vm.$toasted.error('Something went wrong...', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
      });
  },

  updateAnswer({ dispatch, state, commit }) {
    dispatch('updateAnswerParams', state.answer.id);
    axios
      .post(`/api/answers/${state.answer.id}/update`, state.answer)
      .then(() => {
        vm.$toasted.success('Answer Updated!', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
        dispatch('getQuestions');
      })
      .catch(({ response }) => {
        commit('setValidationErrors', {
          target: 'answer',
          errors: response.data
        });
        vm.$toasted.error('Something went wrong...', {
          theme: 'outline',
          position: 'top-right',
          duration: 2000
        });
      });
    if (state.answer.display_groups[0])
      dispatch(
        'updateAnswersDisplayGroupsMultiLabels',
        state.answer.display_groups[0]
      );
  },

  async validateProcedure({ state, commit, getters, dispatch }) {
    commit('toggleValidatingProcedure');
    commit('toggleShowValidatingProcedure');
    commit('clearValidationErrors');
    let answers = [];

    //? Duplicate Keys Answers
    //!Error :
    let error = getters.duplicatedKeysError;
    console.log({ error });
    if (error) {
      //console.log({ duplicatePrioritiesQuestions: error });
      commit('addDuplicatedKeysErrors', error);
      error = false;
    }

    //? Duplicate Priorities Questions
    //!Error :
    error = getters.duplicatePrioritiesQuestions;
    //console.log({ duplicatePrioritiesQuestions: error });
    if (error) commit('addQuestionsValidationErrors', error);
    error = false;

    //? Questions
    //!Error :
    for (let question of state.questions) {
      if (question.answers.length > 0) answers.push(question.answers);
      if (!question.hasOwnProperty('tag') || !question['tag']) continue;
      error = await dispatch('validateQuestion', question);
      if (error) commit('addQuestionsValidationErrors', error);
      error = false;

      //? Duplicate Priorities Answers
      //!Error :
      error = getters.duplicatePrioritiesAnswers(question);
      if (error) commit('addQuestionsValidationErrors', error);
    }
    //?Answers
    //!Error :
    for (let answer of _.flatMap(answers)) {
      let error = await dispatch('validateAnswer', answer);
      if (error) commit('addAnswersValidationErrors', error);
    }

    commit('toggleValidatingProcedure');
  },

  async validateQuestion({ dispatch }, question) {
    try {
      await dispatch('validateExpression', {
        procedureId: proc_id,
        expression: question.tag
      });
    } catch ({ response: { data: result } }) {
      return {
        id: question.id,
        title: question.title_doctor,
        key: question.tag,
        error: result.message
      };
    }
  },

  async validateAnswer({ dispatch }, answer) {
    let answerFormulaFields = [
      'weight_formula',
      'keys_for_showing_answer',
      'vas_auto_answer_formula',
      'auto_keys',
      'keys_for_end_test'
    ];

    for (let field of answerFormulaFields) {
      if (answer[field] == null || answer[field] == '') continue;

      try {
        await dispatch('validateField', { answer, field });
      } catch ({ response: { data: result } }) {
        return {
          id: answer.id,
          title: answer.title,
          keyType: field,
          key: answer[field],
          questionId: answer.question_id,
          error: result.message
        };
      }
    }
  },

  async validateField({ dispatch }, payload) {
    try {
      console.log(payload.answer[payload.field]);
      await dispatch('validateExpression', {
        procedureId: proc_id,
        expression: payload.answer[payload.field]
      });
    } catch (e) {
      throw e;
    }
  },

  combineQuestions({ state }, payload) {
    return axios.post('/api/questions/combine', {
      first: payload.firstQuestion,
      second: payload.secondQuestion,
      firstAnswers: payload.firstAnswers,
      secondAnswers: payload.secondAnswers
    });
  },

  filterQuestions({ state, commit }, event) {
    let options = {
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ['title_doctor', 'id']
    };
    let term = event.target.value;
    vm.$search(term, state.allQuestions, options).then(results => {
      commit('setQuestions', results.length > 0 ? results : state.allQuestions);
    });
  },

  getNotes({ commit }) {
    axios.get('/notes').then(response => {
      commit('setNotes', response.data);
    });
  },

  updateNotes({ commit }, value) {
    return axios.post('/notes', { title: value });
  },

  async splitAnswer({ dispatch }, { isSingleSubAnswer, characteristics, id }) {
    await axios.post(`/api/answers/${id}/split`, {
      characteristics,
      isSingleSubAnswer
    });
    return dispatch('getQuestions');
  },

  async validateExpression({ commit }, payload) {
    let extension = location.hostname.split('.');
    extension = extension[extension.length - 1];

    let url =
      extension == 'dev'
        ? 'https://medecide-api.dev'
        : //: 'https://core.medecide.net/public';
          'https://corebeta.serenusai.com/public';
    //'https://core.serenusai.com/public';
    if (location.hostname.includes('localhost'))
      //   url = 'http://medecide-api.localhost';
      url = 'http://localhost/medecide-api/public';

    // let { data: indicationSiblings } = await axios.get(
    //   `/api/tags/${proc_id}/indication-siblings/`
    // );

    return axios.post(`${url}/expressions-validation/${payload.procedureId}`, {
      expression: payload.expression
      // indicationSiblings: indicationSiblings.map(ind => `#score ${ind}`)
    });
  },

  getProcedures({ commit }) {
    return axios.get('/procedures-list');
  }
};
