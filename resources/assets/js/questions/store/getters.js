export default {
  unCapitalize: () => string => {
    return string.charAt(0).toLowerCase() + string.slice(1);
  },
  questionOfSplitAnswer: state => question_id => {
    return state.allQuestions.find(q => q.id == question_id);
  },
  compareGlobalLabel(state) {
    // let value = state.answersDisplayGroup[0].pivot.value || null;
    // return state.answersDisplayGroup.every(answer => {
    //   console.log(answer.pivot.value);
    //   return answer.pivot.value == value;
    // });
  },
  getDisplayGroup(state, getters) {
    try {
      return getters.questionAnswers.filter(
        answer => answer.display_groups[0] == state.answer.display_groups[0]
      );
    } catch (err) {
      return {};
    }
  },
  questionAnswers(state) {
    try {
      return state.allQuestions.find(q => q.id == state.answer.question_id)
        .answers;
    } catch (err) {
      return {};
    }
  },
  checkboxParams({ params }) {
    return params.filter(param => param.type == 'checkbox');
  },
  validationParams({ params }) {
    return params.filter(param => param.type == 'validation');
  },
  regularParams({ params }) {
    return params.filter(param => param.type != 'checkbox');
  },
  paramValue: state => paramId => {
    //console.log(state.params.find(param => param.id == paramId).value);
    //console.log('state.params');
    return state.params.find(param => param.id == paramId).value;
  },

  getErrors: state => payload => {
    if (state[payload.target].errors.hasOwnProperty(payload.field)) {
      return state[payload.target].errors[payload.field].join();
    }
  },

  hasErrors: state => payload => {
    return payload.target == 'question'
      ? state.question.errors.has(payload.field)
      : state.answer.errors.has(payload.field);
  },

  hasAnyErrors: state => target => {
    return Object.keys(state[target].errors).length > 0;
  },

  isEdited: state => id => {
    return state.question.id === id;
  },

  isEditingQuestion(state) {
    return state.editing.type === 'question';
  },

  isEditingAnswerParams(state) {
    return state.editing.type === 'answerParams';
  },

  vasDisabled(state) {
    return state.answer.weight > 0 || state.answer.weight < 0;
  },
  getParam: () => (params, title) => {
    try {
      let param = params.find(item => item.title == title);
      return param ? param.pivot.value : '';
    } catch (err) {}
  },
  findDuplicateInArray: () => arra1 => {
    if (arra1.length <= 1) return false;
    var object = {};
    var result = [];
    console.log({ arra1 });
    arra1.forEach(function(item) {
      if (!object[item]) object[item] = 0;
      object[item] += 1;
    });

    for (var prop in object) {
      if (object[prop] >= 2) {
        result.push(prop);
      }
    }
    if (result.length < 1) return false;
    return result;
  },
  duplicatePrioritiesAnswers: (state, getters) => question => {
    let priorities = question.answers.map(a => a.priority);
    let duplicatePriorities = getters.findDuplicateInArray(priorities);
    if (!duplicatePriorities) return false;
    let message = '';
    console.log({ DUPLICATES: duplicatePriorities });
    duplicatePriorities.forEach(duplicatePriority => {
      message +=
        `| Answers with priority  ${duplicatePriority} => ` +
        question.answers
          .filter(ans => ans.priority == duplicatePriority)
          .map(ans => ans.id)
          .join();
    });
    message += ' |';
    return {
      error: message,
      id: question.id,
      key: ' ',
      keyType: ' ',
      title: 'Duplicate priorities in Answers!'
    };
    //  if (error) commit('addQuestionsValidationErrors', error);
  },

  duplicatePrioritiesQuestions(state, getters) {
    let duplicatePriorities = getters.findDuplicateInArray(
      state.questions.map(q => q.priority)
    );

    if (!duplicatePriorities) return false;
    let message = '';
    console.log({ DUPLICATES: duplicatePriorities });
    duplicatePriorities.forEach(duplicatePriority => {
      message +=
        `| Questions with priority  ${duplicatePriority} => ` +
        state.questions
          .filter(q => q.priority == duplicatePriority)
          .map(q => q.id)
          .join();
    });
    message += ' |';
    return {
      error: message,
      id: '',
      key: ' ',
      keyType: ' ',
      title: 'Duplicate questions priority!'
    };
  },
  questionEdit(state, getters) {
    let keys = Object.keys(state.question)
      .map((v, i) => {
        if (v.startsWith('vas')) return false;
        return {
          title: v,
          name: String(i),
          content: `tab ${v} , ${i}`
        };
      })
      .filter(a => a != false);
    return [...keys, getters.vases];
  },
  vases(state) {
    let keys = Object.keys(state.question)
      .filter(n => n.startsWith('vas'))
      .map((v, i) => {
        return {
          title: v,
          name: String(i),
          content: `tab ${v} , ${i}`
        };
      });

    return {
      title: 'VASES',
      name: String(123456),
      content: keys
    };
  },
  answerEdit(state, getters) {
    let keys = Object.keys(state.answer)
      .map((v, i) => {
        if (v.startsWith('vas')) return false;
        return {
          title: v,
          name: String(i),
          content: `tab ${v} , ${i}`
        };
      })
      .filter(a => a != false);
    return [...keys, getters.vases];
  },
  questionsByID({ questions }) {
    return _.groupBy(
      _.flatten(questions.map(a => a.answers)),
      q => q.question_id
    );
  },
  byAnswerGroups(s, { questionsByID }) {
    let byID = { ...questionsByID };
    for (let id in byID) {
      byID[id] = _.groupBy(byID[id], a => a.answer_groups[0]);
    }
    return byID;
  },
  answers(state) {
    try {
      //   return _.flatMap(state.questions, n => n.answers.map(a => _.head(a.tag)));
      return _.flatten(state.questions.map(q => q.answers));
    } catch (error) {}
  },
  procedureKeys(state, getters) {
    try {
      //   return _.flatMap(state.questions, n => n.answers.map(a => _.head(a.tag)));
      return getters.answers.map(a => {
        return { id: a.id, key: _.head(a.tag), question_id: a.question_id };
      });
    } catch (error) {}
  },
  duplicatedKeys(state, getters) {
    try {
      //   return _.flatMap(state.questions, n => n.answers.map(a => _.head(a.tag)));
      let obj = _.groupBy(getters.procedureKeys, 'key');
      for (let name in obj) {
        if (obj[name].length < 2) delete obj[name];
      }
      return obj;
    } catch (error) {}
  },
  duplicatedKeysError(state, getters) {
    if (!getters.duplicatedKeys) return false;
    try {
      //   return _.flatMap(state.questions, n => n.answers.map(a => _.head(a.tag)));
      let arr = [];
      for (let name in getters.duplicatedKeys) {
        let me = {
          error:
            "same key answers! ID's: " +
            getters.duplicatedKeys[name].map(k => k.id).join(','),
          id: '',
          key: name,
          keyType: ' ',
          title: 'Duplicate Answer Keys!'
        };
        arr.push(me);
      }
      return arr;
    } catch (error) {}
  }
};
