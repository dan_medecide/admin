export default {
  focusCreatedKeys(state, focus) {
    state.activateFocusCreatedKeys = !state.activateFocusCreatedKeys;
  },
  setGlobalScoreTypes(state, globalScoreTypes) {
    state.globalScoreTypes = globalScoreTypes;
  },
  setColorOfAnswerRow(state, ColorOfAnswerRow) {
    state.ColorOfAnswerRow = ColorOfAnswerRow;
  },
  setMinusVas(state) {
    state.minusVas = !state.minusVas;
  },
  setPlusVas(state) {
    state.plusVas = !state.plusVas;
  },
  setCompareLabelForDisplayGroup(state, { answers, param_id }) {
    let filtered = answers.filter(
      answer => answer.display_groups[0] == state.answer.display_groups[0]
    );
    filtered = answers.map(answer =>
      answer.params.find(param => param.pivot.param_id == param_id)
    );
    state.answersDisplayGroup = filtered.filter(answer => answer > '');
    // console.log(state.answersDisplayGroup);
  },

  setAnswerParamsOld(state, params) {
    //   alert('params.pivot');

    state.params = params.map(param => {
      console.log(param.answers);
      let answer = param.answers.find(answer => {
        return answer.id === state.editingParams.parent;
      });
      return {
        id: param.id,
        value: answer ? answer.pivot.value : '',
        title: param.title,
        type: param.type,
        errors: []
      };
    });
    // console.log(state.params);
  },
  setAnswerParams(state, params) {
    //alert(state.editingParams.parent);

    state.params = params.map(param => {
      //  console.log(param.answers);
      let answer = param.answers.find(answer => answer.id == state.answer.id);

      //console.log(state.answer.id);
      return {
        id: param.id,
        value: answer ? answer.pivot.value : '',
        title: param.title,
        type: param.type,
        errors: []
      };
    });
    // console.log('state.params');
    // console.log(state.params);
  },
  updateParam(state, updatedParam) {
    console.log({ updatedParam });
    state.params.find(param => param.id == updatedParam.id).value =
      updatedParam.value;
    return console.log(state.params);
  },
  updateParamNew(state, updatedParam) {},
  setQuestions(state, questions) {
    state.questions = questions;
  },

  setAllQuestions(state, questions) {
    state.allQuestions = questions;
  },

  setQuestion(state, question) {
    state.question = question;
  },

  updateQuestionField(state, payload) {
    //alert(payload);
    let field = state.question[payload.field];
    if (Array.isArray(field) && !Array.isArray(payload.value)) {
      state.question[payload.field].push(payload.value);
    } else {
      state.question[payload.field] = payload.value;
    }
  },

  updateAnswerField(state, payload) {
    console.log('inside the lion!');
    console.log(payload);
    let field = state.answer[payload.field];
    console.log(state.answer[payload.field]);

    if (
      Array.isArray(field) &&
      !Array.isArray(payload.value) &&
      payload.field != 'validation'
    ) {
      state.answer[payload.field].push(payload.value);
    } else {
      state.answer[payload.field] = payload.value;
    }
  },

  setValidationErrors(state, payload) {
    console.log({ payload });
    state[payload.target].errors = payload.errors;
    console.log(state[payload.target].errors);
  },

  setAnswer(state, question) {
    state.answer = question;
  },

  resetQuestion(state) {
    state.question = {
      errors: {},
      is_auto: '',
      is_general: '',
      title_doctor: '',
      title: '',
      question_type: '',
      answers_type: '',
      explanation: '',
      priority: '',
      question_tag: [],
      indication: [],
      tag: '',
      tag_teach: '',
      min_score: '',
      max_score: '',
      vas_min: '',
      vas_max: '',
      vas_min_display: '',
      vas_max_display: '',
      vas_step: '',
      numric_min_range: '',
      numric_max_range: ''
    };
  },

  resetAnswer(state) {
    state.answer = {
      errors: {},
      title: '',
      weight: '',
      weight_formula: '',
      display_groups: [],
      vas_auto_answer_formula: '',
      answer_groups: [],
      question_id: '',
      next_question_id: '',
      priority: '',
      tag: [],
      end_test: '',
      keys_for_end_test: '',
      end_test_text: '',
      justification: '',
      auto_keys: '',
      force_score: '',
      notes: '',
      vas_min: '',
      vas_max: '',
      vas_min_display: '',
      vas_max_display: '',
      vas_step: '',
      vas_explanation: '',
      keys_for_showing_answer: '',
      keys_for_showing_answer_teach: '',
      indication: [],
      indication_input: '',
      keys_for_showing_answer_teach: '',
      vas_intermediate_score: [],
      vas_intermediate_display: []
    };
  },

  setLoading(state, value) {
    state.loading = value;
  },

  setKeys(state, keys) {
    console.log({ keys });
    state.keys = keys;
  },

  setAnswerGroups(state, groups) {
    state.answerGroups = groups;
  },

  setDisplayGroups(state, groups) {
    state.displayGroups = groups;
  },

  updateKeys(state, keys) {
    state.keys.push(keys);
  },

  setIndications(state, indications) {
    state.indications = indications;
  },

  updateIndications(state, indication) {
    state.indications.push(indication);
  },

  spliceFromParam(state, payload) {
    let index = state[payload.target][payload.field].indexOf(payload.value);
    state[payload.target][payload.field].splice(index, 1);
  },

  setEditing(state, value) {
    state.editing = value;
  },
  setEditingAnswer(state, value) {
    state.editing = value;
  },
  setEditingParams(state, value) {
    state.editingParams = value;
  },
  //   setEditingParamsNew(state, value) {
  //     // console.log('value');
  //     alert('in' + value);
  //     state.editingParams = value;
  //   },
  clearAllErrors(state) {
    state.answer.errors = {};
  },
  clearErrors(state, payload) {
    if (state[payload.target].errors.hasOwnProperty(payload.field)) {
      let errors = Object.assign({}, state[payload.target].errors);
      delete errors[payload.field];
      state[payload.target].errors = errors;
    }
  },

  toggleVisible(state, question_id) {
    if (state.visible.includes(question_id)) {
      state.visible.splice(state.visible.indexOf(question_id), 1);
    } else {
      state.visible.push(question_id);
    }
  },

  toggleValidatingProcedure(state) {
    state.validatingProcedure = !state.validatingProcedure;
  },

  toggleShowValidatingProcedure(state) {
    state.showProcedureValidation = !state.showProcedureValidation;
  },
  addDuplicatedKeysErrors(state, error) {
    alert('inside!');
    state.questionsValidationErrors.push(...error);
  },
  addQuestionsValidationErrors(state, error) {
    state.questionsValidationErrors.push(error);
  },
  addAnswersValidationErrors(state, error) {
    state.answersValidationErrors.push(error);
  },
  onNewEdit(state, newEdit) {
    state.newEdit = newEdit;
  },
  clearValidationErrors(state) {
    state.questionsValidationErrors = [];
    state.answersValidationErrors = [];
  },

  toggleCombineQuestions(state) {
    state.combinations.show = !state.combinations.show;
  },
  toggleImportQuestion(state) {
    state.importQuestionShow = !state.importQuestionShow;
    state.combinations.show = false;
  },
  setCombinationQuestion(state, payload) {
    state.combinations[payload.target] = state.questions.find(question => {
      return question.id === Number(payload.value);
    });
  },

  resetCombinations(state) {
    state.combinations.firstQuestion = '';
    state.combinations.secondQuestion = '';
    state.combinations.firstAnswers = [];
    state.combinations.secondAnswers = [];
    state.combinations.firstAnswersAll = false;
    state.combinations.secondAnswersAll = false;
  },

  setCombinationAnswers(state, payload) {
    state.combinations[payload.target] = payload.value;
  },

  selectAllCombinationAnswers(state, target) {
    e;
    state.combinations[target] = [];
    state.combinations[target + 'All'] = !state.combinations[target + 'All'];
  },

  setNotes(state, notes) {
    state.notes = notes;
  },
  setModalType(state, modalType) {
    state.modalType = modalType;
  },
  modifyAnswerGroupProbability(state, answer_id) {
    let answerGroupProbabilityValue = state.params.find(
      a => a.title == 'answer_groups_probability'
    );
    console.log({ answerGroupProbabilityValue });

    console.log({
      QuestionAnswers: _.groupBy(
        state.questions.find(q => q.answers[0].id == answer_id).answers,
        a => a.answer_groups[0]
      )
    });
  }
};
