$('.reports-table').DataTable({
    dom: "Bfrtip",
    autoWidth: false,
    order: [[0, "desc"]],
    responsive: true,
    initComplete: function () {
        this.api().columns([0, 1, 2, 3, 4, 5, 6, 7]).every(function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });

            column.data().unique().sort().each(function (d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
        $(this).show();
    },
})