import Vue from 'vue';
import Toasted from 'vue-toasted';
import lodash from 'lodash';
import VueLodash from 'vue-lodash/dist/vue-lodash.min';

Vue.use(Toasted);
Vue.use(VueLodash, lodash);

let vm = new Vue({
        el: '#app',
        components: {

        },
    })
;
