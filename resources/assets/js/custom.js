var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
  $BODY = $('body'),
  $MENU_TOGGLE = $('#menu_toggle'),
  $SIDEBAR_MENU = $('#sidebar-menu'),
  $SIDEBAR_FOOTER = $('.sidebar-footer'),
  $LEFT_COL = $('.left_col'),
  $RIGHT_COL = $('.right_col'),
  $NAV_MENU = $('.nav_menu'),
  $FOOTER = $('footer');

// Sidebar
function init_sidebar() {
  // TODO: This is some kind of easy fix, maybe we can improve this
  var setContentHeight = function() {
    // reset height
    $RIGHT_COL.css('min-height', $(window).height());

    var bodyHeight = $BODY.outerHeight(),
      footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
      leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
      contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

    // normalize content
    contentHeight -= $NAV_MENU.height() + footerHeight;

    $RIGHT_COL.css('min-height', contentHeight);
  };

  $SIDEBAR_MENU.find('a').on('click', function(ev) {
    console.log('clicked - sidebar_menu');
    var $li = $(this).parent();

    if ($li.is('.active')) {
      $li.removeClass('active active-sm');
      $('ul:first', $li).slideUp(function() {
        // setContentHeight();
      });
    } else {
      // prevent closing menu if we are on child menu
      if (!$li.parent().is('.child_menu')) {
        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
        $SIDEBAR_MENU.find('li ul').slideUp();
      } else {
        if ($BODY.is('.nav-sm')) {
          $SIDEBAR_MENU.find('li').removeClass('active active-sm');
          $SIDEBAR_MENU.find('li ul').slideUp();
        }
      }
      $li.addClass('active');

      $('ul:first', $li).slideDown(function() {
        // setContentHeight();
      });
    }
  });

  // toggle small or large menu
  $MENU_TOGGLE.on('click', function() {
    console.log('clicked - menu toggle');
    $('.side-menu-logo').toggle();

    if ($BODY.hasClass('nav-md')) {
      $SIDEBAR_MENU.find('li.active ul').hide();
      $SIDEBAR_MENU
        .find('li.active')
        .addClass('active-sm')
        .removeClass('active');
      $('#sidebar-menu').css('margin-top', 0);
    } else {
      $SIDEBAR_MENU.find('li.active-sm ul').show();
      $SIDEBAR_MENU
        .find('li.active-sm')
        .addClass('active')
        .removeClass('active-sm');
      $('#sidebar-menu').css('margin-top', '135px');
    }

    $BODY.toggleClass('nav-md nav-sm');

    // setContentHeight();

    // $('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
  });

  // check active menu
  $SIDEBAR_MENU
    .find('a[href="' + CURRENT_URL + '"]')
    .parent('li')
    .addClass('current-page');

  $SIDEBAR_MENU
    .find('a')
    .filter(function() {
      return this.href == CURRENT_URL;
    })
    .parent('li')
    .addClass('current-page')
    .parents('ul')
    .slideDown(function() {
      // setContentHeight();
    })
    .parent()
    .addClass('active');

  // // recompute content when resizing
  // $(window).smartresize(function(){
  //     setContentHeight();
  // });
  //
  // setContentHeight();

  // fixed sidebar
  if ($.fn.mCustomScrollbar) {
    $('.menu_fixed').mCustomScrollbar({
      autoHideScrollbar: true,
      theme: 'minimal',
      mouseWheel: { preventDefault: true }
    });
  }
}

// End Sidebar

// Init Components

$(document).ready(function() {
  // Bootstrap Tooltip
  $('[data-toggle="tooltip"]').tooltip({
    container: 'body',
    trigger: 'hover'
  });

  //Bootstrap Popover
  $('[data-toggle="popover"]').popover();

  //Side Bar
  init_sidebar();
});

// End Init Components

// Home Page Helper Functions

function showPieView($pieId, $barId) {
  if ($('#' + $pieId).css('display') == 'none') {
    $('#' + $barId).hide(500);
    $('#' + $pieId).show(500);
  }
}

function showBarView($pieId, $barId) {
  if ($('#' + $barId).css('display') == 'none') {
    $('#' + $pieId).hide(500);
    $('#' + $barId).show(500);
  }
}

// End Home Page Helper Functions

// Confirm Delete Record Modal

$('.delete-confirm').on('click', function() {
  var token = $(this).attr('token');
  var url = $(this).attr('url');
  swal(
    {
      title: 'Are you sure?',
      text:
        'This action will permanently delete this record from your system...',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      closeOnConfirm: false,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          type: 'POST',
          data: {
            _method: 'DELETE',
            _token: token
          },
          dataType: 'html',
          success: function() {
            swal('Done!', 'It was succesfully deleted!', 'success');
            setTimeout(function() {
              location.reload();
            }, 1500);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            swal('Error deleting!', 'Please try again', 'error');
          }
        });
      }
    }
  );
});

// End Confirm Delete Record Modal

// Confirm Duplicate Record Modal

$('.duplicate-confirm').on('click', function() {
  let url = $(this).attr('url');
  let text = $(this).attr('text');
  let token = $(this).attr('token');

  swal(
    {
      title: 'Are you sure?',
      text: 'This action will duplicate this procedure...',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Yes, Do it!',
      cancelButtonText: 'No, Cancel!',
      closeOnConfirm: false,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          type: 'POST',
          data: {
            _token: token
          },
          dataType: 'html',
          success: function() {
            swal('Done!', 'Action was performed successfully!', 'success');
            setTimeout(function() {
              location.reload();
            }, 1000);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            swal('Oops!', 'Something went wrong...', 'error');
          }
        });
      }
    }
  );
});

// End Confirm Duplicate Record Modal

// Confirm Publish Procedure Modal

$('.publish-confirm').on('click', function() {
  let auth = $(this).attr('auth');
  let token = $(this).attr('token');
  let proc_id = $(this).attr('procedure');
  let base_url = $(this).attr('base_url');

  swal(
    {
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, publish!'
    },
    function(isConfirm) {
      if (isConfirm) {
        axios
          //   .post('http://dev.medecide.net/ajax', {
          .get(`/api/procedure/publish/${proc_id}`, {
            action: 'publishProcedure',
            token,
            proc_id,
            auth
          })
          .then(function(response) {
            swal(
              'published!',
              'Procedure has been published to production server.',
              'success'
            );
            console.log(response);
          })
          .catch(function(error) {
            console.log(error);
          });
      }
    }
  );
});

// End Confirm Publish Procedure Modal

// Edit / Add Record Modal

$('body').on('click', '.edit-registry', function() {
  var url = $(this).data('url');
  $.ajax({
    url: url,
    cache: false
  }).done(function(html) {
    $('.modal-content').html(html);
    $('#formModal').modal('show');
  });
});

$('body').on('click', '.add-registry', function() {
  var url = $(this).attr('id');
  $.ajax({
    url: url,
    cache: false
  }).done(function(html) {
    $('.modal-content').html(html);
    $('#formModal').modal('show');
  });
});

// End Edit / Add Record Modal

// Submit Ajax Form

$('document').ready(function() {
  // Prepare reset.
  function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group')
      .find('.help-block')
      .remove();
  }

  // Intercept submit.
  $('body').on('click', '.bootstrap-modal-submit', function() {
    $('.bootstrap-modal-form').submit(function(e) {
      e.preventDefault();
    });
    // submission.preventDefault();

    // Set vars.
    var form = $('.bootstrap-modal-form'),
      url = form.attr('action'),
      submit = $('.bootstrap-modal-submit');

    // Check for file inputs.
    if (form.find('[type=file]').length) {
      // If found, prepare submission via FormData object.
      var input = form.serializeArray(),
        data = new FormData(),
        contentType = false;

      // Append input to FormData object.
      $.each(input, function(index, input) {
        data.append(input.name, input.value);
      });

      // Append files to FormData object.
      $.each(form.find('[type=file]'), function(index, input) {
        if (input.files.length == 1) {
          data.append(input.name, input.files[0]);
        } else if (input.files.length > 1) {
          data.append(input.name, input.files);
        }
      });
    } else {
      // If no file input found, do not use FormData object (better browser compatibility).
      var data = form.serialize(),
        contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    }

    // Please wait.
    if (submit.is('button')) {
      var submitOriginal = submit.html();
      submit
        .html('Processing ')
        .append('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
    } else if (submit.is('input')) {
      var submitOriginal = submit.val();
      submit
        .val('Processing ')
        .append('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
    }

    // Request.
    $.ajax({
      type: 'POST',
      url: url,
      data: data,
      dataType: 'json',
      cache: false,
      contentType: contentType,
      processData: false

      // Response.
    }).always(function(response, status) {
      // Reset errors.
      resetModalFormErrors();

      // Check for errors.
      if (response.status == 422) {
        var errors = $.parseJSON(response.responseText);

        // Iterate through errors object.
        $.each(errors, function(field, message) {
          console.error(field + ': ' + message);
          //handle arrays
          if (field.indexOf('.') != -1) {
            field = field.replace('.', '[');
            //handle multi dimensional array
            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
              field = field.replace('.', '][');
            }
            field = field + ']';
          }
          var formGroup = $('[name=' + field + ']', form).closest(
            '.form-group'
          );
          formGroup
            .addClass('has-error')
            .append('<p class="help-block">' + message + '</p>');
        });

        // Reset submit.
        if (submit.is('button')) {
          submit.html(submitOriginal);
        } else if (submit.is('input')) {
          submit.val(submitOriginal);
        }

        // If successful, reload.
      } else {
        setTimeout(() => {
          submit
            .attr('class', 'btn btn-success')
            .html('Submitted ')
            .append('<i class="fa fa-check" aria-hidden="true"></i>');
          location.reload();
        }, 500);
      }
    });
  });

  // Reset errors when opening modal.
  $('.bootstrap-modal-form-open').click(function() {
    resetModalFormErrors();
  });
});

// End Submit Ajax Form
