$('.parameters-table').DataTable({
  dom: 'Bfrtip',
  autoWidth: true,
  order: [[0, 'desc']],
  responsive: true,
  buttons: [
    {
      text: '+ NEW PARAMETER',
      action: function() {
        $.ajax({
          url: 'client-params/create',
          cache: false
        }).done(function(html) {
          $('.modal-content').html(html);
          $('#formModal').modal('show');
        });
      }
    }
  ],
  initComplete: function() {
    this.api()
      .columns()
      .every(function() {
        var column = this;
        var select = $('<select><option value=""></option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());

            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });

        column
          .data()
          .unique()
          .sort()
          .each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
      });
  }
});
