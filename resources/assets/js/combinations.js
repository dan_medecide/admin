require('./cars');
var table;
$(() => {
  window.tableToArchive = table;
  //alert(connection);
});
table = $('.combinations-table').DataTable({
  dom: 'Bfrtip',
  autoWidth: true,
  order: [[0, 'desc']],
  responsive: true,
  buttons: [
    {
      text: '+ NEW COMBINATION',
      action: function() {
        $.ajax({
          url: 'combinations/create',
          cache: false
        }).done(function(html) {
          $('.modal-content').html(html);
          $('#formModal').modal('show');
        });
      }
    },
    {
      extend: 'collection',
      text: 'CONNECTION: ' + connection,
      buttons: [
        {
          text: 'beta',
          action: function(e, dt, node, config) {
            window.location.replace(BASE_URL + '/combinations?connection=beta');
          }
        },
        {
          text: 'production',
          action: function(e, dt, node, config) {
            window.location.replace(
              BASE_URL + '/combinations?connection=production'
            );
          }
        }
      ]
    }
  ],
  initComplete: function() {
    this.api()
      .columns([0, 1, 2, 3, 4])
      .every(function() {
        var column = this;
        var select = $('<select><option value=""></option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());

            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });

        column
          .data()
          .unique()
          .sort()
          .each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
      });
    $(this).show();
  }
});

$('.procedure-input').on('change', function(params) {
  alert('234');
});

// function ondragstart() {
//   console.log('asss');
// }
window.removeRow = function(combinationID) {
  console.log('inside remove Row COMBINATION!' + combinationID);
  table
    .row($('#' + combinationID))
    .remove()
    .draw();
};
