const table = $('.conflicts-table').DataTable({
    dom: "Bfrtip",
    autoWidth: false,
    order: [[0, "desc"]],
    responsive: true,
    select: false,
    initComplete: function () {
        this.api().columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]).every(function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });

            column.data().unique().sort().each(function (d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
    },
})

$('.delete-confirm').on('click', function () {
    let id = this.id;
    swal({
        title: "Are you sure?",
        text: "This action will permanently delete this record from your system...",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: true
    }, isConfirm => {
        if (isConfirm) {
            axios.post('/conflicts/' + id + '/delete').then(response => {
                swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function () {
                    location.reload();
                }, 500);
            }).catch(error => {
                swal("Error deleting!", "Please try again", "error");
                console.log(error);
            })
        }
    });
});
