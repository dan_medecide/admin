$('.params-table').DataTable({
  dom: "Bfrtip",
  autoWidth: false,
  order: [[0, "desc"]],
  responsive: true,
  buttons: [
    {
      text: '+ NEW PARAM',
      className: 'bootstrap-modal-form-open',
      action: function () {
        $.ajax({
          url: 'params/create',
          cache: false
        })
         .done(function (html) {
           $(".modal-content").html(html);
           $('#formModal').modal('show');
         });
      }
    }
  ],
  initComplete: function () {
    $(this).show();
  },
})