<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indication extends Model
{
    use SoftDeletes;

    protected $table = 'indications';

    protected $dates = [ 'deleted_at' ];

    protected $fillable = [
        'title',
    ];
}
