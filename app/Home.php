<?php

namespace App;

use function base64_encode;
use Illuminate\Database\Eloquent\Model;
use DB;
use Charts;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Procedure;

/**
 * App\Home
 * @mixin \Eloquent
 */
class Home extends Model {
    
    public static function auth($x)
    {
        
        return substr(sha1($x . '1QAZXSW23EDC'), -8);
    }
    
    // gets a url that redirects to core app engine, authenticates user and then redirects to $link
    public static function getCoreAppLink($link)
    {
        return 'http://beta.medecide.net/auth/' . auth()->user()->token . '/' . base64_encode($link);
    }
    
    public static function getProductionLink($link)
    {
        return 'http://medecide.net/auth/' . auth()->user()->token . '/' . base64_encode($link);
    }
    
    public static function getData()
    {
        $collections = [];
        $collections['users'] = User::all();
        $collections['categories'] = Category::all();
        $collections['procedures'] = Procedure::all();
        $collections['tests'] = DB::table('tests')->limit(1000)->get();
        $collections['labels'] = DB::table('teach')->limit(1000)->get();
        $date = date('Y-m-d H:i:s', time() - ( 7 * 86400 ));
        foreach ($collections as $collection) {
            $collection->since_last_week = $collection->where('created_at', '>', $date)
                                                      ->where('deleted_at', '=', null)
                                                      ->count();
            $collection->total_count = $collection->where('deleted_at', '=', null)->count();
        }
        
        return $collections;
    }
    
    public static function getLabelsByDate()
    {
        $labels = DB::table('teach')
                    ->select(DB::raw('DATE(created_at) date'), DB::raw('count(*) as labels'))
                    ->where('deleted_at', '=', null)
                    ->groupBy('date')
                    ->get();
        
        $chart = Charts::create('bar', 'morris')
                       ->title('Ordered By Date')
                       ->labels($labels->pluck('date'))
                       ->values($labels->pluck('labels'))
                       ->elementLabel('Labels')
                       ->dimensions(0, 500);
        
        return $chart;
    }
    
    public static function getLabelsByProcedure()
    {
        $labels = DB::table('teach')
                    ->select(DB::raw('COUNT(teach.id) as labels'), 'procedures.title', 'procedures.id')
                    ->where('tests.deleted_at', '=', null)
                    ->where('procedures.deleted_at', '=', null)
                    ->groupBy('procedures.title')
                    ->join('tests', 'teach.test_id', '=', 'tests.id')
                    ->join('procedures', 'tests.proc_id', '=', 'procedures.id')
                    ->limit(10)
                    ->get();
        
        $chart = Charts::create('pie', 'c3')
                       ->title('Divided By Procedures')
                       ->labels($labels->pluck('title'))
                       ->values($labels->pluck('labels'))
                       ->elementLabel('Quantity')
                       ->dimensions(0, 500);
        
        return $chart;
    }
    
    public static function getTestsCharts()
    {
        $date30DaysBack = Carbon::now()->subDays(30)->toDateTimeString();
        $tests = DB::table('tests')
                   ->select(DB::raw('COUNT(*) as count'), DB::raw('DATE(created_at) as date'))
                   ->where('combination_instance_id', '!=', 0)
                   ->whereBetween('created_at', [
                       $date30DaysBack,
                       Carbon::parse('now')->toDateTimeString(),
                   ])
                   ->groupBy('date')
                   ->where('deleted_at', '=', null)
                   ->get();
        
        $dates = [];
        foreach ($tests as $test) {
            $dates[] = Carbon::parse($test->date)->toFormattedDateString();
        }
        
        $bar_chart = Charts::create('bar', 'morris')
                           ->title('Ordered By Date')
                           ->labels($dates)
                           ->values($tests->pluck('count'))
                           ->elementLabel('Tests')
                           ->dimensions(0, 500);
        
        $pie_chart = Charts::create('pie', 'c3')
                           ->title('Divided By Date')
                           ->labels($dates)
                           ->values($tests->pluck('count'))
                           ->elementLabel('Tests')
                           ->dimensions(0, 500);
        
        return ['bar_chart' => $bar_chart, 'pie_chart' => $pie_chart];
    }
    
    public static function getCombinationInstances()
    {
        $softDeleted = [
            'tests.deleted_at'                 => null,
            'clients.deleted_at'               => null,
            'combinations.deleted_at'          => null,
            'combination_instances.deleted_at' => null,
            'clients.is_deleted'               => 0,
            'combinations.is_deleted'          => 0,
        ];
        
        $reports = DB::table('combination_instances')
                     ->join('combinations', 'combination_instances.combination_id', '=', 'combinations.id')
                     ->join('clients', 'combinations.client_id', '=', 'clients.id')
                     ->join('tests', 'combination_instances.id', '=', 'tests.combination_instance_id')
                     ->join('users', 'combination_instances.id', '=', 'tests.user_id')
                     ->select('combination_instances.id', 'combination_instances.created_at', 'clients.client_name', 'tests.proc_id', 'combinations.procedures', 'combination_instances.patient_id', 'combinations.code', 'tests.id as test_id', 'tests.physician_name')
                     ->where('tests.combination_instance_id', '>', '0')
                     ->where($softDeleted)
                     ->where('tests.is_done', '=', 1)
                     ->orderBy('combination_instances.created', 'desc')
                     ->get()
                     ->toArray();
        
        return $reports;
    }
}
