<?php

namespace App\Listeners;

use App\Events\AnswerUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAnswerUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AnswerUpdate  $event
     * @return void
     */
    public function handle(AnswerUpdate $event)
    {
        dd("notify answer update!");
    }
}
