<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeachModel extends Model {
    use SoftDeletes;

    protected $table = 'teach';

    protected $dates = [ 'deleted_at' ];

    protected $fillable = [
        'test_id','label_json','id'
    ];
}
