<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Conflict
 *
 * @property int $id
 * @property int $test_id
 * @property int $user_id
 * @property int $question_id
 * @property int $answer_id
 * @property string $sub_answer
 * @property int $test_answer_id
 * @property string $created
 * @property string $modified
 * @property float $probability
 * @property int $proc_id
 * @property int $conflict_set
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Conflict onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereConflictSet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereProbability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereProcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereSubAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereTestAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereTestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conflict whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Conflict withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Conflict withoutTrashed()
 * @mixin \Eloquent
 */
class Conflict extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'test_conflicts';
}
