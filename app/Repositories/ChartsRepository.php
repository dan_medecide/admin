<?php

namespace App\Repositories;

use DB;
use Carbon\Carbon;
use Charts;
use function dd;


class ChartsRepository {

    public function getLabels ( $user )
    {
        $labels = $this->countLabelsByDate( $user->id );
        $labelsCount=0;
        foreach ($labels as  $value) {
           $labelsCount+=(int)$value->labels;
        }
        // dd($labelsCount);
        return Charts::create( 'bar', 'morris' )
                     ->title( 'Total: '.$labelsCount )
                     ->labels( $labels->pluck( 'date' ) )
                     ->values( $labels->pluck( 'labels' ) )
                     ->elementLabel( 'Labels' )
                     ->dimensions( 0, 500 );
    }

    protected function countLabelsByDate ( $id )
    {
        return DB::table( 'teach' )
                 ->select( DB::raw( 'DATE(created_at) date' ), DB::raw( 'count(*) as labels' ) )
                 ->where( 'user_id', '=', $id )
                 ->groupBy( 'date' )
                 ->get();
    }
}