<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use DB;
use App\CombinationInstance;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Test
 * @package App
 */
class Test extends Model
{
    use SoftDeletes; 
    protected $timezone='-5:00';
    protected $fillable = ['supervisor_decision', 'supervisor_decision_explanation','decision_code','label_json','is_done_reason'];
      public function __construct()
    {
        $client_id=auth()->user()->client_id;
        $this->timezone=  $client_id==26 ||  $client_id==28 ? '-5:00' : '+2:00' ;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function isJson($string) 
    {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
    }
    
     public function addTitleToTestManualDecision($row, $decisions)
    {
       try {
            $row=collect(json_decode($row,true));
            return $row->map( function($key,$value) use ($decisions) {
            return  ['key' => $key  ,
                     'title'=> (isset($decisions[$key])? $decisions[$key]: '')
                    ];
                });
            } catch (Exception $e) {
        return false;
        }
    }

    public function labelJson($query, $decisions)
    {
     //$comb_inst=new CombinationInstance;
        $array=[];
            foreach ($query as $key => $row) {
            $myJson=json_decode($row->teach_json,true);
           $timestamp=$row->created_at;
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp);
            $date-> setTimezone($this->timezone);
            // dd($flattened = array_flatten( $myJson)[1]);
           // 
           $array[$key]=collect($row); 
           $array[$key]['created_at'] =$date->format('M-d-y  H:i:s');
           // $array[$key]['combination']=$comb_inst->find($array[$key]['combination_instance_id']) ;
           //if($array[$key]['combination'])$array[$key]['combination']=$array[$key]['combination']->combination()->first()->code;
            $array[$key]['tests_json']=json_decode($array[$key]['tests_json'],true);
            $array[$key]['manualDecision']=json_decode($row->teach_json,true);  
            $array[$key]['tests_manual_decision']= $array[$key]['tests_manual_decision'] ? $this->addTitleToTestManualDecision($row->tests_manual_decision,$decisions):  $array[$key]['manualDecision'];
        };  
        
       return  $array;
    }

    public function getClientUsers()
    {
        return auth()->user()->client_id ? auth()->user()->client->users->pluck('id') : array_flatten(auth()->user()->where('client_id',auth()->user()->client_id)->select('id')->get()->toArray());
    }

    public function testsTable($user_id,$clientUsers)
    {
        
      
       
        return  DB::table('tests')
         ->leftJoin('teach', 'tests.id', '=', 'teach.test_id')
         ->join('procedures','tests.proc_id', '=','procedures.id')
         ->leftJoin('users', 'users.id', '=', 'tests.user_id')
        ->leftJoin('combination_instances','tests.combination_instance_id', '=','combination_instances.id')
        ->select('users.title as users_title','users.first_name','users.last_name',
        'tests.id as test_id', 'tests.manual_decision as tests_manual_decision','tests.label_json as tests_json',
        'tests.is_done' ,
        'tests.proc_id' , 
        'tests.user_id' , 
        'tests.created_at', 
        'tests.notes',
        'tests.supervisor_decision',
        'tests.supervisor_decision_explanation',
        'tests.deleted_at',
        'tests.is_done_reason',
        'procedures.title',
        'combination_instances.patient_id',
        'teach.label_json as teach_json' )
        // ->where('tests.user_id','=',$user_id) 
        ->whereIn('tests.user_id',  $clientUsers) 
        ->whereNull('tests.deleted_at')
        ->take('4000')
        // ->whereNotNull('teach.label_json')
        ->latest()->get();
    }

    public function procedure()
    {
        //  return $this->belongsTo('App\Procedure');
       return $this->belongsTo(Procedure::class, 'proc_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function decision()
    {
        return $this->belongsTo(Decision::class, 'decision_code', 'code');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
       public function teach()
    {
        return $this->hasOne(TeachModel::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function CombinationInstance()
    {
        return $this->belongsTo(CombinationInstance::class, 'combination_instance_id');
    }
    
    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeDone($query)
    {
        return $query->where('is_done', '=', 1);
    }
    
    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeWithDecision($query)
    {
        return $query->where('decision_code', '!=', 0);
    }
    
    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeUnapproved($query)
    {
        return $query->where('supervisor_decision', '=', null);
    }
    
    /**
     * @param $query
     * @param $value
     *
     * @return Builder
     */
    public function scopeFilterBy($query, $value)
    {
        return $query->where('tests.id', 'LIKE', "%{$value}%")
                     ->orWhere('tests.physician_name', 'LIKE', "%{$value}%");
    }
    
    /**
     * @param $query
     * @param $table
     * @param $order
     *
     * @return Builder
     */
    public function scopeSortBy($query, $column, $order)
    {
        return $query->orderBy($column, $order);
    }
    
    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeThisYear($query)
    {
        $year = Carbon::today()->subDays(365);
        
        return $query->where('tests.created_at', '>=', $year);
    }
    
    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeWithProcedures($query)
    {
        return $query->join('procedures', 'tests.proc_id', '=', 'procedures.id')
                     ->addSelect('procedures.abbreviation as procedure', 'procedures.title as procedure_title');
    }
    
    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeWithMonths($query)
    {
        return $query->addSelect(DB::raw('MONTH(tests.created_at) as month'));
    }
    
    /**
     * @param $query
     *
     * @return Builder
     */
    public function scopeWithPhysicians($query)
    {
        return $query->addSelect('physician_name as physician');
    }
    
    /**
     * @param $query
     * @param $physician
     *
     * @return mixed
     */
    public function scopeWherePhysician($query, $physician)
    {
        return $query->where('tests.physician_name', '=', $physician);
    }
}
