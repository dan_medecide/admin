<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Parameter extends Model {

    use SoftDeletes;

    protected $table = 'client_params';

    protected $dates = [ 'deleted_at' ];

    // public $connection = 'local';

    protected $fillable = [
        'param_name',
        'param_value',
        'client_id',
    ];

    public function client ()
    {
        return $this->belongsTo( Client::class, 'client_id' );
    }
}
