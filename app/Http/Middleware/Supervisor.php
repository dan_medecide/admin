<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Supervisor
{
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole('supervisor') or auth()->user()->hasRole('admin')) {
                return $next($request);
            }
        }

        return redirect('/');
    }
}
