<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Doctor
{
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
         
            if (auth()->user()->hasRole('doctor') or auth()->user()->hasRole('admin')) {
                return $next($request);
            }
           if(auth()->user()->hasRole('supervisor')   and (starts_with($request->path(),'run-report') or starts_with($request->path(),'run-unfinished-combination') ) ) 
            //  else (auth()->user()->hasRole('supervisor')) { 
             return $next($request); 
        }
        else if (null!=auth()->user() &&  auth()->user()->hasRole('supervisor')) { 
            return $next($request);}
        session(['redirect_url' => $request->path()]);
        return redirect('/');
    }
}
