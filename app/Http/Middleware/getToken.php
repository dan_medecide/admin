<?php

namespace App\Http\Middleware;

use Closure;

class getToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = json_decode(getToken($request->email, $request->password))->token;
        session(['api-token' => $token]);
        return $next($request);
    }
}
