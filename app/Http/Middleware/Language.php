<?php

namespace App\Http\Middleware;

use Closure;

class Language {
    
    public function handle($request, Closure $next)
    {
        if (session()->has('locale') and array_key_exists(session()->get('locale'), config()->get('languages'))) {
            app()->setLocale(session()->get('locale'));
        }
        
        return $next($request);
    }
}
