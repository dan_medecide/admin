<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        if (auth()->user() and auth()->user()->hasRole('admin')) {
            return $next($request);
        }

        return redirect('/');
    }
}
