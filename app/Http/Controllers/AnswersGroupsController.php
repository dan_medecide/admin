<?php

namespace App\Http\Controllers;

use DB;

class AnswersGroupsController extends Controller
{

    /**
     * @param $category_id
     *
     * @return array
     */
    public function index($category_id)
    {
        return $this->getGroupsByCategory($category_id);
    }

    /**
     * @param $category_id
     *
     * @return static
     */
    protected function getGroupsByCategory($category_id)
    {
        return DB::table('answers')
                 ->join('questions', 'answers.question_id', '=', 'questions.id')
                 ->join('procedures', 'questions.proc_id', '=', 'procedures.id')
                 ->where('procedures.cat_id', '=', $category_id)
                 ->pluck('answer_groups')
                 ->filter()
                 ->flatMap(function ($groups) {
                     return array_map('trim', explode(',', $groups));
                 })
                 ->unique();
    }
}
