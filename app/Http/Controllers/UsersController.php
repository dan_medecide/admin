<?php

namespace App\Http\Controllers;

use App\Category;
use App\Client;
use App\Repositories\ChartsRepository;
use App\Role;
use App\Decision;
use App\Test;
use App\User;
use App\TeachModel;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use Illuminate\Http\Request;
use DB;
/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
  
        $users = User::with(['category', 'client','roles'])->get()
        ->map(function($user) {
            $user->role_exists =head(head($user->roles->map->name )) ? true : false  ;
           
        return $user;
            
            //dd($users[0]->roles->map->name->toArray());
        } );
      
        return view('user.users', compact('users'));
        
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('user.partials.create', [
            'categories' => Category::list(),
            'clients' => Client::list(),
        ]);
    }

    /**
     * @param StoreUser $request
     *
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function store(StoreUser $request)
    {
        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->title = $request->title;
        $user->email = $request->email;
        $user->token = $this->generateToken($request->email);
        $user->password = bcrypt($request->password);
        $client = Client::find($request->client_id);
        $category = Category::find($request->category_id);
        $user->client()->associate($client);
        $user->category()->associate($category);
        $user->save();
        return $user;
    }

    /**
     * @param $identifier
     *
     * @return string
     */
    protected function generateToken($identifier)
    {
        $token = sha1($identifier . time() . 'medecide');

        return $token;
    }

    /**
     * @return mixed
     */
    protected function updateToken()
    {
        $token = User::generateToken($this->id);
        $this->token = $token;
        $this->save();

        return $token;
    }

    /**
     * @param User             $user
     * @param ChartsRepository $charts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user, ChartsRepository $charts)
    {
        $testModel = new Test;
        $decisions = Decision::all()->keyBy('code')->map->title; // $myQuery= $testModel->testsTable($user->id);
        $clientUsers =  [$user->id];
        $mappedDecisions = collect($decisions)->only(2,8,1,3,4)->sortBy(function ($product, $key) {return $key;});
        $teach = $testModel->labelJson($testModel->testsTable($user->id, $clientUsers ),$decisions,'teach_json'); 
        $role = $user->roles->first()->name;
        $labels_chart = $charts->getLabels($user);
        $names = array_dot(array_pluck($teach, 'tests_json') );
        $isIndicated = array_where($names , function ($value, $key) {
            return ends_with($key,'is_indication');
        });
        return view('user.profile', compact('user', 'labels_chart','teach','decisions','role','isIndicated', 'mappedDecisions','clientUsers' ));
    }

    public function newSupervisor( ){
        $user = auth()->user();
        if($user->hasRole('admin')) return  redirect('/');
        $testModel = new Test;
       $clientUsers = $testModel->getClientUsers();
        $decisions = Decision::all()->keyBy('code')->map->title // $myQuery= $testModel->testsTable($user->id);
         ->map(function ($a) {return str_replace(", subject to a doctor's decision.",'',$a);}); 
        $mappedDecisions = collect($decisions)->only(2,8,1,3,4)->sortBy(function ($product, $key) {return $key;});
        $teach = $testModel->labelJson($testModel->testsTable($user->id, $clientUsers),$decisions,'teach_json'); 
        $role = $user->roles->first()->name;
        $names = array_dot(array_pluck($teach, 'tests_json') );
        $isIndicated = array_where($names , function ($value, $key) {
            return ends_with($key,'is_indication');
        });
        $byProcedures = collect($teach)->groupBy('title');
      
        return view('user.new-supervisor', compact('user' ,'teach','decisions','role','isIndicated', 'mappedDecisions','clientUsers','byProcedures') );
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('user.partials.edit', [
            'user' => $user,
            'categories' => Category::list(),
            'clients' => Client::list(),
        ]);
    }

    /**
     * @param UpdateUser $request
     * @param User       $user
     *
     * @return bool
     */
    public function update(UpdateUser $request, User $user)
    {
        $client = Client::find($request->client_id);
        $user->client()->associate($client);
        $category = Category::find($request->category_id);
        $user->category()->associate($category);

        $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'title' => $request->title,
            'role' => $request->roles,
            'email' => $request->email,
        ]);

        if ($request->password != 'oldpassword') {
            $user->update(['password' => bcrypt($request->password)]);
        }

        return ['action' => 'User updated!', 'user'=>$user];
    }

    /**
     * @param User $user
     *
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
    }
}
