<?php

namespace App\Http\Controllers;

use App\Home;
use App\User;

class HomeAdminController extends Controller
{
    public function index()
    {
        $data = Home::getData();
//        $labels_date_chart = Home::getLabelsByDate();
//        $labels_procedures_chart = Home::getLabelsByProcedure();
        $users = User::all();
//        $reports = Home::getCombinationInstances();
//        $tests_charts = Home::getTestsCharts();

        return view('home', compact('data', 'labels_date_chart', 'users', 'reports', 'labels_procedures_chart', 'tests_charts'));
    }
}
