<?php

namespace App\Http\Controllers;

use App\Procedure;
use App\User;
use Illuminate\Http\Request;

/**
 * Class UsersProceduresController
 * @package App\Http\Controllers
 */
class UsersProceduresController extends Controller {
    
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected $procedures;
    
    /**
     * UsersProceduresController constructor.
     *
     * @param $procedures
     */
    public function __construct(Procedure $procedures)
    {
        $this->procedures = $procedures->with('category')->get();
    }
    
    /**
     * @return \View
     */
    public function edit(User $user)
    {
         
        return view('user.partials.procedures')->with([          
            'user'       => $user,
            'procedures' => $this->procedures->sortBy('cat_id'),
        ]);
    }
    
    public function update(User $user, Request $request)
    {
        $procedures = $request->except(['_method', '_token']);
        $user->procedures()->sync($procedures);
        
        return $user->procedures()->pluck('title');
    }
}
