<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Note;
use function compact;
use DB;
use function dd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function redirect;
use function response;
use function view;

class NotesController extends Controller {
    public function index ()
    {
        $notes = Note::all();
        
        if (\request()->wantsJson()){
            return $notes;
        }

        return view( 'notes.notes', compact( 'notes' ) );
    }


    public function create ()
    {
        return view( 'notes.add_note' );
    }


    public function store ( Request $request )
    {
        $this->validate( $request, [ 'title' => 'required|string' ] );

        return Note::create( [
            'title' => $request->title,
        ] );
    }


    public function show ( Note $note )
    {
        //
    }


    public function edit ( Note $note )
    {
        return view( 'notes.edit_note', compact( 'note' ) );
    }


    public function update ( Request $request, Note $note )
    {
        $this->validate( $request, [ 'title' => 'required|string' ] );

        return $note->update( $request->all() );
    }

    public function destroy ( Note $note )
    {
        $note->delete();

        return 'note deleted';
    }
}
