<?php

namespace App\Http\Controllers;

use App\Client;
use Carbon\Carbon;
use PhpParser\Builder;
use DB;

/**
 * Class ClientsReportsController
 * @package App\Http\Controllers
 */
class ClientsReportsController extends Controller {
    
    /**
     * @var
     */
    protected $sortParams;
    protected $column;
    protected $order = 'desc';
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function init()
    {
        return view('client.reports.index');
    }
    
    /**
     * @param $client
     *
     * @return mixed
     */
    public function index($client = '' , $connection = '', $unapprovedFlag = 0, $todayFlag = 1)
    {
        $client = !$client ? auth()->user()->client->id : $client;
        $connection = !$connection ? env('DB_CONNECTION') : $connection;
      
        $this->setSortParams();
        
        $reports = $this->getInitialQuery($client, $connection);
      
        $reports = $this->applyFilters($unapprovedFlag, $reports, $todayFlag);
        if ($todayFlag) {
            return $reports->get();
        }
        
        return      $reports->paginate(10);
    }
    
    /**
     * @param $client
     * @param $connection
     *
     * @return Builder
     */
    protected function getInitialQuery($client, $connection)
    {
        $reports = DB::table('tests')
                     ->join('combination_instances', 'tests.combination_instance_id', '=', 'combination_instances.id')
                     ->join('decision_codes', 'tests.decision_code', '=', 'decision_codes.code')
                     ->join('procedures', 'tests.proc_id', '=', 'procedures.id')
                     ->select('tests.id',
                         'tests.updated_at',
                         'tests.created_at',
                         'tests.physician_name',
                         'tests.supervisor_decision',
                         'tests.decision_code',
                         'decision_codes.title as decision_title',
                         'decision_codes.is_indication',
                         'procedures.title as proc_title',
                         'combination_instances.client_id',
                         'combination_instances.patient_id')
                        ->where('tests.is_done', '=', 1)
                       ->where('tests.decision_code', '!=', 0)
                  ->where('tests.deleted_at', '=', null)
                  ->where('combination_instances.client_id', '=', $client)
                  ;
        return $reports;
    }
      protected function getNotApprovedInitialQuery($client, $connection)
    {
        $reports = DB::table('tests')
                     ->join('combination_instances', 'tests.combination_instance_id', '=', 'combination_instances.id')
                 //    ->leftJoin('decision_codes', 'tests.decision_code', '=', 'decision_codes.code')
                     ->join('procedures', 'tests.proc_id', '=', 'procedures.id')
                     ->select('tests.id',
                         'tests.updated_at',
                         'tests.created_at',
                         'tests.physician_name',
                         'tests.supervisor_decision',
                         'tests.decision_code',
                     //    'decision_codes.title as decision_title',
                     //    'decision_codes.is_indication',
                         'procedures.title as proc_title',
                         'combination_instances.client_id',
                         'combination_instances.patient_id')
                           ->where('tests.supervisor_decision',null)
                        ->where('tests.is_done', '=', 1)
                     //  ->where('tests.decision_code', '!=', 0)
                  ->where('tests.deleted_at', '=', null)
                  ->where('combination_instances.client_id', '=', $client)
                  ;
        return $reports;
    }
    /**
     * @param $unapprovedFlag
     * @param $reports
     *
     * @return mixed
     */
    protected function applyFilters($unapprovedFlag, $reports, $todayFlag)
    {
        // if ($todayFlag) {
        //     $reports = $reports->whereDate('tests.updated_at', Carbon::today());
        // }
        
        if ($unapprovedFlag) {
            $reports = $reports->where('supervisor_decision', '=', null);
        }
        
        if (request()->has('filter')) {
            $filter = request('filter');
            $reports = $reports->where('tests.id', 'LIKE', "%{$filter}%")
                               ->orWhere('tests.physician_name', 'LIKE', "%{$filter}%")
                               ->orWhere('procedures.title', 'LIKE', "%{$filter}%");
        }
        
        if ($this->sortParams) {
            $reports = $reports->orderBy($this->column, $this->order);
        }
        
        return $reports;
    }
    
    protected function setSortParams()
    {
        if (request()->has('sort')) {
            //dd( request('sort'));
            $this->sortParams = explode('|', request('sort'));
            $this->column = str_replace('-','',$this->sortParams[0]);
            $this->order = $this->sortParams[0] ;
           
        }
    }
    public function notApproved($client, $connection){
        $reports = $this->getNotApprovedInitialQuery($client, $connection)
              ->where('tests.supervisor_decision',null)
              ->get()->toArray();
        return    ['data'=>$reports,'from'=>1,'per_page'=> 10,'total'=>200];
    }
}
