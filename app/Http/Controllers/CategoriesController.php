<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Session;

/**
 * Class CategoriesController
 * @package App\Http\Controllers
 */
class CategoriesController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $categories = Category::all()->toArray();

        return view( 'categories.categories', compact( 'categories' ) );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ()
    {
        return view( 'categories.add_categories_modal' );
    }

    /**
     * @param Request $request
     *
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function store ( Request $request )
    {
        $this->validate( $request, [
            'title' => 'required',
        ] );

        if ( $request->production )
        {
            Category::on( 'production' )->create( [
                'title'       => $request->title,
                'description' => $request->description,
            ] );
        }

        return Category::create( [
            'title'       => $request->title,
            'description' => $request->description,
        ] );
    }

    /**
     * @param Category $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit ( Category $category )
    {
        return view( 'categories.edit_category_modal', compact( 'category' ) );
    }

    /**
     * @param Request $request
     * @param Category $category
     */
    public function update ( Request $request, Category $category )
    {
        $this->validate( $request, [
            'title' => 'required',
        ] );

        $category->update( $request->all() );
    }

    /**
     * @param Category $category
     *
     * @return string
     */
    public function destroy ( Category $category )
    {
        $category->delete();

        return 'Category deleted';
    }

}
