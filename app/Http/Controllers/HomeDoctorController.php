<?php

namespace App\Http\Controllers;

use App\Repositories\ChartsRepository;
use Illuminate\Http\Request;

class HomeDoctorController extends Controller {
    
    public function index(ChartsRepository $charts)
    {
        $user = auth()->user();
        $procedures = $user->procedures()
                           ->where('is_deleted', '=', 0)
                           ->get();
        
        $labels_chart = $charts->getLabels($user);
        
        return view('home.home_doctor', compact('user', 'procedures', 'labels_chart'));
    }
}
