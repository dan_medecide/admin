<?php

namespace App\Http\Controllers;

use App\DisplayGroup;
use App\Question;
use App\Answer;
use Illuminate\Http\Request;

class DisplayGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return DisplayGroup::all(); 
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
          $d = (new DisplayGroup);   
        return  (new Answer)->storeGroup( $request , $d );
    //   try { 
    //     $this->validate($request, [
    //         'probability' => 'required|numeric',
    //         'priority' => 'required|numeric',
    //     ]);
    //      } catch (\Illuminate\Validation\ValidationException $th) { 
 
    //         return  [ 'error' => collect($th->response->original)->values() ];
    //      }
             
    //     try { 
           
    //         $d->Create($request->toArray());
    //          $answerGroup = head($d->answers())->handleAnswerGroup();   
             
    //            return ["display_gorup" => $request->all(), 'answer_group' => $answerGroup];
    //          //Todo - Handle Answer Group
           
    //         } catch (\Illuminate\Database\QueryException $e) {
    //          $d = $d->where('question_id',$request->question_id)
    //          ->where('name',$request->name)
    //          ->first();
            
    //         if (  $request->priority  &&  $request->probability
    //           &&  $d->priority !=  $request->priority ||
    //          $d->probability !=  $request->probability)        
    //          try {
          
    //          $d->priority =  $request->priority;
    //          $d->probability =  $request->probability;
    //          $d->name =  $request->name;   
    //          $d->save();
    //          $answerGroup = head($d->answers())->handleAnswerGroup();
    //          //Todo - Handle Answer Group
              
    //          return ["display_group" => $d, 'answer_group' => $answerGroup];
    //            //$d->update($request->all());
                 
    //         } catch (\Throwable $th) {
    //               return ['error' =>$th];
    //         }   ;    
    //               return ['error' => $e->errorInfo[2]] ; //getCode()
            }

       
    

    /**
     * Display the specified resource.
     *
     * @param  \App\DisplayGroup  $displayGroup
     * @return \Illuminate\Http\Response
     */
    public function show(DisplayGroup $displayGroup)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DisplayGroup  $displayGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(DisplayGroup $displayGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DisplayGroup  $displayGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DisplayGroup $displayGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DisplayGroup  $displayGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(DisplayGroup $displayGroup)
    {
        //
    }

      public function qTest(DisplayGroup $displayGroup)
    {
       dd ( $displayGroup->question()->get() ) ;
    }
       public function qTest2(DisplayGroup $displayGroup)
    {
       dd ( $displayGroup->answers() ) ;
    }

          public function answerGroup(DisplayGroup $displayGroup)
    {
         
      return $displayGroup->answerGroup()  ;
    }
        public function qTest3(Question $question)
    {
       dd ( $question->displayGroups()->get() ) ;
    }
        public function qTest4(Answer $answer)
    {
       dd ( $answer->displayGroup() ) ;
    }

}
