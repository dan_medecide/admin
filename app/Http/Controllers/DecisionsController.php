<?php

namespace App\Http\Controllers;

use App\Decision;
use Illuminate\Http\Request;
use Session;

class DecisionsController extends Controller {

    public function index ()
    {
        $decisions = Decision::all()->toArray();

        return view( 'decisions.decisions', compact( 'decisions' ) );
    }

    public function create ()
    {
        return view( 'decisions.add_decision_modal' );
    }

    public function store ( Request $request )
    {
        $this->validate( $request, [
            'title'         => 'required',
            'description'   => 'required',
            'code'          => 'required|numeric',
            'is_indication' => 'required',
        ] );

        return Decision::create( $request->all() );
    }

    public function show ( Decision $decision )
    {
        //
    }

    public function edit ( Decision $decision )
    {
        return view( 'decisions.edit_decision_modal', compact( 'decision' ) );
    }

    public function update ( Request $request, Decision $decision )
    {
        $this->validate( $request, [
            'title'         => 'required',
            'description'   => 'required',
            'code'          => 'required|numeric',
            'is_indication' => 'required',
        ] );

        return $decision->update( $request->all() );
    }

    public function destroy ( Decision $decision )
    {
        $decision->delete();

        return 'Decision deleted';
    }
}
