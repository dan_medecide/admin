<?php

namespace App\Http\Controllers;

use App\Report;
use function compact;
use function dd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function view;
use DB;

class ReportsController extends Controller {

    public function index ()
    {
        $soft_deleted = [
            'tests.deleted_at'                 => NULL,
            'clients.deleted_at'               => NULL,
            'combinations.deleted_at'          => NULL,
            'combination_instances.deleted_at' => NULL,
            'clients.is_deleted'               => 0,
            'combinations.is_deleted'          => 0,
        ];

        $reports = DB::connection( 'production' )
                                ->table( 'combination_instances' )
                                ->join( 'combinations', 'combination_instances.combination_id', '=', 'combinations.id' )
                                ->join( 'clients', 'combinations.client_id', '=', 'clients.id' )
                                ->join( 'tests', 'combination_instances.id', '=', 'tests.combination_instance_id' )
                                ->select( 'combination_instances.id', 'combination_instances.created_at', 'clients.client_name', 'tests.proc_id', 'combinations.procedures', 'combination_instances.patient_id', 'combinations.code', 'tests.id as test_id', 'tests.physician_name' )
                                ->where( 'tests.combination_instance_id', '>', '0' )
                                ->where( $soft_deleted )
                                ->where( 'tests.is_done', '=', 1 )
                                ->orderBy( 'combination_instances.created', 'desc' )
                                ->get();

        $tests = DB::connection( 'production' )->table( 'teach' )->pluck( 'test_id' )->toArray();

        return view( 'reports.reports', compact( 'reports', 'tests' ) );
    }

    public function create ()
    {
        //
    }

    public function store ( Request $request )
    {
        //
    }

    public function show ( Report $report )
    {
        //
    }

    public function edit ( Report $report )
    {
        //
    }

    public function update ( Request $request, Report $report )
    {
        //
    }

    public function destroy ( Report $report )
    {
        //
    }
}
