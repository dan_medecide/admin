<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ClientReferralsController extends Controller {
    
    public function index()
    {
        return DB::table('tests')
                       ->join('combination_instances', 'tests.combination_instance_id', '=', 'combination_instances.id')
                       ->where('client_id', '=', auth()->user()->client->id)
                       ->join('decision_codes', 'tests.decision_code', '=', 'decision_codes.code')
                       ->select('tests.physician_name as physician',
                           DB::raw('SUM(decision_codes.is_indication) as indicated'),
                           DB::raw('COUNT(tests.id) as count'))
                       ->groupBy('tests.physician_name')
                       ->get();
    }
}
