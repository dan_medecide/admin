<?php

namespace App\Http\Controllers;

use App\Category;
use App\Combination;
use App\Question;
use App\Procedure;
use App\ProcedureScores;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class ProceduresController
 * @package App\Http\Controllers
 */
class ProceduresController extends Controller
{
    //use SoftDeletes;
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View | Collection
     */
    public function index(Request $request)
    {
       
        $procedure = new Procedure;
      $archiveRequest = ($request->has('temp')) ?: false;
        $procedures = Procedure::with('category:id,title')
            ->with('users')
            ->where('is_deleted', '=', 0)
            ->orderBy('cat_id')
            ->get()
            ->filter(function ($procedure) use( $archiveRequest) {
                if($archiveRequest) return   $procedure->category->title=="Temp" ?: false;
                else if($procedure->category->title=="Temp") return false; //Archive
                return !empty($procedure->category) ? $procedure : null;
            });

        if ($request->wantsJson()) {
            
            return $procedures;
        }
        
         // $userProcedures= auth()->user()->procedures;
       // $procedures=$procedure->pluckClient($procedures); 
        return view('procedures.procedures', compact('procedures'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('procedures.procedures_form')->with('categories', Category::all());
         
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $procedure = Procedure::create($request->all());
        $scores = ProcedureScores::exampleScores()->toArray();
        $procedure->scores()->createMany($scores);

        return redirect()->route('procedures.index')->with('alert-success', 'New procedure has been created!');
    }

    /**
     * @param Procedure $procedure
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Procedure $procedure, Request $request)
    {
        if ($request->wantsJson()) {
            return Question::where('proc_id', $procedure->id)->where('is_deleted', 0)->with(['answers' => function ($query) {
                return $query->where('is_deleted', 0);
            }])->get();
        };

        return view('procedures.show', ['procedure' => $procedure]);
    }

    /**
     * @param Procedure $procedure
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Procedure $procedure)
    {
       $proc_id = $procedure->id;
       $proc_is_ready = $procedure->_is_ready;
       $proc_title = $procedure->_title;
        return view('procedures.procedures_form')->with([
            'procedure' => $procedure,
            'proc_title' => $proc_title,
            'proc_is_ready' => $proc_is_ready,
            'proc_id' => $proc_id,
            'categories' => Category::all(),
        ]);
    }

    /**
     * @param Request $request
     * @param Procedure $procedure
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Procedure $procedure)
    {
        $procedure->update($request->all());

        return redirect()->route('procedures.index')->with('alert-success', 'Procedure has been updated!');
    }

    /**
     * @param Procedure $procedure
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Procedure $procedure)
    {
        $procedure->delete();

        return redirect('procedures');
    }

     public function combinations($procedure)
    {
        //dd($procedure->title);
 

     return Combination::where('procedures', 'LIKE', '%'.$procedure->title.'%')->select('title as combTitle')->get()->toArray() ;
    }

     public function publish(Procedure $procedure)
    {
      
       $procedure = $procedure->load('questions.answers.params');         
        $stagingProcedure = $procedure ->setConnection('medecide_beta');
        //$something = $stagingProcedure->find(1);
          $stagingProcedure->update(); 
          return $stagingProcedure;
        //dd( $stagingProcedure  );
    }
}
