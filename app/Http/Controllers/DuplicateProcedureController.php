<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Procedure;
use App\ProcedureScores;

/**
 * Class duplicateProcedureController
 * @package App\Http\Controllers
 */
class DuplicateProcedureController extends Controller {
    
    /**
     * @param Procedure $procedure
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Procedure $procedure)
    {
        $new_proc = $this->createNewProcedure($procedure);
        $this->duplicateRelationsAndAssociate($procedure, $new_proc);
        $this->associateExampleScores($new_proc);
        
        return redirect()->route('procedures.index');
    }
    protected function duplicateAnswersAndAssociate($question, $new_question)
    {
        foreach ($question->answers as $answer) {
            $new_answer = $answer->replicate();
            $new_answer->question()->dissociate();
            $new_answer->question()->associate($new_question);
            $new_answer->save();
            $this->duplicateAnswerParamsAndAssociate($answer,$new_answer);
        }
    }
    public function duplicateAnswerParamsAndAssociate($answer, $new_answer){  //specific for params!
            //$new_answer->params()->sync($params);
            foreach ($answer->params()->withPivot('value','param_id')->get() as $param){
                $new_answer->params()->attach($param->pivot->param_id, ["value"=>$param->pivot->value]);
            }
    
    }
    /**
     * @param $question
     * @param $new_proc
     *
     * @return mixed
     */
    protected function duplicateQuestionAndAssociate($question, $new_proc)
    {
        $new_question = $question->replicate();
        $new_question->procedure()->dissociate();
        $new_question->procedure()->associate($new_proc);
        $new_question->save();
        
        return $new_question;
    }
    

    
    /**
     * @param Procedure $procedure
     * @param           $new_proc
     */
    protected function duplicateRelationsAndAssociate($procedure, $new_proc)
    {
        foreach ($procedure->questions as $question) {
            $new_question = $this->duplicateQuestionAndAssociate($question, $new_proc);
            $this->duplicateAnswersAndAssociate($question, $new_question);
        }
    }
    
    /**
     * @param $new_proc
     */
    protected function associateExampleScores($new_proc)
    {
        $scores = ProcedureScores::exampleScores();
        foreach ($scores as $score) {
            $new_score = $score->replicate();
            $new_score->procedure()->dissociate();
            $new_score->procedure()->associate($new_proc);
            $new_score->save();
        }
    }
    
    /**
     * @param Procedure $procedure
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function createNewProcedure($procedure)
    {
        $new_proc = $procedure->replicate();
        $new_proc->title = $new_proc->title . ' (copy)';
        $new_proc->save();
        
        return $new_proc;
    }
}
