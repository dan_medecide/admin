<?php

namespace App\Http\Controllers;

use App\Param;
use Illuminate\Http\Request;

class ParamsController extends Controller {
    
    public function index()
    {
        
        if (request()->wantsJson()) {
            
            return Param::with(['answers' => function ($query) {
           
                return $query->withPivot('value')->get();
            }])
            ->when(request()->has('target') && request('target') == 'questions' || request('target') == 'both',function($query){
                return $query->where('target','questions');
            })
            ->when(request()->has('target') && request('target') == 'answers' || request('target') == 'both', function ($query) {
                return $query->where('target', 'answers');
            })
            ->get();
        }
        
        return view('params.index', [
            'params' => Param::all(),
        ]);
    }
    
    public function create()
    {
        return view('params.create');
    }
    
    public function store(Request $request)
    {
        return Param::create([
            'title'  => $request->title,
            'type'   => $request->type,
            'target' => $request->target,
        ]);
    }
    
    public function edit(Param $param)
    {
        return view('params.edit', [
            'param' => $param,
        ]);
    }
    
    public function update(Request $request, Param $param)
    {
        $param->update([
            'title'  => $request->title,
            'type'   => $request->type,
            'target' => $request->target,
        ]);
        
        return response('param was updated successfully', 200);
    }
    
    public function destroy(Param $param)
    {
        $param->delete();
        
        return response('param was deleted successfully', 200);
    }
}
