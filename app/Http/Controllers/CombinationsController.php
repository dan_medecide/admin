<?php

namespace App\Http\Controllers;

use App\Combination;
use Illuminate\Http\Request;
use App\Category;
use App\Client;
use App\Procedure;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\StoreCombination;

/**
 * Class CombinationsController
 * @package App\Http\Controllers
 */
class CombinationsController extends Controller
{
    protected $categories;
    protected $clients;
    protected $procedures;
    protected $procedures_ids;

    /**
     * CombinationsController constructor.
     */
    public function __construct()
    {
        $procedures = new  Procedure;
       $this->procedures_ids  =   $procedures->all()->keyBy('title')->map(function ($procedure) {
            return trim($procedure->title). "@split@".$procedure->id;
        });
        $this->categories   = Category::all()->keyBy('id')->map->title;
        $this->clients      = Client::all()->keyBy('id')->map->client_name;
//               $source = ' procedure title <id> Desired text </id>';
//         preg_match("'<id>(.*?)</id>'si", $source, $match);
// if($match) 
// dd(str_replace($match[0],'',$source));
        $this->procedures   = $procedures->all()->keyBy('title')->map(function ($procedure) {
            return trim($procedure->title)."#{$procedure->id}#";
        });
         
    }

    /**
     * @return View
     */
    public function index(Request $request)
    {
         
      $combinationsTemp= collect([]);
       $medicalCombinations= collect([]);
        $combinations = Combination::on($request->query('connection', 'beta'))->with('category:id,title', 'client:id,client_name')->get();
          $combinations = $combinations->transform(function($row) use($combinationsTemp, $medicalCombinations) { 
                 if(collect($row->description)->contains("**TEMP**")) 
                 {
                      $combinationsTemp->push($row)  ;
                      return false;
                 }

                else if ($row->client AND $row->client->client_name == "Serenus") {
                      $medicalCombinations->push($row)  ;
                      return false;
                 } ;
                 return $row;
            })->filter() ; 
    
        return view('combinations.index')->with([
            'combinations' => $combinations,
            'combinationsTemp' => $combinationsTemp,
            'medicalCombinations' => $medicalCombinations,
            'connection' => $request->query('connection', 'beta'),
        ]);
    }

    /**
     * @return View
     */
    public function create()
    {
        
        
        return view('combinations.create', [
            'clients'    => $this->clients,
            'procedures' => $this->procedures,
            'categories' => $this->categories,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(StoreCombination $request)
    {
        $procedures = implode(' , ', $request->procedures);
        $category   = Category::find($request->cat_id);
        $client     = Client::find($request->client_id);
        $combination              = new Combination;
        $combination->title       = $request->title;
        $combination->description =  $request->description.' ';
        $combination->code        = $request->code;
        $combination->procedures  = $procedures;
        $combination->category()->associate($category);
        $combination->client()->associate($client);
        $combination->save();

        return redirect()->route('combinations.index');
    }

    /**
     * @param Combination $combination
     */
    public function show(Combination $combination)
    {
        //
    }

    /**
     * @param Combination $combination
     *
     * @return View
     */
    public function edit(Combination $combination)
    {
       // dd($this->procedures_ids);
        $current_procedures = explode(',', $combination->procedures);
       
       $current_procedures = Procedure::whereIn('title',   $current_procedures  )->get()->pluck('id','title')->map(function($id,$name)
       { 
    return "{$name}#{$id}#";})->flatten()->toArray();
    //dd($current_procedures );
        return view('combinations.edit', [
            'procedures_ids'  => $this->procedures_ids,
            'combination'        => $combination,
            'clients'            => $this->clients,
            'procedures'         => $this->procedures,
            'current_procedures' => $current_procedures,
            'categories'         => $this->categories,
            'current_categories' => $combination->cat_id,
            'current_client_id'  =>  $combination->client_id,
            'current_client' => $this->clients[$combination->client_id] ?? null
        ]);
    }

    /**
     * @param Request $request
     * @param Combination $combination
     *
     * @return RedirectResponse
     */
    public function update(StoreCombination $request, Combination $combination)
    {
        info(json_encode($request->client_id));
         //               $source = ' procedure title <id> Desired text </id>';
        $procedures =collect(explode( ',',$request->procedures) );
        $procedures->transform(function($procedure){
        preg_match("'#(.*?)#'si", $procedure, $match);
        info(print_r( $match,true));
        if ($match) $procedure = str_replace($match[0],'',$procedure);
        
        return $procedure;

});
//  info(print_r( $procedures,true));die;

        $category   = Category::find($request->cat_id);
        $client     = Client::find($request->client_id);
        $combination->title       =collect($procedures)->implode(",");// $request->title;
        $combination->description = $request->description;
        $combination->code        = $request->code;
        $combination->procedures  = collect($procedures)->implode(",");
        // $combination->category()->associate($category);
        // $combination->client()->associate($client);
        $combination->cat_id = $request->category;
        $combination->client_id = $request->clients;
        $combination->save();
        return redirect()->route('combinations.index');
    }

    /**
     * @param Combination $combination
     *
     * @return RedirectResponse
     */
    public function destroy(Combination $combination)
    {
        $combination->delete();

        return redirect()->back();
    }
}
