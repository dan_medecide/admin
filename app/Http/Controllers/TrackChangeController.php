<?php

namespace App\Http\Controllers;

use App\TrackChange;
use Illuminate\Http\Request;

class TrackChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd("in store!!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrackChange  $trackChange
     * @return \Illuminate\Http\Response
     */
    public function show(TrackChange $trackChange)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrackChange  $trackChange
     * @return \Illuminate\Http\Response
     */
    public function edit(TrackChange $trackChange)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrackChange  $trackChange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrackChange $trackChange)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrackChange  $trackChange
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrackChange $trackChange)
    {
        //
    }
}
