<?php

namespace App\Http\Controllers;
use App\Procedure;

use Illuminate\Http\Request;

class WebHookController extends Controller {
    
    public function index(Procedure $procedure)
    {
        dd($procedure->load('questions.answers.params')->toArray() );
    }
}
