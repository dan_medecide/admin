<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

/**
 * Class ClientsController
 * @package App\Http\Controllers
 */
class ClientsController extends Controller {
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $clients = Client::all();
        
        return view('client.index', compact('clients'));
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('client.create');
    }
    
    /**
     * @param Request $request
     *
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'client_name' => 'required|min:2|max:100',
            'client_code' => 'required|min:2|max:100',
        ]);
        
        return Client::create([
            'client_name'        => request('client_name'),
            'client_code'        => request('client_code'),
            'send_report_emails' => request('send_report_emails'),
        ]);
    }
    
    /**
     * @param Client $client
     */
    public function show(Client $client)
    {
        //
    }
    
    /**
     * @param Client $client
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Client $client)
    {
        return view('client.edit', compact('client'));
    }
    
    /**
     * @param Request $request
     * @param Client  $client
     *
     * @return Client
     */
    public function update(Request $request, Client $client)
    {
        $this->validate($request, [
            'client_name' => 'required|min:2|max:100',
            'client_code' => 'required|min:2|max:100',
        ]);
        
        $client->update([
            'client_name'        => request('client_name'),
            'client_code'        => request('client_code'),
            'send_report_emails' => request('send_report_emails'),
        ]);
        
        return $client;
    }
    
    /**
     * @param Client $client
     */
    public function destroy(Client $client)
    {
        $client->delete();
    }
}
