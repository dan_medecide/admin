<?php

namespace App\Http\Controllers;

use App\Procedure;
use function compact;
use function dd;
use function functionCallback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function view;
use DB;

class ClientsStatisticsController extends Controller {
    
    public function index ( $client )
    {
        return view( 'client.statistics', compact( 'client' ) );
    }
}
