<?php

namespace App\Http\Controllers;

use App\Statistic;
use Illuminate\Http\Request;
use Charts;

class StatisticsController extends Controller {
  public function index()
  {
    $statistics = Statistic::getStatistics();
    $tests_chart = Charts::create('pie', 'c3')
                         ->title(' ')
                         ->labels(['Indicated', 'Not indicated', 'Not done'])
                         ->values([$statistics['indicated'], $statistics['not_indicated'], $statistics['not_done']])
                         ->elementLabel('Quantity')
                         ->dimensions(0, 500);

    return view('statistics.statistics', compact('statistics', 'tests_chart'));
  }

  public function create()
  {
    //
  }

  public function store(Request $request)
  {
    //
  }

  public function show(Statistic $statistic)
  {
    //
  }

  public function edit(Statistic $statistic)
  {
    //
  }

  public function update(Request $request, Statistic $statistic)
  {
    //
  }

  public function destroy(Statistic $statistic)
  {
    //
  }
}
