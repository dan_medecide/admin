<?php

namespace App\Http\Controllers;

use App\Question,
    App\Procedure;
use App\Tag;
use App\Combination;
use DB;
use Illuminate\Http\Request;

class TagsController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index($proc_id)
    {
     
        $procedure = new Procedure;   
            $title =   $procedure->find($proc_id)->title;
            
        $combination_procedure_siblings_names_keys  = (new Combination )->procSiblingsKeys($title); //!names
            
            //Todo  get all indications from all of the combinations that include this Procedure.
            $combination_procedure_score_indications_keys =   $procedure->indicationSiblingsKeys($combination_procedure_siblings_names_keys);
            
            $proceduresIDs = ($procedure)->getIDs($combination_procedure_siblings_names_keys);
            
            if(  $proceduresIDs->isEmpty()){
   
                  $proceduresIDs = [$proc_id];
        }
           
            $returned = collect();
           

        foreach ($proceduresIDs as $procedureID ) {
            
 
                $proc_id = $procedureID;
        $relations = [
            'answers' => function ($query) {
                return $query->where('is_deleted', 0)
                    ->select('tag', 'id', 'question_id', 'display_groups', 'indication');
            },
        ];

        $questions = Question::with($relations)
            ->where('proc_id', $proc_id)
            ->where('is_deleted', 0)
            ->select('question_tag', 'id', 'indication','proc_id')
            ->get();
 
        $indications_keys = collect();
        $groups_keys = collect();
        $answers_keys = collect();
        $questions_keys = collect();
        $globalKeys = collect();
        foreach ($questions as $question) {
            $questions_keys = $questions_keys->concat($question->question_tag);
            $indications_keys = $indications_keys->concat($question->indication);
            foreach ($question->answers as $answer) {
                $groups_keys = $groups_keys->concat($answer->display_groups);
            //  if($answer->title)   $globalKeys = $globalKeys->concat($answer->title) ;
                $exists_keys = collect($answer->tag)->map(function ($key) {
                    return $key . ' exists';
                });
                $answers_keys = $answers_keys->concat($answer->tag)->concat($exists_keys);
                $indications_keys = $indications_keys->concat($answer->indication);
            }
        }

        $indications_keys = $indications_keys->unique()->filter(function ($indication) {
            return !ends_with($indication, '*');
        });
      
       
       
        $scores_keys = $indications_keys->merge($combination_procedure_score_indications_keys)->map(function ($indication) {
            return '#score ' . $indication;
        });
      //  dd(  $scores_keys);
        $decisions_keys = $indications_keys->map(function ($indication) {
            return '#decision ' . $indication;
        });

        // $groups_keys = $groups_keys->transform(function ($group) {
        //     return '#' . $group;
        // });
           $groups_keys = $groups_keys->transform(function ($group) {
            return '#' . $group;    
        });
 
        $returned->push($questions_keys
            ->merge($answers_keys)
            ->merge($groups_keys)
            ->merge($scores_keys)
            ->merge($combination_procedure_siblings_names_keys )
            ->merge($decisions_keys)
            ->merge([$questions[0]->procedure->title])
            ->merge(['#score', '#decision'])
            ->unique() );
             
       }
        
       return $returned->flatten()->unique();
    
}
    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag $key
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $key)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag $key
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $key)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tag $key
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $key)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag $key
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $key)
    {
        //
    }

    public function indicationSiblings(Procedure $procedure, $id)
    {
        $combination_procedure_siblings_names_keys  = (new Combination)->procSiblingsKeys($procedure->find($id)->title); //!names
        return   $procedure->indicationSiblingsKeys($combination_procedure_siblings_names_keys);
    }
}
