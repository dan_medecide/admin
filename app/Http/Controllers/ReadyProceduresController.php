<?php

namespace App\Http\Controllers;

use App\Procedure;
use Illuminate\Http\Request;

/**
 * Class ReadyProceduresController
 * @package App\Http\Controllers
 */
class ReadyProceduresController extends Controller {
    
    /**
     * @var Procedure
     */
    protected $procedure;
    
    /**
     * ReadyProceduresController constructor.
     *
     * @param $procedure
     */
    public function __construct(Request $request)
    {
        
        $this->procedure = Procedure::find($request->procedure);
    }
    
    /**
     * @param Request $request
     */
    public function update()
    {
        $this->procedure->is_ready = $this->procedure->is_ready == 0 ? 1 : 0;
        $this->procedure->save();
        
        return redirect()->back();
    }
}
