<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Queries\CombineQuestions;

class CombineQuestionsController extends Controller {
    
    public function create(Request $request)
    {
        $combined_question = new CombineQuestions($request->first, $request->second, $request->firstAnswers, $request->secondAnswers);
        
        return $combined_question->get();
    }
}
