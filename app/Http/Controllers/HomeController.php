<?php

namespace App\Http\Controllers;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index()
    {
        
        if (!auth()->check()) {
            return redirect()->route('login')->with('status', 'You have not been assigned a role, please contact out support.');
        }
        $user_role = auth()->user()->roles->first()->name;
        switch ($user_role) {
            case 'admin':
                $origin = session()->get('origin_destination');
                if ( $origin AND !str_contains($origin , "home/supervisor" )  )
                    return redirect(session()->get('origin_destination'));
                
                return redirect()->route('home.admin');
                break;
            case 'doctor':
                return redirect()->route('home.doctor');
                break;
            case 'supervisor':
                return redirect()->route('home.supervisor');
                break;
        }
       
        return redirect('login');
    }
}
