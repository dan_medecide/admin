<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Conflict;
use App\Question;
use App\User;
use stdClass;

class ConflictsController extends Controller
{
    public function index($procedure)
    {
        $initial_conflicts = Conflict::where('proc_id', '=', $procedure)->get();
        $conflicts         = $initial_conflicts->map(function ($initial_conflict, $key) {
            $conflict                 = new stdClass();
            $conflict->id             = $initial_conflict->id;
            $conflict->date           = $initial_conflict->created_at;
            $conflict->test_id        = $initial_conflict->test_id;
            $user                     = User::find($initial_conflict->user_id);
            $conflict->user_id        = $initial_conflict->user_id;
            $conflict->user           = $user->first_name . ' ' . $user->last_name;
            $conflict->question_id    = $initial_conflict->question_id;
            $question                 = Question::find($initial_conflict->user_id);
            $conflict->question       = $question ? $question->title : '';
            $conflict->answer_id      = $initial_conflict->answer_id;
            $answer                   = Answer::find($initial_conflict->answer_id);
            $conflict->answer         = $answer ? $answer->title : '';
            $conflict->test_answer_id = $initial_conflict->test_answer_id;
            $conflict->probability    = $initial_conflict->probability;
            $conflict->conflict_set   = $initial_conflict->conflict_set;

            return $conflict;
        });

        return view('conflicts.conflicts', compact('conflicts'));
    }

    public function destroy(Conflict $conflict)
    {
        $conflict->delete();

        return response(200);
    }
}
