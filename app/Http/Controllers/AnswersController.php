<?php

namespace App\Http\Controllers;

use App\Question,
    App\Param,
    App\Answer;
use DB;

//use App\AnswerHelper;
use Illuminate\Http\Request;;

class AnswersController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function getTagsData($source)
    {
        $data = DB::table('answers')
            ->select($source . ' as title')
            ->where($source, '!=', '')
            ->groupBy($source)
            ->get();

        $delimiter = $source == 'indication' || $source == 'tag' ? ',' : '||';

        return $data->pluck('title')->map(function ($item, $key) use ($delimiter) {
            return explode($delimiter, $item);
        })->flatten()->unique()->filter();
    }

    public function getFormulas()
    {
        $data = DB::table('answers')
            ->select('weight_formula', 'keys_for_showing_answer', 'keys_for_showing_answer_teach', 'vas_auto_answer_formula', 'auto_keys', 'keys_for_end_test')
            ->get()
            ->unique()
            ->flatMap(function ($fields) {
                foreach ($fields as $field) {
                    return !empty($field);
                }
            });
    }

    public function store(Request $request, $question_id)
    {
        $this->validate($request, [
            'title' => 'required',
            'priority' => 'required|numeric',
            'tag.*' => ['regex:/^[a-zA-Z0-9 \<\>\=\/\\\-]+$/'],
        ], [
            'tag.*' => 'Created key format is invalid.',
        ]);

        if ($request->has('vas_intermediate_score')) {
            $size = count($request->input('vas_intermediate_score'));
            $this->validate($request, [
                'vas_intermediate_display' => "array|size:{$size}",
            ], [
                'vas_intermediate_display.size' => 'Must be the same length as intermediate score.',
            ]);
        }

        $question = Question::findOrFail($question_id);
        if ($request->has('vas_min_display') &&
            $request->has('vas_max_display') && ($request->vas_min_display != $request->vas_max_display) &&
            $question->is_auto) {
        // if ($request->has('vas_min_display') &&
        //     $request->has('vas_max_display') && ($request->vas_min_display != $request->vas_max_display) &&
        //     $question->is_auto) {
            $this->validate($request, [
                'vas_auto_answer_formula' => 'required'
            ]);
        }

        $question = Question::find($question_id);
        $answer = new Answer($request->all());
        $answer->question()->associate($question);
        $answer->save();

        return $answer;
    }

    public function show(Answer $answer)
    {
        //
    }

    public function edit(Answer $answer)
    {
        return $answer->load('params');
    }

    public function update(Request $request, Answer $answer)
    {
//   $ansParamsCtrl= new AnswerParamsController;
//   $ansParamsCtrl->updateHelpers(
//       $request->input('id'),
//       $request  );
        //                         'vas_dont_show_global_no' => $request->input('vas_dont_show_global_no')] ) );
        //  $answerHelper->updateOrCreate(['answer_id' => $request->input('id')],[ 
        //                         'vas_dont_show_global_unknown' => $request->input('vas_dont_show_global_unknown'),
        //                         'vas_dont_show_global_no' => $request->input('vas_dont_show_global_no')]
        //                 );
    
            
    
        $this->validate($request, [
            'title' => 'required',
            'priority' => 'required|numeric',
          
        ]);

        $question = $answer->question()->first();
        //$answer->isVas()
        if ($question->is_auto AND $answer->hasVasRequest($request)) {
            $this->validate($request, [
                'vas_auto_answer_formula' => 'required',
            ]);
        }   
          if ($answer->hasVasRequest($request)  ) {
            $this->validate($request, [
                 'vas_min_display' => 'required',
                 'vas_max_display' => 'required'
            ]);
        }   
              
             if (head($request->vas_intermediate_score  )!= null  OR head($request->vas_intermediate_display)!= null )
             if ($request->has('vas_intermediate_score') AND count($request->vas_intermediate_score) )  {
            $size = count($request->input('vas_intermediate_score'));
            $this->validate($request, [
                'vas_intermediate_display' => "array|size:{$size}", 
                'vas_intermediate_display.*' => "required",
                'vas_intermediate_score.*' => "required"
   
            ], [
                'vas_intermediate_display.size' => 'Must be the same length as intermediate score.',
            ]);
        }
  
        $answer->update($request->all());
        return $answer;
    }

    public function split(Request $request, Answer $answer)
    {
          
        foreach ($request->input('characteristics') as $characteristic) {
            $id = (Param::where('title','answer_type')->first()->id);
            $new_answer = $answer->replicate(['title', 'tag']);
        
            $new_answer->title = $characteristic;
            $new_answer->display_groups = $answer->title;
            $new_answer->tag = $answer->tag[0] . ' ' . strtolower($characteristic);
            $new_answer->save();

            if ($request->isSingleSubAnswer) {
                
                $new_answer->vas_min_display = $new_answer->vas_max_display = $new_answer->vas_intermediate_display =$new_answer->vas_min = $new_answer->vas_max = null;
                $new_answer->params()->sync([ $id => ['value' => "single sub-answer",'answer_id' =>$new_answer->id]]);
                $new_answer->save();
            }    
                

           
        }

        $answer->delete();

        return ['done'=> true, 'response'=> $new_answer->load('params')];
    }

    public function destroy(Answer $answer)
    {
        $answer->delete();

        return 'Answer Deleted.';
    }

    public function duplicate(Answer $answer)
    {
        $duplicatedAnswer = $answer->replicate();
        $answer->title .= "Copy";
        $answer->save();
       // $duplicatedAnswer->title .= "Copy-new-is-down";
        $duplicatedAnswer->save();
        return $duplicatedAnswer;
    }
}
