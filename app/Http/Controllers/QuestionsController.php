<?php

namespace App\Http\Controllers;

use App\Question;
use App\Answer;
use DB;
use Illuminate\Http\Request;
use App\Procedure;

class QuestionsController extends Controller {
    
    public function init(Request $request)
    {
        $proc_id = $request->proc_id;
        $procedure = Procedure::where('id', $proc_id)->with('category:id')->first();
        $proc_title = trim($procedure->title);
        $proc_is_ready = $procedure->is_ready;
        $user_token = auth()->user()->token;
        $cat_id = $procedure->category->id;
        
        return view('questions.questions', compact('procedure','proc_id', 'proc_title', 'user_token', 'cat_id','proc_is_ready'));
    }
    
    public function index($proc_id)
    {
        $questions = Question::with([
            'answers' => function($answer) {
                $answer->where('is_deleted', '=', '0');
                $answer->where('deleted_at', '=', null);
                $answer->orderBy('priority', 'desc');
            },
        ])
                             ->where('is_deleted', '=', 0)
                             ->whereNull('deleted_at')
                             ->where('proc_id', '=', $proc_id)
                             ->orderBy('priority', 'desc')
                             ->get();
        
        $questions->map(function($question) {
            return $question['display'] = false;
        });
        
        return $questions;
    }
    
    public function getProceduresScores($proc_id)
    {
        $procedure_scores = DB::table('procedures_score')
                              ->select('score_from', 'score_to', 'title')
                              ->where('proc_id', '=', $proc_id)
                              ->where('deleted_at', '=', null)
                              ->where('is_deleted', '=', 0)
                              ->orderBy('score_from','asc')
                              ->get();
        
        return $procedure_scores;
    }
    
    public function create()
    {
        //
    }
    
    public function getTagsData($source)
    {
        $data = DB::table('questions')
                  ->select($source . ' as title')
                  ->where($source, '!=', '')
                  ->groupBy($source)
                  ->get();
        
        return $data->pluck('title')->map(function($item, $key) {
            return explode(',', $item);
        })->flatten()->unique()->filter();
    }
    
    public function store(Request $request, $proc_id)
    {
        $this->validate($request, [
            'title_doctor'   => 'required',
            'priority'       => 'required|numeric',
            'question_tag.*' => ['regex:/^[a-zA-Z0-9 \<\>\=\/\\\-]+$/'],
        ], [
            'question_tag.*' => 'Created key format is invalid.',
        ]);
        
        $procedure = Procedure::find($proc_id);
        $question = new Question($request->all());
        $question->procedure()->associate($procedure);
        $question->save();
        
        return $question;
    }
    
    public function show(Question $question)
    {
        return $question;
    }
    
    public function edit(Question $question)
    {
        return $question;
    }
    
    public function update(Request $request, Question $question)
    {
        $this->validate($request, [
            'title_doctor'   => 'required',
            'priority'       => 'required|numeric',
            'question_tag.*' => ['regex:/^[a-zA-Z0-9 \<\>\=\/\\\-]+$/'],
        ], [
            'question_tag.*' => 'Created key format is invalid.',
        ]);
        
        $question->update($request->all());
        
        return $question;
    }
    
    public function destroy(Question $question)
    {
        $question->delete();
        
        return 'Question Deleted.';
    }
    
    public function duplicate($id)
    {                               
        
        $question = Question::with('answers')->find($id);
        $duplicatedQuestion = $question->replicate(); 
        $copy = ' - Copy';
        if (request()->query('proc_id') )
        { 
            $copy = " - Imported";
            $duplicatedQuestion->proc_id = request()->query('proc_id') ; //!replicate from another procedure
        }
        $duplicatedQuestion->title_doctor .=  $copy;
        
        $duplicatedQuestion->save();
        
        foreach ($duplicatedQuestion->answers as $key => $answer) {

            $duplicatedAnswer = $answer->replicate();

            $duplicatedAnswer->question_id = $duplicatedQuestion->id;

            $duplicatedAnswer->save();

         }

        return $duplicatedQuestion;




        return $clone->load('answers');
    }

    public function createAutoQuestion(Request $request,Question $question)
    {   
       // $request= $request->all()[''];
        $keysForShowingAnswers=$request->keyForShowingAnswers;
        $priority=$question->priority;
        $question->changePriorities($priority,$question->proc_id);
        $newAutoQuestion = [
            'indication'=>rtrim( $request->indication,'*'),
            'priority'=>$priority-1,
            'title_doctor'=> 'global score auto question',
            'is_auto'=>1,
            'proc_id'=> $question->proc_id 
        ];
      $auto_question_id =  $question->create($newAutoQuestion)->id;
        // dd($auto_question_id);
        $answer= new Answer;
        foreach ($keysForShowingAnswers as $val) {
        $answer->globalScoreAnswer($val,$auto_question_id);   
        }
        return $request->all();
    }
}
