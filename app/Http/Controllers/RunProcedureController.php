<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class RunProcedureController extends Controller
{
    public function __invoke($procedure_id)
    {
        $token = session('api-token');

        //return redirect("http://test.medecide.net/run-test/{$procedure_id}?token={$token}");
        return redirect("http://client.medecide.net/run-test/{$procedure_id}?token={$token}");
    }
}
