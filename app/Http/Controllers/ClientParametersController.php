<?php

namespace App\Http\Controllers;

use App\Client;
use App\Parameter;
use function dd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientParametersController extends Controller {
    public function index ()
    {

        $beta_params       =      Parameter::all(); //$this->getBetaParams( new Parameter );
        $production_params =   Parameter::all(); // $this->getProdcutionParams( new Parameter );

        return view( 'parameters.parameters', compact( 'beta_params', 'production_params' ) );
    }

    protected function getBetaParams ( Parameter $param )
    {
        dd($param->connection);
        $param->connection = 'beta';

        return Parameter::all();
    }

    protected function getProdcutionParams ( Parameter $param )
    {
        $param->connection = 'production';

        return Parameter::all();
    }

    public function create ( Parameter $param )
    {
        $clients_id = Client::all()->keyBy( 'id' )->map->client_name;

        return view( 'parameters.add_param_modal', compact( 'clients_id' ) );
    }

    public function store ( Request $request )
    {
        $this->validate( $request, [
            'param_name'  => 'required',
            'param_value' => 'required',
            'client_id'   => 'required|numeric',
        ] );

        return Parameter::create( $request->all() );
    }

    public function show ( Parameter $param )
    {
        //
    }

    public function edit (  $param )
    {
        $params =  new Parameter ;
        $param=$params->find($param);
        //$production_params = $this->getProdcutionParams( new Parameter );
        $clients_id = Client::all()->keyBy( 'id' )->map->client_name;
            
        return view( 'parameters.edit_params_modal', compact( 'param', 'clients_id' ) );
    }

    public function update ( Request $request, $param )
    {
          $params =  new Parameter  ;
          $param=$params->find($param);    

        $this->validate( $request, [
            'param_name'  => 'required',
            'param_value' => 'required',
            'client_id'   => 'required|numeric',
        ] );

        $param->update( $request->all() );

        return  $param;
    }

    public function destroy ( $param )
    {
         $params = $this->getBetaParams( new Parameter );
         $param=$params->find($param);    
         $param->delete();

        return 'deleted';
    }
}
