<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Queries\ExportTables;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller {
    
    /**
     * @param  Request $request
     * @param  string  $table
     *
     * @return Collection
     */
    public function index(Request $request, $table)
    {
        if ($this->userNotAuthorized(auth()->user())) {
            return response('Access denied', 403);
        }
        if ($this->tableDoesntExistInDb($table)) {
            return response('Table name does not exist in database', 422);
        }
        if ($table == 'users') {
            return response('Table access forbidden', 403);
        }
        if ($request->has('limit') and $this->exceedsAllowedLimit($request->query('limit'))) {
            return response('Max limit is 10000', 403);
        }
        if ($request->has('columns')) {
            foreach (ExportTables::requestedColumnNames($request->query('columns')) as $column) {
                if (! in_array($column, ExportTables::allColumnNames($table))) {
                    return response('Column name does not exist in requested table', 422);
                }
            }
        }
        
        return ExportTables::get($request, $table);
    }
    
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        if (empty($request->json()->all())) {
            return response('Could not parse your request, make sure you provide a valid JSON', 422);
        }
        
        if (! $request->has('test_id')) {
            return response('Test ID parameter is required', 422);
        }
        
        if (! $request->has('decision_code')) {
            return response('Decision code parameter is required', 422);
        }
        
        try {
            $test = Test::findOrFail($request->test_id);
        } catch (ModelNotFoundException $exception) {
            return response('Test could not be found', 400);
        }
        
        $test->update([
            'decision_code' => $request->decision_code ? 4 : 1,
            'label_json'    => json_encode($request->except('test_id', 'decision_code')),
        ]);
        
        return response('Tests have been updated successfully', 200);
    }
    
    /**
     * @return array
     */
    protected function dbTableNames()
    {
        return DB::connection()->getDoctrineSchemaManager()->listTableNames();
    }
    
    /**
     * @param $table
     *
     * @return bool
     */
    protected function tableDoesntExistInDb($table)
    {
        return ! in_array($table, $this->dbTableNames());
    }
    
    /**
     * @param $limit
     *
     * @return bool
     */
    protected function exceedsAllowedLimit($limit)
    {
        return $limit > 10000;
    }
    
    /**
     * @param $user
     *
     * @return mixed
     */
    protected function userNotAuthorized($user)
    {
        return $user->doesntHaveRole('admin');
    }
}
