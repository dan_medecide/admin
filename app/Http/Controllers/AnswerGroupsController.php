<?php

namespace App\Http\Controllers;

use App\AnswerGroup,
    App\Answer;
use Illuminate\Http\Request;

class AnswerGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $d = (new AnswerGroup);   
        return  (new Answer)->storeGroup( $request , $d );
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnswerGroup  $answerGroup
     * @return \Illuminate\Http\Response
     */
    public function show(AnswerGroup $answerGroup)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnswerGroup  $answerGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(AnswerGroup $answerGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnswerGroup  $answerGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnswerGroup $answerGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnswerGroup  $answerGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnswerGroup $answerGroup)
    {
        //
    }
}
