<?php

namespace App\Http\Controllers;

use App\Answer;
use App\AnswerGroup;
use App\DisplayGroup;

use App\Param;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use DB;
class AnswerParamsController extends Controller {
    
     
    public function show($answer_id)
    {
        return Answer::find($answer_id)->params()->get();
    }
    
    public function update($answer_id, Request $request)
    {
  
        $answer = Answer::find($answer_id);
       //Todo - Handle Answer Group
        $answer->handleAnswerGroup();   
        $oldAnswerParams =  $answer->params();        
        $params = collect($request->input('params'))
            ->filter(function($param) {
                return $param['value'] || $param['value'] == '0';
            })
            ->reduce(function($carry, $item) {
                $carry[ $item['id'] ] = ['value' => $item['value']];
                        
                return $carry;
            });
        //  dd(['oap' => $oldAnswerParams->get()->toArray() , 'p' =>  $params]);
        $answer->params()->sync($params);   
        $answerParamResponse = DB::table('answer_param')->where('answer_id',$answer_id)->get();
        return response($answerParamResponse, 200);
    }

    public function answersParamsSameGroups($question_id, Request $request)
    {
        $answer=Answer::select('answers.id','display_groups')->where('question_id',$question_id)
        ->with('params:title')
       // ->where('params:title' ,'vas_dont_show_global_no')
        ->get();
       return response($answer, 200);
    }

    public function updateAnswersMultiLabels($display_group, Request $request)
    {



        $answers = Answer::select('answers.id','display_groups','priority','keys_for_showing_answer')->where('question_id',$request->input('answer')['question_id'])
            ->where('display_groups',$request->input('answer')['display_groups'])
            ->with('params:title,value')
            ->get();
        $currentAnswer=$answers->find($request->input('answer')['id']);
            foreach ($answers  as $answerToUpdate){

               if($answerToUpdate->id != $currentAnswer->id){
                //    $answerToUpdate->priority=$currentAnswer->priority;
                    $answerToUpdate->keys_for_showing_answer=$currentAnswer->keys_for_showing_answer;
                    $answerToUpdate->save();
                $params = $currentAnswer->params()->withPivot('value','param_id')->get();

                foreach( $params as  $param){

                     if(str_contains($param->title ,'global') ){
                        $answerToUpdate->params()->syncWithoutDetaching([ $param->pivot->param_id => ['value' => $param->pivot->value]]);
                        }
                    }
                }
    }
        return response($answers, 200);
    }
  
}
