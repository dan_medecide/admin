<?php

namespace App\Http\Controllers;

use App\Queries\Indications;

class IndicationsController extends Controller {
    
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index($proc_id)
    {
        $indications = new Indications($proc_id);
        
        return $indications->get();
    }
}
