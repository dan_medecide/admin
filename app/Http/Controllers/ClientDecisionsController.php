<?php

namespace App\Http\Controllers;

use DB;
use App\Test;
use Illuminate\Http\Request;

/**
 * Class ClientDecisionsController
 * @package App\Http\Controllers
 */
class ClientDecisionsController extends Controller {
    
    /**
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return DB::table('decision_codes')->select('title', 'code')->where('show_in_teach', '!=', 0)->get();
    }
    
    /**
     * @param Request $request
     *
     * @return string
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'supervisor_decision' => 'required',
        ]);
        
        $test = Test::find($request->test_id);
        
        $test->update([
            'supervisor_decision'             => $request->supervisor_decision,
            'supervisor_decision_explanation' => $request->supervisor_decision_explanation,
        ]);
        
        return $test;
    }
}
