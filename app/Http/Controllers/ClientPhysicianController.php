<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ClientPhysicianController extends Controller {
    
    public function index($client)
    {
        return DB::table('tests')
                 ->join('combination_instances', 'tests.combination_instance_id', '=', 'combination_instances.id')
                 ->where('client_id', '=', $client)
                 ->select('tests.physician_name')
                 ->groupBy('tests.physician_name')
                 ->pluck('tests.physician_name');
    }
}
