<?php

namespace App\Http\Controllers;

use App\Decision;
use App\Procedure;
use App\ProcedureScores;
use Illuminate\Http\Request;
use  Input;
class ProcedureScoresController extends Controller
{
    public function index(Procedure $procedure )
    {
       $proc_id=$procedure->id;
       $proc_title=$procedure->title;
       $proc_is_ready=$procedure->is_ready;
        $scores = $procedure->scores()->with('decision:code,title,is_indication')->get();
        
        return view('procedure_scores.index', compact('scores','proc_id','proc_title','proc_is_ready'));
    }
    
    public function edit(ProcedureScores $score)
    {
        $decisions = Decision::all()->keyBy('code')->map->title;
        
        return view('procedure_scores.edit', compact('decisions', 'score'));
    }
    
    public function update(Request $request, ProcedureScores $score)
    {
        $this->validate($request, [
            'title'         => 'required',
            'score_from'    => 'required|numeric',
            'score_to'      => 'required|numeric',
            'decision_code' => 'required',
        ]);
        
        $score->update($request->all());
    }

    public function destroy(ProcedureScores $score)
    {
        $score->delete();
    }
}
