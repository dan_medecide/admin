<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use DB;
use Session;


class RolesController extends Controller {
    public function init ()
    {
        return view( 'roles.roles' );
    }

    public function index ()
    {
        return Role::all()->toArray();
    }

    public function create ()
    {
        return view( 'roles.add_role_modal', compact( 'users' ) );
    }

    public function store ( Request $request )
    {
        $this->validate( $request, [
            'name'        => 'required|string|min:2|max:255',
            'description' => 'required|string|min:2|max:255',
        ] );

        return Role::create( $request->all() );
    }

    public function show ( $id )
    {
        //
    }

    public function edit ( Role $role )
    {
        return view( 'roles.edit_role_modal', compact( 'role' ) );
    }

    public function update ( Request $request, Role $role )
    {
        $this->validate( $request, [
            'name'        => 'required|string|min:2|max:255',
            'description' => 'required|string|min:2|max:255',
        ] );

        $role->update( $request->all() );

        return $role;
    }

    public function destroy ( Role $role )
    {
        $role->users()->detach();

        $role->delete();

        return 'Role deleted';
    }
}
