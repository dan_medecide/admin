<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserRolesController extends Controller {

    public function edit ( User $user )
    {
        $roles = Role::all()->toArray();

        return view( 'user.partials.roles', compact( 'user', 'roles' ) );
    }

    public function update ( User $user )
    {
        $user->roles()->sync(request()->all());
    }
}
