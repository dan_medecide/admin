<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use function redirect;
use Session;
use Auth;
use Illuminate\Database\QueryException;

class LoginController extends Controller {
    
    use AuthenticatesUsers;

    public function __construct()
    {
      
        $this->middleware('getToken')->except('logout');
    }

    public function redirectTo()
    {
       
        $user_roles = auth()->user()->roles()->get()->map->name->toArray();
        if (in_array('admin', $user_roles)) {
           
            return route('home.admin');
        } else if (in_array('doctor', $user_roles)) {
            return route('home.doctor');
        } else if (in_array('supervisor', $user_roles)) {
            return route('home.supervisor');
        }
        
        return route('home');
    }
}
