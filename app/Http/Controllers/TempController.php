<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Procedure;
use App\Combination;

class TempController extends Controller {
    
    public function index($category_id)
    {
        return Procedure::join('cat', 'procedures.cat_id', '=', 'cat.id')
                        ->where('procedures.is_deleted', '=', 0)
                        ->where('cat.id', '=', $category_id)
                        ->select('procedures.title', 'procedures.abbreviation')
                        ->get();
    }
    public function setTempCategory(  $id, Request $request)
    {
   if (str_contains($request->path() ,'combinations'  ) ) { 
       $combination =  Combination::find($id);
       $combination->description = "**TEMP**" ;
       $combination->save();
       return $combination;
   }
        //(Procedure $procedure, Request $request)
        $previousCatId= $procedure->category->id;
        $oldCategoryTitle=  $procedure->category->title;
        if($request->previousCatId) {   //* If Undo
        $procedure->cat_id=$request->previousCatId;
        } else {  $procedure->cat_id=12;}
        //dd($procedure);
         $procedure->save(); 
         return response()->json(
            ['previousTitle'=>$oldCategoryTitle,
            'undo'=> ($request->previousCatId ? true : false),
            'previousCatId'=>$previousCatId,
            'massage'=>'Procedure category was successfully updated to '.$procedure->category->title,
            'id'=>$procedure->id,
            'status'=>200]);
    }
}
