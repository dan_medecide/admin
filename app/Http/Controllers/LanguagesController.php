<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguagesController extends Controller {
    
    public function update($lang)
    {
        if (array_key_exists($lang, config()->get('languages'))) {
            session()->put('locale', $lang);
        }
        
        return redirect()->back();
    }
}
