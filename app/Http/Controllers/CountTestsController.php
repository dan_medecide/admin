<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Test;
use DB;
use PhpParser\Builder;

class CountTestsController extends Controller
{
    
    /**
     * Returns tests count filtered by params
     *
     * @param  int    $from The month to count from.
     * @param  string $to The month to count to.
     * @param  string $group Parameter to group by.
     * @param  string $physician Physician name to filter by.
     *
     * @return Builder
     */
    public function index($from, $to, $group = 'month', $physician = '', Request $request)
    {
        $user_client = auth()->user()->client->id;
        $tests = Test::done()
                    // ->thisYear()
                     ->join('combination_instances', 'tests.combination_instance_id', '=', 'combination_instances.id')
                     ->join('decision_codes', 'tests.decision_code', '=', 'decision_codes.code')
                     ->where('combination_instances.client_id', '=', $user_client)
                     ->when($request->query('proc'), function ($query) use ($request) {
                         return $query->where('tests.proc_id', $request->query('proc'));
                     })
                     ->whereBetween(DB::raw('MONTH(tests.created_at)'), [$from, $to])
                     ->select(
                        'tests.id',
                        DB::raw('COUNT(tests.id) as count'),
                        DB::raw('SUM(decision_codes.is_indication) as indicated'),
                        'decision_codes.is_indication as indication',
                        'tests.proc_id'
                    );

        if (! empty($physician)) {
            $tests = $tests->wherePhysician($physician);
        }
        
        if ($group == 'procedure') {
            $tests = $tests->withProcedures()->groupBy('procedures.title');
        }
        
        if ($group == 'month') {
            $tests = $tests->withMonths()->groupBy('month');
        }
        
        if ($group == 'physician_name') {
            $tests = $tests->withProcedures()->withPhysicians()->groupBy('physician_name');
        }
        
        $tests = $tests->get();
        
        return $tests;
    }
}
