<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;

class DisplayGroupsController extends Controller {
    
    public function index($question_id)
    {
        return Answer::whereQuestionId($question_id)->pluck('display_groups')->filter()->flatten();
    }
    
}
