<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ProductionLinkController
 * @package App\Http\Controllers
 */
class ProductionLinkController extends Controller
{
    
    /**
     * @param $link
     *
     * @return string
     */
    public function __invoke($type, $id)
    {
        //'http://beta.medecide.net/auth/' 
        return 'http://beta.medecide.net/auth/' . auth()->user()->token . '/' . base64_encode('/' . $type . '/' . $id . '/'. salt($id) );
    }
}
