<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreCombination
 * @package App\Http\Requests
 */
class StoreCombination extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        $rules = [
          //  'title'      => 'required|min:2|max:400',
            'code'       => 'required',
        ];

        if ( $this->has( 'description' ) )
        {
         //   $rules['description'] = 'min:10|max:300';
        }

        return $rules;
    }
}
