<?php

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

// gets a url that redirects to core app engine, authenticates user and then redirects to $link
function getCoreAppLink($link)
{
    return 'http://beta.medecide.net/auth/' . auth()->user()->token . '/' . base64_encode($link);
}

function salt($x)
{
    return substr(sha1($x . '1QAZXSW23EDC'), -8);
}

function getProductionLink($link)
{
    return 'http://medecide.net/auth/' . auth()->user()->token . '/' . base64_encode($link);
}

function getToken($email, $password)
{
    $client = new Client();

    try {
        return $client->request('GET', 'http://core.medecide.net/public/login', [
            'query' => [
                'email' => $email,
                'password' => $password
            ]
        ])->getBody()->getContents();

    } catch (GuzzleException $exception) {
        return $exception->getMessage();
    }
}
function clientSymbol($id) {
 switch ($id) {
        case '29':
          return "&#120138;";
           break;
        case '26':
          return "&#8459;";
           break;
        case '18':
          return "&#x0224e;";
          break;
        default: return "&#120074;";
          break;
      }
}
function walker($bind){
        //$bind = ' Unknown-0.05 , Not tested-0.05  ,  logNormal-0.9' ;
        $nums = explode(',',$bind);
        info("inside wakjer");
        foreach ($nums as $key => $num)
        {
            $num =   strtolower($num);
            $num = str_replace( "'" , '', $num);
            $num = str_replace(  ' " ' , '', $num);
            info($num.' <---num');
            $num = explode('-',$num);
            $nums[trim($num[0])]=  trim ($num[1] ) ;
            unset($nums[$key]);

        }
        return $nums;
    }