<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Statistic extends Model {

  public static function getStatistics($client_id = 0, $proc_id = 0)
  {
    $tests = DB::table('tests')
               ->select('tests.id', 'tests.proc_id', 'tests.is_done', 'decision_codes.is_indication', 'combination_instances.client_id')
               ->leftJoin('decision_codes', 'tests.decision_code', '=', 'decision_codes.code')
               ->Join('combination_instances', 'tests.combination_instance_id', '=', 'combination_instances.id')
               ->get();

    if ($client_id)
    {
      $tests = $tests->where('client_id', '=', $client_id);
    }
    if ($proc_id)
    {
      $tests = $tests->where('proc_id', '=', $proc_id);
    }
    if ($client_id && $proc_id)
    {
      $tests = $tests->where('proc_id', '=', $proc_id)->where('client_id', '=', $client_id);
    }
    $total_tests = $tests->count();
    $indicated = $tests->where('is_indication', '=', 1)->count();
    $not_indicated = $tests->where('is_indication', '=', 0)->count();
    $not_done = $tests->where('is_done', '=', 0)->count();

    return $statistics = [
      'tests' => $tests,
      'total_tests' => $total_tests,
      'indicated' => $indicated,
      'not_indicated' => $not_indicated,
      'not_done' => $not_done,
    ];

  }
}
