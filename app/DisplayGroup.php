<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class DisplayGroup extends GroupModel
{
     use SoftDeletes;
    
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
  //  protected $dateFormat = 'U';
    protected $guarded= [];
  protected $table = 'display_groups';
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['created_at', 'updated_at', 'deleted_at'];

       public function answerGroup()
    {
          
         return head(( head($this->answers() )  ) )->answerGroup() ;
    }
    //    public function question()
    // {
    //     return $this->belongsTo(Question::class,'question_id');
    // }

    //      public function answers()
    // {
    //     //return $this->belongsTo(Question::class,'question_id')->get();
    //      return $this->question->answers()->select('display_groups')->where('display_groups','like' , $this->name)->get(); 
    // }
}
