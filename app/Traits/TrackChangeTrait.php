<?php

namespace App\Traits;

use App\TrackChange;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;

/**
 * Class TrackChangeTrait 
 * @package App\Traits
 *
 *  Automatically throw Add, Update, Delete events of Model.
 */
trait TrackChangeTrait {


    static protected function  setTrackChangeValue($original, $model, $key) {
               
       if ( $original->isEmpty() ) return '';
       else if(  is_array( $model[$key]) ) return  json_encode($model[$key]) ;
       else    return $model[$key];

    }

    static public function setTrackChangeOldValue($original, $model, $key) {
             switch (true) {
            case $original->isEmpty():
                $oldValue = '';
                break;
            case  is_array( $original[$key]) : 
                $oldValue = json_encode($original[$key]);
                break;
            default: $oldValue = $original[$key];
        }
    }
        public static function boot()
	{
      
      static::saved(function ($model) {
        //  
       
      //if (method_exists($model, 'params')) $model = $model->with('params')->get() ;    
      $table =   $model->getTable();
      $model_key_arrays = collect($model->model_arrays); 
      $original = collect($model->getOriginal())->except(['updated_at','created_at','question_tag']); 
     
                foreach ($model_key_arrays as $model_key_array ) {
                 
                   $fnName = camel_case('get_'.$model_key_array.'_attribute');
                   if (method_exists(new self, $fnName) AND !$original->isEmpty()  )
                      { 
                        //maybe doesnt exist in model 
                    
                        $original 
                        [$model_key_array]
                         =
                          $model->$fnName($original 
                          [$model_key_array] );
                        }
                }
             $model = collect($model)->except(['updated_at','created_at','question_tag']);
        //   dd( $original == $model);
        $keys = (collect($model)->keys());
       
       $keys ->map(function ($key) use ($model, $original,$table) { 
        
         
         

          if( $original->isEmpty() OR $model[$key] != $original[$key]  ) 
            TrackChange::create ([ 
                    'column'    => $key,
                    'table'     => $table,
                    'old_value' => static::setTrackChangeOldValue($original,$model,$key),
                    'value'     => static::setTrackChangeValue($original, $model ,$key),
                    'user_id'   => auth()->user()->id,
                    'row_id'    => $model['id']
            ]);
        });    
    });
 }

    /**
     * Set the default events to be recorded if the $recordEvents
     * property does not exist on the model.
     *
     * @return array
     */
    
} 