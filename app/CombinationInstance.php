<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CombinationInstance
 *
 * @package App
 * @property int $id
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon $created_at
 * @property string|null $deleted_at
 * @property int $combination_id
 * @property int $client_id
 * @property string $created
 * @property string $modified
 * @property string $patient_id
 * @property-read \App\Client $client
 * @property-read \App\Combination $combination
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereCombinationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CombinationInstance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CombinationInstance extends Model {
    
    /**
     *
     */
    public function combination()
    {
        return $this->belongsTo(Combination::class);
    }
    
    /**
     *
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    
}
