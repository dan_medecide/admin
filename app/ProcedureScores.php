<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProcedureScores
 * @package App
 */
class ProcedureScores extends Model {
    
    /**
     * @var string
     */
    protected $table = 'procedures_score';
    
    /**
     * @var array
     */
    protected $fillable = [
        'score_from',
        'score_to',
        'title',
        'description',
        'is_indication',
        'decision_code'
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function procedure()
    {
        return $this->belongsTo(Procedure::class, 'proc_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function decision()
    {
        return $this->belongsTo(Decision::class, 'decision_code', 'code');
    }
    
    /**
     * @return Model
     */
    public static function exampleScores()
    {
        return self::where('proc_id', '=', 50)->get();
    }
}
