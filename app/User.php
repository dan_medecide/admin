<?php
namespace App;

use App;
use function dd;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Carbon\Carbon;
use Charts;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App
 */
class User extends Authenticatable
{

    use Notifiable;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role');
    }
       public function tests()
    {
        return $this->hasMany('App\Test');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function procedures()
    {
        return $this->belongsToMany(Procedure::class);
    }

    use SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'title', 'category_id', 'email', 'password', 'token', 'client_id' ,'role'];

    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token', ];

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasRole($name)
    {
        $roles = $this->roles->map->name->toArray();
        if (in_array($name, $roles)) {
            return true;
        }

        return false;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function doesntHaveRole($name)
    {
        $roles = $this->roles()->pluck('name')->toArray();
        if (!in_array($name, $roles)) {
            return true;
        }

        return false;
    }

    /**
     * @param $procedure
     *
     * @return bool
     */
    public function hasProcedure($procedure)
    {
        $procedures = $this->procedures()->pluck('id')->toArray();

        if (in_array($procedure, $procedures)) {
            return true;
        }

        return false;
    }

     public function isAdmin()
    {
      return $this->role == "admin";

    }

    

}
