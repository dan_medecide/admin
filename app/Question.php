<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Traits\TrackChangeTrait;

class Question extends Model {
    
    use SoftDeletes;
    use TrackChangeTrait;
    protected $model_arrays =  ['tag','indication'];
    protected $fillable = [
        'title',
        'proc_id',
        'tag',
        'priority',
        'justification',
        'is_auto',
        'title_doctor',
        'vas_max',
        'vas_min',
        'min_score',
        'max_score',
        'question_type',
        'is_general',
        'indication',
        'answers_type',
        'numric_min_range',
        'numric_max_range',
        'question_tag',
        'vas_min_display',
        'vas_max_display',
        'vas_step',
        'teach_max_answers',
        'tag_teach',
    ];
    
    protected $dates = ['deleted_at'];
    
    public static function getAnswers()
    {
        $answers = DB::table('answers')->whereNull('deleted_at')->where('is_deleted', '=', '0')->get();
        
        return $answers;
    }
    
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
    
      public function displayGroups()
    {
        return $this->hasMany(DisplayGroup::class);
    }

       public function answerGroups()
    {
      
        return $this->hasMany(AnswerGroup::class);
    }

    public function procedure()
    {
        return $this->belongsTo(Procedure::class, 'proc_id');
    }
    
    public function getQuestionTagAttribute($tags)
    {
        return ! empty($tags) ? preg_replace('/:(.*)/', '', explode(',', $tags)) : [];
    }
    
    public function getIndicationAttribute($indications)
    {
        return ! empty($indications) ? explode(',', $indications) : [];
    }
    
    public function setIndicationAttribute($value)
    {
        $this->attributes['indication'] = is_array($value) ? implode(',', $value) : $value;
    }
    
    public function setQuestionTagAttribute($value)
    {
        $this->attributes['question_tag'] = is_array($value) ? implode(',', $value) : $value;
    }

    public function changePriorities($value,$proc_id)
    {
        $query = $this->where('proc_id',$proc_id)->where('priority','<', $value)->where('deleted_at', '=', null);
        $nextQuestion = $this ->where('priority','<', $value)->orderBy('priority','desc')->first();
        $nextQuestionPriority = $nextQuestion ?  $nextQuestion->priority : '';

      if($value -  $nextQuestionPriority == 1)  
      { //dd('changePriorities!');
        $query =  $query->get();
        foreach ($query as $value) {
        // echo( $value->id);
        $value->priority= $value->priority-1;
        $value->save();
            }
        }
    }
}
