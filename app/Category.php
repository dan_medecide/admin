<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Category
 *
 * @property int $id
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property string $title
 * @property string $created
 * @property string $modified
 * @property bool $is_deleted
 * @property string|null $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Combination[] $combinations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Procedure[] $procedures
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Category onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Category withoutTrashed()
 * @mixin \Eloquent
 */
class Category extends Model {
    
    protected $table = 'cat';
    
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['title', 'description',];
    
    public function procedures()
    {
        return $this->hasMany(Procedure::class, 'cat_id');
    }
    
    public function users()
    {
        return $this->hasMany(User::class);
    }
    
    public function combinations()
    {
        return $this->hasMany(Combination::class);
    }
    
    public static function list()
    {
        return static::all()->keyBy('id')->map->title;
    }
    
}
