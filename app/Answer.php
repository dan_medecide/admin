<?php

namespace App;

// use App\Events\UserSaved;
// use App\Events\UserDeleted;
use Illuminate\Notifications\Notifiable;




use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Answer
 * @property int $id
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property int $question_id
 * @property string|null $title
 * @property string|null $answer_groups
 * @property int|null $weight
 * @property string|null $weight_formula
 * @property int|null $next_question_id
 * @property int|null $priority
 * @property string|null $created
 * @property string|null $modified
 * @property bool|null $is_deleted
 * @property string|null $tag
 * @property int|null $end_test
 * @property string|null $keys_for_end_test
 * @property string|null $end_test_text
 * @property string|null $justification
 * @property string|null $auto_keys
 * @property int|null $force_score
 * @property string|null $notes
 * @property int|null $vas_min
 * @property int|null $vas_max
 * @property float|null $vas_min_display
 * @property float|null $vas_max_display
 * @property float|null $vas_step
 * @property string|null $vas_auto_answer_formula
 * @property string|null $vas_explanation
 * @property string|null $keys_for_showing_answer
 * @property string|null $indication
 * @property string|null $keys_for_showing_answer_teach
 * @property bool|null $vas_dont_show_no
 * @property bool|null $vas_dont_show_unknown
 * @property-read \App\Category $question
 * @property string display_groups
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Answer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Answer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Answer withoutTrashed()
 * @mixin \Eloquent
 */
class Answer extends Model
{
    use SoftDeletes;
    use  Traits\TrackChangeTrait;

    protected $dispatchesEvents = [
        'saved' => UserSaved::class,
        'deleted' => UserDeleted::class,
    ];
    
    protected $model_arrays =  ['answer_groups','display_groups','tag','vas_intermediate_score','vas_intermediate_display','validation','indication'];

    protected $dates = ['deleted_at'];

    protected $table = 'answers';

    protected $fillable = [
        'title',
        'question_id',
        'weight',
        'next_question_id',
        'priority',
        'tag',
        'end_test',
        'keys_for_end_test',
        'end_test_text',
        'justification',
        'auto_keys',
        'force_score',
        'notes',
        'vas_min',
        'vas_intermediate_score',
        'vas_max',
        'vas_min_display',
        'vas_intermediate_display',
        'vas_max_display',
        'vas_step',
        'vas_explanation',
        'vas_auto_answer_formula',
        'vas_dont_show_no',
        'vas_dont_show_unknown',
        'keys_for_showing_answer',
        'indication',
        'keys_for_showing_answer_teach',
        'answer_groups',
        'display_groups',
        'weight_formula',
        'vas_no',
        'vas_unknown',
        'vas_less',
        'vas_more',
        'vas_show_more',
        'vas_show_less',
        'vas_no_label',
        'vas_unknown_label',
        'validation'
    ];
    
    public function question()
    {
        return $this->belongsTo(Question::class);
    }
  
    public function params()
    {
        return $this->belongsToMany(Param::class, 'answer_param')->withPivot('value');
    }
       public function displayGroup()
    {
         
         if (empty($this->display_groups)) return false;
          
        return $this->question->displayGroups()->where('name', 'like', $this->display_groups )->first(); //->where('name','like', $this->display_groups );
       
    }
       public function answerGroup()
    {
         if (empty($this->answer_groups)) return false;
        
        return $this->question->answerGroups()->where('name', 'like', $this->answer_groups )->first(); //->where('name','like', $this->answer_groups );
       
    }
    public function setVasIntermediateScoreAttribute($value)
    {
        $this->attributes['vas_intermediate_score'] = is_array($value) ? json_encode($value) : $value;
    }

    public function setVasIntermediateDisplayAttribute($value)
    {
        $this->attributes['vas_intermediate_display'] = is_array($value) ? json_encode($value) : $value;
    }

    public function setTagAttribute($value)
    {
        $this->attributes['tag'] = is_array($value) ? implode(',', $value) : $value;
    }

    public function setAnswerGroupsAttribute($value)
    {
        $this->attributes['answer_groups'] = is_array($value) ? implode(',', $value) : $value;
    }

    public function setDisplayGroupsAttribute($value)
    {
        $this->attributes['display_groups'] = is_array($value) ? implode(',', $value) : $value;
    }

    public function setIndicationAttribute($value)
    {
        $this->attributes['indication'] = is_array($value) ? implode(',', $value) : $value;
    }

    public function getTagAttribute($tags)
    {
        return !empty($tags) ? preg_replace('/:(.*)/', '', explode(',', $tags)) : [];
    }

    public function getVasIntermediateScoreAttribute($scores)
    {
        return !empty($scores) ? json_decode($scores, true) : [];
    }

    public function getVasIntermediateDisplayAttribute($displays)
    {
        return !empty($displays) ? json_decode($displays, true) : [];
    }

    public function getAnswerGroupsAttribute($groups)
    {
        return !empty($groups) ? explode(',', $groups) : [];
    }

    public function getDisplayGroupsAttribute($groups)
    {
        return !empty($groups) ? explode(',', $groups) : [];
    }

    public function getIndicationAttribute($indications)
    {
        return !empty($indications) ? explode(',', $indications) : [];
    }

    public function getValidationAttribute($value)
    {
        return json_decode($value == null ? "{}" : $value);
    }

    public function setValidationAttribute($value)
    {
        $this->attributes['validation'] = is_array($value) ? json_encode($value) : $value;
    }
 
    public function isVas()
    {
        if ((!$this->vas_min_display && !$this->vas_max_display)) {
            return false;
        }

        return $this->vas_min and !empty($this->vas_min)
            or $this->vas_max and !empty($this->vas_max)
            or $this->vas_min_display and !empty($this->vas_min_display)
            or $this->vas_max_display and !empty($this->vas_min_display);
    }

     public function globalScoreAnswer($val,$auto_question_id)    
    {
         
        $newAutoGlobalScoreAnswer=[
            'priority'=>1,
            'auto_keys'=>$val['keyForShowingAnswer'],
            'question_id'=> $auto_question_id,
            'title' => $val['sub_answer'].' global score' ,
            'weight' => $val['weight'],
            'tag' => [  $val['created_keys'] ]
        ];
        $this->create($newAutoGlobalScoreAnswer);
        
    } 
        
    public function storeGroup( $request , $d ) {
         
       $v = \Validator::make($request->all(), [
       'probability' => 'required|numeric',
          'priority' => 'required|numeric',
    ]);
        if ($v->fails())
        {
            return  [ 'error' =>$v->errors() ];
        }
     
        try { 

            $d->Create($request->toArray());
             return $request->all();

            } catch (\Illuminate\Database\QueryException $e) {
                
             $d = $d->where('question_id',$request->question_id)
             ->where('name',$request->name)
             ->first();
              
            if (  $request->priority  &&  $request->probability
              &&  $d->priority !=  $request->priority ||
             $d->probability !=  $request->probability)        
             try {
              
             $d->priority =  $request->priority;
             $d->probability =  $request->probability;
             $d->name =  $request->name;   
             $d->save();
              $answerGroup = ( head(head($d->answers() )))->handleAnswerGroup();
              
              return ["display_group" =>$d, 'answer_group' => $answerGroup];
               //$d->update($request->all());
                 
            } catch (\Throwable $th) {
                  $answerGroup = ( head(head($d->answers() )))->handleAnswerGroup();
                  return ['error' =>$th];
            }   ;    
                  return ['error' => $e->errorInfo[2]] ; //getCode()
            }

       
    }

        public function getParam($paramTitle){

        foreach($this->params as $param){
            if ($param->title == $paramTitle)
                return $param->pivot->value;
        }
        return false;
    }
     public function handleAnswerGroup(){

        $name = head($this->answer_groups);
        if(!$name) return false;
    
       if (!$this->answerGroup())  
            {AnswerGroup::Create(['name'=>$name ,'question_id'=> $this->question_id,'probability'=> 0.323 ,'priority' => 323,'proc_id'=> $this->question->proc_id ]);}
          $collection = $this->answerGroup()->answers()  ;
         
           $prob_vec = [];
           $display_groups_vec = [];
           $array = [];
           $collection->map(function($ans) use(  &$display_groups_vec,&$prob_vec){
               if (head($ans->display_groups)) 
               {
                   if(!in_array(head($ans->display_groups),$display_groups_vec)){
                      
                    
                       $prob_vec[] = $ans->displayGroup()['probability'];
                       $display_groups_vec[] = head($ans->display_groups);
                    }                      
                } else {                    
                    if ($ans->getParam('teach_probability') == "0") {
                        $prob_vec[] = 0;
                    }else{
                        $prob_vec[] = 1-(float)json_decode($ans->getParam('teach_probability'),true)['_no'];
                    }
                }
            });            
            $prob_vec = collect($prob_vec)->transform(function($v){return 1- $v;});         
            $answerGroupProb = 1 - $prob_vec->reduce(function ($carry, $item) {
                return $carry * $item;
            },1);             
            $a_g =  $this->answerGroup();
            $a_g ->probability = $answerGroupProb ;
            $a_g->save();
          return $a_g;    
    }
     public function hasVasRequest($request){
            return (!empty($request->vas_min_display) OR
             !empty($request->vas_max_display) OR
            //   !empty($request->vas_intermediate_score.trim() ) OR
            // !empty($request->vas_intermediate_display.trim() ) OR
            !empty($request->vas_max ) OR
            !empty($request->vas_min ) 
            
            ) || false;

      }

}
