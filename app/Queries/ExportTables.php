<?php

namespace App\Queries;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

class ExportTables {
    
    public static function get($request, $table)
    {
        $builder = DB::table($table)
                     ->when(in_array('deleted_at', self::allColumnNames($table)), function($query) {
                         return $query->where('deleted_at', '=', null);
                     })
                     ->when(in_array('is_deleted', self::allColumnNames($table)), function($query) {
                         return $query->where('is_deleted', '=', 0);
                     })
                     ->when($request->has('from'), function($query) use ($request) {
                         return self::whereIdGreaterThen($query, $request->query('from'));
                     })
                     ->when(self::hasDateConditions($request), function($query) use ($request) {
                         return self::whereBetweenDates($query, $request->query('start'), $request->query('end'));
                     })
                     ->when($request->has('columns'), function($query) use ($request, $table) {
                         return $query->where(self::formatConditions($request->query('columns')));
                     })
                     ->when($request->has('between'), function($query) use ($request) {
                         $input = explode(',', $request->query('between'));
                     
                         return $query->whereBetween($input[0], [$input[1], $input[2]]);
                     })
                     ->when($request->has('limit'), function($query) use ($request) {
                         return $query->limit($request->query('limit'));
                     });
    
        $results_per_page = $request->query('chunk') ? $request->query('chunk') : 1000;
        $current_page = $request->query('page') ? $request->query('page') : 1;
        $current_results = $current_page * $results_per_page;
        if ($request->query('limit') && $current_results > $request->query('limit')) return [];
        
        return $builder->paginate($results_per_page);
    }
    
    public static function allColumnNames($table)
    {
        return DB::getSchemaBuilder()->getColumnListing($table);
    }
    
    /**
     * @param $date
     *
     * @return string
     */
    protected static function formatDate($date)
    {
        return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }
    
    /**
     * @param $builder
     * @param $request
     *
     * @return LengthAwarePaginator
     */
    protected static function makePaginator($builder, $request)
    {
        $results = $builder->get();
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 1000;
        $currentPageSearchResults = $results->slice(( $currentPage - 1 ) * $perPage, $perPage)->all();
        
        return new LengthAwarePaginator($currentPageSearchResults, count($results), $perPage, $currentPage, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]);
    }
    
    /**
     * @param $request
     *
     * @return array
     */
    protected static function formatConditions($columns)
    {
        return array_map(function($column) {
            return explode(',', $column);
        }, $columns);
    }
    
    public static function requestedColumnNames($columns)
    {
        return array_map(function($column) {
            return $column[0];
        }, self::formatConditions($columns));
    }
    
    /**
     * @param $query
     * @param $request
     *
     * @return mixed
     */
    protected static function whereIdGreaterThen($query, $from)
    {
        return $query->where('id', '>=', $from);
    }
    
    /**
     * @param $query
     * @param $this
     * @param $request
     *
     * @return mixed
     */
    protected static function whereBetweenDates($query, $start, $end)
    {
        return $query->whereBetween(
            'created_at',
            [
                self::formatDate($start),
                self::formatDate($end),
            ]
        );
    }
    
    /**
     * @param $request
     *
     * @return bool
     */
    protected static function hasDateConditions($request)
    {
        return $request->has('start') and $request->has('end') and ! $request->has('from');
    }
}
