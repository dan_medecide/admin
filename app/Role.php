<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Role extends Model {
    use SoftDeletes;

    protected $dates = [ 'deleted_at' ];

    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];

    public function users ()
    {
        return $this->belongsToMany( 'App\User', 'user_role' );
    }
}
