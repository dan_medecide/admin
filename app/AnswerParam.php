<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;


class AnswerParam extends Pivot
{
    // use Traits\TrackChangeTrait;
    // protected $fillable = ['answer_id','value'];
    protected $table = 'answer_param';
    protected $guarded= [];
    use SoftDeletes;

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}