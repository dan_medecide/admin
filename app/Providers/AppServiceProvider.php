<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\Blade;

use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     * @return void
     */
    public function boot()
    {
        


        Schema::defaultStringLength(191);
        // Add @optional for Complex Yielding
        
        Blade::directive('optional', function($expression) {
            
            return "<?php if(trim(\$__env->yieldContent{$expression})): ?>";
        });

        // Add @endoptional for Complex Yielding
        Blade::directive('endoptional', function($expression)  {
            
            return "<?php endif; ?>";
        });

        View::composer('layouts.sidebar', function ($view) {
            $view->with('user', auth()->user()->load('roles'));
        });
    }

    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function () {
            return base_path() . '/public_html';
        });

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(DuskServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }

        require_once __DIR__ . '/../Http/helpers.php';
    }
}
