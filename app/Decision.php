<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Decision
 *
 * @package App
 * @property int $id
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property bool $is_deleted
 * @property string $title
 * @property string|null $description
 * @property bool $is_indication
 * @property int $code
 * @property bool $show_in_teach
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProcedureScores[] $scores
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Test[] $tests
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Decision onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereIsIndication($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereShowInTeach($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Decision withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Decision withoutTrashed()
 * @mixin \Eloquent
 */
class Decision extends Model {
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function tests()
    {
        return $this->hasMany(Test::class, 'decision_code', 'code');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scores()
    {
        return $this->hasMany(ProcedureScores::class, 'decision_code', 'code');
    }
    
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    
    protected $table = 'decision_codes';
    
    protected $fillable = [
        'title', 'description', 'is_indication', 'code',
    ];
}
