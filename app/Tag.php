<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use SoftDeletes;

    protected $table = 'tags';

    protected $dates = [ 'deleted_at' ];

    protected $fillable = [
        'title',
    ];
}
