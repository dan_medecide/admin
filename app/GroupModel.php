<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class GroupModel extends Model
{
     use SoftDeletes;
    
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
  //  protected $dateFormat = 'U';
//     protected $guarded= [];
//   protected $table = 'display_groups';
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['created_at', 'updated_at', 'deleted_at'];

       public function question()
    {
        return $this->belongsTo(Question::class,'question_id');
    }

         public function answers()
    {
          
        //return $this->belongsTo(Question::class,'question_id')->get();
         return $this->question->answers()->select('answer_groups','display_groups','id','question_id')->where($this->table,'like' , $this->name)->get(); 
    }

      public function findOrCreate($name)
    {
        
      return $this->where('name', $name)->get()->empty;
    }
}
