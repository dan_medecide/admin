<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Combination
 *
 * @property int $id
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property string $title
 * @property string $description
 * @property bool $is_deleted
 * @property string $created
 * @property string $modified
 //* @property string $procedures
 * @property int $cat_id
 * @property int $client_id
 * @property string $code
 * @property-read \App\Category $category
 * @property-read \App\Client $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CombinationInstance[] $instance
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Combination onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereCatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereProcedures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Combination whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Combination withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Combination withoutTrashed()
 * @mixin \Eloquent
 */
class Combination extends Model
{
    use SoftDeletes;
    use  Traits\TrackChangeTrait;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title',
        'description',
        'code',
        'procedures'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'cat_id');
    }

    public function instance()
    {
        return $this->hasMany(CombinationInstance::class);
    }

     public function procedures($title)
    {
        dd("234");
        return $this->select('procedures')->where('procedures', 'like', '%'.$title.'%');
    }

    public function procSiblingsKeys($title)
    {
      
    return $this
            ->select('procedures')
            ->where('procedures', 'like', '%'.$title.'%')
            ->get()
            ->pluck('procedures')
            ->map ( function($a) { return explode(',', $a); })
            ->collapse()
            ->unique();    

    }
}
