<?php

use App\User;
 
Auth::routes();

Route::get('/d' , function(){
  
    $collection = (new App\AnswerParam)
    ->where('param_id',15)
    ->where('value','like',"'%")
    ->get() ; //->find(2)->toArray();
    //dd($collection);
    
    foreach( $collection as $answer_param  ){
         
      //  $answer_param->value = json_encode(walker($answer_param->value));
        
       // $answer_param->save();
    }
 dd($collection);
    dd(url('/') );
    $c = new App\Http\Controllers\DuplicateProcedureController();

    $answer = App\Answer::find(12260);
    $new_answer = new App\Answer;
    $new_answer->title = "test";
    $new_answer->save();

    $c->duplicateAnswerParamsAndAssociate($answer, $new_answer);
    //dd($new_answer->params()->withPivot('value'));
    
});
 Route::get('/test-me',  function(){
  //DB::enableQueryLog();
 
//$id= 25;
//$c=       App\Client::find($id);
// dd(DB::getQueryLog());
   dd('$c');
 });
Route::middleware(['auth'])->group(function () {
    #Home
    Route::get('/', 'HomeController@index');
    Route::get('home', 'HomeController@index')->name('home');
});
     

 //Route::group(['middleware' => 'auth:api'], function () {
      Route::get('run-beta-teach/{procedure}', 'RedirectController@runBetaTeach')->name('procedures.beta-teach');
 //});
Route::middleware(['doctor'])->group(function () {
    #Home
    Route::get('run-pdf/{test_id}', 'RedirectController@runPdf')->name('user.pdf-report');
    Route::get('run-report/{test_id}', 'RedirectController@runReport')->name('user.test-report');
    Route::get('home/doctor', 'HomeDoctorController@index')->name('home.doctor');
    Route::get('run-procedure/{procedure}', 'RedirectController@runTest')->name('procedures.run');
    Route::get('run-teach/{procedure}', 'RedirectController@runTeach')->name('procedures.teach');
    Route::get('run-beta-teach/{procedure}', 'RedirectController@runBetaTeach')->name('procedures.beta-teach');
    Route::get('run-beta-new-teach/{procedure}', 'RedirectController@runBetaNewTeach')->name('procedures.beta-new-teach');
    Route::get('run-combination/{combination}', 'RedirectController@runCombination')->name('combination.run');
    Route::get('run-unfinished-combination/{test_id}', 'RedirectController@runUnfinishedCombination')->name('combination.unfinished.run');
    
});

Route::middleware(['supervisor'])->group(function () {

    #Home
    Route::get('home/supervisor', 'HomeSupervisorController@index')->name('home.supervisor');
     Route::get('home/supervisor/procedures', 'RedirectController@runProceduresPage')->name('procedures_page.run');
    #Procedures Abbreviations
    Route::get('procedures-abbreviations/{category_id}', 'ProceduresAbbreviationController@index')
        ->name('procedures-abbreviations.index');
     Route::post('procedures/set-temp-category/{procedure}', 'ProceduresAbbreviationController@setTempCategory');
     Route::post('procedures/set-temp-category/{procedure}/undo', 'ProceduresAbbreviationController@setTempCategory');
     #Set Procedures/Combinations to temp
     Route::post('combinations/set-temp-category/{combination}', 'TempController@setTempCategory');
     Route::post('combinations/set-temp-category/{combination}/undo', 'TempController@setTempCategory');
       

    #Production Link
    Route::get('production-link/{type}/{id}', 'ProductionLinkController')->name('production-link');

    #Client Reports
    Route::get('client-reports/{client}', 'ClientsReportsController@init')->name('client-reports');
    Route::get('client-reports/{client}/{connection}/{unapproved?}/{today?}', 'ClientsReportsController@index');
    Route::get('client-reports/link/{test}', 'ClientsReportsController@makeLink');
    Route::get('new-client-reports/{client_id}/{connection}/not-approved', 'ClientsReportsController@notApproved');

    #Client Statistics
    Route::get('client-statistics/{client}', 'ClientsStatisticsController@index')->name('client-statistics');
    Route::get('client-statistics/{client}/procedures', 'ClientsStatisticsController@procedures');
    Route::get('client-statistics/{client}/reports-count', 'ClientsStatisticsController@getReportsCount');

    #Client Referrals
    Route::get('client-referrals', 'ClientReferralsController@index')->name('client-referrals');

    #Client Decisions
    Route::post('client-decisions', 'ClientDecisionsController@store');
    Route::get('client-decisions', 'ClientDecisionsController@index');

    #client Physicians
    Route::get('client-physicians/{physician}', 'ClientPhysicianController@index');

    #Tests
    Route::get('test', 'TestsController@index')->name('tests.index');
    Route::delete('test/{id}', 'TestsController@destroy')->name('tests.destroy');
    #CountTests
    Route::get('count-tests/{from}/{to}/{group?}/{physician?}', 'CountTestsController@index')
        ->name('count-tests.index');

    #procedures
    Route::get('procedures', 'ProceduresController@index')->name('procedures.index');
    Route::get('procedures-list', 'ProceduresController@index');//->name('procedures.index'); //for json!
});

Route::middleware(['admin'])->group(function () {

    #home
    Route::get('home/admin', 'HomeAdminController@index')->name('home.admin');

    #Client
    Route::resource('clients', 'ClientsController');

    #Track Changes
    Route::resource('track-changes', 'TrackChangeController');

    #Answer Groups
    Route::resource('answer-groups', 'AnswerGroupsController');
    Route::get('display-groups/{question}', 'DisplayGroupsController@index')->name('display-groups.index');
    
    #Display Groups
    Route::resource('display-groups', 'DisplayGroupController');
    Route::get('answer-group/display-groups/{displayGroup}', 'DisplayGroupController@answerGroup');
    Route::get('question/display-groups/{displayGroup}', 'DisplayGroupController@qTest');
    Route::get('question/display-groups/{displayGroup}', 'DisplayGroupController@qTest');
    Route::get('answer/display-groups/{displayGroup}', 'DisplayGroupController@qTest2');
    Route::get('display-groups/question/{question}', 'DisplayGroupController@qTest3');
    Route::get('display-groups/answer/{answer}', 'DisplayGroupController@qTest4');
    #combinations
    Route::resource('combinations', 'CombinationsController');

    #Ready procedures
    Route::put('ready-procedures/{procedure}', 'ReadyProceduresController@update')->name('ready-procedures.update');

    #User
    Route::resource('users', 'UsersController');
    Route::get('supervisor', 'UsersController@supervisor');
    #Users Roles
    Route::get('user-roles/{user}/edit', 'UserRolesController@edit')->name('user-roles.edit');
    Route::patch('user-roles/{user}', 'UserRolesController@update')->name('user-roles.update');

    #Users Procedures
    Route::get('users-procedures/{user}/edit', 'UsersProceduresController@edit')->name('users-procedures.edit');
    Route::patch('users-procedures/{user}', 'UsersProceduresController@update')->name('users-procedures.update');

    #Roles
    Route::get('roles', 'RolesController@init');
    Route::get('roles/index', 'RolesController@index');
    Route::post('roles', 'RolesController@store');
    Route::post('roles/{role}/update', 'RolesController@update');
    Route::post('roles/{role}/delete', 'RolesController@destroy');

    #Categories
    Route::resource('categories', 'CategoriesController');

    #procedureScores
    Route::get('procedure-scores/{procedure}', 'ProcedureScoresController@index')->name('procedure-scores.index');
    Route::get('procedure-scores/{score}/edit', 'ProcedureScoresController@edit')->name('procedure-scores.edit');
    Route::put('procedure-scores/{score}', 'ProcedureScoresController@update')->name('procedure-scores.update');
    Route::delete('procedure-scores/{score}', 'ProcedureScoresController@destroy')->name('procedure-scores.destroy');

    #Procedures
    Route::post('duplicate-procedure/{procedure}', 'DuplicateProcedureController@create')->name('procedures.duplicate');
    Route::get('api/procedures/api/create', 'ProceduresController@create');
    Route::resource('api/procedures', 'ProceduresController', ['except' => ['index']]);

    #Decisions
    Route::resource('decisions', 'DecisionsController');

    #Statistics
    Route::get('statistics', 'StatisticsController@index');

    #Client Parameters
    //Route::put('client-params/{param}', 'ClientParamsController@update')->name('go123.update');
    //Route::put('client-params', 'ClientParamsController@update')->name('client-params.update');
    Route::resource('client-params', 'ClientParametersController');

    #Params
    Route::resource('params', 'ParamsController');

    #Answer Params
    Route::post('display-group/{question_id}', 'AnswerParamsController@answersParamsSameGroups');
    Route::post('display-group/multi-labels/update/{display_groups}', 'AnswerParamsController@updateAnswersMultiLabels');
    Route::post('answer-params/{answer}', 'AnswerParamsController@update')->name('answer-params.update');
    Route::resource('answer-params', 'AnswerParamsController', ['except' => 'update']);

    #Questions
    Route::get('questions/{proc_id}', 'QuestionsController@init')->name('questions.init');

    #Notes
    Route::resource('notes', 'NotesController');

    #Reports
    Route::resource('reports', 'ReportsController');

    #Language
    Route::get('languages/{language}', 'LanguagesController@update')->name('languages.update');

    #Conflicts
    Route::get('conflicts/{procedure}', 'ConflictsController@index');
    Route::post('conflicts/{conflict}/delete', 'ConflictsController@destroy');

    Route::group(['prefix' => 'api'], function () {

        #Procedure
        Route::get('procedure/{proc_id}/title', 'ProceduresController@getTitle');
        Route::get('procedure/publish/{procedure}', 'ProceduresController@publish');
 


        #questions
        Route::get('questions/tags/{source}', 'QuestionsController@getTagsData');
        Route::post('questions/combine', 'CombineQuestionsController@create')
            ->name('questions.combine');
        Route::get('questions/{proc_id}/scores', 'QuestionsController@getProceduresScores');
        Route::get('questions/{proc_id}', 'QuestionsController@index');
        Route::get('questions/{question}/edit', 'QuestionsController@edit');
        Route::post('questions/{question_id}/duplicate', 'QuestionsController@duplicate');
        Route::post('questions/{proc_id}', 'QuestionsController@store');
        Route::post('questions/{question}/update', 'QuestionsController@update');
        Route::post('questions/{question}/delete', 'QuestionsController@destroy');
        Route::post('questions/global_score/{question}/create', 'QuestionsController@createAutoQuestion');

        

       

        #Answers
        Route::get('answers/formulas', 'AnswersController@getFormulas');
        Route::get('answers/tags/{source}', 'AnswersController@getTagsData');
        Route::get('answers/{answer}/edit', 'AnswersController@edit');
        Route::post('answers/{question_id}', 'AnswersController@store');
       
        Route::post('answers/{answer}/update', 'AnswersController@update');
        Route::post('answers/{answer}/delete', 'AnswersController@destroy');
        Route::post('answers/{answer}/duplicate', 'AnswersController@duplicate');
        Route::post('answers/{answer}/split', 'AnswersController@split');

        #Indications
        Route::get('indications/{proc_id}', 'IndicationsController@index');

        #Tags
        Route::get('tags/{proc_id}', 'TagsController@index');
        Route::get('tags/{proc_id}/indication-siblings', 'TagsController@indicationSiblings');
    });

        #WebHooks
        Route::get('webhooks/{procedure}', 'WebHookController@index');
});
