<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function(Faker\Generator $faker) {
    static $password;
    
    return [
        'first_name'     => $faker->firstName,
        'last_name'      => $faker->lastName,
        'title'          => $faker->title,
        'email'          => $faker->unique()->safeEmail,
        'token'          => '1dc31811148222851408eaf28c13f714b1eec1b3',
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function(Faker\Generator $faker) {
    return [
        'title'       => $faker->word,
        'description' => $faker->sentence,
        'is_deleted'  => 0,
    ];
});

$factory->define(App\Procedure::class, function(Faker\Generator $faker) {
    return [
        'title'    => $faker->word,
        'priority' => $faker->numberBetween(1, 100),
        'version'  => $faker->numberBetween(1, 100),
        'cat_id'   => null,
    ];
});

$factory->define(App\Role::class, function(Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(App\ProcedureScores::class, function(Faker\Generator $faker) {
    return [
        'proc_id'       => 50,
        'score_from'    => $faker->numberBetween(1, 100),
        'score_to'      => $faker->numberBetween(1, 100),
        'is_deleted'    => 0,
        'title'         => $faker->word,
        'description'   => $faker->sentence,
        'is_indication' => 1,
        'decision_code' => 3,
    ];
});

$factory->define(App\Question::class, function(Faker\Generator $faker) {
    return [
        'title'        => $faker->word,
        'proc_id'      => null,
        'priority'     => $faker->numberBetween(1, 100),
        'is_deleted'   => 0,
        'title_doctor' => $faker->word,
    ];
});

$factory->define(App\Answer::class, function(Faker\Generator $faker) {
    return [
        'title'       => $faker->word,
        'question_id' => null,
        'is_deleted'  => 0,
    ];
});

