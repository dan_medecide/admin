<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::dropIfExists('answer_groups');
        Schema::create('answer_groups', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('proc_id')->index();
            $table->foreign('proc_id')->references('id')->on('procedures')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('question_id')->unsigned()->index();
            $table->foreign('question_id')->references('id')->on('questions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->text('name',255);
            $table->decimal('probability', 10, 2);
            $table->integer('deleted_at');
            $table->integer('priority');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_groups');
    }
}
