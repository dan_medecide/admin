<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('params', function(Blueprint $table)
    {
        $table->increments('id');
        // $table->integer('alpha_id');
        // $table->integer('beta_id');
        // 	$table->text('short_desc', 65535);
		// 	$table->text('general_text', 65535);
        //     $table->text('indications_text', 65535);
            $table->timestamps();
			$table->text('title', 65535);
			$table->text('type', 65535);
			$table->text('target', 65535);
			// $table->boolean('is_preliminary');
			// $table->float('version', 10, 0);
			// $table->string('icd_code', 10);


    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('params');
    }
}
