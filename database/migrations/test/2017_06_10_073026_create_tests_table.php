<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
      Schema::dropIfExists('tests');
		Schema::create('tests', function(Blueprint $table)
		{
      $table->increments('id');
			$table->string('user_info', 500);
			$table->string('ip', 50);
			$table->string('email', 100);
      $table->integer('proc_id')->unsigned()->index();
      $table->foreign('proc_id')->references('id')
        ->on('procedures')->onUpdate('CASCADE')->onDelete('CASCADE');

			$table->string('manual_score', 50);
			$table->smallInteger('manual_decision');
			$table->smallInteger('manual_decision_confidence');
			$table->integer('combination_instance_id');
			$table->boolean('is_done');
			$table->text('test_keys', 65535);
			$table->text('label_json', 65535);
			$table->text('notes', 65535);
			$table->string('physician_name', 100);
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	//	Schema::drop('tests');
	}

}
