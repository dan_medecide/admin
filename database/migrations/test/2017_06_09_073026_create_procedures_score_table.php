<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProceduresScoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('procedures_score', function(Blueprint $table)
		{
      $table->increments('id');
			$table->integer('proc_id')->unsigned()->index();
      $table->foreign('proc_id')->references('id')->on('procedures')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('score_from');
			$table->integer('score_to');
			$table->string('title', 500);
			$table->text('description', 65535);
			$table->boolean('is_indication');
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('procedures_score');
	}

}
