<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCombinationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('combinations', function(Blueprint $table)
		{
      $table->increments('id');
			$table->string('title', 400);
			$table->text('description', 65535);
			$table->text('procedures', 65535);
			$table->integer('cat_id')->unsigned()->index();
      $table->foreign('cat_id')->references('id')->on('cat')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('client_id')->unsigned()->index();
      $table->foreign('client_id')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->string('code', 100);
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('combinations');
	}

}
