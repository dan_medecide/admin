<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questions', function(Blueprint $table)
		{
      $table->increments('id');
			$table->text('title', 65535);
      $table->integer('proc_id')->unsigned()->index();
      $table->foreign('proc_id')->references('id')->on('procedures')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->boolean('is_multiple');
			$table->text('tag', 65535);
			$table->integer('priority');
			$table->text('justification', 65535);
			$table->boolean('is_auto');
			$table->text('title_doctor', 65535);
			$table->integer('vas_max');
			$table->integer('vas_min');
			$table->integer('min_score');
			$table->integer('max_score');
			$table->string('question_type', 50);
			$table->boolean('is_general');
			$table->text('indication', 65535);
			$table->string('answers_type', 20);
			$table->float('numric_min_range', 10, 0);
			$table->float('numric_max_range', 10, 0);
			$table->string('question_tag', 300);
			$table->decimal('vas_min_display', 10, 0);
			$table->decimal('vas_max_display', 10, 0);
			$table->decimal('vas_step', 10, 0);
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questions');
	}

}
