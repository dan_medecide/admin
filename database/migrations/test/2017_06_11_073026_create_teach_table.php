<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeachTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{  
        Schema::dropIfExists('teach');
		Schema::create('teach', function(Blueprint $table)
		{
        
      $table->increments('id');
			$table->text('label_json', 65535);
             $table->integer('user_id')->unsigned()->index();
            //$table->unsignedInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('test_id')->unsigned()->index();
      $table->foreign('test_id')->references('id')->on('tests')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->unique(['user_id','test_id'], 'usertest');
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('teach');
	}

}
