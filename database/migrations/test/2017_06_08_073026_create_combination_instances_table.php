<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCombinationInstancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('combination_instances', function(Blueprint $table)
		{
			$table->integer('combination_id')->unsigned()->index();
      $table->foreign('combination_id')->references('id')->on('combinations')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('client_id')->unsigned()->index();
      $table->foreign('client_id')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->string('patient_id', 100);
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('combination_instances');
	}

}
