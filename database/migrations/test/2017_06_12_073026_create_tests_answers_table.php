<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestsAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tests_answers', function(Blueprint $table)
		{
      $table->increments('id');
      $table->integer('question_id')->unsigned()->index();
      $table->foreign('question_id')->references('id')->on('questions')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->integer('answer_id')->unsigned()->index();
      $table->foreign('answer_id')->references('id')->on('answers')->onUpdate('CASCADE')->onDelete('CASCADE');
	// 		$table->integer('test_id')->unsigned()->index('test_id');
    //   $table->foreign('test_id')->references('id')->on('tests')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->string('tag', 100);
			$table->text('current_keys', 65535);
			$table->string('sub_answer', 11);
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tests_answers');
	}

}
