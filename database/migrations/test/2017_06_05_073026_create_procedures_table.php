<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProceduresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
          //Schema::drop('procedures');
		Schema::create('procedures', function(Blueprint $table)
		{
      $table->increments('id');
			$table->string('title', 100);
			$table->text('description', 65535);
      $table->integer('cat_id')->unsigned()->index();
      $table->foreign('cat_id')->references('id')->on('cat')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('priority');
			$table->text('short_desc', 65535);
			$table->text('general_text', 65535);
			$table->text('indications_text', 65535);
			$table->text('recovery_text', 65535);
			$table->text('risks_text', 65535);
			$table->text('alternative_text', 65535);
			$table->boolean('is_preliminary');
			$table->float('version', 10, 0);
			$table->string('icd_code', 10);
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        
        Schema::drop('procedures');
	}

}
