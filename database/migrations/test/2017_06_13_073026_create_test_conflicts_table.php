<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestConflictsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('test_conflicts', function(Blueprint $table)
		{
    //   $table->integer('test_id')->unsigned()->index();
    //        $table->foreign('test_id')->references('id')->on('tests')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->integer('user_id')->unsigned()->index();
      $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('question_id')->unsigned()->index();
      $table->foreign('question_id')->references('id')->on('questions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('answer_id')->unsigned()->index();
      $table->foreign('answer_id')->references('id')->on('answers')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->string('sub_answer', 30);
			$table->integer('test_answer_id')->unsigned()->index();
      $table->foreign('test_answer_id')->references('id')->on('tests_answers')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->float('probability', 10, 0);
      $table->integer('proc_id')->unsigned()->index();
      $table->foreign('proc_id')->references('id')->on('procedures')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->integer('conflict_set');
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('test_conflicts');
	}

}
