<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('answers', function(Blueprint $table)
		{
      $table->increments('id');
			$table->integer('question_id')->unsigned();
      $table->foreign('question_id')->references('id')->on('questions')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->text('title', 65535);
			$table->integer('weight');
			$table->integer('next_question_id');
			$table->integer('priority');
			$table->string('tag', 300);
			$table->integer('end_test');
			$table->text('keys_for_end_test', 65535);
			$table->text('end_test_text', 65535);
			$table->text('justification', 65535);
			$table->text('auto_keys', 65535);
			$table->integer('force_score');
			$table->text('notes', 65535);
			$table->integer('vas_min');
			$table->integer('vas_max');
			$table->decimal('vas_min_display', 10, 0);
			$table->decimal('vas_max_display', 10, 0);
			$table->decimal('vas_step', 10, 0);
			$table->text('vas_explanation', 65535);
			$table->text('keys_for_showing_answer', 65535);
			$table->text('indication', 65535);
			$table->text('keys_for_showing_answer_teach', 65535);
      $table->timestamps();
      $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('answers');
	}

}
