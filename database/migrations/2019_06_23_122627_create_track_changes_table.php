<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('track_changes');
        Schema::create('track_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('table', 65535);
			$table->text('column', 65535);
            $table->text('old_value', 65535)->nullable();
            $table->text('value', 65535)->nullable();
            $table->integer('user_id')->unsigned() ;
            $table->integer('row_id')->unsigned() ;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_changes');
    }
}
