const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js('resources/assets/js/questions/main.js', 'public/js/questions.js')
  .js('resources/assets/js/roles.js', 'public/js')
  .js('resources/assets/js/conflicts.js', 'public/js')
  .js('resources/assets/js/client-reports.js', 'public/js')
  .js('resources/assets/js/client-statistics.js', 'public/js')
  .js('resources/assets/js/client-dashboard.js', 'public/js')
  .js('resources/assets/js/decisions.js', 'public/js')
  .js('resources/assets/js/parameters.js', 'public/js')
  .js('resources/assets/js/notes.js', 'public/js')
  .js('resources/assets/js/reports.js', 'public/js')
  .js('resources/assets/js/procedures.js', 'public/js')
  .js('resources/assets/js/clients.js', 'public/js')
  .js('resources/assets/js/combinations.js', 'public/js')
  .js('resources/assets/js/custom.js', 'public/js')
  .js('resources/assets/js/categories.js', 'public/js')
  .js('resources/assets/js/params.js', 'public/js');
