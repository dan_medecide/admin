<?php

namespace Tests\Feature;

use App\Question;
use App\Answer;
use App\Procedure;
use App\User;
use Tests\TestCase;
use App\Role;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class AdminQuestionsTest
 * @package Tests\Feature
 */
class AdminQuestionsTest extends TestCase {
    
    use DatabaseTransactions;
    
    protected $procedure;
    protected $questions;
    
    public function setUp()
    {
        parent::setUp();
        $this->procedure = factory(Procedure::class)->create();
    }
    
    /** @test */
    public function an_admin_can_arrive_to_dashboard()
    {
        $response = $this->signInAdmin()->get(route('questions.init', $this->procedure->id));
        $response->assertStatus(200);
        $response->assertViewIs('questions.questions');
        $response->assertSee($this->procedure->title);
    }
    
    /** @test */
    public function a_guest_can_not_arrive_to_dashboard()
    {
        $response = $this->get(route('questions.init', $this->procedure->id));
        
        $response->assertRedirect('/');
    }
    
    /** @test */
    public function an_admin_can_combine_questions()
    {
        $first = factory(Question::class)->create();
        $first->answers()->saveMany(factory(Answer::class, 5)->make());
        $second = factory(Question::class)->create();
        $first->answers()->saveMany(factory(Answer::class, 5)->make());
        
        $response = $this->signInAdmin()
                         ->post(route('questions.combine', [
                             'first'  => $first->id,
                             'second' => $second->id,
                         ]));
        
        $new_question = $response->getOriginalContent();
        $response->assertStatus(200);
        $this->assertEquals($new_question->title_doctor, $first->title_doctor . ' and ' . $second->title_doctor);
        $this->assertEquals($new_question->answers->count(), $first->answers->count() * $second->answers->count());
    }
}
