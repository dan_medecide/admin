<?php

namespace Tests\Feature;

use App\Answer;
use App\Procedure;
use App\ProcedureScores;
use App\Question;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProceduresTest extends TestCase {
    
    use DatabaseTransactions;
    
    protected $procedure;
    protected $scores;
    
    public function setUp()
    {
        parent::setUp();
        $category = factory(Category::class)->create();
        $this->procedure = factory(Procedure::class)->create([
            'title'    => 'Tonsillectomy',
            'id'       => 50,
            'priority' => 88,
            'version'  => 13,
            'cat_id'   => $category->id,
        ]);
        $this->scores = factory(ProcedureScores::class, 10)->create();
        $questions = factory(Question::class, 10)->create(['proc_id' => 50])->each(function($question) {
            $question->answers()->saveMany(factory(Answer::class, 10)->make());
        });
    }
    
    /** @test */
    public function an_admin_can_view_all_procedures()
    {
        $response = $this->signInAdmin()->get(route('procedures.index'));
        $response->assertStatus(200);
        $response->assertViewIs('procedures.procedures');
        $response->assertSee($this->procedure->title);
    }
    
    /** @test */
    public function an_admin_can_add_a_new_procedure_with_duplicated_scores()
    {
        $procedure = factory(Procedure::class)->make()->toArray();
        $response = $this->signInAdmin()->post(route('procedures.store'), $procedure);
        $response->assertRedirect(route('procedures.index'));
        $this->assertDatabaseHas('procedures', $procedure);
    }
    
    /** @test */
    public function an_admin_can_update_a_procedure()
    {
        $update_params = [
            'title'        => 'foo',
            'description'  => 'lorem ipsum',
            'cat_id'       => 12,
            'priority'     => 654,
            'version'      => 20,
            'is_ready'     => 1,
            'abbreviation' => 'FOO',
        ];
        $response = $this->signInAdmin()->put(route('procedures.update', $this->procedure), $update_params);
        $updated_procedure = Procedure::find($this->procedure->id)->first();
        $response->assertRedirect(route('procedures.index'));
        $this->assertEquals($updated_procedure->title, $update_params['title']);
        $this->assertEquals($updated_procedure->description, $update_params['description']);
        $this->assertEquals($updated_procedure->cat_id, $update_params['cat_id']);
        $this->assertEquals($updated_procedure->priority, $update_params['priority']);
        $this->assertEquals($updated_procedure->version, $update_params['version']);
        $this->assertEquals($updated_procedure->is_ready, $update_params['is_ready']);
        $this->assertEquals($updated_procedure->abbreviation, $update_params['abbreviation']);
    }
    
    /** @test */
    public function an_admin_can_dupliacte_a_procedure()
    {
        $response = $this->signInAdmin()->post(route('procedures.duplicate', $this->procedure));
        $response->assertRedirect(route('procedures.index'));
        $new_proc = Procedure::where('title', '=', $this->procedure->title . ' (copy)')->firstOrFail();
        $this->assertEquals($new_proc->categoryId(), $this->procedure->categoryId());
        $this->assertEquals($new_proc->firstQuestionTitle(), $this->procedure->firstQuestionTitle());
        $this->assertEquals($new_proc->questions()->count(), $this->procedure->questions()->count());
        $this->assertEquals($new_proc->firstAnswerTitle(), $this->procedure->firstAnswerTitle());
        $this->assertEquals($new_proc->firstQuestionAnswersCount(), $this->procedure->firstQuestionAnswersCount());
        $this->assertEquals($new_proc->firstScoreTitle(), $this->procedure->firstScoreTitle());
        $this->assertEquals($new_proc->scores()->count(), $this->procedure->scores()->count());
    }
    
    /** @test */
    public function a_guest_can_not_view_all_procedures()
    {
        $response = $this->get(route('procedures.index'));
        $response->assertRedirect('/');
    }
    
    /** @test */
    public function an_admin_can_view_procedures_scores()
    {
        $response = $this->signInAdmin()->get(route('procedure-scores.index', $this->procedure->id));
        $response_scores = $response->getOriginalContent()->getData()['scores'];
        $this->assertEquals($response_scores->first()->title, $this->procedure->scores->first()->title);
        $this->assertEquals($response_scores->count(), $this->procedure->scores->count());
        $response->assertStatus(200);
        $response->assertViewIs('procedure_scores.index');
    }
}
