<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\User;
use App\Role;

abstract class TestCase extends BaseTestCase {
    
    use CreatesApplication;
    
    protected $user;
    protected $admin_role;
    
    public function signIn($user = null)
    {
        if (! $user) {
            $user = factory(User::class)->create();
        }
        
        $this->user = $user;
        
        $this->actingAs($this->user);
        
        return $this;
    }
    
    public function signInAdmin()
    {
        $this->user = factory(User::class)->create();
        $this->admin_role = factory(Role::class)->create(['name' => 'admin']);
        $this->user->roles()->attach($this->admin_role);
        $this->actingAs($this->user);
        
        return $this;
    }
}
