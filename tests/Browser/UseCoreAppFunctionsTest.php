<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Procedure;
use App\Category;
use App\Role;
use App\ProcedureScores;
use App\Question;
use App\Answer;

class UseCoreAppFunctionsTest extends DuskTestCase {
    
    use DatabaseTransactions;
    
    protected $user;
    
    public function setUp()
    {
        parent::setUp();
        $this->user = User::where('email', '=', 'roman@medecide.net')->firstOrFail();
    }
    
    /** @test */
    public function an_admin_can_visit_teach()
    {
        $latest_procedure_title = Procedure::latest()->firstOrFail()->title;
        $this->browse(function($browser) use ($latest_procedure_title) {
            $browser->loginAs($this->user)
                    ->visit('procedures')
                    ->waitFor('.teach-' . $latest_procedure_title)
                    ->click('.teach-' . $latest_procedure_title)
                    ->assertSee('Teach');
        });
    }
    
    /** @test */
    public function a_user_can_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/login')
                    ->type('email', $this->user->email)
                    ->type('password', '123456')
                    ->press('Login')
                    ->assertPathIs('/home/admin');
        });
    }
}
